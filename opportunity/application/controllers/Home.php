<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 *
 */
class Home extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->library("pagination");
    }
   
    public function index(){
        $this->load->model('Model_home');
        $this->load->model('Model_targets');
        $this->load->model('Model_conditions');
        $this->load->model('Model_awards');

         $this->load->model('admin_models/Model_main_data');
         $data["setting"]=$this->Model_main_data->getByArray(array("id"=>1)); 
         $data["awards"]=$this->Model_awards->select_all_cats(); 
         $data["conditions"]=$this->Model_conditions->select_all_cats(); 
         $data["targets"]=$this->Model_targets->select_all_cats(); 

     

     //   print_r($data["setting"]);  exit(); 
        $title = "الصفحة الرئيسية";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'home';
        $this->load->view('index', $data);

            
    }
   
   
    public function add_new_competitor(){

        $this->load->model('Model_competitor'); 
        $data['name']  =$this->input->post('name');
        $data['phone']  =$this->input->post('phone');
        $data['address']  =$this->input->post('address');  
   $get_count=$this->Model_competitor->count_data();
       
   if($get_count <30){
        $this->Model_competitor->insert($data);
       $this->message('success', 'شكرا على تسجيلكم بالتقديم سالات غذائية.'.'<br/>'.'أخوكم المحامى /عزيز الصايد');
        redirect("home");
   } else if ($get_count >=30 && $get_count <45){
    $this->Model_competitor->insert($data);
   $this->message('success', 'شكرا تم اكتمال العدد وسوف يتم اضافتك للاحتياط.'.'<br/>'.'أخوكم المحامى /عزيز الصايد');
    redirect("home");
   }else if ($get_count =45){
   $this->message('error', 'شكرا تم اكتمال العدد نتمنى لك التوفيق بالمرة القادمه .'.'<br/>'.'أخوكم المحامى /عزيز الصايد');
    redirect("home");
   }
        
 
        
    }

    
    
        
}
