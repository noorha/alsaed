<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 *
 */
 error_reporting(E_ERROR);
class Category extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }
    public function index(){
      /*  $this->load->model('Model_client'); 
        $data["all_countries"]=$this->Model_client->get_all_countries();
      
     //   print_r($data["setting"]);  exit(); 
        $title = "تسجيل عميل";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'register_client';
        $this->load->view('index', $data);
*/
            
	}
	
  
   

   
    public function  sub_cats($main_cat){
        $this->load->model('Model_home');
       $this->load->model('Model_category');
     $this->load->model('admin_models/Model_main_data');
       $data["all_cats"]=$this->Model_category->select_all_cats();
       $data["setting"]=$this->Model_main_data->getByArray(array("id"=>1));
     
       $title = "الأقسام الفرعية";
       $data['title']=$title;
       $data['metakeyword']=$title;
       $data['metadiscription']=$title;
       $data["my_footer"]=array("edit_data_table");
      
 //     $investor = ''; 
       if ($main_cat !=null)
{
   

   $this->session->set_userdata('sub_cats', $main_cat);
}
elseif ($this->session->userdata('sub_cats'))
{
   // if term is not in POST use existing term from session
   $main_cat = $this->session->userdata('sub_cats');

}

       
       $config = array();
       $config["base_url"] = base_url() . "Category/sub_cats/";
       $config["total_rows"] = $this->Model_category->record_count_sub_cats($main_cat);
       $config["per_page"] = 30;
       $config["uri_segment"] = 3;
       $config['use_page_numbers'] = TRUE;
          // integrate bootstrap pagination
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['first_link'] = false;
          $config['last_link'] = false;
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';
          $config['prev_link'] = '«';
          $config['prev_tag_open'] = '<li class="prev">';
          $config['prev_tag_close'] = '</li>';
          $config['next_link'] = '»';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
       $this->pagination->initialize($config);
           // 
       $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
       $data['results']=  $this->Model_category->sub_cats($main_cat,$config["per_page"],0); 
       $data['places']=  $this->Model_category->main_places($main_cat,$config["per_page"],0); 
       
       $data["links"] = $this->pagination->create_links();

       

       $data['subview'] = 'sub_cats';
       $this->load->view('index', $data);
    }
    public function investor_cars($investor_id){
        $this->load->model('Model_car');
        $this->load->model('Model_home');
       $this->load->model('admin_models/Model_category');
       $this->load->model('admin_models/Model_property_type');
       $this->load->model('admin_models/Model_city');
       $this->load->model('admin_models/Model_car_brand');
       $this->load->model('admin_models/Model_main_data');
       $data["all_cats"]=$this->Model_category->select_all_cats();
       $data["all_property_types"]=$this->Model_property_type->select_all_cats();
       $data["all_cities"]=$this->Model_city->select_all_city();
       $data["all_brands"]=$this->Model_car_brand->select_all_cats();
       $data["setting"]=$this->Model_main_data->getByArray(array("id"=>1));
     
       $title = "سيارات المستثمر";
       $data['title']=$title;
       $data['metakeyword']=$title;
       $data['metadiscription']=$title;
       $data["my_footer"]=array("edit_data_table");
      
 //     $investor = ''; 
       if ($investor_id !=null)
{
   

   $this->session->set_userdata('search_investor_cars', $investor_id);
}
elseif ($this->session->userdata('search_investor_cars'))
{
   // if term is not in POST use existing term from session
   $investor_id = $this->session->userdata('search_investor_cars');

}

       
       $config = array();
       $config["base_url"] = base_url() . "Cars/investor_cars/";
       $config["total_rows"] = $this->Model_car->record_count_inv_cars($investor_id);
       $config["per_page"] = 30;
       $config["uri_segment"] = 3;
       $config['use_page_numbers'] = TRUE;
          // integrate bootstrap pagination
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['first_link'] = false;
          $config['last_link'] = false;
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';
          $config['prev_link'] = '«';
          $config['prev_tag_open'] = '<li class="prev">';
          $config['prev_tag_close'] = '</li>';
          $config['next_link'] = '»';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
       $this->pagination->initialize($config);
           // 
       $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
       $search_cars=  $this->Model_car->search_cars_investors($investor_id,$config["per_page"],0); 
       $data['results']=  $this->Model_car->search_car_image($search_cars);
       $data["links"] = $this->pagination->create_links();

       

       $data['subview'] = 'investor_cars';
       $this->load->view('index', $data);
    }
    public function investor_booking_cars($investor_id){
        $this->load->model('Model_car');
        $this->load->model('Model_home');
       $this->load->model('admin_models/Model_category');
       $this->load->model('admin_models/Model_property_type');
       $this->load->model('admin_models/Model_city');
       $this->load->model('admin_models/Model_car_brand');
       $this->load->model('admin_models/Model_main_data');
       $data["all_cats"]=$this->Model_category->select_all_cats();
       $data["all_property_types"]=$this->Model_property_type->select_all_cats();
       $data["all_cities"]=$this->Model_city->select_all_city();
       $data["all_brands"]=$this->Model_car_brand->select_all_cats();
       $data["setting"]=$this->Model_main_data->getByArray(array("id"=>1));
     
       $title = "حجوزات المستثمر";
       $data['title']=$title;
       $data['metakeyword']=$title;
       $data['metadiscription']=$title;
       $data["my_footer"]=array("edit_data_table");
      
   //	 print_r($data);  exit(); 
       $search_cars= $this->Model_car->search_booking_cars_investors($investor_id);
      // print_r($search_data);  exit(); 
       
           $data['results']=  $this->Model_car->search_booking_car_image($search_cars);
       

       $data['subview'] = 'investor_booking_cars';
       $this->load->view('index', $data);
    }
    public function client_cars($client_id){
        $this->load->model('Model_car');
        $this->load->model('Model_home');
       $this->load->model('admin_models/Model_category');
       $this->load->model('admin_models/Model_property_type');
       $this->load->model('admin_models/Model_city');
       $this->load->model('admin_models/Model_car_brand');
       $this->load->model('admin_models/Model_main_data');
       $data["all_cats"]=$this->Model_category->select_all_cats();
       $data["all_property_types"]=$this->Model_property_type->select_all_cats();
       $data["all_cities"]=$this->Model_city->select_all_city();
       $data["all_brands"]=$this->Model_car_brand->select_all_cats();
       $data["setting"]=$this->Model_main_data->getByArray(array("id"=>1));
     
       $title = "حجوزات السيارت";
       $data['title']=$title;
       $data['metakeyword']=$title;
       $data['metadiscription']=$title;
       $data["my_footer"]=array("edit_data_table");
      
       if ($client_id !=null)
       {
          
       
          $this->session->set_userdata('search_client_cars', $client_id);
       }
       elseif ($this->session->userdata('search_client_cars'))
       {
          // if term is not in POST use existing term from session
          $client_id = $this->session->userdata('search_client_cars');
       
       }

       $config = array();
       $config["base_url"] = base_url() . "Cars/client_cars/";
       $config["total_rows"] = $this->Model_car->record_count_cli_car($client_id);
       $config["per_page"] = 30;
       $config["uri_segment"] = 3;
       $config['use_page_numbers'] = TRUE;
          // integrate bootstrap pagination
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['first_link'] = false;
          $config['last_link'] = false;
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';
          $config['prev_link'] = '«';
          $config['prev_tag_open'] = '<li class="prev">';
          $config['prev_tag_close'] = '</li>';
          $config['next_link'] = '»';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['cur_tag_open'] = '<li class="active"><a href="#">';
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
       $this->pagination->initialize($config);
           // 
       $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
       $search_cars=  $this->Model_car->search_cars_clients($client_id,$config["per_page"],0); 
       $data['results']=  $this->Model_car->search_car_image($search_cars);
       $data["links"] = $this->pagination->create_links();
     
//print_r( $data['results']);exit();
       $data['subview'] = 'client_cars';
       $this->load->view('index', $data);
    }
	public function search_keyword_cars()
    {
		$this->load->model('Model_car');
         $this->load->model('Model_home');
        $this->load->model('admin_models/Model_category');
        $this->load->model('admin_models/Model_property_type');
        $this->load->model('admin_models/Model_city');
        $this->load->model('admin_models/Model_car_brand');
        $this->load->model('admin_models/Model_main_data');
        $data["all_cats"]=$this->Model_category->select_all_cats();
        $data["all_property_types"]=$this->Model_property_type->select_all_cats();
        $data["all_cities"]=$this->Model_city->select_all_city();
        $data["all_brands"]=$this->Model_car_brand->select_all_cats();
        $data["setting"]=$this->Model_main_data->getByArray(array("id"=>1));
      
        $title = "بحث عن سيارة";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
       

        $search_data = ''; 
        if ($this->input->post('brand_id') || $this->input->post('cat_id') || $this->input->post('color') || $this->input->post('year'))
{
    // use the term from POST and set it to session
    $search_data['brand_id']  =$this->input->post('brand_id');
    $search_data['cat_id']  =$this->input->post('cat_id');
    $search_data['color']  =$this->input->post('color');
    $search_data['year']  =$this->input->post('year');
//print_r( $search_data );exit();
 
    $this->session->set_userdata('search_data_cars', $search_data);
}
elseif ($this->session->userdata('search_data_cars'))
{
    // if term is not in POST use existing term from session
    $search_data = $this->session->userdata('search_data_cars');

}

        
        $config = array();
        $config["base_url"] = base_url() . "Home/search_cars/";
        $config["total_rows"] = $this->Model_home->record_count_cars($search_data);
        $config["per_page"] = 30;
        $config["uri_segment"] = 3;
        $config['use_page_numbers'] = TRUE;
           // integrate bootstrap pagination
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';
           $config['first_link'] = false;
           $config['last_link'] = false;
           $config['first_tag_open'] = '<li>';
           $config['first_tag_close'] = '</li>';
           $config['prev_link'] = '«';
           $config['prev_tag_open'] = '<li class="prev">';
           $config['prev_tag_close'] = '</li>';
           $config['next_link'] = '»';
           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';
           $config['last_tag_open'] = '<li>';
           $config['last_tag_close'] = '</li>';
           $config['cur_tag_open'] = '<li class="active"><a href="#">';
           $config['cur_tag_close'] = '</a></li>';
           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
            // 
        $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
        $search_cars=  $this->Model_home->search_cars($search_data,$config["per_page"],0); 
        $data['results']=  $this->Model_home->search_car_image($search_cars);
        $data["links"] = $this->pagination->create_links();

        $data['subview'] = 'search_result_car';
        $this->load->view('index', $data);
    }
        public function ajax() {
            echo '44444';
            exit();
        }
        
        public function success($type) {
            $this->globals();
            if($type==1){
               $data['type'] = "تم التسجيل بنجاح"; 
            }
            
            if($type==2){
               $data['type'] = "تم إضافة الإعلان بنجاح"; 
            }
            
            if($type==3){
               $data['type'] = "يجب تسجيل الدخول اولا حتي تتمكن من إضافة إعلان"; 
            }
            
            
            $this->loadView(strtolower(__FUNCTION__), $data );
        }
        
         public function error($type) {
             $this->globals();
            if($type==1){
               $data['type'] = "بيانات التسجيل غير صحيحة"; 
            }
            if($type==2){
               $data['type'] = "يجب تسجيل الدخول  حتي تتمكن من رؤية المحتوي"; 
            }
            if($type==3){
               $data['type'] = "يجب تسجيل الدخول  حتي تتمكن من الإضافة للمفضلة"; 
            }
            
            $this->loadView(strtolower(__FUNCTION__), $data );
        }
        public function activate($car_id,$investor_id){
            $this->load->model('Model_car');
            $this->Model_car->active_ad($car_id);
            $this->message('info', 'تم التعديل');
            redirect("Cars/investor_cars/".$investor_id);
        }
        public function deactivate($car_id,$investor_id){
            $this->load->model('Model_car');
            $this->Model_car->deactive_ad($car_id);
            $this->message('info', 'تم التعديل');
            redirect("Cars/investor_cars/".$investor_id);
        }
        
        public function attend_booking($id,$investor_id,$arrive_date,$arrive_time){
            $this->load->model('Model_car');
            
           $date1= date('Y/m/d',$arrive_date);
           $date2 =date('Y/m/d');
         $str1=strtotime($date1);
         $str2= strtotime($date2);
       
          
           $datediff = $str1 - $str2;
    
           $no_of_days =  floor($datediff / (60 * 60 * 24));
          
         if($no_of_days==0){
           
            $this->Model_car->attend_booking($id);
            $this->message('info', 'تم الحضور');
            redirect("cars/all_inv_booking/".$investor_id);
         }
         else {
           
            $this->message('error', 'خطأ برجاء تحديث الحالة بنفس يوم تاريخ الوصول');
            redirect("cars/all_inv_booking/".$investor_id);
         }
    
           
        }
    
        public function unattend_booking($id,$investor_id,$arrive_date){
            $this->load->model('Model_car');
          
            $date1= date('Y/m/d',$arrive_date);
            $date2 =date('Y/m/d');
          $str1=strtotime($date1);
          $str2= strtotime($date2);
        
           
            $datediff = $str1 - $str2;
     
           $no_of_days =  floor($datediff / (60 * 60 * 24));
             if($no_of_days==0){
                $this->Model_car->unattend_booking($id);
                $this->message('error', 'تم إلغاء الحضور');
                redirect("cars/all_inv_booking/".$investor_id);
             }
             else {
                
                $this->message('error', 'خطأ برجاء تحديث الحالة بنفس يوم تاريخ الوصول');
                redirect("cars/all_inv_booking/".$investor_id);
             }
    
           
        }

        public function delete_booking($id,$client_id){
            $this->load->model('Model_car');
            //$Udata["user_show"] = 0;
            $this->Model_car->delete_booking($car_id);
            $this->message('error', 'تم الغاء الحجز');
            redirect("villa_apart/client_bookings/".$client_id);
        }
        public function delete($car_id,$investor_id){
            $this->load->model('Model_car');
            //$Udata["user_show"] = 0;
            $this->Model_car->delete($car_id);
            $this->message('error', 'تم الحذف');
            redirect("Cars/investor_cars/".$investor_id);
        }
        
}
