<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 *
 */
 error_reporting(E_ERROR);
class Auth extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }
    public function  login_inv(){
        $office=$this->uri->segment(3); // 1stsegment;



    $set_data = $this->session->all_userdata(); 
    if( $set_data['user_token']== 1){
       redirect('home/blank_page');
   }
   else {
    $this->load->model('Model_client'); 
    $data["all_cats"]=$this->Model_client->select_all_cats();
    $data["all_countries"]=$this->Model_client->get_all_countries();

       $title = "تسجيل الدخول";
       $data['title']=$title;
       $data['metakeyword']=$title;
       $data['metadiscription']=$title;
       $data["my_footer"]=array("edit_data_table");
       $data['subview'] = 'sign_in_inv';
       if($office !=null){
        $data['office']=$office;
       }
     
       $this->load->view('index', $data);

   }  
    }

    public function  login_marker(){
        $set_data = $this->session->all_userdata(); 
        if( $set_data['user_token']== 2){
           redirect('home');
       }
       else {
        $this->load->model('Model_client'); 
        $data["all_cats"]=$this->Model_client->select_all_cats();
        $data["all_countries"]=$this->Model_client->get_all_countries();
    
           $title = "تسجيل الدخول";
           $data['title']=$title;
           $data['metakeyword']=$title;
           $data['metadiscription']=$title;
           $data["my_footer"]=array("edit_data_table");
           $data['subview'] = 'sign_in_marker';
           $this->load->view('index', $data);
    
       }  
        }

    public function  login_client(){
        $set_data = $this->session->all_userdata(); 
        if( $set_data!= null){
            redirect('home');
        }
        else{
        $this->load->model('Model_client'); 
        $data["all_cats"]=$this->Model_client->select_all_cats();
        $data["all_countries"]=$this->Model_client->get_all_countries();
    
           $title = "تسجيل الدخول";
           $data['title']=$title;
           $data['metakeyword']=$title;
           $data['metadiscription']=$title;
           $data["my_footer"]=array("edit_data_table");
           $data['subview'] = 'sign_in_client';
           $this->load->view('index', $data);
    
        }
        }

    public function index(){
        $set_data = $this->session->all_userdata(); 
	 if( $set_data['user_token']== 1){
        redirect('home/blank_page');
    }
    else {
     $this->load->model('Model_client'); 
     $data["all_cats"]=$this->Model_client->select_all_cats();
     $data["all_countries"]=$this->Model_client->get_all_countries();

        $title = "تسجيل الدخول";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'sign_in_inv';
        $this->load->view('index', $data);

    }  
	}
	 public function logout(){
       $this->session->unset_userdata('is_logged_in_user');
	   $this->session->unset_userdata('user_token');
        $this->session->sess_destroy();
        redirect('home');
    }
 public function sign_in_inv(){
         $this->load->model('admin_models/Model_category');
		 $data["all_cats"]=$this->Model_category->select_all_cats();
		   $this->load->model('system_management/Users');
        $userdata=$this->Users->check_inv_data();
        
        $user_id=$userdata['id'];
        $token=$userdata['user_token'];
        $office=$userdata['office'];

        if($userdata !=''){
            $userdata['is_logged_in_user']=true;
            $userdata['user_id']=$userdata['id'];
            $userdata['user_token']=$token;
            $userdata['office']=$office;

          //  $data['userdata']=$userdata;
          $this->session->set_userdata('office', $office);
			$this->session->set_userdata('user_token', $token);
			$this->session->set_userdata('user_id', $user_id);
            $this->session->set_userdata($userdata);
            $set_data = $this->session->all_userdata(); 
            if( $set_data['user_token']== 1){
               redirect('home/blank_page');
           }
        }else{
             $title = "تسجيل الدخول";
            $data['metakeyword']='تسجيل الدخول';
            $data['metadiscription']='تسجيل الدخول';
            $data['response']="خطأفى بيانات الادخال";
             $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'sign_in_inv';
         $this->load->view('index', $data);
        }
	
            
    } 
    public function sign_in_marker(){
        $this->load->model('admin_models/Model_category');
        $data["all_cats"]=$this->Model_category->select_all_cats();
          $this->load->model('system_management/Users');
       $userdata=$this->Users->check_marker_data();
       
       $user_id=$userdata['id'];
       $token=$userdata['user_token'];
       $username=$userdata['name'];
       if($userdata !=''){
           $userdata['is_logged_in_user']=true;
           $userdata['user_id']=$userdata['id'];
           $userdata['user_token']=$token;
           $userdata['username']=$username;
          
           $this->session->set_userdata('user_token', $token);
           $this->session->set_userdata('user_id', $user_id);
           $this->session->set_userdata('username', $username);
           $this->session->set_userdata($userdata);
           $set_data = $this->session->all_userdata(); 
           if( $set_data['user_token']== 2){
              redirect('home');
          }
       }else{
            $title = "تسجيل الدخول";
           $data['metakeyword']='تسجيل الدخول';
           $data['metadiscription']='تسجيل الدخول';
           $data['response']="خطأفى بيانات الادخال";
            $data["my_footer"]=array("edit_data_table");
       $data['subview'] = 'sign_in_marker';
        $this->load->view('index', $data);
       }
   
           
   } 
    public function sign_in_client(){
        $this->load->model('admin_models/Model_category');
        $data["all_cats"]=$this->Model_category->select_all_cats();
          $this->load->model('system_management/Users');
       $userdata=$this->Users->check_client_data();
       
       $user_id=$userdata['id'];
       $token=$userdata['user_token'];
       $username=$userdata['username'];
       if($userdata !=''){
           $userdata['is_logged_in_user']=true;
           $userdata['user_id']=$userdata['id'];
           $userdata['user_token']=$token;
           $userdata['username']=$username;
         //  $data['userdata']=$userdata;
           $this->session->set_userdata('user_token', $token);
           $this->session->set_userdata('user_id', $user_id);
           $this->session->set_userdata('username', $username);
           $this->session->set_userdata($userdata);
           $set_data = $this->session->all_userdata(); 
         redirect('home');
       }else{
            $title = "تسجيل الدخول";
           $data['metakeyword']='تسجيل الدخول';
           $data['metadiscription']='تسجيل الدخول';
           $data['response']="خطأفى بيانات الادخال";
            $data["my_footer"]=array("edit_data_table");
       $data['subview'] = 'sign_in_client';
        $this->load->view('index', $data);
       }
   
           
   }     
	 public function ajax() {
            echo '44444';
            exit();
        }
        
        public function success($type) {
            $this->globals();
            if($type==1){
               $data['type'] = "تم التسجيل بنجاح"; 
            }
            
            if($type==2){
               $data['type'] = "تم إضافة الإعلان بنجاح"; 
            }
            
            if($type==3){
               $data['type'] = "يجب تسجيل الدخول اولا حتي تتمكن من إضافة إعلان"; 
            }
            
            
            $this->loadView(strtolower(__FUNCTION__), $data );
        }
        
         public function error($type) {
             $this->globals();
            if($type==1){
               $data['type'] = "بيانات التسجيل غير صحيحة"; 
            }
            if($type==2){
               $data['type'] = "يجب تسجيل الدخول  حتي تتمكن من رؤية المحتوي"; 
            }
            if($type==3){
               $data['type'] = "يجب تسجيل الدخول  حتي تتمكن من الإضافة للمفضلة"; 
            }
            
            $this->loadView(strtolower(__FUNCTION__), $data );
        }
        
}
