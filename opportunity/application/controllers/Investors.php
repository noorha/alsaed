<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 *
 */
 error_reporting(E_ERROR);
class Investors extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }
    public function index(){
      /*  $this->load->model('Model_client'); 
        $data["all_countries"]=$this->Model_client->get_all_countries();
      
     //   print_r($data["setting"]);  exit(); 
        $title = "تسجيل عميل";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'register_client';
        $this->load->view('index', $data);
*/
            
	}
    public function register_investor(){
        $this->load->model('admin_models/Model_main_data');
        $this->load->model('Model_investor');
        $office=$this->uri->segment(3); // 1stsegment;
        //print_r($office);exit();
        $data["all_cats"]=$this->Model_investor->select_all_cats();
        $data["all_countries"]=$this->Model_investor->get_all_countries();
        $data["setting"]=$this->Model_main_data->getByArray(array("id"=>1)); 
        $data["all_checkbox_cities"]=$this->Model_investor->get_all_arrive_cities();

//(  $data["all_cats"]);exit();
        $title = "تسجيل مستثمر";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        if($office !=null){
            $data['office']=$office;
           }
        $data['subview'] = 'register_investor';
        $this->load->view('index', $data);

            
    }
    public function fetch_city()
    {
    
       $this->load->model('Model_investor');
      echo $this->Model_investor->fetch_city($this->input->post('country'));
     
    }


    public function add_new_investor(){
        $this->load->model('Model_investor'); 
        $this->load->model('Model_inv_cities'); 
        $data['business_name']  =$this->input->post('business_name');
        $pass =$this->input->post('pass');
        $data["password"]=sha1(md5(($pass))) ;   
        $data['phone']  =$this->input->post('phone');
        $data['phone2']  =$this->input->post('phone2');
        $data['country']  =$this->input->post('country');
        $data['city']  =$this->input->post('city');
        $data['username']  =$this->input->post('username');
        $data['address']  =$this->input->post('address');
        $data['bank_name']  =$this->input->post('bank_name');
        $data['bank_account']  =$this->input->post('bank_account');
        $data['area']  =$this->input->post('area');
        $data['email']  =$this->input->post('email');
        $data['office']=2;

        $config['upload_path']          = './admin_panel/uploads/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|doc';
        $config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;
        $this->load->library('upload', $config);
       
            
            $main_iamge = $this->upload_image("license_photo");
            $data["license_photo"] = $main_iamge;
      

        $main_iamge2 = $this->upload_image("identity_photo");
        $data["identity_photo"] = $main_iamge2;
      $inv_id=  $this->Model_investor->insert($data);

        $all_checkbox_cities=$this->Model_investor->get_all_cities();
       
         foreach ($all_checkbox_cities as $one_city){
             if($one_city->name !=null){
            $value=$this->input->post("check_box_".$one_city->id);
          //  print_r($value);exit();
            if($value == "on"){
                 $data1['inv_id']=$inv_id;
                 $data1['city_id']=$one_city->id;
       //          print_r($data1);exit();  
                 $this->Model_inv_cities->insert($data1);
            }
        }
          
 }
          $this->message('success', 'تمت الاضافة  بنجاح');
        redirect("investors/register_investor");
    }
    function search_keyword_property()
    {
        $this->load->model('Model_home');
        $this->load->model('admin_models/Model_category');
        $this->load->model('admin_models/Model_property_type');
        $this->load->model('admin_models/Model_city');
        $this->load->model('admin_models/Model_car_brand');
        $this->load->model('admin_models/Model_main_data');
        $data["all_cats"]=$this->Model_category->select_all_cats();
        $data["all_property_types"]=$this->Model_property_type->select_all_cats();
        $data["all_cities"]=$this->Model_city->select_all_city();
        $data["all_brands"]=$this->Model_car_brand->select_all_cats();
        $data["setting"]=$this->Model_main_data->getByArray(array("id"=>1)); 
     //   print_r($data["setting"]);  exit(); 
        $title = "الصفحة الرئيسية";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
      
        $search_data['type_id']  =$this->input->post('type_id');
        $search_data['city_id']  =$this->input->post('city_id');
        $search_data['area']  =$this->input->post('area');
        $search_data['rooms_count']  =$this->input->post('rooms_count');  //print_r( $search_data );exit();
        $search_property=  $this->Model_home->search_property($search_data);     // 
       
        $data['results']=  $this->Model_home->search_property_image($search_property);
      //  print_r($data['results']);  exit(); 
        $data['subview'] = 'search_result_property';
        $this->load->view('index', $data);
    }

    function search_keyword_cars()
    {
        $this->load->model('Model_home');
        $this->load->model('admin_models/Model_category');
        $this->load->model('admin_models/Model_property_type');
        $this->load->model('admin_models/Model_city');
        $this->load->model('admin_models/Model_car_brand');
        $this->load->model('admin_models/Model_main_data');
        $data["all_cats"]=$this->Model_category->select_all_cats();
        $data["all_property_types"]=$this->Model_property_type->select_all_cats();
        $data["all_cities"]=$this->Model_city->select_all_city();
        $data["all_brands"]=$this->Model_car_brand->select_all_cats();
        $data["setting"]=$this->Model_main_data->getByArray(array("id"=>1)); 
     //   print_r($data["setting"]);  exit(); 
        $title = "الصفحة الرئيسية";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
       
        $search_data['brand_id']  =$this->input->post('brand_id');
        $search_data['cat_id']  =$this->input->post('cat_id');
        $search_data['color']  =$this->input->post('color');
        $search_data['year']  =$this->input->post('year');
        $search_cars= $this->Model_home->search_cars($search_data);
       // 
        $data['results']=  $this->Model_home->search_car_image($search_cars);
       // print_r(  $data['results']);exit();

        $data['subview'] = 'search_result_car';
        $this->load->view('index', $data);
    }
        public function ajax() {
            echo '44444';
            exit();
        }
        
        public function success($type) {
            $this->globals();
            if($type==1){
               $data['type'] = "تم التسجيل بنجاح"; 
            }
            
            if($type==2){
               $data['type'] = "تم إضافة الإعلان بنجاح"; 
            }
            
            if($type==3){
               $data['type'] = "يجب تسجيل الدخول اولا حتي تتمكن من إضافة إعلان"; 
            }
            
            
            $this->loadView(strtolower(__FUNCTION__), $data );
        }
        
         public function error($type) {
             $this->globals();
            if($type==1){
               $data['type'] = "بيانات التسجيل غير صحيحة"; 
            }
            if($type==2){
               $data['type'] = "يجب تسجيل الدخول  حتي تتمكن من رؤية المحتوي"; 
            }
            if($type==3){
               $data['type'] = "يجب تسجيل الدخول  حتي تتمكن من الإضافة للمفضلة"; 
            }
            
            $this->loadView(strtolower(__FUNCTION__), $data );
        }
        
}
