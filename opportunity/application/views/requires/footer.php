 <!--footer--->
 <div class="footer">
        <div class="container">
            <div class="copyright text-center wow bounceIn" data-wow-delay="1s">
                <p>جميع الحقوق محفوظة </p>
            </div>
            <div class="social-media text-center wow bounceIn" data-wow-delay="1s">
                <ul class="list-inline">
                <?php if(isset($setting) && !empty($setting)){?>
                    <li><a href="<?php echo  $setting['company_facebook'];?>"><i class="fa fa-facebook fa-fw"></i></a></li>
                    <li><a href="<?php echo  $setting['company_twitter'];?>"><i class="fa fa-twitter fa-fw"></i></a></li>
                    <li><a href="<?php echo  $setting['company_instagram'];?>"><i class="fa fa-instagram fa-fw"></i></a></li>
                    <li><a href="<?php echo  $setting['company_youtube'];?>"><i class="fa fa-youtube fa-fw"></i></a></li>
                <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <!--footer--->

    <!--[upscroll]-->
    <div class="upscroll">
        <i class="fa fa-angle-double-up fa-lg"></i>
    </div>
    <!--[/upscroll]-->

    <!-- ================================================== -->
    <script src="<?php echo base_url()?>asisst/js/jquery-2.2.1.min.js"></script>
    <script src="<?php echo base_url()?>asisst/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>asisst/js/WOW.js"></script>
    <script src="<?php echo base_url()?>asisst/js/custom.js"></script>



</body>

</html>
