<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>مسابقة القبيلة الثالثة (المحامى عزيز الصايد)</title>
    <link href="<?php echo base_url()?>asisst/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>asisst/css/bootstrap-rtl.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>asisst/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">
   
    <!-- End of head section HTML codes -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="<?php echo base_url()?>asisst/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text&display=swap" rel="stylesheet">
        <style>
      body {
        font-family: 'Markazi Text', serif;

      }
    </style>
    <!-- ================================================== -->
</head>

<body>


    <!--hedar-->
    <header class="header-all">
        <div class="container">
            <div class="hed wow fadeIn">
                <img src="<?php echo base_url()?>asisst/img/logo.png" class="img-responsive">
            </div>
        </div>
    </header>
    <!--hedar-->


    <span id="message_alert" style="text-align: center;">

<?php

if(isset($_SESSION['message']))

    echo $_SESSION['message'];

unset($_SESSION['message']);

?>

</span>
