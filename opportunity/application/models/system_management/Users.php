<?php
class Users extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    public function record_count() {
        return $this->db->count_all("admins");
    }
    public function select() {
        $this->db->select('*');
        $this->db->from('admins');
        $this->db->order_by('user_id','DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function fetch_users($limit, $start) {
        $this->db->select('*');
        $this->db->from('admins');
        $this->db->order_by('user_id','DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }

    public function display($id){
        $this->db->where('user_id',$id);
        $query=$this->db->get('admins');
        return $query->row_array();
    }
    public function check_client_data(){
		//$user_token=$this->input->post('user_token');
		
        $this->db->select('*');
        $this->db->from("clients");
        $this->db->where('username',$this->input->post('user_name'));
        $this->db->where('password',sha1(md5($this->input->post('password'))));
		   $this->db->where('active','1');
        
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return false;
        }
    }
    public function check_marker_data(){
        //	$user_token=$this->input->post('user_token');
            
            $this->db->select('*');
            $this->db->from("markers");
            $this->db->where('name',$this->input->post('user_name'));
            $this->db->where('password',sha1(md5($this->input->post('password'))));
               $this->db->where('status','1');
           
            $query=$this->db->get();
            if($query->num_rows()>0){
                return $query->row_array();
            }else{
                return false;
            }
        }
    public function check_inv_data(){
	//	$user_token=$this->input->post('user_token');
		
        $this->db->select('*');
        $this->db->from("investors");
        $this->db->where('username',$this->input->post('user_name'));
        $this->db->where('password',sha1(md5($this->input->post('password'))));
		   $this->db->where('status','1');
       
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return false;
        }
    }
    public function check_regester_data(){
        $this->db->select('*');
        $this->db->from('register');
        $this->db->where('user_name',$this->input->post('user_name'));
        $this->db->where('password',$this->input->post('user_pass'));
        $this->db->where('approved',1);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return false;
        }
    }
    public function update_last_login($id){

        $data['last_login']=time();
        $this->db->where('user_id',$id);
        $this->db->update('admins',$data);

    }


}// END CLASS