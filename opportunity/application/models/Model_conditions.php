<?php
class Model_conditions extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="conditions";
		
    }
	  
    public function select_all_cats(){
        $this->db->select('conditions.* ');
        $this->db->from("conditions");
        $this->db->where("id",1);
        $this->db->order_by("id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
  


}  