<?php
class Model_investor extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="";
		
    }
    public  function fetch_city($country_id)
    {
     $this->db->where('country_id', $country_id);
     $this->db->order_by('name', 'ASC');
     $query = $this->db->get('cities');
     $output = '<option value="">اختر</option>';
     foreach($query->result() as $row)
     {
      $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
     }
     
     return $output;
    }
   
    public function select_all_cats(){
        $this->db->select('categories.* ');
        $this->db->from("categories");
        $this->db->order_by("id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_countries(){
        $this->db->select('countries.* ');
        $this->db->from("countries");
        $this->db->order_by("id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_all_arrive_cities(){
        $this->db->select('arrive_cities.* ');
        $this->db->from("arrive_cities");
        $this->db->order_by("id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public  function insert($data){
        $this->db->insert("investors",$data);
        return $this->db->insert_id();
        //return $this->db->insert_id();
    }
    public function search_property_image($property)
    {
            foreach($property as $one_property){
             
                $this->db->select('property_images.image_name');
                $this->db->from('property_images');
                $this->db->where('property_id',$one_property->id);
              
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $one_image = $query->first_row();
                //    print_r($one_image->image_name);exit();
                    $one_property->image_name=$one_image->image_name;
                }
            }
           
        return $property;
     
    }
    public function search_car_image($cars)
    {
            foreach($cars as $one_car){
             
                $this->db->select('car_images.image_name');
                $this->db->from('car_images');
                $this->db->where('car_id',$one_car->id);
              
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $one_image = $query->first_row();
                   // print_r($one_image->image_name);exit();
                    $one_car->image_name=$one_image->image_name;
                }
            }
           
        return $cars;
     
    }
   public function search_property($keyword)
    {

    //    print_r($keyword);exit();
        $this->db->select('*');
        $this->db->from('villa_aparts');
        if($keyword['type_id'] !='none' ){
        $this->db->where('property_type_id',$keyword['type_id']);}
        if($keyword['city_id'] !='none' ){
        $this->db->where('city_id',$keyword['city_id']);}
        if($keyword['area'] !=null ){
        $this->db->or_like('area',$keyword['area']);}
        if($keyword['rooms_count'] !='none' ){
        $this->db->or_where('rooms_count',$keyword['rooms_count']);}
      // print_r( $this->db->get());exit();
        $query = $this->db->get();
        return $query->result();
     
    }
   public function search_cars($keyword)
    {
        $this->db->select('cars.*,investors.username as investor_name,investors.phone as phone');
        $this->db->from('cars');
        $this->db->join('investors' , 'investors.id = cars.investor_id',"inner");

        if($keyword['car_brand'] !='none' ){
        $this->db->where('car_brand',$keyword['brand_id']);
        }
        if($keyword['cat_id'] !='none' ){
        $this->db->or_where('category',$keyword['cat_id']);
        }
        if($keyword['color'] !=null){
        $this->db->or_like('color',$keyword['color']);
        }
        if($keyword['year'] !=null ){
        $this->db->or_like('year',$keyword['year']);
        }

        $query = $this->db->get();
       // print_r( $this->db->get());exit();
        return $query->result();
    }
}  