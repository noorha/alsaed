<?php
class Model_home extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="";
		
    }
    public function search_property_image($property)
    {
            foreach($property as $one_property){
             
                $this->db->select('property_images.image_name');
                $this->db->from('property_images');
                $this->db->where('property_id',$one_property->id);
              
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $one_image = $query->first_row();
                //    print_r($one_image->image_name);exit();
                    $one_property->image_name=$one_image->image_name;
                }
            }
           
        return $property;
     
    }
	  public  function insert_contact($data){
        $this->db->insert("contact_us",$data);
        return $this->db->insert_id();
       
    }
    public function search_car_image($cars)
    {
            foreach($cars as $one_car){
             
                $this->db->select('car_images.image_name');
                $this->db->from('car_images');
                $this->db->where('car_id',$one_car->id);
              
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $one_image = $query->first_row();
                   // print_r($one_image->image_name);exit();
                    $one_car->image_name=$one_image->image_name;
                }
            }
           
        return $cars;
     
    }
   public function search_property($keyword,$limit,$start)
    {
       
    $this->db->select('villa_aparts.*,property_types.name as property_type,cities.name as city_name');
    $this->db->from('villa_aparts');
    $this->db->join('property_types' , 'property_types.id = villa_aparts.property_type_id',"inner");
    $this->db->join('cities' , 'cities.id = villa_aparts.city_id',"inner");
  
    //    $this->db->join('booking_villa_aparts' , 'booking_villa_aparts.property_id= villa_aparts.id',"inner");
    /*    if($keyword['arrive_date'] !=''){
        $this->db->like('reciept_date',$keyword['arrive_date']);}
        if($keyword['leave_date'] !=''){
            $this->db->or_like('deliever_date',$keyword['leave_date']);}*/
        if($keyword['city_id'] !='none'&&$keyword['city_id'] !='all'){
        $this->db->where('city_id',$keyword['city_id']);}
       
        if($keyword['rooms_count'] !='none' &&$keyword['rooms_count'] !='all'){
            $this->db->or_where('(rooms_count+master_rooms_count)' ,$keyword['rooms_count']);
        }      
          //   print_r( $this->db->get());exit();
          $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result();
     
    }
    public function record_count_cars($keyword){
        $this->db->select('cars.*,investors.username as investor_name,investors.phone as phone');
        $this->db->from('cars');
        $this->db->join('investors' , 'investors.id = cars.investor_id',"inner");
        if($keyword['brand_id'] !='none'&&$keyword['brand_id'] !='all' ){
            $this->db->where('car_brand',$keyword['brand_id']);
            }
            if($keyword['cat_id'] !='none' &&$keyword['cat_id'] !='all'){
            $this->db->or_where('category',$keyword['cat_id']);
            }
            if($keyword['color'] !=null){
            $this->db->or_like('color',$keyword['color']);
            }
            if($keyword['year'] !='none'&&$keyword['year'] !='all' ){
            $this->db->or_where('year',$keyword['year']);
            }
            return $this->db->count_all("cars");

    }
    public function record_count_property($keyword) {
        $this->db->select('villa_aparts.*,property_types.name as property_type,cities.name as city_name');
        $this->db->from('villa_aparts');
        $this->db->join('property_types' , 'property_types.id = villa_aparts.property_type_id',"inner");
        $this->db->join('cities' , 'cities.id = villa_aparts.city_id',"inner");
      
        //    $this->db->join('booking_villa_aparts' , 'booking_villa_aparts.property_id= villa_aparts.id',"inner");
        /*    if($keyword['arrive_date'] !=''){
            $this->db->like('reciept_date',$keyword['arrive_date']);}
            if($keyword['leave_date'] !=''){
                $this->db->or_like('deliever_date',$keyword['leave_date']);}*/
            if($keyword['city_id'] !='none'&&$keyword['city_id'] !='all'){
            $this->db->where('city_id',$keyword['city_id']);}
           
            if($keyword['rooms_count'] !='none' &&$keyword['rooms_count'] !='all'){
                $this->db->or_where('(rooms_count+master_rooms_count)' ,$keyword['rooms_count']);
            }      
        return $this->db->count_all("villa_aparts");
    }

    public function faqs(){
        $this->db->select('faqs.*');
        $this->db->from('faqs');
        $query = $this->db->get();    
        return $query->result();
    }
   
   public function search_cars($keyword,$limit,$start)
    {
       $this->db->select('cars.*');
        $this->db->from('cars');

        if($keyword['city_id'] !='none'&&$keyword['city_id'] !='all' ){
            $this->db->join('investors' , 'investors.city ='. $keyword['city_id'],"inner");
        }

        if($keyword['brand_id'] !='none'&&$keyword['brand_id'] !='all' ){
        $this->db->where('car_brand',$keyword['brand_id']);
        }
        if($keyword['cat_id'] !='none' &&$keyword['cat_id'] !='all' && $keyword['cat_id'] !='0'){
        $this->db->or_where('category',$keyword['cat_id']);
        }
        if($keyword['color'] !=null){
        $this->db->or_like('color',$keyword['color']);
        }
        if($keyword['year'] !='none'&&$keyword['year'] !='all' ){
        $this->db->or_where('year',$keyword['year']);
        }
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        
        return $query->result();
    }
}  