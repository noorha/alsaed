<?php
class Model_targets extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="targets";
		
    }
	  
    public function select_all_cats(){
        $this->db->select('targets.* ');
        $this->db->from("targets");
        $this->db->where("id",1);
        $this->db->order_by("id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
  


}  