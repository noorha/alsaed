<?php
class Model_awards extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="awards";
		
    }
	  
    public function select_all_cats(){
        $this->db->select('awards.* ');
        $this->db->from("awards");
        $this->db->order_by("id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
  


}  