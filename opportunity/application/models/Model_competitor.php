<?php
class Model_competitor extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="basket_competitors";
		
    }
    
    public  function check_civil_no($civil_no){
    $this->db->select('competitors.*');
    $this->db->from('competitors');
    $this->db->where('civil_no', $civil_no);
    $query = $this->db->get();
    return $query->num_rows(); // returns first row if has record in db

    }
    public  function update($data,$id){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
    public  function insert($data){
     //   print_r($data);exit();
      $this->db->insert($this->main_table,$data);

      //  $query="insert into competitors values('','$data['name']','$email','$mobile')";
      //  $this->db->query($query);

        return $this->db->insert_id();
        //return $this->db->insert_id();
    }
    public function search_property_image($property)
    {
            foreach($property as $one_property){
             
                $this->db->select('property_images.image_name');
                $this->db->from('property_images');
                $this->db->where('property_id',$one_property->id);
              
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $one_image = $query->first_row();
                //    print_r($one_image->image_name);exit();
                    $one_property->image_name=$one_image->image_name;
                }
            }
           
        return $property;
     
    }
    public function search_car_image($cars)
    {
            foreach($cars as $one_car){
             
                $this->db->select('car_images.image_name');
                $this->db->from('car_images');
                $this->db->where('car_id',$one_car->id);
              
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $one_image = $query->first_row();
                   // print_r($one_image->image_name);exit();
                    $one_car->image_name=$one_image->image_name;
                }
            }
           
        return $cars;
     
    }
   public function search_property($keyword)
    {

    //    print_r($keyword);exit();
        $this->db->select('*');
        $this->db->from('villa_aparts');
        if($keyword['type_id'] !='none' ){
        $this->db->where('property_type_id',$keyword['type_id']);}
        if($keyword['city_id'] !='none' ){
        $this->db->where('city_id',$keyword['city_id']);}
        if($keyword['area'] !=null ){
        $this->db->or_like('area',$keyword['area']);}
        if($keyword['rooms_count'] !='none' ){
        $this->db->or_where('rooms_count',$keyword['rooms_count']);}
      // print_r( $this->db->get());exit();
        $query = $this->db->get();
        return $query->result();
     
    }
   public function search_cars($keyword)
    {
        $this->db->select('cars.*,investors.username as investor_name,investors.phone as phone');
        $this->db->from('cars');
        $this->db->join('investors' , 'investors.id = cars.investor_id',"inner");

        if($keyword['car_brand'] !='none' ){
        $this->db->where('car_brand',$keyword['brand_id']);
        }
        if($keyword['cat_id'] !='none' ){
        $this->db->or_where('category',$keyword['cat_id']);
        }
        if($keyword['color'] !=null){
        $this->db->or_like('color',$keyword['color']);
        }
        if($keyword['year'] !=null ){
        $this->db->or_like('year',$keyword['year']);
        }

        $query = $this->db->get();
       // print_r( $this->db->get());exit();
        return $query->result();
    }
}  