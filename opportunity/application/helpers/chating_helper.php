<?php

function chick_null_value($value){
    if(isset($value) && !empty($value) && $value!=null && $value!=""){
        return $value;
    }else{
        return "0";
    }
}


//======================================================================================================================
function upload_all_files($file_name){
    $folder="images";
    $CI =& get_instance();
    $config['upload_path'] = 'uploads/'.$folder;
    //$config['allowed_types'] = 'gif|Gif|ico|ICO|jpg|JPG|jpeg|JPEG|BNG|png|PNG|bmp|BMP|WMV|wmv|MP3|mp3|FLV|flv|SWF|swf';
    $config['allowed_types'] = 'gif|Gif|ico|ICO|jpg|JPG|jpeg|JPEG|BNG|png|PNG|bmp|BMP|WMV|wmv|MP3|mp3|FLV|flv|SWF|swf|pdf|PDF|xls|xlsx|mp4|doc|docx|txt|rar|tar.gz|zip';
    $config['max_size']    = '10000000000';
    $config['encrypt_name']=true;
    $CI->load->library('upload',$config);
    if(! $CI->upload->do_upload($file_name)){
        return  $CI->upload->display_errors();
    }else{
        $datafile = $CI->upload->data();
        return  $datafile['file_name'];
    }
}
//======================================================================================================================



?>