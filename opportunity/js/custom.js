$(document).ready(function () {



    $("body").prognroll({
        height: 5,
        color: "#8ec641",
        custom: false
    });



    /////////carousel-item"
    $(".carousel-item").owlCarousel({
        rtl: true,
        loop: true,
        items: 4,
        margin: 10,
        nav: false,
        pagination: true,
        dots: true,
        autoplay: true,
        autoplayHoverPause: true,
        navText: ["<i class='fa  fa-angle-double-right fa-lg'></i>",
                  "<i class='fa  fa-angle-double-left fa-lg'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });




});
