<?php
class Model_occ extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="occasions";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
    
  
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
   
    public  function update($id,$data){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
   
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
    
    public function delete_phones($id){
        $this->db->where("occ_id",$id);
        $this->db->delete('occ_phones');
    }
    public function get_all_occ_phones($occ_id){
        $this->db->select('occ_phones.phone_id');
        $this->db->from("occ_phones");
        $this->db->where("occ_phones.occ_id",$occ_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_this_occ_phones($occ_id){
        $this->db->select('occ_phones.phone_id,phone_book.name,phone_book.phone1,phone_book.phone2');
        $this->db->from("occ_phones");
        $this->db->join('phone_book' , 'phone_book.id = occ_phones.phone_id',"inner");

        $this->db->where("occ_phones.occ_id",$occ_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;

    }
    public function get_all_valid_occ(){
        $this->db->select('occasions.*');
        $this->db->from("occasions");
        $date = date('Y/m/d');
        $this->db->where('occasions.occ_date =',$date);
        $this->db->order_by('occasions.name asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;

    }
    public function get_all_occ(){
        $this->db->select('occasions.*');
        $this->db->from("occasions");

        $this->db->order_by('occasions.name asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public  function insert_phones($data){ 
        $this->db->insert("occ_phones",$data);
     return $this->db->insert_id();
      }
    public  function insert($data){ 
      $this->db->insert("occasions",$data);
   return $this->db->insert_id();
    }
  
   
}  