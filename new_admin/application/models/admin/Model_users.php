<?php
class Model_users extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->main_table="users";
        // $this->db->join('users', 'users.usrID = users_profiles.usrpID',"left");
        //  $this->db->where('display_id',$id)->count_all("bookdetails");
        //    return $this->db->insert_id();
        // $this->db->join('people AS a', 'dept.supervisor = a.people_id', 'left');
        //  $this->db->join('people AS b', 'dept.head = b.people_id', 'left');
        /*
           $data["user_pass"]=check_null(sha1(md5(($this->input->post("user_pass")))) );
        $data["user_phone"]=check_null($this->input->post("user_phone") );
        $data["user_email"]=check_null($this->input->post("user_email") );
        $data["user_full_name"]=check_null($this->input->post("user_full_name") );
        $data["user_name"]=check_null($this->input->post("user_full_name") );
        $data["date_registration"]=date("Y-m-d",time());
        $data["user_city"]=check_null($this->input->post("user_city")  );
        $data["user_google_lat"]=check_null($this->input->post("user_google_lat")  );
        $data["user_google_long"]=check_null($this->input->post("user_google_long")  );
        */
        
    }

    public function chek_Null($post_value){
        if($post_value == '' || $post_value==null || (!isset($post_value)) && !empty($post_value)){
            $val="";
            return $val;
        }else{
            return strip_tags($post_value);
        }
    }

    public  function insert($data){
        $this->db->insert($this->main_table,$data);
        return $this->db->insert_id();
        //return $this->db->insert_id();
    }
    //==========================================
    public  function update($id,$data){
        $this->db->where("id",$id);
        $this->db->update($this->main_table,$data);
    }
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }

    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }

    public function select_where($Conditions_arr){
        $this->db->select('*');
        $this->db->from($this->main_table);
        $this->db->where($Conditions_arr);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    
    public function get_all_city(){
        $this->db->select('cities.* ');
        $this->db->from("cities");
        $this->db->order_by("id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }

    public function get_all_country(){
        $this->db->select('countries.* ');
        $this->db->from("countries");
        $this->db->order_by("id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function select_all_user(){
        $this->db->select('users.* ');
        $this->db->from("users");
        $this->db->order_by("id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }

    public function table_truncate (){
        $this->db->from($this->main_table);
        $this->db->truncate();
    }

}//END CLASS


