<?php
class Model_trades extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="needs";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
  
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
     public function getByArray_dept($arr){
        $h = $this->db->get_where("depts2",$arr);
        return $h->row_array();
    }
     public  function update_depts($id,$data){
        $this->db->where("id",$id);       
        $this->db->update("depts2",$data);
    }
    public  function update($id,$data){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
    public  function insert_depts($data){ 
      $this->db->insert("depts2",$data);
   return $this->db->insert_id();
    }
       public  function insert($data){ 
      $this->db->insert($this->main_table,$data);
   return $this->db->insert_id();
    }

    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
    
    public function delete_depts($id){
        $this->db->where("id",$id);
        $this->db->delete("depts2");
    }
      public function get_all_depts(){
        $this->db->select('depts2.*');
        $this->db->from("depts2");
        $this->db->order_by('depts2.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    
        public  function accept_trade($id)
    
    {
        
         $this->db->where("needs.id",$id);
        $data1['status']=4;
        $this->db->update('needs',$data1);
        
    }
        public  function refuse_trade($id)
    
    {
        
         $this->db->where("needs.id",$id);
        $data1['status']=3;
        $this->db->update('needs',$data1);
        
    }
      public  function archief_need($id)
    
    {
        
         $this->db->where("needs.id",$id);
        $data1['status']=1;
        $this->db->update('needs',$data1);
        
    }
      public  function trade_need($id)
    
    {
        
         $this->db->where("needs.id",$id);
        $data1['status']=2;
          $data1['add_date']=date('Y/m/d');
        $this->db->update('needs',$data1);
        
    }
    
    public function get_all_my_trades(){
        $set_data = $this->session->all_userdata();
        $user_id=$set_data['user_id'];
        
        $this->db->select('needs.*,depts2.name as dept_name,thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname,voters.phone as voter_phone');
        $this->db->from("needs");
          $this->db->where("status",2);
            $this->db->where("needs.user_id",$user_id);
         $this->db->join('voters' , 'voters.id = needs.voter_id',"inner");
        $this->db->join('depts2' , 'depts2.id = needs.dept_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");

        $this->db->order_by('needs.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
     public function get_all_trades(){
        $this->db->select('needs.*,depts2.name as dept_name,thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname,voters.phone as voter_phone');
        $this->db->from("needs");
          $this->db->where("status",2);
         $this->db->join('voters' , 'voters.id = needs.voter_id',"inner");
        $this->db->join('depts2' , 'depts2.id = needs.dept_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");

        $this->db->order_by('needs.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
     public function get_all_refuse(){
            $set_data = $this->session->all_userdata();
         $user_id=$set_data['user_id'];
         
        $this->db->select('permissions.*');
         $this->db->from("permissions");
         $this->db->where("user_id",$user_id);       
         $this->db->where("page_id_fk",158);  
         $query = $this->db->get();
         if ($query->num_rows() == 0) { 
             $get_only=1;
         }
		 
         $this->db->select('needs.*,depts2.name as dept_name,thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname,voters.phone as voter_phone');
        $this->db->from("needs");
          $this->db->where("status",3);
          if($get_only==1){
             $this->db->where("needs.user_id",$user_id);  
          }
          
         $this->db->join('voters' , 'voters.id = needs.voter_id',"inner");
        $this->db->join('depts2' , 'depts2.id = needs.dept_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");

        $this->db->order_by('needs.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
      public function get_all_accept(){
           $set_data = $this->session->all_userdata();
         $user_id=$set_data['user_id'];
         
        $this->db->select('permissions.*');
         $this->db->from("permissions");
         $this->db->where("user_id",$user_id);       
         $this->db->where("page_id_fk",158);  
         $query = $this->db->get();
         if ($query->num_rows() == 0) { 
             $get_only=1;
         }
		 
         
          $this->db->select('needs.*,depts2.name as dept_name,thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname,voters.phone as voter_phone');
        $this->db->from("needs");
          $this->db->where("status",4);
           if($get_only==1){
             $this->db->where("needs.user_id",$user_id);  
          }
          
         $this->db->join('voters' , 'voters.id = needs.voter_id',"inner");
        $this->db->join('depts2' , 'depts2.id = needs.dept_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");

        $this->db->order_by('needs.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
      public function get_all_archief(){
          
            $set_data = $this->session->all_userdata();
         $user_id=$set_data['user_id'];
         
        $this->db->select('permissions.*');
         $this->db->from("permissions");
         $this->db->where("user_id",$user_id);       
         $this->db->where("page_id_fk",159);  
         $query = $this->db->get();
         if ($query->num_rows() == 0) { 
             $get_only=1;
         }
          
        $this->db->select('needs.*,depts2.name as dept_name,thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname,voters.phone as voter_phone');
        $this->db->from("needs");
          $this->db->where("status",1);
          if($get_only==1){
             $this->db->where("needs.user_id",$user_id);  
          }
          
         $this->db->join('voters' , 'voters.id = needs.voter_id',"inner");
        $this->db->join('depts2' , 'depts2.id = needs.dept_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");

        $this->db->order_by('needs.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
     public function get_all_my_needs(){
            $set_data = $this->session->all_userdata();
$user_id=$set_data['user_id'];
        $this->db->select('needs.*,depts2.name as dept_name,thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname,voters.phone as voter_phone');
        $this->db->from("needs");
          $this->db->where("status",0);
          $this->db->where("needs.user_id",$user_id);       

         $this->db->join('voters' , 'voters.id = needs.voter_id',"inner");
        $this->db->join('depts2' , 'depts2.id = needs.dept_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");

        $this->db->order_by('needs.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_all_needs(){
        $this->db->select('needs.*,depts2.name as dept_name,thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname,voters.phone as voter_phone');
        $this->db->from("needs");
          $this->db->where("status",0);
         $this->db->join('voters' , 'voters.id = needs.voter_id',"inner");
        $this->db->join('depts2' , 'depts2.id = needs.dept_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");

        $this->db->order_by('needs.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
 
  
   
}  