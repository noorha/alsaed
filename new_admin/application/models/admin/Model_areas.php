<?php
class Model_areas extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="areas";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
    
  
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
   
    public  function update($id,$data){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
   
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
   
    public function get_all_areas(){
        $this->db->select('areas.*');
        $this->db->from("areas");

        $this->db->order_by('areas.name asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
  
    public  function insert($data){ 
      $this->db->insert($this->main_table,$data);
   return $this->db->insert_id();
    }
  
   
}  