<?php
class Model_competitor extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="competitors";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
    public  function update($id,$data){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
    public  function update_state($id){
       
        $this->db->select('competitors.* ');
        $this->db->from("competitors");
        $this->db->where("id",$id);
        $query = $this->db->get();
         $query=$query->result();
          $send_request=$query[0]->send_request;

      $this->db->where("competitors.id",$id);
      $data['competitors.send_request']=$send_request+1;
   $this->db->update("competitors",$data);
  // print_r($this->db);exit();

  

        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
    public  function update_image($id,$data){
        $this->db->where("id",$id);
       
         //   $data['status']=1;
       
        $this->db->update($this->main_table,$data);
    }
    public  function update_accept($id){
        $this->db->where("id",$id);
       
            $data['status']=1;
       
        $this->db->update($this->main_table,$data);
    }
    public  function update_refuse($id){
        $this->db->where("id",$id);
       
            $data['status']=2;
       
        $this->db->update($this->main_table,$data);
    }
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
    public function get_all_presenters(){
        $this->db->select('competitors.* ');
        $this->db->from("competitors");
        $this->db->where("status",0);
        $this->db->order_by("competitors.id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public  function update_conditions($data){
        $this->db->where("id",1);
        $data['descr']=nl2br( $data['descr']);
        $this->db->update("conditions",$data);
    }
    public  function update_targets($data){
        $this->db->where("id",1);
        $data['descr']=nl2br( $data['descr']);
        $this->db->update("targets",$data);
    }
    public function get_all_conditions(){
        $this->db->select('conditions.* ');
        $this->db->from("conditions");
        $this->db->where("id",1);
        $this->db->order_by("id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_targets(){
        $this->db->select('targets.* ');
        $this->db->from("targets");
        $this->db->where("id",1);
        $this->db->order_by("id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }   
    public function get_all_awards(){
        $this->db->select('awards.* ');
        $this->db->from("awards");
        $this->db->order_by("id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_partners(){
        $this->db->select('competitors.* ');
        $this->db->from("competitors");
        $this->db->where("status",1);
        $this->db->order_by("competitors.id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_all_data(){
        $this->db->select('competitors.* ');
        $this->db->from("competitors");
        $this->db->order_by("id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_refused(){
        $this->db->select('competitors.* ');
        $this->db->from("competitors");
        $this->db->where("status",2);
        $this->db->order_by("competitors.id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
  
    public  function insert($data){
        
      $this->db->insert("competitors",$data);

      //  $query="insert into competitors values('','$data['name']','$email','$mobile')";
      //  $this->db->query($query);

        return $this->db->insert_id();
        //return $this->db->insert_id();
    }
    public function search_property_image($property)
    {
            foreach($property as $one_property){
             
                $this->db->select('property_images.image_name');
                $this->db->from('property_images');
                $this->db->where('property_id',$one_property->id);
              
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $one_image = $query->first_row();
                //    print_r($one_image->image_name);exit();
                    $one_property->image_name=$one_image->image_name;
                }
            }
           
        return $property;
     
    }
    public function search_car_image($cars)
    {
            foreach($cars as $one_car){
             
                $this->db->select('car_images.image_name');
                $this->db->from('car_images');
                $this->db->where('car_id',$one_car->id);
              
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $one_image = $query->first_row();
                   // print_r($one_image->image_name);exit();
                    $one_car->image_name=$one_image->image_name;
                }
            }
           
        return $cars;
     
    }
   public function search_property($keyword)
    {

    //    print_r($keyword);exit();
        $this->db->select('*');
        $this->db->from('villa_aparts');
        if($keyword['type_id'] !='none' ){
        $this->db->where('property_type_id',$keyword['type_id']);}
        if($keyword['city_id'] !='none' ){
        $this->db->where('city_id',$keyword['city_id']);}
        if($keyword['area'] !=null ){
        $this->db->or_like('area',$keyword['area']);}
        if($keyword['rooms_count'] !='none' ){
        $this->db->or_where('rooms_count',$keyword['rooms_count']);}
      // print_r( $this->db->get());exit();
        $query = $this->db->get();
        return $query->result();
     
    }
   public function search_cars($keyword)
    {
        $this->db->select('cars.*,investors.username as investor_name,investors.phone as phone');
        $this->db->from('cars');
        $this->db->join('investors' , 'investors.id = cars.investor_id',"inner");

        if($keyword['car_brand'] !='none' ){
        $this->db->where('car_brand',$keyword['brand_id']);
        }
        if($keyword['cat_id'] !='none' ){
        $this->db->or_where('category',$keyword['cat_id']);
        }
        if($keyword['color'] !=null){
        $this->db->or_like('color',$keyword['color']);
        }
        if($keyword['year'] !=null ){
        $this->db->or_like('year',$keyword['year']);
        }

        $query = $this->db->get();
       // print_r( $this->db->get());exit();
        return $query->result();
    }
}  