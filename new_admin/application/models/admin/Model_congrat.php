<?php
class Model_congrat extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="congrat";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
    
     public  function get_msg_with_gender_occ($type_id,$gender)
    
    {
        
        $this->db->select('occ_msgs.*');
        $this->db->from("occ_msgs");
        $this->db->where('occ_msgs.gender', $gender);
        $this->db->where('occ_msgs.type_id', $type_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    } 
    public  function get_msg_with_gender($type_id,$gender)
    
    {
        
        $this->db->select('congrat_msgs.*');
        $this->db->from("congrat_msgs");
        $this->db->where('congrat_msgs.gender', $gender);
        $this->db->where('congrat_msgs.type_id', $type_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    } 
     public  function get_msg_occ($type_id)

    {

        $this->db->select('occ_msgs.*');
        $this->db->from("occ_msgs");
        $this->db->where('occ_msgs.type_id', $type_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    } 

    public  function get_msg($type_id)

    {

        $this->db->select('congrat_msgs.*');
        $this->db->from("congrat_msgs");
        $this->db->where('congrat_msgs.type_id', $type_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    } 

    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
    public function getByArray_msg($arr){
        $h = $this->db->get_where('congrat_msgs',$arr);
        return $h->row_array();
    }
    
    public function getByArray_msg_occ($arr){
        $h = $this->db->get_where('occ_msgs',$arr);
        return $h->row_array();
    }
    public  function update($id,$data){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
    public  function update_msg($id,$data){
        $this->db->where("id",$id);       
        $this->db->update('congrat_msgs',$data);
    }
    
      public  function update_msg_occ($id,$data){
        $this->db->where("id",$id);       
        $this->db->update('occ_msgs',$data);
    }
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
    
    public function delete_phones($id){
        $this->db->where("congrat_id",$id);
        $this->db->delete('congrat_phones');
    }
    public function get_all_congrat_phones($occ_id){
        $this->db->select('congrat_phones.phone_id');
        $this->db->from("congrat_phones");
        $this->db->where("congrat_phones.congrat_id",$occ_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_this_congrat_phones($occ_id){
        $this->db->select('congrat_phones.phone_id,phone_book.name,phone_book.phone1,phone_book.phone2');
        $this->db->from("congrat_phones");
        $this->db->join('phone_book' , 'phone_book.id = congrat_phones.phone_id',"inner");

        $this->db->where("congrat_phones.congrat_id",$occ_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;

    }
    
    public function get_all_occ_msgs(){
        $this->db->select('occ_msgs.*');
        $this->db->from("occ_msgs");

        $this->db->order_by('occ_msgs.type_id asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_congrat_msgs(){
        $this->db->select('congrat_msgs.*');
        $this->db->from("congrat_msgs");

        $this->db->order_by('congrat_msgs.type_id asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_valid_congrat(){
        $this->db->select('congrat.*');
        $this->db->from("congrat");
        $date = date('Y/m/d');
        $this->db->where('congrat.occ_date =',$date);
        $this->db->order_by('congrat.name asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;

    }
    public function get_all_congrat(){
        $this->db->select('congrat.*');
        $this->db->from("congrat");

        $this->db->order_by('congrat.name asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public  function insert_phones($data){ 
        $this->db->insert("congrat_phones",$data);
     return $this->db->insert_id();
      }
    public  function insert($data){ 
      $this->db->insert("congrat",$data);
   return $this->db->insert_id();
    }
  
   
}  