<?php
class Model_diwans extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="diwans";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
    public  function increase_counter($id)
    
    {
        $this->db->select('diwans.* ');
        $this->db->from("diwans");
        $this->db->where("id",$id);
        $query = $this->db->get();
        $query=$query->result();
        $phone_counter=$query[0]->phone_counter;
        
        $this->db->where("diwans.id",$id);
        $data['diwans.phone_counter']=$phone_counter+1 ;
        $this->db->update("diwans",$data);
    }
  
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
   
    public  function update($id,$data){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
   
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
    
    public function get_all_search_result($diwan_type,$area,$day_type){
     
        
        $this->db->select('diwans.*,areas.name as area_name');
        $this->db->from("diwans");
        
        if($day_type !='all' && $day_type !=null){
            $this->db->where("day_type",$day_type);
            $this->db->where("diwan_type",0);
            
            
        }
        if($diwan_type !='all' && $diwan_type !=null){
            $this->db->where("diwan_type",$diwan_type);       

        }
        if($area !='all'){
            $this->db->where("area",$area);       

        }

        $this->db->join('areas' , 'areas.id = diwans.area',"inner");

        $this->db->order_by('diwans.name asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_diwans($diwan_type){
        $set_data = $this->session->all_userdata();
$user_id=$set_data['user_id'];

        if($diwan_type==0){

            $this->db->select('permissions.*');
            $this->db->from("permissions");
            $this->db->where("user_id",$user_id);       
            $this->db->where("page_id_fk",129);  
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                 $all_week='1';
            }   
            else {  $all_week='0'; }

        }
       else if($diwan_type==1){

            $this->db->select('permissions.*');
            $this->db->from("permissions");
            $this->db->where("user_id",$user_id);       
            $this->db->where("page_id_fk",135);  
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                 $all_day='1';
            }   
            else {  $all_day='0'; }

        }
       else if($diwan_type==2){

            $this->db->select('permissions.*');
            $this->db->from("permissions");
            $this->db->where("user_id",$user_id);       
            $this->db->where("page_id_fk",136);  
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                 $unknown='1';
            }   
            else {  $unknown='0'; }

        }
        
        $this->db->select('diwans.*,areas.name as area_name');
        $this->db->from("diwans");
        if($diwan_type !='all'){
            $this->db->where("diwan_type",$diwan_type);       

        }

       if($all_week=='0' || $all_day=='0' || $unknown=='0'){
        $this->db->where("user_id",$user_id);       
       }

        $this->db->join('areas' , 'areas.id = diwans.area',"inner");

        $this->db->order_by('diwans.name asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
     public function get_all_my_diwans($diwan_type){
        $set_data = $this->session->all_userdata();
$user_id=$set_data['user_id'];

        if($diwan_type==0){

            $this->db->select('permissions.*');
            $this->db->from("permissions");
            $this->db->where("user_id",$user_id);       
            $this->db->where("page_id_fk",129);  
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                 $all_week='1';
            }   
            else {  $all_week='0'; }

        }
       else if($diwan_type==1){

            $this->db->select('permissions.*');
            $this->db->from("permissions");
            $this->db->where("user_id",$user_id);       
            $this->db->where("page_id_fk",135);  
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                 $all_day='1';
            }   
            else {  $all_day='0'; }

        }
       else if($diwan_type==2){

            $this->db->select('permissions.*');
            $this->db->from("permissions");
            $this->db->where("user_id",$user_id);       
            $this->db->where("page_id_fk",136);  
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                 $unknown='1';
            }   
            else {  $unknown='0'; }

        }
        
        $this->db->select('diwans.*,areas.name as area_name');
        $this->db->from("diwans");
        if($diwan_type !='all'){
            $this->db->where("diwan_type",$diwan_type);       

        }

       if($all_week=='0' || $all_day=='0' || $unknown=='0'){
        $this->db->where("user_id",$user_id);       
       }
        $this->db->where("diwans.user_id",$user_id);       

        $this->db->join('areas' , 'areas.id = diwans.area',"inner");

        $this->db->order_by('diwans.name asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
  
    public  function insert($data){ 
        $set_data = $this->session->all_userdata();
$data['user_id']=$set_data['user_id'];
      $this->db->insert("diwans",$data);
   return $this->db->insert_id();
    }
  
   
}  