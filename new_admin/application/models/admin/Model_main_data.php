<?php
class Model_main_data extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="main_data";
    }
    public function chek_Null($post_value){
        if($post_value == '' || $post_value==null || (!isset($post_value))){
            $val='';
            return $val;
        }else{
            return $post_value;
        }
    }
	   public function delete_contact($id){
        $this->db->where("id",$id);
        $this->db->delete("contact_us");
    }

	public function select_all_contacts(){
		  
        $this->db->select('contact_us.*');
        $this->db->from("contact_us");
        $this->db->order_by("id","DESC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            
            return $query->result() ;
        }
        return false;
      
    
	}
    //==========================================
    public  function  insert($file){
        $data["company_website"]=( $this->input->post('company_website'));
        $data["company_logo"]= $file["ar"];
        $data["ar_company_address"]=$this->chek_Null( $this->input->post('ar_company_address'));
        $data["en_company_address"]=$this->chek_Null( $this->input->post('en_company_address'));
        $data["ar_company_about"]=$this->chek_Null( $this->input->post('ar_company_about'));
        $data["en_company_about"]=$this->chek_Null( $this->input->post('en_company_about'));
        $data["ar_company_terms"]=$this->chek_Null( $this->input->post('ar_company_terms'));
        $data["en_company_terms"]=$this->chek_Null( $this->input->post('en_company_terms'));

        $data["company_facebook"]= ($this->input->post('company_facebook'));
        $data["company_twitter"]=( $this->input->post('company_twitter'));
        $data["company_instagram"]= ($this->input->post('company_instagram'));
        $data["company_linkedin"]= ($this->input->post('company_linkedin'));
        $data["company_youtube"]=( $this->input->post('company_youtube'));
        $data["company_googlepluse"]= ($this->input->post('company_googlepluse'));
        $data["company_pinterest"]= $this->input->post('company_pinterest');
        $data["company_dribbble"]= $this->input->post('company_dribbble');
        $data["company_whatsapp"]= $this->input->post('company_whatsapp');
        $data["company_snapchat"]= $this->input->post('company_snapchat');

        $data["company_phones"]= ($this->input->post('company_phones'));
        $data["company_emails"]=( $this->input->post('company_emails'));
        $data["company_hot_line"]=( $this->input->post('company_hot_line'));

        $data["company_google_long"]=( $this->input->post('company_google_long'));
        $data["company_google_lat"]=( $this->input->post('company_google_lat'));

        $data["publisher"]=$_SESSION["user_id"];
        $this->db->insert($this->main_table,$data);
    }
    //==========================================
    public  function  update($id,$file){
        $data["company_website"]=( $this->input->post('company_website'));
        $data["ar_company_address"]=$this->chek_Null( $this->input->post('ar_company_address'));
        $data["en_company_address"]=$this->chek_Null( $this->input->post('en_company_address'));
        $data["ar_company_about"]=$this->chek_Null( $this->input->post('ar_company_about'));
        $data["en_company_about"]=$this->chek_Null( $this->input->post('en_company_about'));
        $data["ar_company_terms"]=$this->chek_Null( $this->input->post('ar_company_terms'));
        $data["en_company_terms"]=$this->chek_Null( $this->input->post('en_company_terms'));
        $data["company_snapchat"]= $this->input->post('company_snapchat');
        $data["company_whatsapp"]= $this->input->post('company_whatsapp');
        $data["company_facebook"]= ($this->input->post('company_facebook'));
        $data["company_twitter"]=( $this->input->post('company_twitter'));
        $data["company_instagram"]= ($this->input->post('company_instagram'));
        $data["company_linkedin"]= ($this->input->post('company_linkedin'));
        $data["company_youtube"]=( $this->input->post('company_youtube'));
        $data["company_googlepluse"]= ($this->input->post('company_googlepluse'));
        $data["company_pinterest"]= $this->input->post('company_pinterest');
        $data["company_dribbble"]= $this->input->post('company_dribbble');

        $data["company_phones"]= ($this->input->post('company_phones'));
        $data["company_emails"]=( $this->input->post('company_emails'));
        $data["company_hot_line"]=( $this->input->post('company_hot_line'));
        if(!empty($file["ar"])){
            $data["company_logo"]=$file["ar"];
        }
        $data["company_google_long"]=( $this->input->post('company_google_long'));
        $data["company_google_lat"]=( $this->input->post('company_google_lat'));
        
        $this->db->where("id",$id);
        $this->db->update($this->main_table,$data);
    }
    //==========================================
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
    //==========================================
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
    //==========================================
    public function select_all(){
        $this->db->select('*');
        $this->db->from($this->main_table);
        $this->db->order_by("id","DESC");
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }

     //==================================================
     public function select_counter($table,$arr){
        $this->db->from($table);
   //     $this->db->where($arr);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
        return 0;
    }
     //==================================================
    

}//END CLASS


