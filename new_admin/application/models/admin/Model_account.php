<?php
class Model_account extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="accounts";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
  
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
     public function getByArray_dept($arr){
        $h = $this->db->get_where("depts",$arr);
        return $h->row_array();
    }
     public  function update_depts($id,$data){
        $this->db->where("id",$id);       
        $this->db->update("depts",$data);
    }
    public  function update($id,$data){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
    public  function insert_depts($data){ 
      $this->db->insert("depts",$data);
   return $this->db->insert_id();
    }
       public  function insert($data){ 
      $this->db->insert($this->main_table,$data);
   return $this->db->insert_id();
    }
    public function last_record()
{ 
   
    $this->db->select('balance');
        $this->db->from("accounts");
        $this->db->order_by('accounts.id desc'); 
          $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;  
} 
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
    
    public function delete_depts($id){
        $this->db->where("id",$id);
        $this->db->delete("depts");
    }
      public function get_all_depts(){
        $this->db->select('depts.*');
        $this->db->from("depts");
        $this->db->order_by('depts.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_accounts(){
        $this->db->select('accounts.*');
        $this->db->from("accounts");
        $this->db->order_by('accounts.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
 
  
   
}  