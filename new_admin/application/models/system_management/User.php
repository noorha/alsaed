<?php
class User extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
//-----------------------------------------------------------
    private function chek_Null($post_value){
        if($post_value == '' || $post_value==null || (!isset($post_value)) && !empty($post_value)){
            $val=" ";
            return $val;
        }else{
            return $post_value;
        }
    }
//---------------------------------------------------------
    public function insert($file){
        $file_in=0;
        if(isset($file) && !empty($file) &&   $file!=null && $file!=''){
            $file_in= $file;
        }
        $password = $this->input->post('user_pass', true);
        $password = sha1(md5($password));
            $stores=explode("-",$this->input->post('store_id'));
        $data = array(
            'user_name'=>   $this->chek_Null($this->input->post('user_name')),
            'user_username'=>   $this->chek_Null($this->input->post('user_username')),
            "user_pass" => $password,
            "user_email" =>  $this->chek_Null($this->input->post('user_email')),
            "per_id_fk" =>   $this->chek_Null($this->input->post('per_id_fk')),
            "user_phone" =>  $this->chek_Null($this->input->post('user_phone')),
            "role_id_fk" =>  1,
            'user_photo'=>$file_in

        );
        if($this->db->insert('admin_voters',$data)){
            return true;
        }else{
            return false;
        }
    }
    public function update($id,$file){
      //  $stores=explode("-",$this->input->post('store_id'));
        $data = array(
            'user_name'=>   $this->chek_Null($this->input->post('user_name')),
            'user_username'=>   $this->chek_Null($this->input->post('user_username')),
            "user_email" =>  $this->chek_Null($this->input->post('user_email')),
            "per_id_fk" =>   $this->chek_Null($this->input->post('per_id_fk')),
            "user_phone" =>  $this->chek_Null($this->input->post('user_phone')),

            "role_id_fk" =>  1,
        );
        if(isset($file) && !empty($file) &&   $file!=null && $file!=''){
            $data['user_photo']=  $file;
        }
        $this->db->where('user_id', $id);
        if ($this->db->update('admin_voters', $data)) {
            return true;
        } else {
            return false;
        }
    }
    public function update_pw($id){
        $password = $this->input->post('user_pass', true);
        $password = sha1(md5($password));
        $data = array('user_pass'=> $password);
        $this->db->where('user_id', $id);
        if ($this->db->update('admin_voters', $data)) {
            return true;
        } else {
            return false;
        }
    }

    //-----------------------------------------------------------
    public function all_users($role_id_fk){
        $this->db->select('admin_voters.user_id,admin_voters.user_name , admin_voters.user_username,
        admin_voters.user_photo , admin_voters.role_id_fk , admin_voters.admin_type');
        $this->db->from('admin_voters');
        $this->db->where("admin_voters.role_id_fk >= ",$role_id_fk);
        $this->db->where("admin_voters.admin_type != ",1);
        $this->db->order_by("admin_voters.role_id_fk","ASC");
    //    print_r($this->db->get());exit();
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }
//-----------------------------------------------------------
    public function get_by_id($id){
        $query=$this->db->get_where('admin_voters',array('user_id'=>$id));
        return $query->row_array();
    }

//------------------------------------------------------------
    public function select_user_per(){
        $this->db->select('permissions.user_id, permissions.permission_rol_name ,  
        admin_voters.user_id as u_user_id, admin_voters.user_username as name');
        $this->db->from("permissions");
        $this->db->join('admin_voters', 'admin_voters.user_id = permissions.user_id',"left");
        $this->db->group_by("permissions.user_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
}// END CLASS