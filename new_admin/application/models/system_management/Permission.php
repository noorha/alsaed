<?php
class Permission extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
 //---------------------------------------------------------------
 public  function insert_user_role(){
 
     if (in_array("100-2", $_POST["select-all"]) || in_array("126-2", $_POST["select-all"]) || in_array("127-2", $_POST["select-all"])
         || in_array("128-2", $_POST["select-all"])){
             $data['user_id']=$this->input->post("user_id");
             $data['page_id_fk']='125';
             $data['page_level']='0';
             //   print_r($_POST["select-all"]);exit();
             $this->db->insert('permissions',$data);
     }
     
    
      if (in_array("159-2", $_POST["select-all"]) || in_array("161-2", $_POST["select-all"]) || in_array("158-2", $_POST["select-all"])
         || in_array("160-2", $_POST["select-all"])){
             $data['user_id']=$this->input->post("user_id");
             $data['page_id_fk']='157';
             $data['page_level']='0';
             //   print_r($_POST["select-all"]);exit();
             $this->db->insert('permissions',$data);
     }
      
     if (in_array("162-2", $_POST["select-all"]) || in_array("163-2", $_POST["select-all"]) ){
             $data['user_id']=$this->input->post("user_id");
             $data['page_id_fk']='134';
             $data['page_level']='1';
             //   print_r($_POST["select-all"]);exit();
             $this->db->insert('permissions',$data);
     }
       if (in_array("164-2", $_POST["select-all"]) || in_array("165-2", $_POST["select-all"]) ){
             $data['user_id']=$this->input->post("user_id");
             $data['page_id_fk']='155';
             $data['page_level']='1';
             //   print_r($_POST["select-all"]);exit();
             $this->db->insert('permissions',$data);
     }
     if (in_array("11-1", $_POST["select-all"]) || in_array("12-1", $_POST["select-all"]) || in_array("13-1", $_POST["select-all"])
         || in_array("14-1", $_POST["select-all"]) || in_array("16-1", $_POST["select-all"]) || in_array("15-1", $_POST["select-all"])
         || in_array("17-1", $_POST["select-all"])){
             $data['user_id']=$this->input->post("user_id");
             $data['page_id_fk']='10';
             $data['page_level']='0';
             //   print_r($_POST["select-all"]);exit();
             $this->db->insert('permissions',$data);
     }
     if (in_array("5-1", $_POST["select-all"]) || in_array("6-1", $_POST["select-all"]) || in_array("7-1", $_POST["select-all"])){
             $data['user_id']=$this->input->post("user_id");
             $data['page_id_fk']='4';
             $data['page_level']='0';
             //   print_r($_POST["select-all"]);exit();
             $this->db->insert('permissions',$data);
     }
     if (in_array("9-1", $_POST["select-all"])){
         $data['user_id']=$this->input->post("user_id");
         $data['page_id_fk']='8';
         $data['page_level']='0';
         //   print_r($_POST["select-all"]);exit();
         $this->db->insert('permissions',$data);
     }
      if (in_array("158-2", $_POST["select-all"]) || in_array("159-2", $_POST["select-all"]) ){
         $data['user_id']=$this->input->post("user_id");
         $data['page_id_fk']='157';
         $data['page_level']='0';
         //   print_r($_POST["select-all"]);exit();
         $this->db->insert('permissions',$data);
     }
     if (in_array("140-2", $_POST["select-all"]) || in_array("141-2", $_POST["select-all"]) || in_array("142-2", $_POST["select-all"])
         || in_array("143-2", $_POST["select-all"]) || in_array("145-2", $_POST["select-all"])){
             $data['user_id']=$this->input->post("user_id");
             $data['page_id_fk']='139';
             $data['page_level']='1';
             //   print_r($_POST["select-all"]);exit();
             $this->db->insert('permissions',$data);
     }
   //  print_r($_POST["select-all"]);exit();
     for($x=0;$x<sizeof($_POST["select-all"]);$x++){
         $r=explode("-",$_POST["select-all"][$x]);
         $data['user_id']=$this->input->post("user_id");
         $data['page_id_fk']=$r[0];
         $data['page_level']=$r[1];
         //   print_r($_POST["select-all"]);exit();
         $this->db->insert('permissions',$data);
     }
 }
//---------------------------------------------------------------
    public function select_per($id){
        $this->db->select('*');
        $this->db->from("permissions");
        $this->db->where("user_id",$id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row->page_id_fk;
            }
            return $data;
        }
        return false;
    }
//-----------------------------------------------------------
    public function user_in(){
        $this->db->select('*');
        $this->db->from('permissions');
        $this->db->group_by("user_id");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row->user_id;
            }
            return $data;
        }
        return false;
    }
//-----------------------------------------------------------
    public function users_name(){
        $this->db->select('*');
        $this->db->from('admin_voters');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[$row->user_id] = $row->user_username;
            }
            return $data;
        }
        return false;
    }
//-----------------------------------------------------------
    public function user_all_pages($user_id){
        $this->db->select('permissions.user_id , permissions.page_level,permissions.page_id_fk');
        $this->db->from("permissions");
        $this->db->where("permissions.user_id",$user_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $s_row) {
                $data[]=$s_row->page_id_fk;
            }
            return $data;
        }
        return false;
    }
//-------------------------------------------------------
    public function user_pages($user_id){
          $user_pages=$this->user_all_pages($user_id);
        if($user_pages != false){
            $this->db->select('*');
            $this->db->from('pages');
            $this->db->where("group_id_fk",0);
            $this->db->order_by("page_order","ASC");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $data = array();
                $i=0;
                foreach ($query->result() as $row) {
                    if(in_array($row->page_id, $user_pages)){
                        $data[$i]= $row;
                        $data[$i]->sub= $this->select_sub_pages($row->page_id,$user_pages);
                        $i++;
                    }
                }
                return $data;
            }
            return 0;
        }
        return false;
    }


//-------------------------------------------------------
    public function select_sub_pages($group_id_fk,$user_pages){
        $this->db->select('*');
        $this->db->from("pages");
        $this->db->where('group_id_fk',$group_id_fk);
        $this->db->order_by("page_order","ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                 if(in_array($row->page_id, $user_pages)){
                    $data[]= $row;
                 }
            }
            return $data;
        }
        return 0;
    }
//-----------------------------------------------------------
}// END CLASS