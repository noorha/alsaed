<?php
class Users extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }
    public function record_count() {
        return $this->db->count_all("admin_voters");
    }
    public function select() {
        $this->db->select('*');
        $this->db->from('admin_voters');
        $this->db->order_by('user_id','DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function fetch_users($limit, $start) {
        $this->db->select('*');
        $this->db->from('admin_voters');
        $this->db->order_by('user_id','DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }

    public function display($id){
        $this->db->where('user_id',$id);
        $query=$this->db->get('admin_voters');
        return $query->row_array();
    }
    public function check_user_data(){
        $this->db->select('*');
        $this->db->from('admin_voters');
        $this->db->where('user_username',$this->input->post('user_name'));
        $this->db->where('user_pass',sha1(md5($this->input->post('user_pass'))));
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return false;
        }
    }
    public function check_regester_data(){
        $this->db->select('*');
        $this->db->from('register');
        $this->db->where('user_name',$this->input->post('user_name'));
        $this->db->where('password',$this->input->post('user_pass'));
        $this->db->where('approved',1);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return false;
        }
    }
    public function update_last_login($id){

        $data['last_login']=time();
        $this->db->where('user_id',$id);
        $this->db->update('admin_voters',$data);

    }


}// END CLASS