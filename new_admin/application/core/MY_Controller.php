<?php
 // MY_Controller 


 class MY_Controller extends CI_Controller{
    public   function __construct(){
        parent::__construct();
           if($this->session->userdata('is_logged_in')==0){
                   redirect('login');
              }
        $this->load->model('Difined_model');
        //  ----- pages   ---------------------------------
    $this->load->model('system_management/Groups');
              $this->load->model('system_management/Permission');
          if ($_SESSION["per_id_fk"] == 0) {  // admin_type
              $this->sidbarpages=$this->Permission->user_pages($_SESSION["user_id"]);
          }elseif($_SESSION["per_id_fk"] != 0){
              $this->sidbarpages=$this->Permission->user_pages($_SESSION["per_id_fk"]);
          }
     //   $this->sidbarpages=$this->Groups->get_categories();
        //  ----- pages   ---------------------------------
    }
    public  function test($data=array()){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die;
    }
    public  function thumb($data){
        $config['image_library'] = 'gd2';
        $config['source_image'] =$data['full_path'];
        $config['new_image'] = 'uploads/thumbs/'.$data['file_name'];
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['thumb_marker']='';
        $config['width'] = 275;
        $config['height'] = 250;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }
    public  function upload_image($file_name){
    $config['upload_path'] = 'uploads';
    $config['allowed_types'] = 'gif|Gif|ico|ICO|jpg|JPG|jpeg|JPEG|BNG|png|PNG|bmp|BMP|WMV|wmv|MP3|mp3|FLV|flv|SWF|swf';
    $config['max_size']    = '1024*8';
    $config['encrypt_name']=true;
    $this->load->library('upload',$config);
    if(! $this->upload->do_upload($file_name)){
      return  false;
    }else{
        $datafile = $this->upload->data();
        $this->thumb($datafile);
       return  $datafile['file_name'];
    }
     }
    public  function upload_file($file_name){
        $config['upload_path'] = 'uploads';
        $config['allowed_types'] = 'gif|Gif|ico|ICO|jpg|JPG|jpeg|JPEG|BNG|png|PNG|bmp|BMP|WMV|wmv|MP3|mp3|FLV|flv|SWF|swf|pdf|PDF|xls|xlsx|mp4|doc|docx|txt|rar|tar.gz|zip';
    //    $config['max_size']    = '1024*8';
        $config['overwrite'] = true;
        $this->load->library('upload',$config);
        if(! $this->upload->do_upload($file_name)){
           
            return  false;
        }else {
            $datafile = $this->upload->data();
         
            return $datafile['file_name'];
        }
    }
    public  function url (){
     unset($_SESSION['url']);
        $this->session->set_flashdata('url','http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    }
     public function message($type, $text)
    {
        if ($type == 'success') {
            return $this->session->set_flashdata('message', '<div class="alert alert-success alert-rounded">
                                                              <i class="ti-user"></i>  ' . $text . '.
                                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                              <span aria-hidden="true">×</span> </button>
                                                             </div>');
        } elseif ($type == 'wiring') {
            return $this->session->set_flashdata('message', '<div class="alert alert-warning  alert-rounded">
                                                              <i class="ti-user"></i>  ' . $text . '.
                                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                              <span aria-hidden="true">×</span> </button>
                                                             </div>');
        } elseif ($type == 'error') {
            return $this->session->set_flashdata('message', '<div class="alert alert-danger  alert-rounded">
                                                              <i class="ti-user"></i>  ' . $text . '.
                                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                              <span aria-hidden="true">×</span> </button>
                                                             </div>');
        }elseif ($type == 'info') {
            return $this->session->set_flashdata('message', '<div class="alert alert-info  alert-rounded">
                                                              <i class="ti-user"></i>  ' . $text . '.
                                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                              <span aria-hidden="true">×</span> </button>
                                                             </div>');
        }
    }
     //=====================================================================
     public function  upload_muli_image($input_name ,$folder="images"){
         $filesCount = count($_FILES[$input_name]['name']);
         for($i = 0; $i < $filesCount; $i++){
             $_FILES['userFile']['name'] = $_FILES[$input_name]['name'][$i];
             $_FILES['userFile']['type'] = $_FILES[$input_name]['type'][$i];
             $_FILES['userFile']['tmp_name'] = $_FILES[$input_name]['tmp_name'][$i];
             $_FILES['userFile']['error'] = $_FILES[$input_name]['error'][$i];
             $_FILES['userFile']['size'] = $_FILES[$input_name]['size'][$i];
             $all_img[]=$this->upload_image("userFile",$folder);
         }
         return $all_img;
     }
/**
 *  ================================================================================================================
 * 
 *  ----------------------------------------------------------------------------------------------------------------
 * 
 *  ================================================================================================================
 */


 } // end class 

?>