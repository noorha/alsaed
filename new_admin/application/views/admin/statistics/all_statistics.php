
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-12 col-md-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->

					
					<!-- /.dropdown js__dropdown -->
	    
    
<?php 
     if(isset($all_statistics) && !empty($all_statistics)){
        
    ?>
	<div class="table-responsive" data-pattern="priority-columns"   style=" height: 1130px;">
	
  <table id="example" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			  <tr >
      <th >م</th>
     
        <th    style="  text-align: center;font-weight: bold;">اسم الفخذ</th>
        <th  colspan="2" style="  text-align: center;font-weight: bold;">الحضور</th>
        <th  colspan="2" style="  text-align: center;font-weight: bold;">عدم الحضور</th>

        <th  colspan="2" style="  text-align: center;font-weight: bold;">ملغى</th>

        <th   style="  text-align: center;font-weight: bold;">العدد الكلى</th>

      </tr>
      <tr >
      <th ></th>
      <th ></th>

        <th style="text-align: center;">العدد</th>
        <th style="text-align: center;" >النسبة</th>
        <th style="text-align: center;">العدد</th>
        <th  style="text-align: center;">النسبة</th>
        <th style="text-align: center;">العدد</th>
        <th style="text-align: center;" >النسبة</th>
<th></th>
      </tr>
			</thead>
					
					    <tbody>
   <?php $i=1; foreach ($all_statistics as $row ){ ?>
     <tr  style="border-color:black;">
     
        <td> <?php echo $i;?> </td>
        <td style="  text-align: center;"><?php echo $row->name;?></td>
      
        <?php

$this->db->select('voters.*');
$this->db->from('voters');
$this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('attendance.attend',0);
$query=$this->db->get();
$cancel=$query->num_rows() ;

$this->db->select('voters.*');
$this->db->from('voters');
$this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('attendance.attend',1);
$query=$this->db->get();
$count_attend=$query->num_rows() ;

$this->db->select('voters.*');
$this->db->from('voters');
$this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('attendance.attend',null);
$query=$this->db->get();
$count_not_attend=$query->num_rows() ;

$this->db->select('voters.*,thighs.name as thigh_name ');
$this->db->from("voters");
$this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('kashf_num NOT IN( SELECT kashf_num FROM attendance)');
$query=$this->db->get();
$count_not_attend2=$query->num_rows() ;

$attend_url=base_url()."admin/dashboard/all_attends/".$row->id;
$cancel_url=base_url()."admin/dashboard/cancel/".$row->id;
$not_attend_url=base_url()."admin/dashboard/not_attends/".$row->id;
$all_url=base_url()."admin/dashboard/total_thigh_count/".$row->id;

?>
 <?php
      $stat=$count_attend + $count_not_attend + $cancel + $count_not_attend2;
      $stat_total=$stat_total+$stat;
      $all_count_attend=$all_count_attend + $count_attend;
      $all_not_count_attend=$all_not_count_attend + $count_not_attend + $count_not_attend2;
      $all_count_cancel=$all_count_cancel + $cancel;
       ?>
        <td  style="text-align:center;">
        <a  href="<?=$attend_url?>" target="_blank">
        <span style="tex-align:right"><?php echo $count_attend;?></span>
        </a>
        </td>
        <td  style="text-align:center;">
        <span style="tex-align:left"><?php echo  round($count_attend / ($stat / 100),0).'%';?></span>
        </td>
        <td  style="text-align:center;">

        <a  href="<?=$not_attend_url?>" target="_blank">
        <span style="tex-align:right"><?php echo $count_not_attend + $count_not_attend2;?></span>

      </a>
      </td>
      <td  style="text-align:center;">
      <span style="tex-align:left"><?php echo round(($count_not_attend + $count_not_attend2) / ($stat / 100),0).'%';?></span>  
        </td>
        <td  style="text-align:center;">
        <a  href="<?=$cancel_url?>" target="_blank">
      
        <span style="tex-align:right"><?php echo $cancel;?></span>

      </a>
      </td>
      <td  style="text-align:center;">
      <span style="tex-align:left"><?php echo round($cancel / ($stat / 100),0).'%';?></span>
        </td>
     
        
        <td style="tex-align:center" >
        <a  href="<?=$all_url?>" target="_blank">
      
      <span style="tex-align:right"><?php echo $stat;?></span>

    </a>
        </td>

      </tr>

     <?php $i++;?>
   
    <?php } ?>
    <tr>
    <td colspan="2" style="  text-align: center;font-weight: bold;">الأجمــــــــالى</td>
    
    <td style="  text-align: center;"><?php echo $all_count_attend;?></td>
    
    <td  style="text-align:center;">
      <span style="tex-align:left"><?php echo round($all_count_attend / ($stat_total / 100),0).'%';?></span>
    </td>

    <td style="  text-align: center;"><?php echo $all_not_count_attend;?></td>
    <td  style="text-align:center;">
      <span style="tex-align:left"><?php echo round($all_not_count_attend / ($stat_total / 100),0).'%';?></span>
    </td>

    <td style="  text-align: center;"><?php echo $all_count_cancel;?></td>
    <td  style="text-align:center;">
      <span style="tex-align:left"><?php echo round($all_count_cancel / ($stat_total / 100),0).'%';?></span>
    </td>

    <td style="  text-align: center;"><?php echo $stat_total;?></td>
    </tr>
    </tbody>
					</table>
					
				</div>	
					
		
	 <?php } else{
   
    
   echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد احصائيات!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
	
<div class="col-lg-12 col-md-12">
				<div class="box-content">
					<h4 class="box-title">معدل الاحصائيات</h4>
					<!-- /.box-title -->
								    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		
					<!-- /.dropdown js__dropdown -->
 <div id="piechart"  class="flot-chart" style="height: 320px;    margin-right: 25%;"></div>
 
 <script type="text/javascript">
var attend=<?php echo $all_count_attend;?>;
var total=<?php echo $stat_total;?>;
var not_att=<?php echo $all_not_count_attend;?>;
var canc=<?php echo((int)$all_count_cancel);?>;

      google.charts.load('current', {'packages':['corechart']});

      google.charts.setOnLoadCallback(drawChart);

 function drawChart() {

var data = new google.visualization.DataTable();

data.addColumn('string', 'العدد');
data.addColumn('number', 'النسبة');
console.log(typeof(not_att));


 data.addRows([
  
  
  
  ['العدد الكلى بالفخوذ',Number(total) ], 
 ['اجمالى الحضور', Number(attend) ],
 ['اجمالى عدم الحضور', Number(not_att)],
 ['اجمالى الملغى', Number(canc)] // More typically this would be done using a
  
]);      
	


   var options = {
             title: 'معدل الاحصائيات',
 width: 490,
    height: 250,
    pieHole: 0.5,
    colors: ['#008000', '#ffbf00', '#FF0000','#4E6282'],
    pieSliceText: 'value',
    sliceVisibilityThreshold :0,
    fontSize: 13,
    legend: {
      position: 'labeled'
    }
        };

 var chart = new google.visualization.PieChart(document.getElementById('piechart'));

chart.draw(data, options);

      }

    </script>

  



					<!-- /#flot-chart-1.flot-chart -->
				</div>
				<!-- /.box-content -->
			</div>
			
<div class="col-lg-12 col-md-12">
				<div class="box-content">
					<h4 class="box-title">معدل الاحصائيات</h4>
					<!-- /.box-title -->
								    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		
					<!-- /.dropdown js__dropdown -->
     <div id="barchart_material"   class="flot-chart"  style="height: 700px;"></div>

 <script type="text/javascript">
      google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'الفخذ');
      data.addColumn('number', 'حضور');
      data.addColumn('number', 'عدم حضور');
      data.addColumn('number', 'ملغى');
<?php  
		  foreach ($all_statistics as $row ){ 
   
$this->db->select('voters.*');
$this->db->from('voters');
$this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('attendance.attend',0);
$query=$this->db->get();
$cancel=$query->num_rows() ;

$this->db->select('voters.*');
$this->db->from('voters');
$this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('attendance.attend',1);
$query=$this->db->get();
$count_attend=$query->num_rows() ;

$this->db->select('voters.*');
$this->db->from('voters');
$this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('attendance.attend',null);
$query=$this->db->get();
$count_not_attend=$query->num_rows() ;

$this->db->select('voters.*,thighs.name as thigh_name ');
$this->db->from("voters");
$this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('kashf_num NOT IN( SELECT kashf_num FROM attendance)');
$query=$this->db->get();
$count_not_attend2=$query->num_rows() ;

$attend_url=base_url()."admin/dashboard/all_attends/".$row->id;
$cancel_url=base_url()."admin/dashboard/cancel/".$row->id;
$not_attend_url=base_url()."admin/dashboard/not_attends/".$row->id;

      $stat=$count_attend + $count_not_attend + $cancel + $count_not_attend2;
	  ?>
	  
	  data.addRows([['<?php echo$row->name;?>', <?php echo $count_attend;?>,<?php echo $count_not_attend + $count_not_attend2;?>,<?php echo $cancel;?>]]);
		 
		<?php  } ?>
		  
		
 
      var options = {
        title: 'معدل احصائيات الفخوذ',
        colors: ['red', 'blue' ,'green'],
        hAxis: {
          title: 'الفخوذ'
         
        },
        vAxis: {
          title: 'العدد',
          viewWindow: {
              max:500,
              min:0
            }
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('barchart_material'));
      chart.draw(data, options);
    }
    
 /*
var attend=<?php echo $all_count_attend;?>;
var total=<?php echo $stat_total;?>;
var not_att=<?php echo $all_not_count_attend;?>;
var canc=<?php echo((int)$all_count_cancel);?>;

      google.charts.load('current', {'packages':['corechart']});

      google.charts.setOnLoadCallback(drawChart);

 function drawChart() {

var data = new google.visualization.DataTable();

data.addColumn('string', 'العدد');
data.addColumn('number', 'النسبة');
console.log(typeof(not_att));


 data.addRows([
  
  
  
  ['العدد الكلى بالفخوذ',Number(total) ], 
 ['اجمالى الحضور', Number(attend) ],
 ['اجمالى عدم الحضور', Number(not_att)],
 ['اجمالى الملغى', Number(canc)] // More typically this would be done using a
  
]);      
	


   var options = {
             title: 'معدل الاحصائيات',
 width: 490,
    height: 250,
    pieHole: 0.5,
    colors: ['#008000', '#ffbf00', '#FF0000','#4E6282'],
    pieSliceText: 'value',
    sliceVisibilityThreshold :0,
    fontSize: 13,
    legend: {
      position: 'labeled'
    }
        };

 var chart = new google.visualization.PieChart(document.getElementById('piechart'));

chart.draw(data, options);

      }
*/
    </script>

  



					<!-- /#flot-chart-1.flot-chart -->
				</div>
				<!-- /.box-content -->
			</div>
			
	<!-- Flot Chart -->
	
	</div>
		<!-- /.row small-spacing -->		
		
	