<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
				<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>
					
					<!-- /.dropdown js__dropdown -->
<?php 
     if(isset($all_statistics) && !empty($all_statistics)){
        
    ?> 	
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
     <th>اسم الفخذ</th>
        <th>عدم الحضور</th>
			</thead>
						<tfoot>
				 <th>م</th>
     <th>اسم الفخذ</th>
        <th>عدم الحضور</th>
				</tfoot>
			 <tbody>
   <?php $i=1; foreach ($all_statistics as $row ){ ?>
     <tr>
     
        <td> <?php echo $i;?> </td>
        <td><?php echo $row->name;?></td>
      
        <?php


$this->db->select('voters.*');
$this->db->from('voters');
$this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('attendance.attend',null);
$query=$this->db->get();
$count_not_attend=$query->num_rows() ;

$this->db->select('voters.*,thighs.name as thigh_name ');
$this->db->from("voters");
$this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
$this->db->where('voters.thigh_id',$row->id);
$this->db->where('kashf_num NOT IN( SELECT kashf_num FROM attendance)');
$query=$this->db->get();
$count_not_attend2=$query->num_rows() ;

$not_attend_url=base_url()."admin/dashboard/not_attends/".$row->id;

?>
 
       
        <td>

        <a  href="<?=$not_attend_url?>" target="_blank">
        <span style="tex-align:right">عدم حضور</span>

      </a>
        </td>
       
      </tr>

     <?php $i++;?>
   
    <?php } ?>
    
    <tr>
           <td>  <?php echo $i; ?> </td>
           <td>  الكل </td>
           <td>
<?php $all_not_attend_url=base_url()."admin/dashboard/not_attends/";
?>
<a  href="<?=$all_not_attend_url?>" target="_blank">
<span style="tex-align:right">عدم حضور</span>

</a>
</td>
   </tr>
    </tbody>
					</table>
					</div>
	 <?php } else{
  
  echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد عدم حضور!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	