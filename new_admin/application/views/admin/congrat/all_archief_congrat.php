        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                
                <div class="page-header">
                    <div class="page-title">

                        
                    </div>
                </div>

                <div class="row" id="cancel-row">
                
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                                           <h4><?php echo $title;?></h4>
    <?php 
     if(isset($all_congrat) && !empty($all_congrat)){
        
    ?> 

                            <div class="table-responsive mb-4 mt-4">
                                <table id="default-ordering" class="table table-hover" style="width:100%">
                                            <thead>
                                               <thead>
			 <th>م</th>
        <th>نوع الرسالة</th>
        <th>الاسم</th>
        <th>الأشخاص</th>
        <th>التاريخ</th>
        <th>ملاحظة</th>
        <th></th>
			</thead>
                                       <tbody>
   <?php $i=1; foreach ($all_congrat as $row ){ ?>
    <?php 
   $add_date=$row->occ_date;
  
     $add_time=$row->for_timepicker;
   $congrat_date=$add_date.' '.$add_time;
     date_default_timezone_set('Asia/Kuwait');
    $current = strtotime(date('Y/m/d H:i'));
   $date    = strtotime($congrat_date);
   $datediff = $date - $current;

   $difference = floor($datediff/(60*60*24));
 if($difference <0)
 {?>
        <tr>
     
  

        <td> <?php echo $i;?> </td>
            
        <?php if( $row->occ_type==0){ 
               $type='عرس';
        } else if( $row->occ_type==1){ 
 $type='حفل تخرج';
 } else if( $row->occ_type==2){ 
                    $type='عزاء';
 } else if( $row->occ_type==3){ 
   $type='عودة من السفر مرافق مريض';
 } else if( $row->occ_type==4){ 
     $type='عشاء عودة من علاج';
 } else if( $row->occ_type==5){ 
  $type='زيارة مريض';
 } else if( $row->occ_type==6){ 
  $type='عشاء تمايم';
 } else if( $row->occ_type==7){ 
  $type='عشاء منزل جديد';
 }else if( $row->occ_type==8){ 
  $type='حج';
 }else if( $row->occ_type==9){
                    $type='وظيفة';
                }else if( $row->occ_type==10){
                    $type='افتتاح مشروع';
                }else if( $row->occ_type==11){
                    $type='حادث تصادم';
                }else if( $row->occ_type==12){
                    $type='نجاح بالانتخابات';
                }else if( $row->occ_type==13){
                    $type='عودة من سفر';
                }else if( $row->occ_type==14){
                    $type='ترقية';
                }else if( $row->occ_type==15){
                    $type='عقد قران';
                }else if( $row->occ_type==16){
                    $type='عودة من عمرة';
                }?> 
        <td><?php echo $type;?></td>
        <td><?php echo $row->name;?></td>
        <td> <?php
 $this->db->select('congrat_phones.phone_id,phone_book.name,phone_book.phone1,phone_book.phone2');
        $this->db->from("congrat_phones");
        $this->db->join('phone_book' , 'phone_book.id = congrat_phones.phone_id',"inner");
        $this->db->where("congrat_phones.congrat_id",$row->id);
        $query = $this->db->get();
        $all_phones=$query->result() ;
         foreach($all_phones as $one_phone){
                    
                    $phone1=$one_phone->phone1;
                    $phone2=$one_phone->phone2;
                    $name=$one_phone->name;
                 if($phone2!=null){ echo $name.'-'.$phone1.'-'.$phone2.'<br/>';}
               else { echo $name.'-'.$phone1.'<br/>';}
                    }
             
        
?>
            </td>
        
        <td><?php echo $row->occ_date;?> <br/> <?php echo $row->for_timepicker;?></td>
        <td><?php echo $row->note;?></td>


        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       $up_url=base_url()."admin/dashboard/UpdateCongrat/".$row->id;
                       $delete_url=base_url()."admin/dashboard/DeleteCongrat/".$row->id;
                 ?>
                        
                           <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                        </ul>
                    </div>
        </td>
      </tr>
         <?php } ?>
     <?php  if($difference <0) { $i++;} ?>
   
    <?php } ?>
    </tbody>
                                        </table>

  
					</div>

 <?php } else{
   
    echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد تهانى!</strong> .
           </div>';
}  ?>
                                   
                                </div>
                            </div>
                        </div>
                    
                      
                    </div>

                </div>
          