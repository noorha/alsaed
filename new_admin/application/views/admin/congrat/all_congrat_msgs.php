        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="container">
                <div class="container">

                
                    <div class="row layout-top-spacing">
                        
                        <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">
                                    <div class="row">
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                            <h4><?php echo $title;?></h4>
                                           
                                        </div>                 
                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">
<?php 
     if(isset($all_congrat_msgs) && !empty($all_congrat_msgs)){
        
    ?> 
    	
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover mb-4">
                                            <thead>
                                                <tr>
                                                   	 			<thead>
			 <th>م</th>
     <th>نوع المناسبة</th>
        <th>الرسالة الخاصة بالمناسبة</th>
   
    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                          <?php $i=1; foreach ($all_congrat_msgs as $row ){ ?>
     <tr>
     
  

        <td> <?php echo $i;?> </td>
        <?php if( $row->type_id==0){ 
               $type='عرس';
        } else if( $row->type_id==1){ 
        $type='حفل تخرج';
  } else if( $row->type_id==2){ 
  $type='عزاء';
 } else if( $row->type_id==3){ 
  $type='عودة من السفر مرافق مريض';
 } else if( $row->type_id==4){ 
  $type='عشاء عودة من علاج';
 } else if( $row->type_id==5){ 
  $type='زيارة مريض';
 } else if( $row->type_id==6){ 
  $type='عشاء تمايم';
 } else if( $row->type_id==7){ 
  $type='عشاء منزل جديد';
 }else if( $row->type_id==8){ 
  $type='حج';
 }else if( $row->type_id==9){
     $type='وظيفة';
 }else if( $row->type_id==10){
     $type='افتتاح مشروع';
 }else if( $row->type_id==11){
     $type='حادث تصادم';
 }else if( $row->type_id==12){
     $type='نجاح بالانتخابات';
 }else if( $row->type_id==13){
     $type='عودة من سفر';
 }else if( $row->type_id==14){
     $type='ترقية';
 }else if( $row->type_id==15){
     $type='عقد قران';
 }else if( $row->type_id==16){
     $type='عودة من عمرة';
 }?>   
        <?php $gender=''; 
      if($row->gender !=null) { 
         if($row->gender ==0 && $row->type_id==2){
        $gender='ذكر';
         } else if($row->gender ==1 && $row->type_id==2){
        $gender='أنثى';
         } else if($row->gender ==2 && $row->type_id==2){
             $gender='طفل';
         }
         else if($row->gender ==3 && $row->type_id==2){
             $gender='طفلة';
         }
         
         else if($row->gender ==0 && $row->type_id==6){
             $gender='مولود';
         }
         else if($row->gender ==1 && $row->type_id==6){
             $gender='مولوده';
         }
     ?>
     <td><?php echo $type.'('.$gender.')';?>   </td>
    <?php } else {?> 
     <td><?php echo $type;?>   </td>
    <?php } ?>
      
        <td><?php echo $row->text;?></td>
      
        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       $up_url=base_url()."admin/dashboard/UpdateCongratMsg/".$row->id;
                 ?>
                            <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a>
                          
                        </ul>
                    </div>
        </td>
      
      </tr>
     <?php $i++;?>
   
    <?php } ?>

                                            </tbody>
                                        </table>
                                    </div>
 <?php } else{
   
    echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد رسائل تهنئة!</strong> .
           </div>';
}  ?>
                                   
                                </div>
                            </div>
                        </div>
                    
                      
                    </div>

                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->
