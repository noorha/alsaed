<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	<h5>				
					<button class="btn btn-primary" style="background: #f1d4d4;">
					    <a  href="<?=base_url()."admin/dashboard/add_attends/"?>"  style="font-weight: bold;color:black;">اضافة تحضير</a>


  </button> </h5>
					
					<!-- /.dropdown js__dropdown -->
							<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>	    
    
<?php 
     if(isset($all_attends) && !empty($all_attends)){
        
    ?> 
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
    
        <th>رقم الكشف</th>
        <th>الأسم</th>
        <th>الفخذ</th>
       
        <th>الحالة</th>
        <th>السبب فى حالة الإلغاء</th>
        <th>وقت التحضير</th>

        <th> عمليات</th>
			</thead>
						<tfoot>
				 <th>م</th>
  
        <th>رقم الكشف</th>
        <th>الأسم</th>
        <th>الفخذ</th>
       
        <th>الحالة</th>
        <th>السبب فى حالة الإلغاء</th>
        <th>وقت التحضير</th>

        <th> عمليات</th>
				</tfoot>
					 <tbody>
   <?php $i=1; foreach ($all_attends as $row ){ ?>
     <tr>
     
        <td> <?php echo $i;?> </td>
        <td><?php echo $row->v_num;?></td>
        <td><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname;?></td>
        <td><?php echo $row->thigh_name;?></td>
     <?php 
        if($row->attend==0){ ?>
        <td>ملغى</td>

      <?php  } else if($row->attend==1){ ?>
        <td>حضور</td>

      <?php } ?>
        <td>
      <?php  if($row->reason1!=null){ ?> 
      <?php echo $row->reason1; } else { ?>
<?php echo '';?>
      <?php } ?> 
        </td>
        <td><?php echo $row->add_time;?></td>
        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                    
                    $up_url=base_url()."admin/dashboard/UpdateAttend/".$row->id.'/'.$row->kashf_num;
                       $delete_url=base_url()."admin/dashboard/DeleteAttend/".$row->id;
                 ?>
              
              <li><a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a></li>

                            <li><a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a></li>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
    </tbody>
					</table>
					</div>
	 <?php } else{
   
   echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد تحضير!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	