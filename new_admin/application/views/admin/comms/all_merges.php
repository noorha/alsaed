	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    
<!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                
                <div class="page-header">
                    <div class="page-title">

                        
                    </div>
                </div>

                <div class="row" id="cancel-row">
                
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                                           <h4><?php echo $title;?></h4>
     
   <?php 
     if(isset($all_voters) && !empty($all_voters)){
        
    ?> 

                            <div class="table-responsive mb-4 mt-4">
                                <table id="default-ordering" class="table table-hover" style="width:100%">
                                            <thead>
                                               <thead>
			 <th>م</th>
      <th>الأسم</th>
        <th>العمر</th> 
        <th>الهواتف</th>
        <th></th>
			</thead>
                                            <tbody>
                 <?php $i=1; foreach ($all_voters as $row ){ ?>
   
 <tr>
     

        <td> <?php echo $i;?> </td>
       
        <td><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname.' '.$row->fvname.' '.$row->thigh_name;?></td>
        <td> <?php 
        $dateOfBirth = "1-"."1-".$row->birth_date;
        $today = date("Y-m-d");
        $diff = date_diff(date_create($dateOfBirth), date_create($today));
        echo $diff->format('%y');
        $delete_url=base_url()."admin/dashboard/DeleteMerge/".$row->id.'/'.$row->main_voter_id;

        ?>
        </td>

        <td><?php echo $row->phone;?> <br/> <?php echo $row->phone2;?></td>
    
      <td>  <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a></td>
      
      </tr>
     <?php $i++;?>
   
    <?php } ?>
                                            </tbody>
                                        </table>

  
					</div>

 <?php } else{
   
    echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد مدمجين!</strong> .
           </div>';
}  ?>
                                   
                                </div>
                            </div>
                        </div>
                    
                      
                    </div>

                </div>
          