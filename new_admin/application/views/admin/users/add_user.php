<?php if(isset($result_id) && !empty($result_id) && $result_id!=null ):
    $out=$result_id;
    $out['form']='admin/Users/UpdateUser/'.$result_id['id'];
    $out['input']='UPDTATE';
    $out['requierd']='';
    $out['model']='';
    $out['input_title']='تعديل ';
    $out['salon_user_id_fk']='';
    $out['span']='لعدم تغير الصوره لا تختار شئ  ';
else:
    foreach ($out_fild as $key=>$value){
        $out[$value]='';
    }
    $out['requierd']='data-validation="required" ';
    $out['form']='admin/Users/User';
    $out['input']='INSERT';
    $out['model']='-add';
    $out['input_title']='حفظ ';
    $out['span']=' ';
endif?>

<div class="card card-shadow  card-outline-primary">
    <div class="card-header">
        <h4 class="m-b-0 text-white"><?php echo $title?></h4>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <div class="card-block">
        <?php echo form_open_multipart($out['form'])?>

        <div class="col-sm-12 row form-group">
              <div class=" col-sm-6">
                    <label class="label label-green  hquart" >الصورة   * </label>
                    <div class="input-group  hthreequart">
                        <input type="file" name="image"    class="form-control hthreequart " <?=$out['requierd']?>  >
                        <div class="input-group-addon" title="مشاهدة" data-toggle="modal" data-target="#myModal-1<?=$out['model']?>"><i class="fa fa-eye " ></i></div>
                    </div>
                    <small style="color: red;"><?=$out['span']?></small>
                </div>
                 <?php if(isset($out['image']) && !empty($out['image']) && $out['image']!=null ): ?>
                    <div class=" col-sm-6 col-xs-12">
                        <img src="<?php echo base_url()."voters/uploads/".$out['image']?>" class="img-circle" alt="User Image" width="50" height="50"/>
                    </div>
        <?php endif; ?>
         </div> 

        <div class="col-sm-12 row">
            <div class="form-group col-sm-4">
                <label class="label label-green  half">اسم المستخدم </label>
                <input type="text" name="data_post[username]" value="<?php echo $out['username']; ?>"  class="form-control half " placeholder="اسم المستخدم" data-validation="required">
            </div>
            
            <div class="form-group col-sm-4">
                <label class="label label-green  half">الاسم الاول </label>
                <input type="text" name="data_post[f_name]" value="<?php echo $out['f_name']; ?>"  class="form-control half " placeholder="الاسم الاول" data-validation="required">
            </div>

            <div class="form-group col-sm-4">
                <label class="label label-green  half">الاسم الاخير </label>
                <input type="text" name="data_post[l_name]" value="<?php echo $out['l_name']; ?>"  class="form-control half " placeholder="الاسم الاخير" data-validation="required">
            </div>
</div>

<div class="col-sm-12 row">
             <div class="form-group col-sm-6">
                <label class="label label-green  half">رقم الهاتف </label>
                <input type="text" name="data_post[mobile]"    value="<?php echo $out['mobile']; ?>"
                     onkeypress="validate_number(event);" class="form-control half unique-field" 
                      field-name="mobile" data-db="users"placeholder="أدخل البيانات" >
                 <span style="color: red;" id="span_mobile"> </span>
            </div>


        </div>
        <div class="col-sm-12 row">
            <?php if((isset($result_id)) && !empty($result_id) && $result_id != null): ?>
                <div class="form-group col-sm-6">
                    <label class="label label-green  half"> كلمة المرور</label>
                    <button type="button" class="btn dropdown-toggle btn-info" data-toggle="modal" data-target="#modal-update-pass">تغير كلمة المرور <i class="fa fa-pencil"></i></button>
                </div>
              
            <?php else: ?>
                <div class="form-group col-sm-6">
                    <label class="label label-green  half">كلمة المرور</label>
                    <input type="password" name="data_post[password]" id="password" onkeyup="return valid();"  data-validation="required" value="<?php echo $out['password']; ?>" class="form-control half  " placeholder="********" >
                    <span  id="validate1" class="help-block"></span>
                </div>
                <div class="form-group col-sm-6">
                    <label class="label label-green  half">تأكيد كلمة المرور</label>
                    <input type="password" onkeyup="return valid2();"  id="password_validate" data-validation="required" value="" class="form-control half  " placeholder="***********" >
                    <span  id="validate" class="help-block"></span>
                </div>
            <?php endif; ?>
        </div>
        
        
        <div class="col-sm-12 row form-group">
            
            <div class=" col-sm-6 ">
                <label class="label label-green  half">الدولة  </label>
                <select  name="data_post[country_id]"  class="selectpicker form-control half" data-validation="required" aria-required="true"  data-show-subtext="true" data-live-search="true">
                    <option value="">    إختر   </option>
                    <?php if(isset($all_country) && !empty($all_country) && $all_country!=null){
                        foreach ($all_country as $one_country):
                            $select="";
                            if($one_country->id == $out['country_id']){$select='selected="selected"';}?>
                            <option  value="<?php echo $one_country->id?>" <?php echo  $select?>>
                                <?php echo $one_country->name_ar?></option>
                        <?php endforeach;
                    }?>
                </select>
            </div>
            <div class=" col-sm-6">
                <label class="label label-green  half">المدينة </label>
                <select  name="data_post[city_id]"  class="selectpicker form-control half" data-validation="required" aria-required="true" data-show-subtext="true" data-live-search="true"  >
                    <option value=""> إختر     </option>
                    <?php if(isset($all_city) && !empty($all_city) && $all_city!=null){
                        foreach ($all_city as $one):
                            $select="";
                            if($one->id == $out['city_id']){$select='selected="selected"';}?>
                            <option  value="<?php echo $one->id?>" <?php echo  $select?>>
                                <?php echo $one->name_ar?></option>
                        <?php endforeach;
                    }?>
                </select>
            </div>
        </div>
      
     
        
        <div class="col-sm-12 row">
            <div class="form-group col-sm-6">
                    <label class="label label-green  half">البريد الالكترونى   </label>
                    <input type="text" name="data_post[email]" value="<?php echo $out['email']; ?>"  class="form-control half " placeholder="البريد الالكترونى" data-validation="required">
            </div>
            <div class="form-group col-sm-6">
                    <label class="label label-green  half">العنوان  </label>
                    <input type="text" name="data_post[address]" value="<?php echo $out['address']; ?>"  class="form-control half " placeholder="العنوان" data-validation="required">
            </div>
                
        </div>
    
     <!--  <div class="col-md-12 ">
            <div class="form-group row">
                <label class="control-label">الموقع على الخريطة  </label>
                <input type="hidden" name="user_google_long" id="lng" value="<?php  echo $out['user_google_long'] ?>" />
                <input type="hidden" name="user_google_lat" id="lat" value="<?php  echo $out['user_google_lat'] ?>" />
                <?php  echo $maps['html']?>
            </div>
        </div> -->
        <div class="col-xs-12 ">
            <button  type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"  class="btn btn-success w-md m-b-5">
                <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?></button>
        </div>
        <?php echo form_close()?>

    </div>
</div>

<!--------------------------------------------------------------------->
<!--
<div class="modal fade " id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myLargeModalLabel"> الصورة الرئيسية  </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

                <div class="col-sm-12 form-group ">
                   
                <img src="<?php echo base_url().ImagePath.$out['user_photo']?>" class="center-block" width="400" height="400" >
       
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">إغلاق</button>
            </div>
        </div>
      
    </div>
   
</div> -->   
<!--------------------------------------------------------------------->



  <div class="modal" id="modal-update-pass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-wow-duration="0.5s">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title">تعديل بيانات كلمة المرور : <?php echo $out['user_name'] ;?></h1>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                            </div>
                            <div class="modal-body col-sm-12">
                                <div class="col-sm-12">
                                    <div class="form-group col-sm-6">
                                        <label class="control-label   ">كلمة المرور</label>
                                        <input type="password" name="user_pass" id="user_pass" onkeyup="return valid();"    class="form-control " placeholder="أدخل كلمة المرور" >
                                        <span  id="validate1" class="help-block"></span>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label class="control-label   ">تأكيد كلمة المرور</label>
                                        <input type="password" name="user_pass_validate" onkeyup="return valid2();"  id="user_pass_validate"  class="form-control " placeholder="أدخل كلمة المرور" >
                                        <span  id="validate" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">إغلاق</button>
                                <button type="submit" name="update_pw" value="update_pw" class="btn btn-success">تعديل</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>


<script>


    function valid()
    {
        if($("#password").val().length > 5){
            document.getElementById('validate1').style.color = '#00FF00';
            document.getElementById('validate1').innerHTML = 'كلمة المرور قوية';
        }else{
            document.getElementById('validate1').style.color = '#F00';
            document.getElementById('validate1').innerHTML = 'كلمة المرور ضعيفة';
        }
    }
    function valid2()
    {
        if($("#password").val() == $("#password_validate").val()){
            document.getElementById('validate').style.color = '#00FF00';
            document.getElementById('validate').innerHTML = 'كلمة المرور متطابقة';
        }else{
            document.getElementById('validate').style.color = '#F00';
            document.getElementById('validate').innerHTML = 'كلمة المرور غير متطابقة';
        }
    }

</script>