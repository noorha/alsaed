<?php
  class Dashboard extends MY_Controller{
    public   function __construct(){
        parent::__construct();
    }
  /**
 *  ================================================================================================================
 * 
 *  ----------------------------------------------------------------------------------------------------------------
 * 
 *  ================================================================================================================
 */

    public function  index(){
   
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/index_empty';
        $this->load->view('admin_index', $data);
    }
    public function  messages(){
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "الرسائل الخاصة";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/voters/all_msg';
        $this->load->view('admin_index', $data);
    } 
    public function  voters(){
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "الناخبين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/voters/all_voters';
        $this->load->view('admin_index', $data);
    }
    public function  attends(){
        $this->load->model('admin/Model_attendance');
        $data["all_attends"]=$this->Model_attendance->get_all_attends();
    //  print_r($data);exit();
        $title = "التحضير";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/all_attends';
        $this->load->view('admin_index', $data);
    }
    public function  thighs(){
   
        $this->load->model('admin/Model_thighs');
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
        $title = "الفخوذ";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/thighs/all_thighs';
        $this->load->view('admin_index', $data);
    }
    public function  awards(){
   
        $this->load->model('admin/Model_competitor');
        $data["all_awards"]=$this->Model_competitor->get_all_awards();
        $title = "الجوائز";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/award/all_awards';
        $this->load->view('admin_index', $data);
    }
    public function  not_attends($id){
   
        $this->load->model('admin/Model_voters');
      
        $data["all_not_attends2"]=$this->Model_voters->get_all_not_attends2($id);
        $data["all_not_attends"]=$this->Model_voters->get_all_not_attends($id);
        
        $title = "عدم الحضور";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/not_attend';
        $this->load->view('admin_index', $data);
    } 
    public function  cancel($id){
   
        $this->load->model('admin/Model_voters');

        $data["all_cancel"]=$this->Model_voters->get_all_cancel($id);
        
        $title = "ملغى";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/all_cancel';
        $this->load->view('admin_index', $data);
    } 
    
    public function  friends_statistics(){
   
        $this->load->model('admin/Model_voters');
        $data["all_statistics"]=$this->Model_voters->get_all_statistics();
     
        $title = "أحصائيات الأصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_statistics';
        $this->load->view('admin_index', $data);
    }
    public function  sure(){
   
        $this->load->model('admin/Model_voters');
   
        $data["friends"]=$this->Model_voters->get_status_friends('0');
        
        $title = "أصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
    public function  not_sure(){
   
        $this->load->model('admin/Model_voters');
    
        $data["friends"]=$this->Model_voters->get_status_friends('1');
        
        $title = "أصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
    public function  maybe(){
   
        $this->load->model('admin/Model_voters');
     
        $data["friends"]=$this->Model_voters->get_status_friends('2');
        
        $title = "أصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
    public function  friends(){
   
        $this->load->model('admin/Model_voters');
        $set_data = $this->session->all_userdata();
        $user_id=$set_data['user_id'];
        $data["friends"]=$this->Model_voters->get_my_friends($user_id);
        
        $title = "أصدقائى";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
    public function  all_friends(){
   
        $this->load->model('admin/Model_voters');

        $data["friends"]=$this->Model_voters->get_all_friends();
        
        $title = "كل الأصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
       
    public function  all_friend_attend(){
   
        $this->load->model('admin/Model_voters');

        $data["all_attends"]=$this->Model_voters->get_all_friend_attends();
        
        $title = "الحاضرين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/attend';
        $this->load->view('admin_index', $data);
    }
    public function  all_attends($id){
   
        $this->load->model('admin/Model_voters');

        $data["all_attends"]=$this->Model_voters->get_all_attends($id);
        
        $title = "الحضور";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/attend';
        $this->load->view('admin_index', $data);
    }
    
    public function  count_not_attend(){
   
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');

        $data["all_statistics"]=$this->Model_voters->get_all_statistics();
        
        $title = "عدم حضور";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/statistics/count_not_attend';
        $this->load->view('admin_index', $data);
    }
    
    public function  attend_friends(){
   
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');

        $data["all_notAttend_friends"]=$this->Model_voters->get_all_notAttend_friends();
        $data["all_notAttend_friends2"]=$this->Model_voters->get_all_notAttend_friends2();
     //   $merged_arr = array_merge_recursive($all_notAttend_friends,$all_notAttend_friends2);
     //   $data["all_notAttend_friends"]=$merged_arr;
      //  print_r( $all_notAttend_friends2);
     //   print_r( $data["all_notAttend_friends2"]);
      // exit();
        $title = "تحضير الأصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/attend_friends';
        $this->load->view('admin_index', $data);
    }
    public function  jobs(){
   
        $this->load->model('admin/Model_jobs');
        $data["all_jobs"]=$this->Model_jobs->get_all_jobs();

        $title = "المهن";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/jobs/all_jobs';
        $this->load->view('admin_index', $data);
    }
    public function  statistics(){
   
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');

        $data["all_statistics"]=$this->Model_voters->get_all_statistics();
        
        $title = "الاحصائيات";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/statistics/all_statistics';
        $this->load->view('admin_index', $data);
    }
    public function add_jobs(){  // AppClient/User
        $this->load->model('admin/Model_jobs');
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_jobs->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/jobs", 'refresh');
        }
        $data["out_fild"] = $this->Model_jobs->get_field();
       
         $data['title']="اضافة مهنة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/jobs/add_jobs';
        $this->load->view('admin_index', $data);
    }
    public function add_thighs(){  // AppClient/User
        $this->load->model('admin/Model_thighs');
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_thighs->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/thighs", 'refresh');
        }
        $data["out_fild"] = $this->Model_thighs->get_field();
       
         $data['title']="اضافة فخذ";
        $data['metakeyword']="اضافة فخذ";
        $data['metadiscription']="اضافة فخذ";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/thighs/add_thighs';
        $this->load->view('admin_index', $data);
    }
    public function add_friend_with_voter($id){  // AppClient/User
        $this->load->model('admin/Model_voters');

        if ($this->input->post('INSERT') == "INSERT") {
           // print_r($Idata);exit();
            $Idata = $this->input->post('data_post');
            $set_data = $this->session->all_userdata();
            $Idata['user_id']=$set_data['user_id'];
            $this->Model_voters->insert_friend($Idata);

            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/friends", 'refresh');
        }
        $data["out_fild"] = $this->Model_voters->get_field();
        $voter_details= $this->Model_voters->fetch_voter_withId($id);

        $data['voter_details']=$voter_details;
                 $data['title']="اضافة صديق";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/friends/add_friend';
        $this->load->view('admin_index', $data);
    }    
    public function add_friend(){  // AppClient/User
        $this->load->model('admin/Model_voters');

     
        $data["out_fild"] = $this->Model_voters->get_field();
        $data["all_voters"]=$this->Model_voters->get_all_voters();
         $data['title']="اضافة صديق";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_voters';
        $this->load->view('admin_index', $data);
    }
    public function add_voters(){  // AppClient/User
        $this->load->model('admin/Model_voters');
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_jobs');
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_voters->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/add_voters", 'refresh');
        }
        $data["out_fild"] = $this->Model_voters->get_field();
        $data["all_jobs"]=$this->Model_jobs->get_all_jobs();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
         $data['title']="اضافة ناخب";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_voter';
        $this->load->view('admin_index', $data);
    }

    public function   add_msgs($voter_id){
        $this->load->model('admin/Model_voters');
       
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            
            $this->Model_voters->update($voter_id,$Idata);

            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/messages", 'refresh');
        }



        $voter_details= $this->Model_voters->fetch_voter_withId($voter_id);
        $data['voter_details']=$voter_details;
        $data["out_fild"] = $this->Model_voters->get_field();
        $data["all_voters"]=$this->Model_voters->get_all_voters();
         $data['title']="اضافة رسالة خاصة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_msg';
     //   print_r($data);exit();
        $this->load->view('admin_index', $data);
    }
    public function fetch_with_num ()

    {  
        $this->load->model('admin/Model_attendance');

    $this->load->model('admin/Model_voters');
    $Idata = $this->input->post('data_post');

     $voter_details= $this->Model_voters->fetch_voter($Idata['kashf_num']);

     $data['voter_details']=$voter_details;
     $data["out_fild"] = $this->Model_attendance->get_field();
     $data["all_voters"]=$this->Model_voters->get_all_voters();
      $data['title']="اضافة تحضير";
     $data['metakeyword']= $data['title'];
     $data['metadiscription']= $data['title'];
     $data["my_footer"]=array("validator","live_select");
     $data['subview'] = 'admin/attends/add_attend_with_num';
  //   print_r($data);exit();
     $this->load->view('admin_index', $data);

    // print_r($voter_details);exit();
} 
  
public function UpdateAttend($attend_id,$kashf_num){  // AppClient/UpdateUser/
       
    $this->load->model('admin/Model_attendance');
    $this->load->model('admin/Model_voters'); 

   if ($this->input->post('UPDTATE') == "UPDTATE") {
        $Udata = $this->input->post('data_post');
        if($Udata['attend']==1){
            $Udata['reason1']=null;
        }
       else if ($Udata['attend']==0){ $Udata['reason1']= $this->input->post('reason1');
       }
        $Udata['add_time']=date('h:i:s');
        $voter_details= $this->Model_voters->fetch_voter($Udata['kashf_num']);

              
                $this->add_msg_to_voter($voter_details[0]->phone_msg,$voter_details[0]->phone);
               $this->add_msg_to_voter($voter_details[0]->phone_msg,$voter_details[0]->phone2);
             
         $this->Model_attendance->update($attend_id, $Udata);
        //-----------------------------------------------
        $this->message('info', 'تم التعديل بنجاح');
        redirect("admin/dashboard/attends");
    }
  
    $data["result_id"] = $this->Model_attendance->getByArray(array("id" => $attend_id));
//   print_r( $data["result_id"]); exit();
 $this->load->model('admin/Model_voters');

  $voter_details= $this->Model_voters->fetch_voter($kashf_num);

  $data['voter_details']=$voter_details;
    $data['title']="تعديل تحضير";
    $data['metakeyword']=$data['title'];
    $data['metadiscription']=$data['title'];
    $data["my_footer"]=array("validator","live_select");
    $data['subview'] = 'admin/attends/add_attend_with_num';
    $this->load->view('admin_index', $data);
}
public function UpdateFriend($id){  // AppClient/UpdateUser/
       
    $this->load->model('admin/Model_voters'); 

   if ($this->input->post('UPDTATE') == "UPDTATE") {
        $Udata = $this->input->post('data_post');
    
        $this->Model_voters->update_friend($id, $Udata);
        //-----------------------------------------------
        $this->message('info', 'تم التعديل بنجاح');
        redirect("admin/dashboard/friends");
    }
  
    $data["result_id"] = $this->Model_voters->getByArray_friend(array("id" => $id));

    $data["all_voters"]=$this->Model_voters->get_all_voters();

    $data['title']="تعديل صديق";
    $data['metakeyword']=$data['title'];
    $data['metadiscription']=$data['title'];
    $data["my_footer"]=array("validator","live_select");
    $data['subview'] = 'admin/friends/add_friend';
    $this->load->view('admin_index', $data);
}    
public function fetch_voter ()

    {

    $this->load->model('admin/Model_voters');

     $voter_details= $this->Model_voters->fetch_voter($this->input->post('kashf_num'));
if($voter_details !=null){
  $html_str='';

  

     $fname=$voter_details[0]->fname;

     $sname=$voter_details[0]->sname;

     $tname=$voter_details[0]->tname;

     $foname=$voter_details[0]->foname;
     $phone=$voter_details[0]->phone;
     $phone2=$voter_details[0]->phone2;
     $kashf_num=$voter_details[0]->kashf_num;
     $thigh_name=$voter_details[0]->thigh_name;


     $html_str.=" <label>الأسم: ".$fname." ".$sname." ".$tname." ".$fname." ".$thigh_name."</label>"."<br/>";
    $html_str.=" <label> الهواتف : ".$phone." , ".$phone2."</label>"."<br/>";
    $html_str.=" <label> رقم الكشف : ".$kashf_num."</label>"."<br/>";


   

    }
    else {
        $html_str.=" <label>"."لا يوجد ناخبين لهذا الرقم"."</label>"."<br/>";

    }
    echo $html_str;
    }
    public function add_occ(){  // AppClient/User
        $this->load->model('admin/Model_occ');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_occ->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/occasions", 'refresh');
        }
        $data["out_fild"] = $this->Model_occ->get_field();
         $data['title']="اضافة مناسبة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/occasions/add_occ';
        $this->load->view('admin_index', $data);
    } 
    public function add_diwan(){  // AppClient/User
        $this->load->model('admin/Model_diwans');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_diwans->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/diwans", 'refresh');
        }
        $data["out_fild"] = $this->Model_diwans->get_field();
        $this->load->model('admin/Model_areas');
        $data["all_areas"]=$this->Model_areas->get_all_areas();
         $data['title']="اضافة دوانية";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/diwans/add_diwan';
        $this->load->view('admin_index', $data);
    }  
    
    public function  occasions(){
   
        $this->load->model('admin/Model_occ');

        $data["all_occ"]=$this->Model_occ->get_all_occ();
        $title = "المناسبات";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/occasions/all_occ';
        $this->load->view('admin_index', $data);
    }
    public function  search_result(){
         $area=$this->input->post('area');
         $diwan_type=$this->input->post('diwan_types');
        $this->load->model('admin/Model_diwans');
        $this->load->model('admin/Model_areas');
        $page=$this->input->post('page');

         $data['page']=$page;
        $data["all_diwans"]=$this->Model_diwans->get_all_search_result($diwan_type,$area);
        $data["all_areas"]=$this->Model_areas->get_all_areas();
          $data['area_result']=$area;
          $data['type_result']=$diwan_type;
        $title = "نتائج البحث";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/diwans/all_diwans';
        $this->load->view('admin_index', $data);
    }
    
    public function  diwans($diwan_type){
         
        
        $this->load->model('admin/Model_diwans');
        $this->load->model('admin/Model_areas');

            if ($diwan_type==null){
                $diwan_type='all';
                $page='all_d';
            }
        $data["all_diwans"]=$this->Model_diwans->get_all_diwans($diwan_type);
        $data["all_areas"]=$this->Model_areas->get_all_areas();
            $data['d_type']=$diwan_type;
            $data['page']=$page;
        $title = "الدوانيات";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/diwans/all_diwans';
        $this->load->view('admin_index', $data);
    }
    
    public function  friend_notes($voter_id){
   
        $this->load->model('admin/Model_voters');

        $data["all_notes"]=$this->Model_voters->get_all_friend_notes($voter_id);
        $data['voter_id']=$voter_id;
        $title = "ملاحظات الصديق";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/notes';
        $this->load->view('admin_index', $data);
    }
    public function  notes($voter_id){
   
        $this->load->model('admin/Model_voters');

        $data["all_notes"]=$this->Model_voters->get_all_notes($voter_id);
        $data['voter_id']=$voter_id;
        $title = "ملاحظات الناخب";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/voters/notes';
        $this->load->view('admin_index', $data);
    }
    public function Add_note($voter_id){  // AppClient/User
        $this->load->model('admin/Model_voters');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_voters->insert_notes($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/notes/".$Idata['voter_id'], 'refresh');
        }
        $data['voter_id']=$voter_id;
         $data['title']="اضافة ملاحظة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_note';
        $this->load->view('admin_index', $data);
    }
    public function add_msg_to_voter($phone_msg,$phone){

        $url="http://62.150.26.41/SmsWebService.asmx/send";
           
          
        $params = array(
                'username' => 'azizalsaid1',
                'password' => 'plkmnbcg',
                'token' => 'plomnzbvfrtaghxvdrajhgfx',
                'sender' => 'azizalsaid',
                'message' =>$phone_msg ,
                'dst' => $phone,
                'type' => 'text',
                'coding' => 'unicode', 
                'datetime' => 'now'
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);

// This should be the default Content-type for POST requests
//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));

$result = curl_exec($ch);
if(curl_errno($ch) !== 0) {
    //error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
}

curl_close($ch);  


    }
  public function  send_voter_msgs($msg_num){
    $this->load->model('admin/Model_voters'); 
    $all_voters= $this->Model_voters->get_all_voters();
   foreach ($all_voters as $row ){ 
   
    if($msg_num=='msg1'){
        $this->add_msg_to_voter($row->msg1,$row->phone);
        $this->add_msg_to_voter($row->msg1,$row->phone2);
        }
        else  if($msg_num=='msg2'){
            $this->add_msg_to_voter($row->msg2,$row->phone);
            $this->add_msg_to_voter($row->msg2,$row->phone2);
            }
        else  if($msg_num=='msg3'){
            $this->add_msg_to_voter($row->msg3,$row->phone);
            $this->add_msg_to_voter($row->msg3,$row->phone2);
                }

    }

    
    $this->message('success', 'تم ارسال الرسالة بنجاح');
    redirect("admin/dashboard/messages");
    }
    public function add_attends(){  // AppClient/User
        $this->load->model('admin/Model_voters');
        $this->load->model('admin/Model_attendance');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $Idata['reason1']= $this->input->post('reason1');
            $Idata['add_time']=date('h:i:s');
            $this->Model_attendance->insert($Idata);
            
            $kashf_num=$Idata['kashf_num'];
            $this->load->model('admin/Model_voters'); 
            $voter_details= $this->Model_voters->fetch_voter($Idata['kashf_num']);

                  
                    $this->add_msg_to_voter($voter_details[0]->phone_msg,$voter_details[0]->phone);
                   $this->add_msg_to_voter($voter_details[0]->phone_msg,$voter_details[0]->phone2);
                  

            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/add_attends", 'refresh');
        }
        $data["out_fild"] = $this->Model_attendance->get_field();
        $data["all_voters"]=$this->Model_voters->get_all_voters();
         $data['title']="اضافة تحضير";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/attends/add_attend';
        $this->load->view('admin_index', $data);
    }
    
    public function UpdateNotes($note_id,$voter_id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_voters');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_voters->update_note($note_id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/notes/".$voter_id);
        }
      
        $data["result_id"] = $this->Model_voters->getByArray_note(array("id" => $note_id));
     //   print_r( $data["result_id"]); exit();
   
         $data['voter_id']=$voter_id;
        $data['title']="تعديل ملاحظة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_note';
        $this->load->view('admin_index', $data);
    }
    public function UpdateOcc($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_occ');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_occ->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/occasions/");
        }
      
        $data["result_id"] = $this->Model_occ->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل مناسبة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/occasions/add_occ';
        $this->load->view('admin_index', $data);
    }
    public function UpdateDiwan($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_diwans');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_diwans->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/diwans/");
        }
        $this->load->model('admin/Model_areas');
        $data["all_areas"]=$this->Model_areas->get_all_areas();

        $data["result_id"] = $this->Model_diwans->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل ديوانية";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/diwans/add_diwan';
        $this->load->view('admin_index', $data);
    }
    public function UpdateVoters($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_voters');
       
        if ($this->input->post('update_msg')) {
            $this->Model_voters->update_msg($id);
            $this->message('info','تم إضافة الرسالة بنجاح');
            redirect('admin/dashboard/messages', 'refresh');
        }

       if ($this->input->post('UPDTATE') == "UPDTATE") {
        //   print_r();exit();
        
  
        $Udata = $this->input->post('data_post');

             $this->Model_voters->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/voters", 'refresh');
        }
      
        $data["result_id"] = $this->Model_voters->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
     $this->load->model('admin/Model_thighs');
     $this->load->model('admin/Model_jobs');

     $data["all_jobs"]=$this->Model_jobs->get_all_jobs();

     $data["all_thighs"]=$this->Model_thighs->get_all_thighs();

        $data['title']="تعديل ناخب";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_voter';
        $this->load->view('admin_index', $data);
    }
    public function UpdateJobs($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_jobs');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_jobs->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/jobs", 'refresh');
        }
      
        $data["result_id"] = $this->Model_jobs->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل مهنة";
        $data['metakeyword']="تعديل مهنة";
        $data['metadiscription']="تعديل مهنة";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/jobs/add_jobs';
        $this->load->view('admin_index', $data);
    }
    public function UpdateThighs($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_thighs');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Idata = $this->input->post('data_post');

            $Udata["name"]=$Idata["name"];
            $Udata["count"]=$Idata["count"];
             $this->Model_thighs->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/thighs", 'refresh');
        }
      
        $data["result_id"] = $this->Model_thighs->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل فخذ";
        $data['metakeyword']="تعديل فخذ";
        $data['metadiscription']="تعديل فخذ";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/thighs/add_thighs';
        $this->load->view('admin_index', $data);
    }
    public function UpdateAward($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_award');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Idata = $this->input->post('data_post');

            $Udata["name"]=$Idata["name"];
            $Udata["descr"]=$Idata["descr"];
             $this->Model_award->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/awards", 'refresh');
        }
      
        $data["result_id"] = $this->Model_award->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل جائزة";
        $data['metakeyword']="تعديل جائزة";
        $data['metadiscription']="تعديل جائزة";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/award/add_award';
        $this->load->view('admin_index', $data);
    }
    
    public function DeleteAttend($id){
        $this->load->model('admin/Model_attendance');
        $this->Model_attendance->delete($id);
      
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/attends", 'refresh');
    }
    public function DeleteNotes($id,$voter_id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete_note($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/notes/".$voter_id);
    }
    public function DeleteVoters($id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/voters", 'refresh');
    }  
    public function DeleteFriend($id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete_friend($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/friends", 'refresh');
    }   
    public function DeleteDiwan($id){
        $this->load->model('admin/Model_diwans');
        $this->Model_diwans->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/diwans", 'refresh');
    }
    
    public function DeleteOcc($id){
        $this->load->model('admin/Model_occ');
        $this->Model_occ->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/occasions", 'refresh');
    }
    public function DeleteJobs($id){
        $this->load->model('admin/Model_jobs');
        //$Udata["user_show"] = 0;
        $this->Model_jobs->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/jobs", 'refresh');
    }
    public function DeleteThighs($id){
        $this->load->model('admin/Model_thighs');
        //$Udata["user_show"] = 0;
        $this->Model_thighs->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/thighs", 'refresh');
    }
    //------------------------------------------------------------------
    public function DeleteAward($id){
        $this->load->model('admin/Model_award');
        //$Udata["user_show"] = 0;
        $this->Model_award->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/awards", 'refresh');
    }
    public function add_area(){  // AppClient/User
        $this->load->model('admin/Model_areas');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_areas->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/areas", 'refresh');
        }
        $data["out_fild"] = $this->Model_areas->get_field();
         $data['title']="اضافة منطقة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/areas/add_area';
        $this->load->view('admin_index', $data);
    }
    public function UpdateArea($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_areas');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_areas->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/areas/");
        }
      
        $data["result_id"] = $this->Model_areas->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل منطقة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/areas/add_area';
        $this->load->view('admin_index', $data);
    }
    public function DeleteArea($id){
        $this->load->model('admin/Model_areas');
        //$Udata["user_show"] = 0;
        $this->Model_areas->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/areas", 'refresh');
    }
    public function  areas(){
   
        $this->load->model('admin/Model_areas');
        $data["all_areas"]=$this->Model_areas->get_all_areas();
        $title = "المناطق";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/areas/all_areas';
        $this->load->view('admin_index', $data);
    }
    public function  conditions(){
   
        $this->load->model('admin/Model_competitor');
        $data["all_conditions"]=$this->Model_competitor->get_all_conditions();
        $title = "الشروط";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/competitor/all_conditions';
        $this->load->view('admin_index', $data);
    }
    public function  targets(){
   
        $this->load->model('admin/Model_competitor');
        $data["all_targets"]=$this->Model_competitor->get_all_targets();
        $title = "الأهداف";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/competitor/all_targets';
        $this->load->view('admin_index', $data);
    }
    public function  refused_partners(){
   
        $this->load->model('admin/Model_competitor');
        $data["all_refused"]=$this->Model_competitor->get_all_refused();
        $title = "المرفوضين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/competitor/all_refused';
        $this->load->view('admin_index', $data);
    }
    function send_sms_accept($phone,$name)
    {
                
      
        $url="http://62.150.26.41/SmsWebService.asmx/send";
           
          
        $params = array(
                'username' => 'azizalsaid1',
                'password' => 'plkmnbcg',
                'token' => 'plomnzbvfrtaghxvdrajhgfx',
                'sender' => 'azizalsaid',
                'message' => 'تم قبولك في مسابقة القبيلة
                باسم / '.$name.'
                ونتمنى لك التوفيق',
                'dst' => $phone,
                'type' => 'text',
                'coding' => 'unicode', 
                'datetime' => 'now'
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);

// This should be the default Content-type for POST requests
//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));

$result = curl_exec($ch);

if(curl_errno($ch) !== 0) {
    //error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
}

curl_close($ch);
//print_r($result); 
                    

    }
    public function AcceptPresenter ($phone,$id,$name){
        $this->load->model('admin/Model_competitor');
        //$Udata["user_show"] = 0;
        $this->send_sms_accept($phone,$name);

        $this->Model_competitor->update_accept($id);
        $this->message('info', 'تم قبول المتقدم وتحوليه لصفحة المشاركين');
        redirect("admin/dashboard", 'refresh');
    }
    public function RefusedPresenter ($id){
        $this->load->model('admin/Model_competitor');
        //$Udata["user_show"] = 0;
        $this->Model_competitor->update_refuse($id);
        $this->message('info', 'تم رفض المتقدم وتحويله لصفحة المرفوضين');
        redirect("admin/dashboard", 'refresh');
    }
    public function DeletePresenter($id){
        $this->load->model('admin/Model_competitor');
        //$Udata["user_show"] = 0;
        $this->Model_competitor->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard", 'refresh');
    }
    public function  form(){

        $data['subview'] = 'backend/form';
        $this->load->view('admin_index', $data);
    }
/**
 * ===============================================================================================================
 * 
 * ========================             ALL FUNCTIONS            =================================================
 * 
 * ===============================================================================================================
 */
    //===================================================================================================================
    public  function GroupsPages(){  // Dashboard/GroupsPages
        $this->load->model('system_management/Groups');
        $this->load->model('Difined_model');
        if ($this->input->post('add_group')) {
            $file= $this->upload_image('page_image',"images");
            $this->Groups->addGroup($file);
            $this->message('success','تمت إضافة المجموعة بنجاح');
            redirect('admin/Dashboard/GroupsPages', 'refresh');
        }
        $data["font_icon"]=$this->Difined_model->select_limit("font_icons","","id","ASC");
        $data["last_order"]=$this->Groups->select_order(0);
        $data["groups_table"] = $this->Groups->fetch_groups('', '');
        $data['title'] = 'إضافة إدارات البرنامج';
        $data['metakeyword'] = 'إضافة إدارات البرنامج';
        $data['metadiscription'] = 'إضافة إدارات البرنامج';
        $data['subview'] = 'backend/groups_pages/group';
        $data["my_footer"]=array("edit_data_table","validator","live_select");
        $this->load->view('admin_index', $data);
    }
    public function UpdateGroupsPages($id){
        $this->load->model('system_management/Groups');
        $this->load->model('Difined_model');
        $data['result_id'] = $this->Groups->getgroupbyid($id);
        if ($this->input->post('edit_group')) {
            $file= $this->upload_image('page_image',"images");
            $this->Groups->updateGroup($id,$file);
            $this->message('info','تمت تعديل المجموعة بنجاح');
            redirect('admin/Dashboard/GroupsPages', 'refresh');
        }
        $data["font_icon"]=$this->Difined_model->select_limit("font_icons","","id","ASC");
        $data['title'] = 'تعديل بيانات الإدارة';
        $data['metakeyword'] = 'تعديل بيانات الإدارة';
        $data['metadiscription'] = '';
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'backend/groups_pages/group';
        $this->load->view('admin_index', $data);
    }
    /*  public function DeleteGroupsPages($id){
     $this->load->model('Difined_model');
     $this->Difined_model->delete("pages",array("page_id"=>$id));
    // $this->message('success','تم الحذف');
     redirect('Dashboard/GroupsPages', 'refresh');
     }
    */
    //===================================================================================================================
    public  function AddPages(){  // Dashboard/AddPages
        $this->load->model('system_management/Pages');
        $this->load->model('system_management/Groups');
        $this->load->model('Difined_model');
        if ($this->input->post('add_group')) {
            // $file= $this->upload_image('page_image');
            $file="0";
            $this->Pages->insert($file);
            $this->message('success','تمت إضافة الصفحة بنجاح');
            redirect('admin/Dashboard/AddPages', 'refresh');
        } if ($this->input->post('get_page_order')) {
            echo (1+$this->Groups->select_order($this->input->post('get_page_order')));
        }else{
            $data["font_icon"]=$this->Difined_model->select_limit("font_icons","","id","ASC");
            $data["groups_table"] = $this->Pages->all_pages('', '');
            $data['pages_name']=$this->Pages->main_groups_name();
            $data["main_groups"]=$this->Groups->level_groups();
            $data['title'] = ' بيانات صفحة تحكم';
            $data['metakeyword'] = 'اعدادات الموقع ';
            $data['metadiscription'] = '';
            $data["my_footer"]=array("edit_data_table","validator","live_select");
            $data['subview'] = 'backend/groups_pages/pages';
            $this->load->view('admin_index', $data);
        }
    }
    public function UpdatePages($id){
        $this->load->model('system_management/Pages');
        $this->load->model('system_management/Groups');
        $this->load->model('Difined_model');
        $data['result_id'] = $this->Pages->get_by_id($id);
        if ($this->input->post('edit_group')) {
            $file= $this->upload_image('page_image',"images");
            $this->Pages->update($id,$file);
            $this->message('info','تمت تعديل الصفحة بنجاح');
            redirect('admin/Dashboard/AddPages', 'refresh');
        }
        $data['pages_name']=$this->Pages->main_groups_name();
        $data["main_groups"]=$this->Groups->level_groups();
        $data["font_icon"]=$this->Difined_model->select_limit("font_icons","","id","ASC");
        $data['title'] = 'تعديل بيانات صفحة تحكم';
        $data['metakeyword'] = 'اعدادات الموقع ';
        $data['metadiscription'] = '';
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'backend/groups_pages/pages';
        $this->load->view('admin_index', $data);
    }
    public function DeletePages($id){
        $this->load->model('Difined_model');
        $this->Difined_model->delete("pages",array("page_id"=>$id));
        $this->message('error','تم الحذف');
        redirect('admin/Dashboard/AddPages', 'refresh');
    }
    //===================================================================================================================
    public function AddUsers(){  // Dashboard/AddUsers
        $this->load->model('system_management/User');
        if ($this->input->post('add_user')) {
            $file= $this->upload_image('user_photo');
            $this->User->insert($file);
            $this->message('success','تمت إضافة المستخدم بنجاح');
            redirect('admin/Dashboard/AddUsers', 'refresh');
        }
        $data["user_per"]=$this->User->select_user_per();
        $data["users_table"]=$this->User->all_users($_SESSION["role_id_fk"]);
        $data['title']='بيانات المستخدم ';
        $data['metakeyword']='بيانات المستخدم ';
        $data['metadiscription']='بيانات المستخدم ';
        $data["my_footer"]=array("edit_data_table","validator","live_select");
        $data['subview'] = 'backend/users/user';
        $this->load->view('admin_index', $data);
    }

    public function UpdateUsers($id){
        $this->load->model('system_management/User');
        $data['result_id'] = $this->User->get_by_id($id);
        if ($this->input->post('edit_user')) {
            $file= $this->upload_image('user_photo');
            $this->User->update($id,$file);
            $this->message('info','تمت تعديل المستخدم بنجاح');
            redirect('admin/Dashboard/AddUsers', 'refresh');
        }
        if ($this->input->post('update_pw')) {
            $this->User->update_pw($id);
            $this->message('info','تمت تعديل كلمة المرور بنجاح');
            redirect('admin/Dashboard/UpdateUsers/'.$id, 'refresh');
        }
        $data["user_per"]=$this->User->select_user_per();
        $data['title']='بيانات المستخدم ';
        $data['metakeyword']='بيانات المستخدم ';
        $data['metadiscription']='بيانات المستخدم ';
        $data["my_footer"]=array("edit_data_table","validator");
        $data['subview'] = 'backend/users/user';
        $this->load->view('admin_index', $data);
    }
    public function DeleteUsers($id){
        $this->Difined_model->delete("users",array("user_id"=>$id));
        $this->message('error','تم الحذف');
        redirect('admin/Dashboard/AddUsers', 'refresh');
    }
    public function  UpdateUserPassWord(){
        $this->load->model('system_management/User');
        if ($this->input->post('update_pw')) {
            $this->User->update_pw($this->input->post('id'));
            $this->message('info','تمت تعديل كلمة المرور بنجاح');
            redirect('admin/Dashboard', 'refresh');
        }
    }
   //===================================================================================================================
    public function CreateRole(){ // Dashboard/CreateRole
        //-------------------------------------------
        $this->load->model('system_management/Groups');
        $this->load->model('system_management/Permission');
        $this->load->model('Difined_model');
        $data["results_main"]=$this->Groups->get_categories();
        $data["user_in"]=$this->Permission->user_in();
        if ($this->input->post('add_role')) {
            $this->Permission->insert_user_role();
            $this->message('success','تم إضافة الدور بنجاح');
            redirect('admin/Dashboard/CreateRole', 'refresh');
        }
        $data['users']=$this->Difined_model->select_all("admin_voters","user_id","ASC");
        $data['per_table']=$this->Difined_model->select_where_groupy("permissions",array("user_id !="=>0),"user_id");//
        $data["user_name"]=$this->Permission->users_name();
        $data['title'] = 'إضافة أدوار التحكم';
        $data['metakeyword'] = 'إعدادات الموقع ';
        $data['metadiscription'] = '';
        $data["my_footer"]=array("edit_data_table","validator","checkbox");
        $data['subview'] = 'backend/users/roles';
        $this->load->view('admin_index', $data);
    }
    public function UpdateCreateRole($id){
        $this->load->model('system_management/Groups');
        $this->load->model('system_management/Permission');
        $this->load->model('Difined_model');
        $data['users']=$this->Difined_model->select_all("admins","user_id","ASC");
        $data["results_main"]=$this->Groups->get_categories();
        $data['user_data'] = $this->Difined_model->getByArray("admins",array("user_id"=>$id));
        $data["user_permations"]=$this->Permission->select_per($id);
        if ($this->input->post('edit_role')) {
            $this->Difined_model->delete('permissions',array("user_id"=>$id));
            $this->Permission->insert_user_role();
            $this->message('info','تم التعديل بنجاح');
            redirect('admin/Dashboard/CreateRole', 'refresh');

        }
         $data['title'] = 'إضافة أدوار التحكم';
        $data['metakeyword'] = 'إعدادات الموقع ';
        $data['metadiscription'] = '';
        $data["my_footer"]=array("validator","checkbox");
        $data['subview'] = 'backend/users/roles';
        $this->load->view('admin_index', $data);
    }
    public function DeleteCreateRole($user_id){
        $this->load->model('Difined_model');
        $this->Difined_model->delete("permissions",array("user_id"=>$user_id));
        $this->message('error','تم الحذف');
        redirect('admin/dashboard/CreateRole', 'refresh');
    }
   //===================================================================================================================

}// END CLASS 