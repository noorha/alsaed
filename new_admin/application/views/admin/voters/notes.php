<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title">ملاحظات الناخب</h4>
					<!-- /.box-title -->
	<h5>				
					<button class="btn btn-primary" style="background: #f1d4d4;">
					  <a  href="<?=base_url()."admin/dashboard/Add_note/".$voter_id?>"  style="font-weight: bold;color:black;">اضافة ملاحظة</a>

  </button> </h5>
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	 		<script type="text/javascript">
			
$(document).ready(function() {
	 
	   $('#example1').DataTable( {
	        "paging":   false,
	        "ordering": false,
	        "info":     false
	    } );
});
</script>					
					<!-- /.dropdown js__dropdown -->
	    
<?php 
     if(isset($all_notes) && !empty($all_notes)){
        
    ?> 		
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
     <th>الملاحظة</th>
        <th>اسم المستخدم</th>
        <th> عمليات</th>
			</thead>
						<tfoot>
				 <th>م</th>
     <th>الملاحظة</th>
        <th>اسم المستخدم</th>
        <th> عمليات</th>
				</tfoot>
						<tbody>
				 <?php $i=1; foreach ($all_notes as $row ){ ?>
     <tr>
     
        <td> <?php echo $i;?> </td>
     
        <?php if($row->task_id !=null){
            
            $this->db->select('tasks.*');
$this->db->from('tasks');
$this->db->where('tasks.id',$row->task_id);
$query=$this->db->get();
$task_info=$query->result();

$task_type=$task_info[0]->task_type;
if($task_type==0){$type="اتصال";}
else if($task_type==1){$type="زيارة";}
else if($task_type==2){$type="اخرى";}

$task_status=$task_info[0]->status;
if($task_status==0){$status="غير محدد";}
else if($task_status==1){$status="اكتمال";}
else if($task_status==2){$status="إلغاء";}
$note="تم اضافة مهمة ".$type."<br/>";
$note=$note."الحالة:"." ".$status;
        } else {
          $note=$row->note;   
        }
        ?>
        
        <td><?php echo $note;?></td>
        <td><?php echo $row->username;?></td>

        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       $up_url=base_url()."admin/dashboard/UpdateNotes/".$row->id.'/'.$row->voter_id;
                       $delete_url=base_url()."admin/dashboard/DeleteNotes/".$row->id.'/'.$row->voter_id;
                 ?>
                            <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a><br/>
                            <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
						</tbody>
					</table>
					</div>
	 <?php } else{
   
   echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد ملاحظات!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	