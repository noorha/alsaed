				<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
                 <div class="col-xs-12">
                    <div class="box-content" style=" width: 300px;">
                                  <div class="white-box">
                <div class="r-icon-stats">
                    <i class="fa fa-dollar"></i>
                    <div class="bodystate">
                        <?php
                         $this->db->select('balance');
        $this->db->from("accounts");
        $this->db->order_by('accounts.id desc'); 
          $this->db->limit(1);
        $query = $this->db->get();
       $last_data=$query->result();
        $last_b=$last_data[0]->balance;
        ?>
                       <h4> <?php echo number_format($last_b,2);?></h4> 
                        <span class="text-muted">المتوفر كاش</span>
                    </div>
                </div>  
            </div>
                            </div>
                </div>
			<div class="col-xs-12">
                           
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	<h5>		
             
          
    
					<button class="btn btn-primary" style="background: #f1d4d4;">
					  <a  href="<?=base_url()."admin/dashboard/add_account/"?>"  style="font-weight: bold;color:black;">اضافة رصيد جديد</a>


  </button> 
        <button class="btn btn-primary" style="background: #f1d4d4;">
					  <a  href="<?=base_url()."admin/dashboard/account_depts/"?>"  style="font-weight: bold;color:black;">أقسام حساب الخزنة</a>


  </button> 
        </h5>
					
					<!-- /.dropdown js__dropdown -->
<?php 
     if(isset($all_accounts) && !empty($all_accounts)){
        
    ?> 
  
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
        <th>الرصيد</th>
          <th>مدين</th>
         <th>دائن</th>
        <th> القسم</th>
          <th> التاريخ</th>
           <th> صورة الفاتورة</th>
          <th style="display:none;"> عمليات</th>
			</thead>
						<tfoot>
				 <th>م</th>
        <th>الرصيد</th>
        <th>مدين</th>
         <th>دائن</th>
        <th> القسم</th>
          <th> التاريخ</th>
           <th> صورة الفاتورة</th>
          <th> عمليات</th>
				</tfoot>
						<tbody>
				<?php $i=1; foreach ($all_accounts as $row ){ ?>
     <tr>

         <td> <?php echo $i;?> </td>
        <td><?php echo $row->balance;?></td>
        <td><?php echo $row->inside;?></td>
        <td><?php echo $row->outside;?></td>
        <?php if($row->type==0){ 
            $type='ايراد';
        }
        else if($row->type==1){ 
            $type='مصروفات';
        }
         else if($row->type==2){ 
            $type='تصدير';
        }
            ?>
        
            <?php if ($row->dept_id !=null && $row->dept_id !=0 ){
           $this->db->select('depts.*');
$this->db->from('depts');
$this->db->where('depts.id',$row->dept_id);
$query=$this->db->get();
$data=$query->result();
$dept_name=$data[0]->name;
       }else if($row->dept_id ==0) {
       $dept_name='-';    
       }?>
        <td><?php echo $type.'<br/>';?><?php echo $dept_name;?></td>
		
      
        <td><?php echo $row->add_date; ?><?php echo '<br/>'.$row->details;?></td>
       
        <td>
        <?php if($row->my_image !=null){ ?>   
      <a href="<?php echo base_url()."uploads/".$row->my_image?>" target="_blank">صورة الفاتورة</a>
      
        <?php } else { ?>
            لا يوجد
        <?php }?>
        </td>
        <td style="display:none;">
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       
                       $up_url=base_url()."admin/dashboard/UpdateAcccount/".$row->id;
                       $delete_url=base_url()."admin/dashboard/DeleteAccount/".$row->id;
                 ?>
              
                           <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a><br/>
                           <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
						</tbody>
					</table>
					</div>
	 <?php } else{
   
 echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد رصيد!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	