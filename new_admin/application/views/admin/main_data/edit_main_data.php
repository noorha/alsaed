<?php if (isset($result_id) && !empty($result_id) && $result_id != null):
$out = $result_id;
$out['form'] = 'admin/MainData/UpadteMainData/' . $result_id['id'];
$out['div_len'] = '5 ';
$out['input'] = 'UPDTATE';
$out['input_title'] = 'تعديل ';
$out['span'] = 'لعدم تغير الصورة لاتختار شيء ';
$out['required'] = '';
endif ?>

<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title?></h4>
					<!-- /.box-title -->
					
					
					
					<!-- /.dropdown js__dropdown -->
			   <div class="col-sm-12 row form-group " >
            <div class=" col-sm-6">
                <label >عدد المشاهدات للصفحة الرئيسية </label>
                <label type="text" class="form-control half" > <?php echo $out['seen_counter']; ?> </label>
            </div>
        </div>
					
					
					 <?php echo form_open_multipart($out['form']);
              ?>
              
  
 <div class="col-sm-12 row form-group " style="display:none;">
            <div class=" col-sm-6">
                <label class="">الموقع الالكترونى * </label>
                <input type="text" name="company_website" value="<?php echo $out['company_website']; ?>"
                       class="form-control half" placeholder="إدخل البيانات " data-validation="required">
            </div>
            <div class=" col-sm-<?= $out['div_len'] ?>">
                <label class="">اللوجو * </label>
                <input type="file" name="company_logo" class="form-control half" <?= $out['required'] ?> >
                <span id="" class="help-block text-danger"
                      style="color: red;font-size: 12px;"><?= $out['span'] ?></span>
                <?php if (isset($result_id) && !empty($result_id) && $result_id != null): ?>
                    <div class=" col-sm-1 col-xs-12 no-padding">
                        <img src="<?php echo base_url() . "uploads/images/" . $out['company_logo'] ?>"
                             class="img-circle" alt="لا توجد صورة" width="70" height="70"/>
                    </div>
                <?php endif ?>
            </div>
        </div>
        <div class="col-sm-12 row form-group " style="display:none;">
            <div class=" col-sm-6">
                <label class="">العنوان * </label>
                <input type="text" name="ar_company_address" value="<?php echo $out['ar_company_address']; ?>"
                       class="form-control half" placeholder="إدخل البيانات " data-validation="required">
            </div>
            <div class=" col-sm-6">
                <label class="">رقم الحوال * </label>
                <input type="text" name="company_hot_line" value="<?php echo $out['company_hot_line']; ?>"
                       class="form-control half" placeholder="إدخل البيانات " data-validation="required"  onkeypress="validate_number(event);">
            </div>


        </div>

        <div class="col-sm-12 row form-group " style="display:none;">
            <div class=" col-sm-6">
                <label class="">الهاتف * </label>
                <input type="text" name="company_phones" value="<?php echo $out['company_phones']; ?>"
                       class="form-control half" placeholder="إدخل البيانات " data-validation="required"  onkeypress="validate_number(event);">
            </div>
            <div class=" col-sm-6">
                <label class="">البريد الالكترونى * </label>
                <input type="text" name="company_emails" value="<?php echo $out['company_emails']; ?>"
                       class="form-control half" placeholder="إدخل البيانات" data-validation="required">
            </div>
        </div>

        <div class="col-sm-12 row form-group " style="display:none;">
            <div class=" col-sm-6">
                <label class="">نبذة عنا * </label>
                <!--  <input type="text"  value="<?php // echo $out['ar_company_about']; ?>" class="form-control" placeholder="إدخل البيانات "> -->
                    <textarea name="ar_company_about" class="form-control  half " rows="3" data-validation="required">
                        <?php echo $out['ar_company_about']; ?>
                    </textarea>
            </div>
         
            </div>
           
<div class="col-sm-12 row form-group " style="display:none;">
                <div class=" col-sm-6">
                    <label class="">الشروط والاحكام * </label>
                  <!--  <input type="text"  value="<?php // echo $out['ar_company_about']; ?>" class="form-control" placeholder="إدخل البيانات "> -->
                    <textarea name="ar_company_terms" class="form-control  half " rows="3"  data-validation="required">
                        <?php  echo $out['ar_company_terms']; ?>
                    </textarea>
                </div>

            </div>


        <div class="col-sm-12 row form-group ">
            <div class=" col-sm-6">
                <label class="">وسائل التواصل الاجتماعى </label>
            </div>
        </div>
        <div class="col-sm-12 row form-group ">
            <div class=" col-sm-6">
                <label class="">Facebook </label>
                <div class="input-group minquart">
                    <input type="text" name="company_facebook" value="<?php echo $out['company_facebook']; ?>"
                           class="form-control">
                    <div class="input-group-addon"><i class="fa fa-facebook"></i></div>
                </div>
                <!--   <input type="text" name="company_facebook" value="<?php echo $out['company_facebook']; ?>" class="form-control half" placeholder="إدخل البيانات " data-validation="required">
               --> </div>
            <div class=" col-sm-6">
                <label class=""> Twitter </label>
                <div class="input-group minquart">
                    <input type="text" name="company_twitter" value="<?php echo $out['company_twitter']; ?>"
                           class="form-control">
                    <div class="input-group-addon"><i class="fa fa-twitter"></i></div>
                </div>
                <!-- <input type="text" name="company_twitter" value="<?php echo $out['company_twitter']; ?>" class="form-control half" placeholder="إدخل البيانات" data-validation="required">
                --></div>
        </div>
        <div class="col-sm-12 row form-group ">
            <div class=" col-sm-6">
                <label class="">Instagram </label>
                <div class="input-group minquart">
                    <input type="text" name="company_instagram" value="<?php echo $out['company_instagram']; ?>"
                           class="form-control">
                    <div class="input-group-addon"><i class="fa fa-instagram"></i></div>
                </div>
                <!--    <input type="text" name="company_instagram" value="<?php echo $out['company_instagram']; ?>" class="form-control half" placeholder="إدخل البيانات " data-validation="required">
             -->   </div>
             <div class=" col-sm-6">
                <label class=""> YouTube </label>
                <div class="input-group minquart">
                    <input type="text" name="company_youtube" value="<?php echo $out['company_youtube']; ?>"
                           class="form-control">
                    <div class="input-group-addon"><i class="fa fa-youtube"></i></div>
                </div>
                <!--       <input type="text" name="company_youtube" value="<?php echo $out['company_youtube']; ?>" class="form-control half" placeholder="إدخل البيانات " data-validation="required">
               --> </div>
        </div>
       
   
      


        
   

   <div class="col-xs-12 ">
            <button  type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"  class="btn btn-primary" style="background: #f1d4d4;font-weight: bold;color:black;">
                <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?></button>
        </div>

        
        <?php echo form_close()?>
        
   
					
					
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->			
	