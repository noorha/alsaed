<?php
foreach ($out_fild as $key=>$value){
    $out[$value]='';
}
$out["company_facebook"]= "#";
$out["company_twitter"]= "#";
$out["company_instagram"]= "#";
$out["company_linkedin"]="#";
$out["company_youtube"]= "#";
$out["company_googlepluse"]= "#";
$out["company_dribbble"]= "#";
$out["company_pinterest"]= "#";
$out["company_snapchat"]= "#";
$out["company_whatsapp"]= "#";

$out['form']='admin/MainData/AddMainData';
$out['div_len']='6 ';
$out['input']='INSERT';
$out['input_title']='حفظ ';
$out['span']=' ';
$out['required']='data-validation="required"';
?>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title?></h4>
					<!-- /.box-title -->
					
			
			<?php if(isset($data_tables ) && $data_tables!=null && !empty($data_tables)):?>
			
    
    

    
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>الموقع الالكترونى</th>

                    <th>الهاتف </th>
                    <th>البريد الالكترونى</th>
                    <th>اللوجو</th>
                    <th>التحكم</th>
			</thead>
						<tfoot>
				 <th>الموقع الالكترونى</th>

                    <th>الهاتف </th>
                    <th>البريد الالكترونى</th>
                    <th>اللوجو</th>
                    <th>التحكم</th>
				</tfoot>
			 <tbody>
     <tr>
                    
                        <td > <?php echo $data_tables["company_website"]?> </td>

                        <td ><?php echo $data_tables["company_phones"]?></td>
                        <td ><?php echo $data_tables["company_emails"]?></td>
                        <td><img src="<?php echo base_url()?>uploads/images/<?php echo $data_tables["company_logo"]?>" class="img-circle" alt="صورة  " width="50" height="50"> </td>

                        <td data-title="التحكم" class="text-center">
                            <a href="<?php echo base_url().'admin/MainData/UpadteMainData/'.$data_tables["id"]?>">
                                <button type="button" class="btn btn-add btn-xs" title="تعديل ">
                                    <i class="fa fa-pencil"></i></button></a>
                            <a href="<?php echo base_url().'admin/MainData/DeleteMainData/'.$data_tables["id"]?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                <button type="button" class="btn btn-danger btn-xs" ><i class="fa fa-trash-o"> </i> </button>
                            </a>
                        </td>
                       
                    </tr>
   
    </tbody>
					</table>
					</div>
					<!-- /.dropdown js__dropdown -->
					
			 <?php else:?>

            <?php  echo form_open_multipart($out['form'])?>
            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label> الموقع الالكترونى  * </label>
                    <input type="text" name="company_website" value="<?php echo $out['company_website']; ?>" class="form-control half " placeholder="إدخل البيانات " data-validation="required">
                </div>
                <div class=" col-sm-<?=$out['div_len']?>">
                    <label >اللوجو  * </label>
                    <input type="file" name="company_logo"  class="form-control half " <?=$out['required']?> >
                    <span  id="" class="help-block text-danger" style="color: red;font-size: 12px;"><?=$out['span']?></span>
                    <?php if(isset($result_id) && !empty($result_id) && $result_id!=null ):?>
                        <div class=" col-sm-1 col-xs-12 no-padding">
                            <img src="<?php echo base_url()."uploads/images/".$out['company_logo']?>" class="img-circle" alt="لا توجد صورة" width="70" height="70"/>
                        </div>
                    <?php endif?>

                </div>
            </div>
            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label>العنوان    * </label>
                    <input type="text" name="ar_company_address" value="<?php echo $out['ar_company_address']; ?>" class="form-control half " placeholder="إدخل البيانات " data-validation="required">
                </div>
                <div class=" col-sm-6">
                    <label>رقم الجوال   * </label>
                    <input type="text" name="company_hot_line" value="<?php echo $out['company_hot_line']; ?>" class="form-control half " placeholder="إدخل البيانات " data-validation="required"  onkeypress="validate_number(event);"  >
                </div>
            </div>

            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label>الهاتف   * </label>
                    <input type="text" name="company_phones" value="<?php echo $out['company_phones']; ?>" class="form-control half " placeholder="إدخل البيانات " data-validation="required"  onkeypress="validate_number(event);">
                </div>
                <div class=" col-sm-6">
                    <label>البريد الالكترونى    * </label>
                    <input type="text" name="company_emails" value="<?php echo $out['company_emails']; ?>" class="form-control half " placeholder="إدخل البيانات" data-validation="required">
                </div>
            </div>

            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label>نبذة عنا باللغة العربية * </label>
                  <!--  <input type="text"  value="<?php // echo $out['ar_company_about']; ?>" class="form-control" placeholder="إدخل البيانات "> -->
                    <textarea name="ar_company_about" class="form-control  half " rows="3"  data-validation="required">
                        <?php  echo $out['ar_company_about']; ?>
                    </textarea>
                </div>

                <div class=" col-sm-6">
                    <label>نبذة عنا باللغة الانجليزية * </label>
                  <!--  <input type="text"  value="<?php // echo $out['ar_company_about']; ?>" class="form-control" placeholder="إدخل البيانات "> -->
                    <textarea name="en_company_about" class="form-control  half " rows="3"  data-validation="required">
                        <?php  echo $out['en_company_about']; ?>
                    </textarea>
                </div>
            </div>

<div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label>الشروط والاحكام باللغة العربية * </label>
                  <!--  <input type="text"  value="<?php // echo $out['ar_company_about']; ?>" class="form-control" placeholder="إدخل البيانات "> -->
                    <textarea name="ar_company_terms" class="form-control  half " rows="3"  data-validation="required">
                        <?php  echo $out['ar_company_terms']; ?>
                    </textarea>
                </div>

                <div class=" col-sm-6">
                    <label>الشروط والاحكام باللغة الانجليزية * </label>
                  <!--  <input type="text"  value="<?php // echo $out['ar_company_about']; ?>" class="form-control" placeholder="إدخل البيانات "> -->
                    <textarea name="en_company_terms" class="form-control  half " rows="3"  data-validation="required">
                        <?php  echo $out['en_company_terms']; ?>
                    </textarea>
                </div>
            </div>

            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label>وسائل التواصل الاجتماعى  </label>
                </div>

            </div>

            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label >Facebook  </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_facebook" value="<?php echo $out['company_facebook']; ?>"   class="form-control">
                        <div class="input-group-addon"><i class="fa fa-facebook"></i></div>
                    </div>
                 <!--   <input type="text" name="company_facebook" value="<?php echo $out['company_facebook']; ?>" class="form-control half " placeholder="إدخل البيانات " data-validation="required">
               --> </div>
                <div class=" col-sm-6">
                    <label> Twitter  </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_twitter" value="<?php echo $out['company_twitter']; ?>"   class="form-control">
                        <div class="input-group-addon"><i class="fa fa-twitter"></i></div>
                    </div>
                   <!-- <input type="text" name="company_twitter" value="<?php echo $out['company_twitter']; ?>" class="form-control half " placeholder="إدخل البيانات" data-validation="required">
                --></div>
            </div>

            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label>Instagram </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_instagram" value="<?php echo $out['company_instagram']; ?>"   class="form-control">
                        <div class="input-group-addon"><i class="fa fa-instagram"></i></div>
                    </div>
                    <!--    <input type="text" name="company_instagram" value="<?php echo $out['company_instagram']; ?>" class="form-control half " placeholder="إدخل البيانات " data-validation="required">
             -->   </div>
                <div class=" col-sm-6">
                    <label >linkedin </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_linkedin" value="<?php echo $out['company_linkedin']; ?>"   class="form-control">
                        <div class="input-group-addon"><i class="fa fa-linkedin"></i></div>
                    </div>
                    <!--   <input type="text" name="company_linkedin" value="<?php echo $out['company_linkedin']; ?>" class="form-control half " placeholder="إدخل البيانات" data-validation="required">
               --> </div>
            </div>

            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label > YouTube </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_youtube" value="<?php echo $out['company_youtube']; ?>"  class="form-control">
                        <div class="input-group-addon"><i class="fa fa-youtube"></i></div>
                    </div>
                    <!--       <input type="text" name="company_youtube" value="<?php echo $out['company_youtube']; ?>" class="form-control half " placeholder="إدخل البيانات " data-validation="required">
               --> </div>
                <div class=" col-sm-6">
                    <label>+ Google  </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_googlepluse" value="<?php echo $out['company_googlepluse']; ?>"  class="form-control">
                        <div class="input-group-addon"><i class="fa fa-google-plus"></i></div>
                    </div>
                 <!--   <input type="text" name="company_googlepluse" value="<?php echo $out['company_googlepluse']; ?>" class="form-control half " placeholder="إدخل البيانات" data-validation="required">
              -->  </div>
            </div>

            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label > Snapchat </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_snapchat" value="<?php echo $out['company_snapchat']; ?>"  class="form-control">
                        <div class="input-group-addon"><i class="fa fa-snapchat-ghost"></i></div>
                    </div>
                   </div>
                <div class=" col-sm-6">
                    <label > WhatsApp  </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_whatsapp" value="<?php echo $out['company_whatsapp']; ?>"  class="form-control">
                        <div class="input-group-addon"><i class="fa fa-whatsapp"></i></div>
                    </div>
               </div>
            </div>

            <div class="col-sm-12 row form-group ">
                <div class=" col-sm-6">
                    <label > Dribbble  </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_dribbble" value="<?php echo $out['company_dribbble']; ?>"  class="form-control">
                        <div class="input-group-addon"><i class="fa fa-dribbble"></i></div>
                    </div>
                    <!--    <input type="text" name="company_dribbble" value="<?php echo $out['company_dribbble']; ?>" class="form-control half " placeholder="إدخل البيانات " data-validation="required">
                --></div>
                <div class=" col-sm-6">
                    <label >Pinterest </label>
                    <div class="input-group minquart">
                        <input type="text" name="company_pinterest" value="<?php echo $out['company_pinterest']; ?>" class="form-control">
                        <div class="input-group-addon"><i class="fa fa-pinterest"></i></div>
                    </div>
                </div>
            </div>

            <div class="row ">
                <h2> <span class="badge badge-success">الموقع على الخريطة </span> </h2>
                    <input type="hidden" name="company_google_long" id="lng" value="<?php  echo $out['company_google_long'] ?>" />
                    <input type="hidden" name="company_google_lat" id="lat" value="<?php  echo $out['company_google_lat'] ?>" />
                    <?php  echo $maps['html']?>
            </div>

   

   <div class="col-xs-12 ">
            <button  type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"  class="btn btn-primary" style="background: #f1d4d4;font-weight: bold;color:black;">
                <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?></button>
        </div>

        
        <?php echo form_close()?>
        
   
		 <?php endif;?>			
					
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->			
	