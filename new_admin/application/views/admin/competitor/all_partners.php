
<div id="wrapper">
<div class="main-content">
<div class="row small-spacing">
<div class="col-xs-12">
<div class="box-content">
<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
 
    
<?php 
     if(isset($all_accepted) && !empty($all_accepted)){
        
    ?> 
    
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>الرقم التسلسلى</th>
     
        <th>الاسم</th>
        <th>الرقم المدنى</th>
        <th>رقم القيد</th>
        <th>رقم الهاتف</th>
        <th>رقم هاتف اخر</th>
        <th>النوع</th>
        <th> صورة الوصل </th>
        <th> عمليات</th>
			</thead>
						<tfoot>
				 <th>الرقم التسلسلى</th>
     
        <th>الاسم</th>
        <th>الرقم المدنى</th>
        <th>رقم القيد</th>
        <th>رقم الهاتف</th>
        <th>رقم هاتف اخر</th>
        <th>النوع</th>
        <th> صورة الوصل </th>
        <th> عمليات</th>
				</tfoot>
			 <tbody>
    <?php $i=1; foreach ($all_accepted as $row ){ ?>
     <tr>
     
        <td> <?php echo $i;?> </td>
        <td><?php echo $row->name;?></td>
        <td><?php echo $row->civil_no;?></td>
        <td><?php echo $row->register_no;?></td>
        <td><?php echo $row->phone;?></td>
        <?php if($row->phone2 !=null && $row->phone2 !=0){ ?>
          <td><?php echo $row->phone2;?></td>
        <?php } else {?>
          <td></td>
        <?php } ?>
        <?php if($row->gender==0){?>  <td>ذكر</td>

        <?php }if($row->gender==1){?>  <td>أنثى</td>
        <?php }?>
           
      
<td> <a href="<?php echo base_url()."uploads/".$row->wasl_photo?>"> <img src="<?php echo base_url()."uploads/".$row->wasl_photo?>" class="img-circle" alt="User Image" width="50" height="50"/></a>
 </td>
        <td >
        <div class="el-overlay">
                        <ul class="el-info"   style="    padding: 3px;"> 
                        <?php  $acc_url="#"; $ref_url="#";  $delete_url="#";  //  DeleteUser
                                              $edit_image_url=base_url()."admin/Dashboard/edit_image/".$row->id.'/'.'accepted_partners';
                                              $send_message_url=base_url()."admin/Dashboard/send_message/".$row->phone.'/'.$row->id.'/'.$row->name.'/'.'presenters';
                                              $send_accept_message_url=base_url()."admin/Dashboard/send_accept_message/".$row->phone.'/'.'accepted_partners';

                          $delete_url=base_url()."admin/Dashboard/DeletePresenter/".$row->id;
                 ?>
                  <a  href="<?=$edit_image_url?>">
                            تعديل الصورة</a>
                          <br/>
                            <a  href="<?=$send_message_url?>">
                            ارسال رابط لإعادة رفع الصورة
                            <span>( <?php echo $row->send_request?> )</span>
                            </a>
                            <br/>
                            <a  href="<?=$send_accept_message_url?>">
                            ارسال رابط قبول
                            </a>
                         
                            <br/>
             <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                           حذف</a>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
    </tbody>
					</table>
					</div>
	 <?php } else{
   
	     echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد مشاركين بالمسابقة!</strong> .
           </div>';
}  ?>	</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	