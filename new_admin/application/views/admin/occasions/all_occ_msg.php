        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                
                <div class="page-header">
                    <div class="page-title">

                        
                    </div>
                </div>

                <div class="row" id="cancel-row">
                
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                                           <h4><?php echo $title;?></h4>
     
  							<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>	
 					<!-- /.dropdown js__dropdown -->
<?php 
     if(isset($all_occ) && !empty($all_occ)){
        
    ?> 
    	
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
     <th>نوع المناسبة</th>
        <th>الرسالة الخاصة بالمناسبة</th>
      <th></th>
			</thead>
						
					 <tbody>

     
                      <?php $i=1; foreach ($all_occ as $row ){ ?>
     <tr>
     
  

        <td> <?php echo $i;?> </td>
        <?php if( $row->type_id==0){ 
               $type='عرس';
        } else if( $row->type_id==1){ 
        $type='حفل تخرج';
  } else if( $row->type_id==2){ 
  $type='عزاء';
 } else if( $row->type_id==3){ 
  $type='عودة من السفر مرافق مريض';
 } else if( $row->type_id==4){ 
  $type='عشاء عودة من علاج';
 }  else if( $row->type_id==5){ 
  $type='عشاء تمايم';
 } else if( $row->type_id==6){ 
  $type='عشاء منزل جديد';
 }else if( $row->type_id==7){ 
  $type='حج';
 }else if( $row->type_id==8){
     $type='نجاح بالانتخابات';
 }else if( $row->type_id==9){
     $type='ترقية';
 }?>   
        <?php $gender=''; 
      if($row->gender !=null) { 
         if($row->gender ==0 && $row->type_id==2){
        $gender='ذكر';
         } else if($row->gender ==1 && $row->type_id==2){
        $gender='أنثى';
         } else if($row->gender ==2 && $row->type_id==2){
             $gender='طفل';
         }
         else if($row->gender ==3 && $row->type_id==2){
             $gender='طفلة';
         }
         
         else if($row->gender ==0 && $row->type_id==5){
             $gender='مولود';
         }
         else if($row->gender ==1 && $row->type_id==5){
             $gender='مولوده';
         }
     ?>
     <td><?php echo $type.'('.$gender.')';?>   </td>
    <?php } else {?> 
     <td><?php echo $type;?>   </td>
    <?php } ?>
      
        <td><?php echo $row->text;?></td>
      
        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       $up_url=base_url()."admin/dashboard/UpdateOccMsg/".$row->id;
                 ?>
                            <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a>
                          
                        </ul>
                    </div>
        </td>
      
      </tr>
     <?php $i++;?>
   
    <?php } ?>
                          
                                             
    </tbody>
					</table>
					</div>
 <?php } else{
   
    echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد رسائل مناسبات!</strong> .
           </div>';
}  ?>
                                   
                                </div>
                            </div>
                        </div>
                    
                      
                    </div>

                </div>
          