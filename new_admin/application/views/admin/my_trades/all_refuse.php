				<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
                 
			<div class="col-xs-12">
                           
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->

					
					<!-- /.dropdown js__dropdown -->
<?php 
     if(isset($all_refuse) && !empty($all_refuse)){
        
    ?> 
  
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
        <th>الأسم</th>
          <th>الهاتف</th>
         <th>تاريخ الأضافة</th>
        <th> القسم</th>
          <th> الملاحظة</th>
			</thead>
						<tfoot>
				 <th>م</th>
        <th>الأسم</th>
          <th>الهاتف</th>
         <th>تاريخ الأضافة</th>
        <th> القسم</th>
          <th> الملاحظة</th>
				</tfoot>
						<tbody>
				<?php $i=1; foreach ($all_refuse as $row ){ ?>
     <tr>

         <td> <?php echo $i;?> </td>
        <td><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname.' '.$row->thigh_name;?></td>
        <td><?php echo $row->voter_phone;?></td>
        <td><?php echo $row->add_date;?></td>
        <td><?php echo $row->dept_name;?></td>
	 <td><?php echo $row->notes;?></td>
       
       
      </tr>
     <?php $i++;?>
   
    <?php } ?>
						</tbody>
					</table>
					</div>
	 <?php } else{
   
 echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد معاملات مرفوضة!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	