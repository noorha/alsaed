				<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
                 
			<div class="col-xs-12">
                           
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	<h5>		
             
         
        <button class="btn btn-primary" style="background: #f1d4d4;">
					  <a  href="<?=base_url()."admin/dashboard/trade_depts/"?>"  style="font-weight: bold;color:black;">أقسام المعاملات</a>


  </button> 
               <button class="btn btn-primary" style="background: #f1d4d4;">
					  <a  href="<?=base_url()."admin/dashboard/trade_refused/"?>"  style="font-weight: bold;color:black;">معاملات مرفوضة</a>


  </button>
             <button class="btn btn-primary" style="background: #f1d4d4;">
					  <a  href="<?=base_url()."admin/dashboard/trade_accepted/"?>"  style="font-weight: bold;color:black;">معاملات تم انجازها</a>


  </button> 
        </h5>
					
					<!-- /.dropdown js__dropdown -->
<?php 
     if(isset($all_needs) && !empty($all_needs)){
        
    ?> 
  
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
        <th>الأسم</th>
          <th>الهاتف</th>
         <th>تاريخ الأضافة</th>
        <th> القسم</th>
          <th> الملاحظة</th>
          <th> عمليات</th>
			</thead>
						<tfoot>
				 <th>م</th>
        <th>الأسم</th>
          <th>الهاتف</th>
         <th>تاريخ الأضافة</th>
        <th> القسم</th>
          <th> الملاحظة</th>
          <th> عمليات</th>
				</tfoot>
						<tbody>
				<?php $i=1; foreach ($all_needs as $row ){ ?>
     <tr>

         <td> <?php echo $i;?> </td>
        <td><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname.' '.$row->thigh_name;?></td>
        <td><?php echo $row->voter_phone;?></td>
        <td><?php echo $row->add_date;?></td>
        <td><?php echo $row->dept_name;?></td>
	 <td><?php echo $row->notes;?></td>
       
        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       
                       $up_url=base_url()."admin/dashboard/UpdateAcccount/".$row->id;
                       $delete_url=base_url()."admin/dashboard/DeleteAccount/".$row->id;
                       $archief_url=base_url()."admin/dashboard/refuse_trade/".$row->id;
                       $trades_url=base_url()."admin/dashboard/accept_trade/".$row->id;

                 ?>
              
                           <a  href="<?=$up_url?>" style="display:none;">
                                       <i class="fa fa-pencil"></i></a>
                           <a  href="<?=$delete_url?>" style="display:none;" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                                    
                                       <a  class="btn btn-primary" style="background: #f1d4d4;margin-bottom: 5px;" href="<?=$archief_url?>">مرفوضة</a><br/>
                                 
                                       <a class="btn btn-primary" style="background: #f1d4d4;" href="<?=$trades_url?>">منجزة</a><br/>

                      
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
						</tbody>
					</table>
					</div>
	 <?php } else{
   
 echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد احتياجات!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	