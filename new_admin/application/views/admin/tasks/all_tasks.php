
<!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="container">
                <div class="container">

                
                    <div class="row layout-top-spacing">
                        
                        <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">
                                    <div class="row">
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                            <h4><?php echo $title;?></h4>
                                            <h5>
                                                  <?php if(!isset($archief)){ ?>				
					<button class="btn btn-primary" style="background: #f1d4d4;">
  <a  href="<?=base_url()."admin/dashboard/add_task/"?>" style="font-weight: bold;color:black;">اضافة مهمة</a>

  </button>
  	
					<button class="btn btn-primary" style="background: #f1d4d4;">
  <a  href="<?=base_url()."admin/dashboard/archief_task/"?>" style="font-weight: bold;color:black;">الأرشيف</a>

  </button>
  <?php } ?>
   </h5>
                                        </div>                 
                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">
<?php 
if(isset($all_tasks) && !empty($all_tasks)){
        
    ?> 	
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover mb-4">
                                            <thead>
                                                <tr>
                                                   	 <th>م</th>
       <th>الاسم</th>
       <th>رقم الهاتف</th>
       <th>نوع المهمة</th>
       <th>ملاحظة</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php $i=1; foreach ($all_tasks as $row ){ ?>
     <tr>
     <td> <?php echo $i;?> </td>
        <td><?php echo $row->name;?></td>
        <td><?php echo $row->phone;?></td>
         <td>
             <?php if($row->task_type==0){ 
                 
                 $type='اتصال';
             } else if($row->task_type==1){ 
                 
                 $type='زيارة';
             } else  if($row->task_type==2){ 
                 
                 $type='اخرى';
             }
?>
             
             <?php echo $type ;?>
         </td>
                 <?php if($row->status==1){ 
                 
                 $note1='تم اكتمال المهمة';
             } else if($row->status==2){ 
                 
                 $note1='تم إلغاء المهمة';
             }
?>
         <?php if($note1!=null){ ?>
          <td><?php echo $row->note.' ('.$note1.') ';?></td>
         <?php } else {?>
           <td><?php echo $row->note;?></td>
             <?php }?>
        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
    
                       $up_url=base_url()."admin/dashboard/UpdateTask/".$row->id;
                       $delete_url=base_url()."admin/dashboard/DeleteTask/".$row->id;
                       $archief_url=base_url()."admin/dashboard/archiefTask/".$row->id;
                       
                 ?>
              
                           <?php if(!isset($archief)){ ?>
                            <a  href="<?=$up_url?>">
                                       <i class="far fa-edit"></i></a><br/>
                              <a  href="<?=$archief_url?>">
                                       تحويل المهمة إلى الأرشيف</a><br/>          
                             <?php } ?>          
                            <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="far fa-trash-alt"></i></a>
                        </ul>
                    </div>
        </td>
      
      </tr>
     <?php $i++;?>
   
    <?php } ?>  
                                               
                                            </tbody>
                                        </table>
                                    </div>
 <?php } else{
   
    echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد مهام!</strong> .
           </div>';
}  ?>
                                   
                                </div>
                            </div>
                        </div>
                    
                      
                    </div>

                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->
    </div>
<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->

    