<?php if(isset($result_id) && !empty($result_id) && $result_id!=null ):
    $out=$result_id;
    $out['form']='admin/Dashboard/UpdateTask/'.$result_id['id'];
    $out['input']='UPDTATE';
    $out['requierd']='';
    $out['model']='';
    $out['input_title']='تعديل ';
  
else:
    foreach ($out_fild as $key=>$value){
        $out[$value]='';
    }
    $out['requierd']='data-validation="required" ';
    $out['form']='admin/Dashboard/add_task';
    $out['input']='INSERT';
    $out['model']='-add';
    $out['input_title']='حفظ ';
endif?>     
<!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="container">
                <div class="container">

                
                    <div class="row layout-top-spacing">
                        
                          <div class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">                                
                                    <div class="row">
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                            <h4><?php echo $title?></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content widget-content-area">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script>
    $(document).ready(function(){
       // Initialize select2
  
  
 $('.task_type_list').on('change', function() {
$('.task_type_list').not(this).prop('checked', false); 
     
 });
  $('#voter_id').on('change', function() {
  var voter_id = $('#voter_id').val();
  if(voter_id !== '')
  {
   $.ajax({
    url:"<?php echo base_url(); ?>admin/dashboard/fetch_voter_id",
    method:"POST",
    data:{voter_id:voter_id},
    success:function(data)
    {
         var obj = JSON.parse(data);
     console.log(obj.fname+' '+obj.sname+' '+obj.tname+' '+obj.foname+' '+obj.fvname);
   var name=obj.fname+' '+obj.sname+' '+obj.tname+' '+obj.foname+' '+obj.fvname;
   var num=obj.phone+' - '+obj.phone2;
        document.getElementById("name").value=name;
     document.getElementById("phone").value=num;
    }
   });
  }
  else
  {
  $('#name').html('');
     $('#phone').html('');  }
 });
    });
    

</script>	
  <script type="text/javascript">

function disable_data()
{
 document.getElementById("occ_time_group").style.display = "none";
 document.getElementById("occ_gender_group").style.display = "none";
 document.getElementById("occ_gender_group2").style.display = "none";

}
</script>                             
					 <?php echo form_open_multipart($out['form']);?>

                                        <div class="form-group mb-4">
                                                 <label>اختيار ناخب</label>
        <select class="selectpicker form-control half input-style" data-live-search="true"name="data_post[voter_id]" id="voter_id"  >

<option value="">    إختر   </option>

<?php if(isset($all_voters) && !empty($all_voters) && $all_voters!=null){

foreach ($all_voters as $one_voter):

$select="";
if($one_voter->id == $result_id['voter_id']){$select='selected="selected"';}?>

<option  value="<?php echo $one_voter->id?>" <?php echo  $select?>>

    <?php echo $one_voter->fname.' '. $one_voter->sname .' '.$one_voter->tname.' '.$one_voter->foname.' '.$one_voter->fvname;?>

</option>

<?php endforeach; 

}?>

</select>
                                        </div>
                                        <div class="form-group mb-4">
                                             <label >الأسم</label>
               <input type="text" name="data_post[name]" id="name"  value="<?php echo $result_id['name']; ?>"   class="form-control half input-style" placeholder="إدخل البيانات "  >
          
                                        </div>
                                        <div class="form-group mb-4">
                                             <label>الهاتف</label>
  <input type="text" name="data_post[phone]" id="phone"  value="<?php echo $result_id['phone']; ?>"   class="form-control half input-style" placeholder="إدخل البيانات "  >
          
                                        </div>
                                        <div class="form-group mb-4">
                                          <input   type="checkbox" id="task_type0"class="task_type_list" name="data_post[task_type]" class="form-control half input-style" value="0" <?php echo ($out['task_type'] == '0' ? 'checked' : null); ?> > اتصال 
<input   type="checkbox" id="task_type1"class="task_type_list" name="data_post[task_type]" class="form-control half input-style" value="1" <?php echo ($out['task_type'] == '1' ? 'checked' : null); ?>  > زيارة 
<input   type="checkbox" id="task_type2"class="task_type_list" name="data_post[task_type]" class="form-control half input-style" value="2" <?php echo ($out['task_type'] == '2' ? 'checked' : null); ?>  > اخرى 
  </div>
                                        <div class="form-group mb-4 mt-3">
                                            <label >ملاحظة</label>
          <textarea name="data_post[note]" id="task_note" class="form-control" style="height: 125px;margin-bottom: 10px;" rows="30" cols="50" data-validation="required"><?php echo $out['note']; ?></textarea>
          </div>
                                    <button  type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"  class="btn btn-primary" style="background: #f1d4d4;font-weight: bold;color:black;">
                <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?></button>
 <?php echo form_close()?>
                                </div>
                            </div>
                        </div>

                      
                    </div>

                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->
