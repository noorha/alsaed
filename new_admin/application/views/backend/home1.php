<div class="row">
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-info card-shadow">
            <div class="box_dash bg-info text-center">
                <h1 class="font-light text-white"><?php if(isset($all_ads)){echo $all_ads;}else {echo 0;}?></h1>
                <h6 class="text-white"><i class="fa fa-list-ul"></i></h6>
                <h6 class="text-white">إجمالى الاعلانات  </h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-primary card-inverse card-shadow">
            <div class="box_dash text-center">
                <h1 class="font-light text-white"><?php if(isset($fav_ads)){echo $fav_ads;}else {echo 0;}?></h1>
                <h6 class="text-white"><i class="fa fa-calendar-check-o"></i></h6>
                <h6 class="text-white">  الاعلانات المفضلة  </h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-success card-shadow">
            <div class="box_dash text-center">
                <h1 class="font-light text-white">
                    <!--  <i class="fa fa-facebook"></i>-->
                    <?php if(isset($paypal_ads)){echo $paypal_ads;}else {echo 290;}?></h1>
                <h6 class="text-white"><i class="fa fa-plus-square-o"></i></h6>
                <h6 class="text-white">الاعلانات المدفوعة </h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-warning card-shadow">
            <div class="box_dash text-center">
                <h1 class="font-light text-white"><?php if(isset($partners)){echo $partners;}else {echo "2";}?></h1>
                <h6 class="text-white"><i class="fa fa-cogs "></i></h6>
                <h6 class="text-white">الاشتراكات  </h6>
            </div>
        </div>
    </div>
</div>
<div class="row">
 
    <!--------------------------------------------------------------------------------------------------------->
    <!-- Column -->
    <div class="col-lg-6">
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <!-- <div class="col-sm-6">
                <div class="card card-block card-shadow">
                    <div class="row p-t-10 p-b-10">
                        <div class="col p-r-0">
                            <h1 class="font-light"><?php if(isset($show_1)){echo $show_1;}else {echo "0";}?></h1>
                            <h6 class="text-muted">العملاء </h6></div>
                       
                        <div class="col text-right align-self-center">
                            <div data-label="100%" class="css-bar m-b-0 css-bar-primary css-bar-100">
                                <i class="mdi mdi-account-circle"></i></div>
                        </div>
                    </div>
                </div>
            </div>  -->
            <!-- Column -->
            <div class="col-sm-6">
                <div class="card card-block card-shadow">
                    <!-- Row -->
                    <div class="row p-t-10 p-b-10">
                        <!-- Column -->
                        <div class="col p-r-0">
                            <h1 class="font-light"><?php if(isset($indeividual_ads)){echo $indeividual_ads;}else {echo "0";}?></h1>
                            <h6 class="text-muted">اعلانات الافراد  </h6></div>
                        <!-- Column -->
                        <?php
                        if(isset($indeividual_ads) ){
                            if($all_ads == 0){
                                $online_pecent =1;
                            }else{
                                $online_pecent =round(  100 * ($indeividual_ads /$all_ads) );
                            }
                        }else {$online_pecent=10;}
                        if($online_pecent %5 != 0 ){
                            $dif = $online_pecent %5;
                            $online_pecent = $online_pecent - $dif ;
                        }
                        ?>
                        <div class="col text-right align-self-center">
                            <div data-label="<?=$online_pecent?>%" class="css-bar m-b-0 css-bar-danger css-bar-<?=$online_pecent?>">
                                <i class="mdi mdi-account-location"></i></div>
                        </div>
                    </div>
                </div>
            </div>
              <div class="col-sm-6">
                <div class="card card-block card-shadow">
                    <!-- Row -->
                    <div class="row p-t-10 p-b-10">
                        <!-- Column -->
                        <div class="col p-r-0">
                            <h1 class="font-light"><?php if(isset($company_ads)){echo $company_ads;}else {echo "0";}?></h1>
                            <h6 class="text-muted">اعلانات الشركات  </h6></div>
                        <!-- Column -->
                        <?php
                        if(isset($company_ads) ){
                            if($all_ads == 0){
                                $online_pecent =1;
                            }else{
                                $online_pecent =round(  100 * ($company_ads /$all_ads) );
                            }
                        }else {$online_pecent=10;}
                        if($online_pecent %5 != 0 ){
                            $dif = $online_pecent %5;
                            $online_pecent = $online_pecent - $dif ;
                        }
                        ?>
                        <div class="col text-right align-self-center">
                            <div data-label="<?=$online_pecent?>%" class="css-bar m-b-0 css-bar-warning  css-bar-<?=$online_pecent?>">
                                <i class="mdi mdi-account-network"></i></div>
                        </div>
                    </div>
                </div>
            </div>
          <!--  <div class="col-sm-6">
                <div class="card card-block card-shadow">
                 
                    <div class="row p-t-10 p-b-10">
                     
                        <div class="col p-r-0">
                            <h1 class="font-light"><?php if(isset($show_4)){echo $show_4;}else {echo "0";}?></h1>
                            <h6 class="text-muted">الطلبات الملغية  </h6></div>
                       
                        <?php
                        if(isset($show_4) ){
                            if($show_1 == 0){
                                $online_pecent =1;
                            }else{
                                $online_pecent =round(  100 * ($show_4 /$show_1) );
                            }
                        }else {$online_pecent=10;}
                        if($online_pecent %5 != 0 ){
                            $dif = $online_pecent %5;
                            $online_pecent = $online_pecent - $dif ;
                        }
                        ?>
                        <div class="col text-right align-self-center">
                            <div data-label="<?=$online_pecent?>%" class="css-bar m-b-0 css-bar-info css-bar-<?=$online_pecent?>">
                                <i class="mdi mdi-briefcase-check"></i></div>
                        </div>
                    </div>
                </div>
            </div>  -->
            <!-- Column -->
        </div>
    </div>
</div>



