<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>لوحة تحكم الناخبين</title>
    <link rel="icon" type="image/x-icon" href="<?php echo base_url()?>assets_files/assets/img/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="<?php echo base_url()?>assets_files/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>assets_files/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL CUSTOM STYLES -->
    <link href="<?php echo base_url()?>assets_files/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
       <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets_files/plugins/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets_files/assets/css/forms/theme-checkbox-radio.css">
    <link href="<?php echo base_url()?>assets_files/assets/css/tables/table-basic.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="<?php echo base_url()?>assets_files/plugins/font-icons/fontawesome/css/regular.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets_files/plugins/font-icons/fontawesome/css/fontawesome.css">
    <!-- END PAGE LEVEL CUSTOM STYLES -->
       <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets_files/plugins/bootstrap-select/bootstrap-select.min.css">
    <link href="<?php echo base_url()?>assets_files/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
 <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets_files/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets_files/plugins/table/datatable/dt-global_style.css">
    <!-- END PAGE LEVEL STYLES -->
</head>
<body class="alt-menu sidebar-noneoverflow" data-spy="scroll" data-target="#navSection" data-offset="140">
    
    <!--  BEGIN NAVBAR  -->
    <div class="header-container fixed-top">
        <header class="header navbar navbar-expand-sm expand-header">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <ul class="navbar-item flex-row ml-auto">

                    <li class="nav-item dropdown user-profile-dropdown  order-lg-0 order-1">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-check"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><polyline points="17 11 19 13 23 9"></polyline></svg>
                    </a>
                    <div class="dropdown-menu position-absolute e-animated e-fadeInUp" aria-labelledby="userProfileDropdown"style="    background-color: black;">
                        <div class="user-profile-section">                            
                            <div class="media mx-auto">
 <?php if(!empty($_SESSION['user_photo'])){ ?>

                            <img src="<?php echo base_url()."uploads/".$_SESSION['user_photo']?>" class="ico-img" style="width:50%" />

                            <?php 	}else{ ?>

                            <img src="<?php echo base_url()?>asisst/favicon/DE.png" class="ico-img" style="width:50%" />

                            <?php }?>
                            <div class="media-body">
                                
                                    <h5>Alan Green</h5>
                                    <p>Web Developer</p>
                                </div>
                            </div>
                        </div>
                      
                        <div class="dropdown-item">
                            <a href="<?php echo base_url()."Login/logout"?>" >
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg> <span>تسجيل الخروج</span>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->
<div class="main-container sidebar-closed sbar-open" id="container">

        <div class="overlay"></div>
        <div class="cs-overlay"></div>
        <div class="search-overlay"></div>


   <?php  $this->load->view('backend/requires/sidebar'); ?>

    <!--  BEGIN MAIN CONTAINER  -->
    
