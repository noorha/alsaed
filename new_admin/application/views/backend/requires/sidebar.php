    <!--  BEGIN SIDEBAR  -->
        <div class="sidebar-wrapper sidebar-theme">
            
            <nav id="sidebar">

                <ul class="navbar-nav theme-brand flex-row  text-center">
                    <li class="nav-item theme-logo">
                        <a href="index.html">
                            <img src="<?php echo base_url()?>uploads/logo.png" class="navbar-logo" alt="logo">
                        </a>
                    </li>
                    <li class="nav-item theme-text">
                        <a href="#" class="nav-link"> الناخبين </a>
                    </li>
                </ul>

                <ul class="list-unstyled menu-categories" id="accordionExample">
               
                   
   <?php if(isset($this->sidbarpages) && !empty($this->sidbarpages) && $this->sidbarpages!=null){?>

<?php foreach( $this->sidbarpages as $main_row){ ?>
   

<?php if(  $main_row->page_link=="#"){ ?>

       <li class="menu">
                        <a href="<?php echo '#title'.$main_row->page_id?>" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i class="<?php echo $main_row->page_icon_code?>"></i>
								<span ><?php echo $main_row->page_title?></span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
       <?php if(sizeof($main_row->sub) >0 && !empty($main_row->sub)){ ?>

                        <ul class="collapse submenu list-unstyled" id="<?php echo 'title'.$main_row->page_id?>" data-parent="#accordionExample">
    <?php foreach( $main_row->sub as $sub_row){ ?>
                    
					<li>
<a href="<?php echo base_url().$sub_row->page_link?>">

      <?php echo $sub_row->page_title?></a>
			</li>
                <?php }// end sub foereach ?>
        
                         
                        </ul>
						<?php } // end sub  if ?>
   </li>
	<?php } else { ?>
                    <li class="menu">
 <a href="<?php echo base_url().$main_row->page_link?>" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                               <i class="<?php echo $main_row->page_icon_code?>"></i>
							   <span><?php echo $main_row->page_title?></span>
                            </div>
                        </a>
                    </li>
<?php } ?>


                    
					<?php }// end main foereach ?>

<?php } // end if ?>	



                    
                    
            
                 
                </ul>
                
            </nav>

        </div>
        <!--  END SIDEBAR  -->
