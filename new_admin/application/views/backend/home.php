<div class="row">
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-info card-shadow">
            <div class="box_dash bg-info text-center">
                <h1 class="font-light text-white"><?php if(isset($aparts)){echo $aparts;}else {echo 0;}?></h1>
                <h6 class="text-white"><i class="fa fa-building"></i></h6>
                <h6 class="text-white">إجمالى العقارات </h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-primary card-inverse card-shadow">
            <div class="box_dash text-center">
                <h1 class="font-light text-white"><?php if(isset($cars)){echo $cars;}else {echo 0;}?></h1>
                <h6 class="text-white"><i class="fa fa-car"></i></h6>
                <h6 class="text-white"> إجمالى السيارات </h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-success card-shadow">
            <div class="box_dash text-center">
                <h1 class="font-light text-white">
                    <!--  <i class="fa fa-facebook"></i>-->
                    <?php if(isset($clients)){echo $clients;}else {echo 290;}?></h1>
                <h6 class="text-white"><i class="fa fa-user"></i></h6>
                <h6 class="text-white">إجمالى العملاء </h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-warning card-shadow">
            <div class="box_dash text-center">
                <h1 class="font-light text-white"><?php if(isset($investors)){echo $investors;}else {echo "2";}?></h1>
                <h6 class="text-white"><i class="fa fa-user "></i></h6>
                <h6 class="text-white">إجمالى المستثمرين  </h6>
            </div>
        </div>
    </div>
</div>
</div>



