<style>
    .panal_border  {
       border: 1px solid #d6e9c6;
    }
    .panel-success.panal_border  > .panel-heading{
        color: #fff;
        background-color: #d6e9c6;
        border-color: #d6e9c6;
    }
    .panel ul {
        list-style: none;
        padding-right: 20px;
    }
    .treeview li>input {
        height: 16px;
        width: 16px;
    }
</style>
   
   <?php if(isset($user_data)):
    $out['form']='admin/Dashboard/UpdateCreateRole/'.$user_data['user_id'];
    $out['user_id']=$user_data["user_id"];
    $out['DES']='disabled="disabled"';//user_permations
    $user_per=$user_permations;
    $out['hidden']=' <input type="hidden" name="user_id" value="'.$user_data['user_id'].'">';
    $out['input']='edit_role';
    $out['input_title']='تعديل ';
else:
    $out['form']='admin/Dashboard/CreateRole';
    $out['user_id']="";
    $out['DES']='';
    $out['hidden']='';
    $user_per=array(0);
    $out['input']='add_role';
    $out['input_title']='حفظ ';
endif;?>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title?></h4>
					<!-- /.box-title -->
					
					
					
					<!-- /.dropdown js__dropdown -->
					
					<?php    function AllRolse($results_main_in ,$user_per_in ){
      $class=array("default","danger","success","primary","info","warning");
    foreach ($results_main_in as $main_row):
        $main_checked="";  if(in_array($main_row->page_id ,$user_per_in)){ $main_checked='checked="checked"';} ?>

        

            
					<div class="col-md-8 col-xs-12">
				<div class="box-content" style="background-color:<?php echo  $main_row->color?>">
					
					
					<h4 class="box-title ribbon ribbon-<?=$class[rand(0,5)]?> ribbon-right"><?php  echo  $main_row->page_title?></h4>
					
                        <ul class="treeview list-unstyled">
                            <li>
						  <?php if(sizeof($main_row->sub) ==0){?>
                                              <input type="checkbox" name="select-all[]" value="<?php  echo  $main_row->page_id."-".$main_row->level?>" <?php  echo  $main_checked?> >
                                              <label for="tall" class="custom-unchecked"><?php echo  $main_row->page_title?> </label>
                                          <?php } ?>
                                              <?php if(sizeof($main_row->sub) >0){?>
                                                <input type="checkbox" name="select-all[]"  value="<?php  echo  $main_row->page_id."-".$main_row->level?>" <?php  echo  $main_checked?>  style="display:none;">
                                              <label for="tall" class="custom-unchecked" style="display:none;"><?php echo  $main_row->page_title?> </label>
                                         
                                                  <ul>
                                        <?php foreach ($main_row->sub as $sub_row ){
                                            $sub_checked="";  if(in_array($sub_row->page_id ,$user_per_in)){ $sub_checked='checked="checked"';}    ?>
                                            <li>
                                                <input type="checkbox" name="select-all[]" value="<?php echo $sub_row->page_id."-".$sub_row->level?>" <?php  echo  $sub_checked?> >
                                                <label for="tall-2" class="custom-unchecked"><?php echo  $sub_row->page_title?></label>
                                                <?php if(sizeof($sub_row->sub) >0){ ?>
                                                    <ul>
                                                        <?php foreach ($sub_row->sub as $sub_sub_row ){
                                                            $sub_sub_checked="";  if(in_array($sub_sub_row->page_id ,$user_per_in)){ $sub_sub_checked='checked="checked"';}         ?>
                                                            <li>
                                                                <input type="checkbox"name="select-all[]" value="<?php echo $sub_sub_row->page_id."-".$sub_sub_row->level?>" <?php  echo  $sub_sub_checked?> >
                                                                <label for="tall-2-1" class="custom-unchecked" ><?php echo  $sub_sub_row->page_title?></label>
                                                                <?php if(sizeof($sub_sub_row->sub) >0){ ?>
                                                                    <ul>
                                                                        <?php foreach ($sub_sub_row->sub as $sub_row_4 ){
                                                                            $sub_sub_checked_4="";  if(in_array($sub_row_4->page_id ,$user_per_in)){ $sub_sub_checked_4='checked="checked"';}         ?>
                                                                            <li>
                                                                                <input type="checkbox"name="select-all[]" value="<?php echo $sub_row_4->page_id."-".$sub_row_4->level?>" <?php  echo  $sub_sub_checked_4?> >
                                                                                <label for="tall-2-1" class="custom-unchecked" ><?php echo  $sub_row_4->page_title?></label>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                <?php } ?>


                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                <?php } ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                        </ul>
            
                            </div>
            </div>
    <?php endforeach;}   ?>

 <?php echo form_open_multipart($out['form'])?>




            <div class="col-sm-12 row">


                <div class="form-group col-sm-6">
                    <label >إسم المستخدم</label>
                    <select  name="user_id" class="form-control half"  data-validation="required" aria-required="true"   >
                        <option value="">إختر المستخدم </option>
                        <?php  if(isset($users) && !empty($users)){
                         $x=1; foreach ($users as $user) {
                            $select="";   if(isset($user_data)){
                                if($user->user_id ==  $out['user_id'] ){$select='selected="selected"';}
                            }
                            if(isset($user_in)){
                                if(in_array( $user->user_id,$user_in )) {continue;}
                            }
                            ?>
                            <option value="<?php echo $user->user_id?>"  <?php echo $select?>  > <?php echo $user->user_username?></option>
                        <?php $x++;}
                              if($x == 1){
                                  echo '<option value="">تم إضافة  جميع الصلاحيات  </option>';
                              }
                        } else{
                            echo '<option value=""> يجب إضافة مستخدمين أولا </option>';
                        }?>
                        <?php  echo $out['hidden'];?>
                    </select>
                </div>


            </div>
            <?php   $total =sizeof($results_main);
            if($total % 3 == 0){
                $count =$total /3 ;
            }else{
                $count=  $total /3 ;
                $count = (int) $count;
                $count =  $count + 1;
            }
            $x=1;
            foreach ($results_main as $main_row){
                if( $x <=  $count ){
                    $frist_array []= $main_row;
                }elseif( $x <=  (2*$count ) ){
                    $secound_array []= $main_row;
                }elseif( $x <=  (3*$count ) ){
                    $thred_array []= $main_row;
                }
                $x++;
            } ?>
            <div class="col-sm-12 row">
                <?php if(isset($frist_array)){?>
                <div class="col-sm-4 row"><?php AllRolse($frist_array,$user_per)?></div>
                <?php } if(isset($secound_array)){ ?>
                <div class="col-sm-4 row"><?php AllRolse($secound_array,$user_per)?></div>
                <?php } if(isset($thred_array)){ ?>
                <div class="col-sm-4 row"><?php AllRolse($thred_array,$user_per)?></div>
                <?php }?>



            </div>




            <div class="col-xs-12 ">
            <button  type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"  class="btn btn-primary" style="background: #f1d4d4;font-weight: bold;color:black;">
                <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?></button>
        </div>

            <?php echo form_close()?>


            <?php if(isset($per_table) && !empty($per_table)):?>
                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="2%">#</th>
                        <th  class="">المستخدم</th>
                        <th>الصلاحيات</th>
                        <th  class="">التحكم</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $count=1; foreach($per_table as $role):?>
                        <tr>
                            <td data-title="#"><?php echo $count; ?></td>
                            <td data-title="العنوان"> <?php if(isset($user_name[$role->user_id])){echo $user_name[$role->user_id];}else{'غير معرف';} ?> </td>
                           <?php  
                           $this->db->select('permissions.user_id,permissions.page_id_fk,pages.page_title');
$this->db->from('permissions');
$this->db->where('permissions.user_id',$role->user_id);
$this->db->join('pages' , 'pages.page_id = permissions.page_id_fk',"inner");

$query=$this->db->get();
 $data1=  $query->result() ;

?>
                            <td data-title="الصلاحيات" >
                            <?php foreach($data1 as $d1):?> 
                                <?php echo '- '. $d1->page_title; ?>
                                <br/>
                                <?php  endforeach ;?>
                            </td>
                            <td data-title="التحكم" class="text-center">
                                <a href="<?php echo base_url().'admin/dashboard/UpdateCreateRole/'.$role->user_id?>">
                                     <button type="button" class="btn btn-add btn-xs" title="تعديل ">
                                    <i class="fa fa-pencil"></i></button></a>
                                <a  href="<?php echo base_url().'admin/dashboard/DeleteCreateRole/'.$role->user_id?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');"> 
                                <button type="button" class="btn btn-danger btn-xs" ><i class="fa fa-trash-o"> </i> </button></a>
                            </td>
                        </tr>
                    
                    <?php 
                    $count++;
                    endforeach ;?>
                    </tbody>
                </table>
            <?php endif;?>


   </div> </div>
   

  
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	
