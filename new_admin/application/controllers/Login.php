<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller
{
public function  __construct(){
    parent::__construct();
}
 function  index(){
    
     if($this->session->has_userdata('is_logged_in')==1){
             redirect('admin/Dashboard');
     }
     $data['title']='تسجيل الدخول الى نظام الإدارة   ';
     $data['metakeyword']='نظام الإدارة';
     $data['metadiscription']='نظام الإدارة';
    $this->load->view('backend/users/login',$data);
 }
    public  function check_login(){
        $this->load->model('system_management/Users');
        $userdata=$this->Users->check_user_data();
        if($userdata !=''){
            $userdata['is_logged_in']=true;
            $this->session->set_userdata($userdata);
            redirect('admin/Dashboard');
        }else{
            $data['title']='تسجيل الدخول الى نظام الإدارة   ';
            $data['metakeyword']='نظام الإدارة';
            $data['metadiscription']='نظام الإدارة';
            $data['response']="خطأفى بيانات الادخال";
            $this->load->view('backend/users/login',$data);
        }
    }
    public function logout(){
        $this->load->model('system_management/Users');
         $this->Users->update_last_login($_SESSION['user_id']); 
        $this->session->sess_destroy();
        redirect('login','refresh');
    }

}