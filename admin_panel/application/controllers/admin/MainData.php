<?php
class MainData extends MY_Controller{
    public function __construct(){
        parent::__construct();
    }
    /**
     *  ================================================================================================================
     *
     *  ----------------------------------------------------------------------------------------------------------------
     *
     *  ================================================================================================================
     */

    public function index(){
        $this->load->model('admin/Model_main_data');

        $data["aparts"]   =$this->Model_main_data->select_counter("villa_aparts",'');     
$data["cars"]   =$this->Model_main_data->select_counter("cars",'');      
$data["clients"]   =$this->Model_main_data->select_counter("clients",''); 
$data["investors"]   =$this->Model_main_data->select_counter("investors",''); 

        $data['subview'] = 'backend/home';
        $this->load->view('admin_index', $data);
    }
	public function contact_us_menu(){
		
		    $this->load->model('admin/Model_main_data');
        $data["all_contacts"]=$this->Model_main_data->select_all_contacts();

//print_r($data["all_contacts"]);exit();
        $title = "قائمة اتصل بنا";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/main_data/contact_us';
        $this->load->view('admin_index', $data);
	}
	
	   public function DeleteContact($id){
        $this->load->model('admin/Model_main_data');
        //$Udata["user_show"] = 0;
        $this->Model_main_data->delete_contact($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/MainData/contact_us_menu", 'refresh');
    }
   
    /**
     * =====================================================================================================================
     *                                 البيانات الاساسية
     *
     */
    public  function AddMainData(){ //  MainData/AddMainData
        $this->load->model('admin/Model_main_data');
       // $this->load->library('google_maps');
        if ($this->input->post('INSERT')  == "INSERT") {
            $file["ar"]= $this->upload_image('company_logo');
            $this->Model_main_data->insert($file);
            redirect('admin/MainData/AddMainData', 'refresh');
        }
        //------------------------------------------------
        $this->load->library('google_maps');
        $config = array();
        $config['zoom'] = "auto";
        $config['center'] = '27.511410, 41.720825';//'auto';
        $config['onboundschanged'] = '  if (!centreGot) {
                                                var mapCentre = map.getCenter();
                                                marker_0.setOptions({
                                                    position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng()) 
                                                });
                                                $("#lat_map").val(mapCentre.lat());
                                                $("#lng_map").val(mapCentre.lng());
                                            }
                                            centreGot = true;';
        $config['geocodeCaching'] = TRUE;
        $marker = array();
        $marker['draggable'] = true;
        $marker['ondragend'] = '$("#lat_map").val(event.latLng.lat());$("#lng_map").val(event.latLng.lng());';
        $marker['title'] = 'أنت هنا .. من فضلك قم بسحب العلامة ووضعها على المكان الصحيح';
        $this->google_maps->initialize($config);
        $this->google_maps->add_marker($marker);
        $data['maps'] = $this->google_maps->create_map();
        //-------------------------------------------------
        $data["out_fild"]=$this->Model_main_data->get_field();
        $data["data_tables"]=$this->Model_main_data->select_all();
        $data['title']='البيانات الأساسية ';
        $data['metakeyword']='البيانات الأساسية ';
        $data['metadiscription']='البيانات الأساسية ';
        $data["my_footer"]=array("validator");
        $data['subview'] = 'admin/main_data/add_main_data';
        $this->load->view('admin_index', $data);
    }
    public  function UpadteMainData($id){
        $this->load->model('admin/Model_main_data');
        if ($this->input->post('UPDTATE')  == "UPDTATE") {
            $file["ar"]= $this->upload_image('company_logo');
            $this->Model_main_data->update($id,$file);
            $this->message('info','تم التعديل بنجاح ');
            redirect('admin/MainData/AddMainData', 'refresh');
        }
        //--------------------------------------
        $result_id=$data["result_id"]=$this->Model_main_data->getByArray(array("id"=>$id));
        $this->load->library('google_maps');
        $config = array();

        $marker = array();
        $marker['draggable'] = true;
        $marker['ondragend'] = '$("#lat").val(event.latLng.lat());$("#lng").val(event.latLng.lng());';
        $lat_long= $result_id['company_google_lat'].','.$result_id['company_google_long'];
        $chick_lat=$this->google_maps->is_lat_long($lat_long);
        if($chick_lat == true){
            $marker['position'] =$lat_long;
            $config['center'] =  $lat_long;//'auto';
            $config['zoom'] = "16";
        }else{
            $config['zoom'] = "auto";
            $config['center'] = '27.517571447136426,41.71273613027347';//'auto';
            $config['onboundschanged'] = '  if (!centreGot) {
                                                var mapCentre = map.getCenter();
                                                marker_0.setOptions({
                                                    position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng()) 
                                                });
                                                $("#lat").val(mapCentre.lat());
                                                $("#lng").val(mapCentre.lng());
                                            }
                                            centreGot = true;';
            $config['geocodeCaching'] = TRUE;
            $marker['title'] = 'أنت هنا .. من فضلك قم بسحب العلامة ووضعها على المكان الصحيح';
        }
        $marker['title'] = 'من فضلك قم بسحب العلامة ووضعها على المكان الصحيح';
        $this->google_maps->initialize($config);
        $this->google_maps->add_marker($marker);
        $data['maps'] = $this->google_maps->create_map();
        //--------------------------------------
        $data['title']='البيانات الأساسية ';
        $data['metakeyword']='البيانات الأساسية ';
        $data['metadiscription']='البيانات الأساسية ';
         $data["my_footer"]=array("validator");
        $data['subview'] = 'admin/main_data/edit_main_data';
        $this->load->view('admin_index', $data);
    }
    public  function DeleteMainData($id){
        $this->load->model('admin/Model_main_data');
        $this->Model_main_data->delete($id);
        $this->message('error','تم الحذف');
        redirect('admin/MainData/AddMainData', 'refresh');
    }

    /**   //  MainData/EncomingMessages
     * =====================================================================================================================
     *                              الرسائل الواردة
     *
     */
    public function EncomingMessages(){
        $this->load->model('admin/Model_contacts');
        $data["data_tables"]=$this->Model_contacts->select_where(array("contact_us.id !="=>0));
        $data['title']='رسائل التواصل';
        $data['metakeyword']='رسائل التواصل';
        $data['metadiscription']='رسائل التواصل';
        $data['my_footer']=array("edit_data_table");
        $data['subview'] = 'admin/contact/contact_us';
        $this->load->view('admin_index', $data);
    }

    public  function DeleteEncomingMessages($id){  // MainData/DeleteEncomingMessages
        $this->load->model('admin/Model_contacts');
        $this->Model_contacts->delete($id);
        $this->message('error','تم الحذف');
        redirect('admin/MainData/EncomingMessages', 'refresh');
    }
    
    public function SendReply($id){
        $this->load->model('admin/Model_contacts');
        $this->load->model('admin/Model_main_data');
           $this->load->helper('send_emails');
       //---------------------------------------
        /* if ($this->input->post('id')  == $id) {
            $this->Model_contacts->update_reply($id);
               $data_pass["from_mail"]="";
               $data_pass["to_mail"]=$this->input->post('email');
               $data_pass["subject"]="تم الرد على رسالتك ";
               $data_pass["type_content"]="";
               $data_pass["message"]="";
               $data_pass["data_load"]="";
             //  send_my_mail($data_pass);
            $this->message('wiring','تم ارسال الرد عن طريق البريد الالكترونى بنجاح ');  
            redirect('MainData/EncomingMessages', 'refresh');
         }*/
         redirect('admin/MainData/EncomingMessages', 'refresh');
    }
}  //END CLASS
?>