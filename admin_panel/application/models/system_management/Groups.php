<?php
class Groups extends  CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('system_management/Permission');
    }
    //---------------------------------------------------
    private function chek_Null($post_value){
        if($post_value == '' || $post_value==null || (!isset($post_value)) && !empty($post_value)){
            $val="";
            return $val;
        }else{
            return $post_value;
        }
    }
//---------------------------------------------------------
    public function select_order($group_id_fk){
        $this->db->select('group_id_fk , page_order');
        $this->db->from("pages");
        $this->db->where('group_id_fk',$group_id_fk);
        $this->db->order_by("page_order","DESC");
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data = $row->page_order;
            }
            return $data;
        }
        return 0;
    }
//---------------------------------------------------------
    public function fetch_groups($limit, $start) {
        $this->db->where('group_id_fk',0);
        $this->db->limit($limit, $start);
        $query = $this->db->get("pages");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
//-------------------------------------------------------
    public function addGroup($file){
        $file_in=0;
        if(isset($file) && !empty($file) &&   $file!=null && $file!=''){
            $file_in= $file;
        }
        $data = array(
            'page_title'=>  $this->chek_Null($this->input->post('page_title')),
            'page_order'=>  $this->chek_Null($this->input->post('page_order')),
            'page_link'=>   $this->chek_Null($this->input->post('page_link')),
            'page_icon_code'=> $this->chek_Null($this->input->post('page_icon_code')),
            'group_id_fk'=>0,
            'level'=>0,
            'page_image'=>$this->chek_Null($file_in));
        $this->db->insert('pages', $data);
    }
//-------------------------------------------------------
    public  function getgroupbyid($id){
        $query= $this->db->get_where('pages',array('page_id'=>$id));
        return $query->row_array();
    }
//----------------------------------------------------
    function updateGroup($id,$file){
        $data = array(
            'page_title'=>  $this->chek_Null($this->input->post('page_title')),
            'page_order'=>  $this->chek_Null($this->input->post('page_order')),
            'page_link'=>   $this->chek_Null($this->input->post('page_link')),
            'page_icon_code'=>$this->chek_Null($this->input->post('page_icon_code')));
        if(isset($file) && !empty($file) &&   $file!=null && $file!=''){
            $data['page_image']=  $file;
        }
        $this->db->where('page_id', $id);
        $this->db->update('pages', $data);
    }
    //------------------------------------------------
    public function level_groups() {
        $this->db->where('group_id_fk',0);
        $this->db->or_where('level',2);
        $query = $this->db->get("pages");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    //-----------------------------------------------------------
    public function get_categories(){
        $this->db->select('*');
        $this->db->from('pages');
        $this->db->where("group_id_fk",0);
        $this->db->order_by("page_order","ASC");
        $parent = $this->db->get();
        $categories = $parent->result();
        $i=0;
        foreach($categories as $p_cat){
            $categories[$i]->sub = $this->sub_categories($p_cat->page_id);
            $i++;
        }
        return $categories;
    }
//-----------------------------------------------------------
    public function sub_categories($id){
        $this->db->select('*');
        $this->db->from('pages');
        $this->db->where('group_id_fk', $id);
        $this->db->order_by("page_order","ASC");
        $child = $this->db->get();
        $categories = $child->result();
        $i=0;
         foreach($categories as $p_cat){
             $categories[$i]->sub = $this->sub_categories($p_cat->page_id);
             $i++;
         }
        return $categories;
    }
//-----------------------------------------------------------
    public  function get_count_level($id){
        $this->db->select('*');
        $this->db->from('pages');
        $this->db->where('group_id_fk', $id);
        $child = $this->db->get();
        $total=0;
        if ($child->num_rows() > 0){
            $categories = $child->result();
            foreach($categories as $p_cat){
                $total += $this->get_count_sub_level($p_cat->page_id);
            }
        }
        return $total;
    }
    public  function get_count_sub_level($id){
        $this->db->select('*');
        $this->db->from('pages');
        $this->db->where('group_id_fk', $id);
        $child = $this->db->get();
        $total=$child->num_rows();
        return $total;
    }
//---------------------------------------------------------
}// END CLASS
?>