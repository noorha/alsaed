<?php
class Model_thighs extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="thighs";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
    
    public  function update($id,$data){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
    public  function update_image($id,$data){
        $this->db->where("id",$id);
       
         //   $data['status']=1;
       
        $this->db->update($this->main_table,$data);
    }
    public  function update_accept($id){
        $this->db->where("id",$id);
       
            $data['status']=1;
       
        $this->db->update($this->main_table,$data);
    }
    public  function update_refuse($id){
        $this->db->where("id",$id);
       
            $data['status']=2;
       
        $this->db->update($this->main_table,$data);
    }
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
   
    
    public  function update_conditions($data){
        $this->db->where("id",1);
        $data['descr']=nl2br( $data['descr']);
        $this->db->update("conditions",$data);
    }
    public  function update_targets($data){
        $this->db->where("id",1);
        $data['descr']=nl2br( $data['descr']);
        $this->db->update("targets",$data);
    }
  
    public function get_all_thighs(){
        $this->db->select('thighs.* ');
        $this->db->from("thighs");
        $this->db->order_by("id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
   
   
    public  function insert($data){ 
      $this->db->insert("thighs",$data);
   return $this->db->insert_id();
    }
  
 
  
}  