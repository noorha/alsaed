<?php
class Model_award extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->main_table="awards";
     
        
    }
   

    public  function insert($data){
        $this->db->insert($this->main_table,$data);
        return $this->db->insert_id();
        //return $this->db->insert_id();
    }
    //==========================================
    public  function update($id,$data){
        $this->db->where("id",$id);
        $this->db->update($this->main_table,$data);
    }
    
    
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }

    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }

    public function select_where($Conditions_arr){
        $this->db->select('*');
        $this->db->from($this->main_table);
        $this->db->where($Conditions_arr);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }
  
    public function select_all_cats(){
        $this->db->select('awards.* ');
        $this->db->from("awards");
      $this->db->order_by("id","asc");
          $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }

    public function table_truncate (){
        $this->db->from($this->main_table);
        $this->db->truncate();
    }

}//END CLASS


