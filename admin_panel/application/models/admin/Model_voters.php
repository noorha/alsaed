<?php
class Model_voters extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="voters";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
    public function getByArray_phone($arr){
        $h = $this->db->get_where('phone_book',$arr);
        return $h->row_array();
    }
    public function getByArray_task($arr){
        $h = $this->db->get_where('tasks',$arr);
        return $h->row_array();
    }
    public function getByArray_friend($arr){
        $h = $this->db->get_where('friends',$arr);
        return $h->row_array();
    }
    public function getByArray_note($arr){
        $h = $this->db->get_where('notes',$arr);
        return $h->row_array();
    }
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
    
    public  function increase_diwan_counter($id)
    
    {
        $this->db->select('diwans.* ');
        $this->db->from("diwans");
        $this->db->where("id",$id);
        $query = $this->db->get();
        $query=$query->result();
        $loc_counter=$query[0]->loc_counter;
        
        $this->db->where("diwans.id",$id);
        $current_date=date("Y/m/d");
        $data['diwans.loc_counter']=$loc_counter+1;
        $data['diwans.loc_date']=$current_date;
        $this->db->update("diwans",$data);
          $this->insert_visit($id);
    }
     public function insert_visit($id){
       date_default_timezone_set('Asia/Kuwait');
               //print_r( date('Y/m/d H:i'));exit();
                 $current_dat = date('Y/m/d');
                 $current_time = date('H:i');
                $data['visit_date']=$current_dat;
                $data['visit_time']=$current_time;
                $data['diwan_id']=$id;
                  $set_data = $this->session->all_userdata();
                $data['user_id']=$set_data['user_id'];
                 $this->db->insert("diwan_visits",$data);  
    }
     public  function edit_last_visit_using_task($id,$task_id)
    
    {
        $this->db->select('voters.* ');
        $this->db->from("voters");
        $this->db->where("id",$id);
        $query = $this->db->get();
        $query=$query->result();
        $last_visit=$query[0]->last_visit;
        
        $this->db->where("voters.id",$id);
        $current_date=date("Y/m/d");
        $data['voters.last_visit']=$current_date;
        $this->db->update("voters",$data);
        
         $this->db->where("tasks.id",$task_id);
        $data1['status']=1;
        $this->db->update('tasks',$data1);
        
    }
    public  function increase_counter_using_task($id,$task_id)
    
    {
        $this->db->select('voters.* ');
        $this->db->from("voters");
        $this->db->where("id",$id);
        $query = $this->db->get();
        $query=$query->result();
        $phone_counter=$query[0]->phone_counter;
        $last_call=$query[0]->last_call;
        
        $this->db->where("voters.id",$id);
        $current_date=date("Y/m/d");
        $data['voters.phone_counter']=$phone_counter+1;
        $data['voters.last_call']=$current_date;
        
        $this->db->update("voters",$data);
        
         $this->db->where("tasks.id",$task_id);
        $data1['status']=1;
        $this->db->update('tasks',$data1);
    }
      public  function get_all_visits($voter_id){
      
            $this->db->select('voter_visits.*,voters.fname,voters.sname,voters.tname,voters.foname,admin_voters.user_name ');
        $this->db->from("voter_visits");
          $this->db->join('voters' , 'voters.id = voter_visits.voter_id',"inner");
        $this->db->join('admin_voters' , 'admin_voters.user_id = voter_visits.user_id',"inner");
        $this->db->where('voter_visits.voter_id', $voter_id);
        $this->db->order_by("voter_visits.id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
     }
     public  function get_all_calls($voter_id){
         
            $this->db->select('voter_calls.*,voters.fname,voters.sname,voters.tname,voters.foname,admin_voters.user_name ');
        $this->db->from("voter_calls");
        $this->db->join('voters' , 'voters.id = voter_calls.voter_id',"inner");
        $this->db->join('admin_voters' , 'admin_voters.user_id = voter_calls.user_id',"inner");

        $this->db->where('voter_calls.voter_id', $voter_id);

        $this->db->order_by("voter_calls.id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
     }
    public  function edit_last_visit($id)
    
    {
        $this->db->select('voters.* ');
        $this->db->from("voters");
        $this->db->where("id",$id);
        $query = $this->db->get();
        $query=$query->result();
        $last_visit=$query[0]->visit_counter;
        
        $this->db->where("voters.id",$id);
        $current_date=date("Y/m/d");
        $data['visit_counter']=$last_visit+1;
        $data['voters.last_visit']=$current_date;
        $this->db->update("voters",$data);
        
              $this->insert_visit_voter($id);
               
    }
   public function insert_visit_voter($id){
       date_default_timezone_set('Asia/Kuwait');
               //print_r( date('Y/m/d H:i'));exit();
                 $current_dat = date('Y/m/d');
                 $current_time = date('H:i');
                $data['visit_date']=$current_dat;
                $data['visit_time']=$current_time;
                $data['voter_id']=$id;
                  $set_data = $this->session->all_userdata();
                $data['user_id']=$set_data['user_id'];
                 $this->db->insert("voter_visits",$data);  
    }
    public  function increase_counter($id)
    
    {
        $this->db->select('voters.* ');
        $this->db->from("voters");
        $this->db->where("id",$id);
        $query = $this->db->get();
        $query=$query->result();
        $phone_counter=$query[0]->phone_counter;
        $last_call=$query[0]->last_call;
        
        $this->db->where("voters.id",$id);
        $current_date=date("Y/m/d");
        $data['voters.phone_counter']=$phone_counter+1;
        $data['voters.last_call']=$current_date;
        
        $this->db->update("voters",$data);
        
          $this->insert_phone_voter($id);
    }
    public function insert_phone_voter($id){
       date_default_timezone_set('Asia/Kuwait');
               //print_r( date('Y/m/d H:i'));exit();
                 $current_dat = date('Y/m/d');
                 $current_time = date('H:i');
                  $set_data = $this->session->all_userdata();
                $data['user_id']=$set_data['user_id'];
                $data['call_date']=$current_dat;
                $data['call_time']=$current_time;
                $data['voter_id']=$id;
                
                 $this->db->insert("voter_calls",$data);  
    }
   
    public  function check_voter($id)

    {
        $set_data = $this->session->all_userdata();
        $user_id=$set_data['user_id'];

        $this->db->select('friends.*');
        $this->db->from("friends");
        $this->db->where('friends.voter_id', $id);
        $this->db->where('friends.user_id', $user_id);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    } 
    public  function fetch_diwan_withId($id)

    {

        $this->db->select('diwans.*');
        $this->db->from("diwans");
        $this->db->where('diwans.id', $id);

        $this->db->order_by("diwans.id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    } 
    public  function fetch_voter_withId($id)

    {

        $this->db->select('voters.*,thighs.name as thigh_name ');
        $this->db->from("voters");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->where('voters.id', $id);

        $this->db->order_by("voters.id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    } 
    public  function fetch_voter($kashf_num)

    {

        $this->db->select('voters.*,thighs.name as thigh_name ');
        $this->db->from("voters");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->where('voters.kashf_num', $kashf_num);

        $this->db->order_by("voters.id","asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    } 

    
    public  function update_task($id,$data){
        $this->db->where("id",$id);
        //print_r($id);exit();
        $this->db->update('tasks',$data);
    }
    public  function convert_to_archief_task($id){
        $this->db->where("id",$id);
        $data['status']=2;
        $this->db->update('tasks',$data);
    }
    
    public  function update_phone($id,$data){
        $this->db->where("id",$id);   
        //print_r($id);exit();    
        $this->db->update('phone_book',$data);
    }
    
    public  function update($id,$data){
        $this->db->where("id",$id);   
        //print_r($id);exit();    
        $this->db->update($this->main_table,$data);
    }
    public  function update_note($id,$data){
        $this->db->where("id",$id);       
        $this->db->update('notes',$data);
    }
    public  function update_friend($id,$data){
        $this->db->where("id",$id);       
        $this->db->update('friends',$data);
    }
    
    public  function update_image($id,$data){
        $this->db->where("id",$id);
       
         //   $data['status']=1;
       
        $this->db->update($this->main_table,$data);
    }
    public  function update_accept($id){
        $this->db->where("id",$id);
       
            $data['status']=1;
       
        $this->db->update($this->main_table,$data);
    }
    
    public  function update_voter_msg($data,$id){
        $this->db->where("id",$id);
       
        $this->db->update($this->main_table,$data);
    }
    public  function update_refuse($id){
        $this->db->where("id",$id);
       
            $data['status']=2;
       
        $this->db->update($this->main_table,$data);
    }
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
    public function delete_merge($id){
        $this->db->where("voter_id",$id);
        $this->db->delete('merges');
    }
    public function delete_task($id){
        $this->db->where("id",$id);
        $this->db->delete('tasks');
    }
    public function delete_exclude($id){
        $this->db->where("voter_id",$id);
        $this->db->delete('excludes');
    }
    public function delete_phone($id){
        $this->db->where("id",$id);
        $this->db->delete('phone_book');
    }
    public function delete_friend($id){
        $this->db->where("id",$id);
        $this->db->delete('friends');
    }
    public function delete_note($id){
        $this->db->where("id",$id);
        $this->db->delete('notes');
    } 
    public  function update_conditions($data){
        $this->db->where("id",1);
        $data['descr']=nl2br( $data['descr']);
        $this->db->update("conditions",$data);
    }
    public  function update_targets($data){
        $this->db->where("id",1);
        $data['descr']=nl2br( $data['descr']);
        $this->db->update("targets",$data);
    }
    public function get_status_friends($status){
        $this->db->select('friends.*,thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname ');
        $this->db->from("friends");
        $this->db->join('voters' , 'voters.id = friends.voter_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->where("friends.status",$status);
        $this->db->group_by('friends.voter_id'); 

        $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc,thigh_name asc'); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_my_friends($user_id){
        $this->db->select('friends.*,thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname ');
        $this->db->from("friends");
        $this->db->join('voters' , 'voters.id = friends.voter_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->where("friends.user_id",$user_id);

        $this->db->group_by('friends.voter_id'); 

        $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc, thigh_name asc'); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }  
    public function get_all_friends(){

        $set_data = $this->session->all_userdata();
        $user_id=$set_data['user_id'];

        $this->db->select('permissions.*');
        $this->db->from("permissions");
        $this->db->where("user_id",$user_id);       
        $this->db->where("page_id_fk",141);  
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
             $res='1';
        }   
        else {  $res='0'; }


        $this->db->select('friends.*,thighs.name as thigh_name,voters.friend_msg1,voters.friend_msg2,voters.friend_msg3,voters.fname,voters.sname,voters.tname,voters.foname ');
        $this->db->from("friends");
        if($res == 0){
            $this->db->where("user_id",$user_id);       

        }
        $this->db->join('voters' , 'voters.id = friends.voter_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        
        $this->db->group_by('friends.voter_id'); 
        $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, thigh_name asc'); 
       
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_all_friend_attends(){
        $this->db->select('friends.voter_id,friends.note,friends.user_id,friends.status,voters.*');
        $this->db->from("friends");
        $this->db->join('voters' , 'voters.id = friends.voter_id',"inner");

      $this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");

         $this->db->where('attendance.attend',1);
         $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc'); 
         $this->db->group_by('friends.voter_id'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_attends($id){
        $this->db->select('voters.*,thighs.name as thigh_name ');
        $this->db->from("voters");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
        $this->db->where('voters.thigh_id',$id);

         $this->db->where('attendance.attend',1);
         $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc, thigh_name asc'); 
         $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_user_operations($user_id){
       
        
        $this->db->select('operations.*,admin_voters.user_name');
        $this->db->from("operations");
           $this->db->where('operations.user_id',$user_id);
        $this->db->join('admin_voters' , 'admin_voters.user_id = operations.user_id',"inner");
         $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_notifications(){
        // $this->db->where("id",1);
        //$data['descr']=nl2br( $data['descr']);
       $data['status']=1;
        $this->db->update("operations",$data);
        $this->db->select('operations.*,admin_voters.user_name');
        $this->db->from("operations");
        $this->db->join('admin_voters' , 'admin_voters.user_id = operations.user_id',"inner");
                       $this->db->order_by('operations.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_all_total($id){
        $this->db->select('voters.*,thighs.name as thigh_name ');
        $this->db->from("voters");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
     
        $this->db->where('voters.thigh_id',$id);
         $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc, thigh_name asc'); 
         $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_all_total_friends($id){
        $this->db->select('friends.*,voters.thigh_id,voters.sname,voters.fname
        ,voters.tname,voters.foname,voters.fvname,thighs.name as thigh_name ');
$this->db->from('friends');
$this->db->join('voters' , 'voters.id = friends.voter_id',"inner");
$this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
$this->db->where('voters.thigh_id',$id);
$this->db->group_by('friends.voter_id'); 
$this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc, thigh_name asc'); 
$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_cancel($id){
        $this->db->select('voters.*,thighs.name as thigh_name ,attendance.reason1');
        $this->db->from("voters");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
        $this->db->where('voters.thigh_id',$id);

         $this->db->where('attendance.attend',0);
         $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc, thigh_name asc'); 
         $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_not_attends2($id){
       
        $this->db->select('voters.*,thighs.name as thigh_name ');
        $this->db->from("voters");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        if($id!=null){
            $this->db->where('voters.thigh_id',$id);
        }

        $this->db->where('kashf_num NOT IN( SELECT kashf_num FROM attendance)');
        $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc, thigh_name asc'); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function update_msg($id){
        $msg = $this->input->post('phone_msg', true);
        $data = array('phone_msg'=> $msg);
        $this->db->where('id', $id);
        if ($this->db->update('voters', $data)) {
            return true;
        } else {
            return false;
        }
    }
    public function get_all_not_attends($id){
        $this->db->select('voters.*,thighs.name as thigh_name ');
        $this->db->from("voters");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
        if($id!=null){
            $this->db->where('voters.thigh_id',$id);
        }
         $this->db->where('attendance.attend',null);
         $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc, thigh_name asc'); 
         $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_phone_numbers(){
        $this->db->select('phone_book.*');
        $this->db->from("phone_book");
        $this->db->order_by('phone_book.id desc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_all_archief_tasks(){
            
            $set_data = $this->session->all_userdata();
         $user_id=$set_data['user_id'];
         
        $this->db->select('permissions.*');
         $this->db->from("permissions");
         $this->db->where("user_id",$user_id);       
         $this->db->where("page_id_fk",164);  
         $query = $this->db->get();
         if ($query->num_rows() == 0) { 
             $get_only=1;
         }
         
        $this->db->select('tasks.*');
        $this->db->from("tasks");
         if($get_only==1){
             $this->db->where("tasks.user_id",$user_id);  
          }
          
        $this->db->where("tasks.status",2);
           $this->db->or_where("tasks.status",1);
        $this->db->order_by('tasks.id desc');
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
     public function get_all_my_tasks(){
           $set_data = $this->session->all_userdata();
$user_id=$set_data['user_id'];
        $this->db->select('tasks.*');
        $this->db->from("tasks");
        $this->db->where("tasks.status",0);
        $this->db->where("tasks.user_id",$user_id);       

        $this->db->order_by('tasks.id desc');
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_tasks(){
        $this->db->select('tasks.*');
        $this->db->from("tasks");
        $this->db->where("tasks.status",0);
        $this->db->order_by('tasks.id desc');
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_all_voters_merges($main_id){
        $this->db->select('merges.voter_id,merges.main_voter_id,voters.*,thighs.name as thigh_name,jobs.name as job_name ');
        $this->db->from("merges");
        $this->db->where("merges.main_voter_id",$main_id);
        $this->db->join('voters' , 'voters.id = merges.voter_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->join('jobs' , 'jobs.id = voters.job_id',"inner");
      $this->db->order_by("thigh_id asc , fvname asc ,voters.foname asc,voters.tname asc, voters.sname asc, birth_date asc ");


        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_voters_excludes(){
        $this->db->select('excludes.voter_id,voters.*,thighs.name as thigh_name,jobs.name as job_name ');
        $this->db->from("excludes");
        $this->db->join('voters' , 'voters.id = excludes.voter_id',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->join('jobs' , 'jobs.id = voters.job_id',"inner");
      $this->db->order_by("thigh_id asc , fvname asc ,voters.foname asc,voters.tname asc, voters.sname asc, birth_date asc ");


        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
     public function get_name($user_id){
        $this->db->select('admin_voters.*');
        $this->db->from("admin_voters");
        $this->db->where('admin_voters.user_id', $user_id);
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            
            return $query->result() ;
        }
        return false;
    }
    public function get_phone_info($phone_id){
        $this->db->select('phone_book.*');
        $this->db->from("phone_book");
        $this->db->where('phone_book.id', $phone_id);
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            
            return $query->result() ;
        }
        return false;
    }
    
    public function check_merge($cart){
        $this->db->select('merges.*');
        $this->db->from("merges");
        $this->db->where_in('merges.main_voter_id', $cart);
        $this->db->or_where_in('merges.main_voter_id', $cart);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
         //   print_r( $query->result());exit();

            return $query->result() ;
        }
        return false;
    }
   
     public function get_all_voters_with_ids($cart){
        $selected_rows = explode(',', $cart);
//   print_r($selected_rows);exit();
        $this->db->select('voters.*,thighs.name as thigh_name,jobs.name as job_name ');
        $this->db->from("voters");
        $this->db->where_in('voters.id', $selected_rows);
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->join('jobs' , 'jobs.id = voters.job_id',"inner");
        $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc, thigh_name asc'); 


        $query = $this->db->get();
        if ($query->num_rows() > 0) {
         //   print_r( $query->result());exit();

            return $query->result() ;
        }
        return false;
    }
   
    public function get_all_voters_comms(){
        $this->db->select('voters.*,thighs.name as thigh_name,jobs.name as job_name ');
        $this->db->from("voters");
        $this->db->where("voters.id NOT IN (select excludes.voter_id  from excludes)");

        $this->db->join('excludes', 'voters.id != excludes.voter_id', 'left');
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->join('jobs' , 'jobs.id = voters.job_id',"inner");
                   $this->db->order_by("thigh_id asc , fvname asc ,voters.foname asc,voters.tname asc, voters.sname asc, birth_date asc ");

       

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_voters(){
        $this->db->select('voters.*,thighs.name as thigh_name,jobs.name as job_name ');
        $this->db->from("voters");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $this->db->join('jobs' , 'jobs.id = voters.job_id',"inner");
      ///  $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc, thighs.name asc');          
    //    $this->db->order_by("voters.fvname Desc,voters.foname Desc,voters.tname Desc,voters.sname Desc");
      $this->db->order_by("thigh_id asc , fvname asc ,voters.foname asc,voters.tname asc, voters.sname asc, birth_date asc ");
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    
    public function get_all_notAttend_friends(){
   
        $set_data = $this->session->all_userdata();
        $user_id=$set_data['user_id'];

        $this->db->select('permissions.*');
        $this->db->from("permissions");
        $this->db->where("user_id",$user_id);       
        $this->db->where("page_id_fk",141);  
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
             $res='1';
        }   
        else {  $res='0'; }


        $this->db->select('friends.voter_id,friends.note,friends.user_id,friends.status,voters.*');
        $this->db->from("friends");
        $this->db->join('voters' , 'voters.id = friends.voter_id',"inner");
        $this->db->join('attendance' , 'attendance.kashf_num = voters.kashf_num',"inner");
        if($res=='0'){
            $this->db->where('friends.user_id',$user_id);
         }
        $this->db->where('attendance.attend',null);
        $this->db->group_by('friends.voter_id'); 
                $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }  
    public function get_all_notAttend_friends2(){
        $set_data = $this->session->all_userdata();
        $user_id=$set_data['user_id'];

        $this->db->select('permissions.*');
        $this->db->from("permissions");
        $this->db->where("user_id",$user_id);       
        $this->db->where("page_id_fk",141);  
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
             $res='1';
        }   
        else {  $res='0'; }

        $this->db->select('friends.voter_id,friends.note,friends.user_id,friends.status,voters.*');
        $this->db->from("friends");
        $this->db->join('voters' , 'voters.id = friends.voter_id',"inner");
         if($res=='0'){
            $this->db->where('friends.user_id',$user_id);
         }
        $this->db->where('kashf_num NOT IN( SELECT kashf_num FROM attendance)');
        $this->db->group_by('friends.voter_id'); 
        $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, voters.fvname asc'); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    } 
    public function get_all_friend_notes($id){
        $this->db->select('friends.*,admin_voters.user_name as username');
        $this->db->from("friends");
         $this->db->join('admin_voters' , 'admin_voters.user_id = friends.user_id',"inner");

        $this->db->where('voter_id',$id);
        $this->db->where('friends.note !=',null);
        $this->db->where('friends.note !=','');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_notes($id){
        $this->db->select('notes.*,admin_voters.user_name as username');
        $this->db->from("notes");
        $this->db->join('admin_voters' , 'admin_voters.user_id = notes.user_id',"inner");

        $this->db->where('voter_id',$id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public function get_all_statistics(){
        $this->db->select(' thighs.*');
        $this->db->from("thighs");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
    public  function insert($data){ 
      $this->db->insert("voters",$data);
   return $this->db->insert_id();
    }
       
    public  function insert_operation($data){ 
  
        $this->db->insert("operations",$data);
     return $this->db->insert_id();
      }
    public  function insert_notes($data){ 
        $set_data = $this->session->all_userdata();
$data['user_id']=$set_data['user_id'];
        $this->db->insert("notes",$data);
     return $this->db->insert_id();
      }
    public  function insert_task($data){
        $this->db->insert("tasks",$data);
        $data1['task_id']=$this->db->insert_id();
        $data1['voter_id']=$data['voter_id'];
      $set_data = $this->session->all_userdata();
$data1['user_id']=$set_data['user_id'];
        $this->db->insert("notes",$data1);
        return  $data1['task_id'];
    }
    
    public  function insert_excludes($data){ 
        $this->db->insert("excludes",$data);
     return $this->db->insert_id();
      }
    public  function insert_merges($data){ 
        $this->db->insert("merges",$data);
     return $this->db->insert_id();
      }
    public  function insert_phone($data){ 
        $this->db->insert("phone_book",$data);
     return $this->db->insert_id();
      }
    public  function insert_friend($data){ 
        $this->db->insert("friends",$data);
     return $this->db->insert_id();
      }

  
}  