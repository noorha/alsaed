<?php
class Model_attendance extends CI_Model{
    public function __construct()
    {
        parent:: __construct();
        $this->main_table="attendance";
		
    }
    public function get_field(){
        $query = $this->db->query("select * from ".$this->main_table);
        $field_array = $query->list_fields();
        return $field_array;
    }
    public function getByArray($arr){
        $h = $this->db->get_where($this->main_table,$arr);
        return $h->row_array();
    }
    
    public  function update($id,$data){
        $this->db->where("id",$id);       
        $this->db->update($this->main_table,$data);
    }
    public  function update_image($id,$data){
        $this->db->where("id",$id);
       
         //   $data['status']=1;
       
        $this->db->update($this->main_table,$data);
    }
    public  function update_accept($id){
        $this->db->where("id",$id);
       
            $data['status']=1;
       
        $this->db->update($this->main_table,$data);
    }
    public  function update_refuse($id){
        $this->db->where("id",$id);
       
            $data['status']=2;
       
        $this->db->update($this->main_table,$data);
    }
    //==========================================
    public function delete($id){
        $this->db->where("id",$id);
        $this->db->delete($this->main_table);
    }
   
    
    public  function update_conditions($data){
        $this->db->where("id",1);
        $data['descr']=nl2br( $data['descr']);
        $this->db->update("conditions",$data);
    }
    public  function update_targets($data){
        $this->db->where("id",1);
        $data['descr']=nl2br( $data['descr']);
        $this->db->update("targets",$data);
    }
  
    public function get_all_attends(){
        $this->db->select('attendance.*,thighs.name as thigh_name,voters.kashf_num as v_num,voters.fname,voters.sname,voters.tname,voters.foname ');
        $this->db->from("attendance");
        $this->db->join('voters' , 'voters.kashf_num = attendance.kashf_num',"inner");
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");

        $this->db->order_by('voters.sname asc, voters.tname asc,voters.foname asc, thigh_name asc'); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result() ;
        }
        return false;
    }
   
   
    public  function insert($data){ 
      $this->db->insert("attendance",$data);
   return $this->db->insert_id();
    }
  
 
  
}  