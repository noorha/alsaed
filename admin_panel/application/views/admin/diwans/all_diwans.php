						<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>	
<script>
function editData(id,voter_id)
{
	if(voter_id==null){

		  $.ajax({
			  
				url:"<?php echo base_url(); ?>admin/dashboard/increase_diwan_counter",

				method:"POST",

				data:{diwan_id:id},

				success:function(data)

				{
				 // var result_id = $('#v_id').val();
				  console.log(data);

				}

				  });
				
		}
	else if(voter_id !=null && voter_id !=0){
		  $.ajax({
				url:"<?php echo base_url(); ?>admin/dashboard/edit_last_visit",

				method:"POST",

				data:{voter_id:voter_id},

				success:function(data)

				{
				 // var result_id = $('#v_id').val();
				  console.log(data);

				}

				  });
		}
	
}


function editphone(id)
{
	  $.ajax({
	url:"<?php echo base_url(); ?>admin/dashboard/increase_phone_counter_diwan",

	method:"POST",

	data:{diwan_id:id},

	success:function(data)

	{
	 // var result_id = $('#v_id').val();
	  console.log(data);

	}

	  });
	
}


</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	<h5>				
					<button class="btn btn-primary" >
					  <a  href="<?=base_url()."admin/dashboard/add_diwan/"?>"  style="font-weight: bold;color:black;">اضافة دوانية</a>


  </button>
		<button class="btn btn-primary" >
		<a  href="<?=base_url()."admin/dashboard/areas/"?>"  style="font-weight: bold;color:black;">المناطق</a>


  </button>
    <br/>
  <br/>
  <form id="form_filter"  role="form" action="<?php echo site_url('admin/dashboard/search_result/'.$d_type);?>" method="post">
 <div class="col-sm-12 row form-group">

          
        
  <?php if(isset($all_areas) && !empty($all_areas)) { ?>
  <div class=" col-sm-6">
    <select class="form-control" name="area" id="area" >

<option value="all">كل المناطق </option>

<?php if(isset($all_areas) && !empty($all_areas) && $all_areas!=null){

foreach ($all_areas as $one_area):

$select="";
if($one_area->id == $area_result){$select='selected="selected"';}?>

<option  value="<?php echo $one_area->id?>" <?php echo  $select?>>

<?php echo $one_area->name?></option>

<?php endforeach; 



}?>

</select>
</div>
 <?php } ?>
 <?php if($page=='all_d'){?>
 <div class=" col-sm-6">
 <select class="form-control" name="diwan_types" id="diwan_types">

<option value="all" >    فلتر حسب نوع الديوانية   </option>


<option  value="all"   <?php echo ($type_result== 'all' ? 'selected' : null); ?>>كل الديوانيات </option>
<option  value="0"  <?php echo ($type_result== '0' ? 'selected' : null); ?>>الديوانيات الأسبوعية </option>
<option  value="1"  <?php echo ($type_result== '1' ? 'selected' : null); ?>>الديوانيات اليومية </option>
<option  value="2"  <?php echo ($type_result== '2' ? 'selected' : null); ?>>الديوانيات غير الرسمية </option>


</select>

</div>
<?php } ?>

 <?php if(isset($d_type) && $d_type==0 && $page !='all_d'){?>
 <div class=" col-sm-6">
 <select class="form-control" name="day_type" id="day_type" >

<option value="all" >    فلتر حسب اليوم  </option>


<option  value="all"   <?php echo ($day_result== 'all' ? 'selected' : null); ?>>كل الأيام </option>
<option  value="0"  <?php echo ($day_result== '0' ? 'selected' : null); ?>>السبت </option>
<option  value="1"  <?php echo ($day_result== '1' ? 'selected' : null); ?>>الأحد </option>
<option  value="2"  <?php echo ($day_result== '2' ? 'selected' : null); ?>>الأثنين </option>
<option  value="3"  <?php echo ($day_result== '3' ? 'selected' : null); ?>>الثلاثاء </option>
<option  value="4"  <?php echo ($day_result== '4' ? 'selected' : null); ?>>الاربعاء </option>
<option  value="5"  <?php echo ($day_result== '5' ? 'selected' : null); ?>>الخميس </option>
<option  value="6"  <?php echo ($day_result== '6' ? 'selected' : null); ?>>الجمعة </option>

</select>

</div>
<?php } ?>

</div>

<input type="text" name="page"  value="<?php echo $page; ?>"   style="display:none;"  >

<button type="submit" class="btn btn-default" style="background: #2785c7;margin-right: 15px;">عرض النتائج</button>


</form>

 
  </h5>
					
					<!-- /.dropdown js__dropdown -->
	    
    
<?php 
     if(isset($all_diwans) && !empty($all_diwans)){
        
    ?> 
    		
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
   <th>اسم الدوانية</th>
      
        <th>العنوان</th>
        <th>الهاتف</th>
        <th>نوع الدوانية</th>
        <th>وقت الديوانية</th>
        <th>اللوكيشن</th>

        

        <th> عمليات</th>
			</thead>
						<tfoot>
				 <th>م</th>
      <th>اسم الدوانية</th>
     

        <th>العنوان</th>
        <th>الهاتف</th>
        <th>نوع الدوانية</th>
       <th>وقت الديوانية</th>
        <th>اللوكيشن</th>

    

        <th> عمليات</th>
				</tfoot>
					  <tbody>
   <?php $i=1; foreach ($all_diwans as $row ){ ?>
     <tr>
     
    
    <?php if($row->voter_id !=null && $row->voter_id !=0){
    $this->db->select('voters.*');
    $this->db->from('voters');
    $this->db->where('voters.id',$row->voter_id);
    $query=$this->db->get();
    $query=$query->result_array();
    $counter=$query[0];
    $phone_counter=$counter['phone_counter'];
    }
    else {
        $phone_counter=$row->phone_counter;
    }
    ?>

        <td> <?php echo $i;?> </td>
       
        <td><?php echo $row->name;?></td>

        <td><?php echo $row->area_name.' - '.$row->address;?></td>
        
        <td style="font-size: small;">                <a  class="btn btn-primary"  href="<?php echo"tel:". $row->phone;?>" onClick="editphone(<?php echo $row->id;?>)" >اتصال</a>
            <?php       if($phone_counter !=null ){ ?>
           <br/>
           <span ><?php echo $row->phone_date ;?></span>
           <br/>
            <a  href="<?=base_url()."admin/dashboard/diwan_calls/".$row->id;?>"  ><?php echo '('.$phone_counter.')' ;?></a>
      <?php }?>
      
         </td> 
        <?php if( $row->diwan_type==0){ 
            if($row->day_type==0){
                $type='اسبوعية (السبت)';
            }
            else if($row->day_type==1){
                $type='اسبوعية (الأحد)';
            }
            else if($row->day_type==2){
                $type='اسبوعية (الأثنين)';
            }
            else if($row->day_type==3){
                $type='اسبوعية (الثلاثاء)';
            }
            else if($row->day_type==4){
                $type='اسبوعية (الأربعاء)';
            }
            else if($row->day_type==5){
                $type='اسبوعية (الخميس)';
            }
            else if($row->day_type==6){
                $type='اسبوعية (الجمعة)';
            }
              
        } else if( $row->diwan_type==1){ 
        $type='يومية';
  } if( $row->diwan_type==2){ 
  $type='غير رسمية';
 } ?>
    <?php if( $row->diwan_time==0){ 
          $tim='صباحية';
   } else if( $row->diwan_time==1){ 
   $tim='مسائية';
  }  else if( $row->diwan_time==2){ 
    $tim='غير محدد';
   }  else if( $row->diwan_time==3){ 
    $tim='صباحية ومسائية';
   }?>
      
        <td>   <span ><?php echo $type ;?></span>
       
       
         <?php if($row->coor_name !=null && $row->coor_phone !=null){ ?>
          <br/>
          <span ><?php echo 'اسم المنسق:'.$row->coor_name ;?></span>
           <br/>
          <span ><?php echo 'هاتف المنسق:'.$row->coor_phone ;?></span>
         <?php } ?> 
        </td>
     
          <td ><?php echo 'الوقت:'.$tim ;?></td>
        <?php
        if($row->location==null){
            $up_url="#";
        }else {
            $up_url=$row->location; } ?>
      <td style="font-size: small;">  <a   class="btn btn-primary" href="<?=$up_url?>" target="_blank" onClick="editData(<?php echo $row->id?>,<?php echo $row->voter_id?>)">التوجه إلى الديوانية</a> 
     
     
       <?php    if($row->voter_id !=null){ ?>
           
         
<?php     $this->db->select('voters.*');
    $this->db->from('voters');
    $this->db->where('voters.id',$row->voter_id);
    $query=$this->db->get();
    $query=$query->result_array();
    $counter=$query[0];
    $last_visit=$counter['last_visit'];
    if($last_visit !=null){ ?>
     <br/>
     <span ><?php echo $last_visit ;?></span>
      
       <?php }  }  else if($row->loc_counter !=null && $row->loc_date !=null){ ?>
           <br/>
           <span ><?php echo $row->loc_date ;?></span>
           <br/>
                       <a  href="<?=base_url()."admin/dashboard/diwan_visits/".$row->id;?>"  ><?php echo '('.$row->loc_counter.')' ;?></a>

        
      <?php } ?>
      
      </td>
      


        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       $up_url=base_url()."admin/dashboard/UpdateDiwan/".$row->id;
                       $delete_url=base_url()."admin/dashboard/DeleteDiwan/".$row->id;
                 ?>
                            <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a><br/>
                           <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
    </tbody>
  </table>
					</table>
					</div>
	 <?php } else{
   
 echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد دوانيات!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	