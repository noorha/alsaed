

<!-- ============================================================== -->
<div class="row el-element-overlay ">
<div class="container">
  <h2>قائمة التواصل</h2>
 
  <style>
#tbl_1 {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#tbl_1 td, #tbl_1 th {
  border: 1px solid #ddd;
  padding: 8px;
}

#tbl_1 tr:nth-child(even){background-color: #f2f2f2;}

#tbl_1 tr:hover {background-color: #ddd;}

#tbl_1 th {
  padding-top: 12px;
  padding-bottom: 12px;
 
  background-color: blueviolet;
  color: white;
}
</style>
<?php 
     if(isset($all_contacts) && !empty($all_contacts)){
        
    ?> 
            <table id="myTable" class="table table-bordered table-striped">
    <thead>
      <tr>
      <th>#</th>
     
        <th>الاسم</th>
        <th>رقم الهاتف</th>
        
        <th>البريد الالكترونى</th>
        <th>نص الرسالة</th>
        <th> عمليات</th>
      </tr>
    </thead>
    <tbody>
   <?php $i=1; foreach ($all_contacts as $row ){ ?>
     <tr>
     
        <td> <?php echo $i;?> </td>
        <td><?php echo $row->user_name;?></td>
        <td><?php echo $row->phone;?></td>
       
        <td><?php echo $row->email;?></td>
		 <td><?php echo $row->msg_details;?></td>
        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       
                      
                       $delete_url=base_url()."admin/MainData/DeleteContact/".$row->id;
                 ?>
                         
                            <li><a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a></li>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
    </tbody>
  </table>
  <?php } else{
   
   echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد جهات اتصال!</strong> .
           </div>';
}  ?>

  </div>

    
    
</div>
