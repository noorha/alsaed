<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	<h5>
	      <?php if(!isset($archief)){ ?>				
					<button class="btn btn-primary" style="background: #f1d4d4;">
  <a  href="<?=base_url()."admin/dashboard/add_task/"?>" style="font-weight: bold;color:black;">اضافة مهمة</a>

  </button>
  	
					<button class="btn btn-primary" style="background: #f1d4d4;">
  <a  href="<?=base_url()."admin/dashboard/archief_task/"?>" style="font-weight: bold;color:black;">الأرشيف</a>

  </button>
  <?php } ?>
   </h5>
							<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>		
					<!-- /.dropdown js__dropdown -->
	    
<?php 
if(isset($all_tasks) && !empty($all_tasks)){
        
    ?> 		
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
       <th>الاسم</th>
       <th>رقم الهاتف</th>
       <th>نوع المهمة</th>
       <th>ملاحظة</th>
        <th> عمليات</th>
			</thead>
						<tfoot>
				 <th>م</th>
        <th>الاسم</th>
       <th>رقم الهاتف</th>
       <th>نوع المهمة</th>
       <th>ملاحظة</th>
       
        <th> عمليات</th>
				</tfoot>
						<tbody>
				 <?php $i=1; foreach ($all_tasks as $row ){ ?>
     <tr>
     <td> <?php echo $i;?> </td>
        <td><?php echo $row->name;?></td>
        <td><?php echo $row->phone;?></td>
         <td>
             <?php if($row->task_type==0){ 
                 
                 $type='اتصال';
             } else if($row->task_type==1){ 
                 
                 $type='زيارة';
             } else  if($row->task_type==2){ 
                 
                 $type='اخرى';
             }
?>
             
             <?php echo $type ;?>
         </td>
                 <?php if($row->status==1){ 
                 
                 $note1='تم اكتمال المهمة';
             } else if($row->status==2){ 
                 
                 $note1='تم إلغاء المهمة';
             }
?>
         <?php if($note1!=null){ ?>
          <td><?php echo $row->note.' ('.$note1.') ';?></td>
         <?php } else {?>
           <td><?php echo $row->note;?></td>
             <?php }?>
        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
    
                       $up_url=base_url()."admin/dashboard/UpdateTask/".$row->id;
                       $delete_url=base_url()."admin/dashboard/DeleteTask/".$row->id;
                       $archief_url=base_url()."admin/dashboard/archiefTask/".$row->id;
                       
                 ?>
              
                           <?php if(!isset($archief)){ ?>
                            <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a><br/>
                              <a  href="<?=$archief_url?>">
                                       تحويل المهمة إلى الأرشيف</a><br/>          
                             <?php } ?>          
                            <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
						</tbody>
					</table>
					</div>
	 <?php } else{
   
    echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد مهام!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	