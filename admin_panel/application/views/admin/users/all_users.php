

<!-- ============================================================== -->
<div class="row el-element-overlay ">

<?php if(isset($all_user) && !empty($all_user)){
         foreach ($all_user as $row ){ ?>
    <div class="col-lg-3 col-md-6 ">
        <div class="card">
            <div class="el-card-item card-shadow">
                <div class="el-card-avatar el-overlay-1" style="height: 235px;"> 
                         <img src="<?=base_url().ImagePath.$row->image?>" onerror="this.src='<?php echo base_url()."asisst/favicon/DE.png"?>'" style="height: -webkit-fill-available;" />
                    <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#"; $active_status_icon=""; $status=""; $active_url="#"; //  DeleteUser
                           $up_url=base_url()."admin/Users/UpdateUser/".$row->id;
                             $delete_url=base_url()."admin/Users/DeleteUser/".$row->id;
                             if($row->active=='0'){
                                 $active_status_icon='active.png';
                                $status='1';
                                 $active_url=base_url()."admin/Users/ActiveUser/".$row->id."/".$status;

                             }
                            else if($row->active=='1'){
                                $status='0';
                                $active_status_icon='deactive.png';
                                $active_url=base_url()."admin/Users/ActiveUser/".$row->id."/".$status;

                            }
                       ?>
                            <li><a class="btn default btn-outline " href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a></li>
                            <li><a class="btn default btn-outline " href="<?=$active_url?>">
                            <img src="<?=base_url().'voters/uploads/'.$active_status_icon?>" style="width: 15px;height: 15px;display: inline-block;background-repeat: no-repeat;" /></a></li>

                            <li><a class="btn default btn-outline" href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h3 class="box-title"><?=$row->username?></h3> 
                    <small>  <i class="fa fa-phone" aria-hidden="true"></i>
                     <?=$row->mobile?></small>
                    <br/> </div>
            </div>
        </div>
    </div>
       <?php } ?>
       <?php } else{
   
   echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد مستخدمين!</strong> .
           </div>';
}  ?>
    
    
</div>
