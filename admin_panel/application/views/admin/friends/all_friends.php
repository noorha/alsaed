<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	<h5>				
					<button class="btn btn-primary" style="background: #f1d4d4;">
					  <a  href="<?=base_url()."admin/dashboard/add_friend/"?>" style="font-weight: bold;color:black;">اضافة صديق</a>



  </button> </h5>
							<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "info":     false
    } );
} );
</script>					
					<!-- /.dropdown js__dropdown -->
	    
<?php 
     if(isset($friends) && !empty($friends)){
        
    ?> 
    

	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
					 <thead>
      <th style="border: 1px solid #696565;text-align:center;">م</th>
     
        <th style="border: 1px solid #696565;text-align:center;">الأسم</th>
        <th style="border: 1px solid #696565;text-align:center;">الفخذ</th>
       
        <th style="border: 1px solid #696565;text-align:center;">الحالة</th>
        <?php if(!isset($my)){ ?>
        <th style="border: 1px solid #696565;text-align:center;">اسم اليوزر</th>
        <?php } ?>
        <th style="border: 1px solid #696565;text-align:center;">عدد  الملاحظات</th>

        <th style="border: 1px solid #696565;text-align:center;"> عمليات</th>
      
    </thead>
						<tfoot>
				   <th style="border: 1px solid #696565;text-align:center;">م</th>
     
        <th style="border: 1px solid #696565;text-align:center;">الأسم</th>
        <th style="border: 1px solid #696565;text-align:center;">الفخذ</th>
       
        <th style="border: 1px solid #696565;text-align:center;">الحالة</th>
        <?php if(!isset($my)){ ?>
        <th style="border: 1px solid #696565;text-align:center;">اسم اليوزر</th>
        <?php } ?>
        <th style="border: 1px solid #696565;text-align:center;">عدد  الملاحظات</th>

        <th style="border: 1px solid #696565;text-align:center;"> عمليات</th>
      
      
				</tfoot>
					  <tbody>
   <?php $i=1; foreach ($friends as $row ){ ?>
     <tr>
     
        <td style="border: 1px solid #696565;text-align:center;"> <?php echo $i;?> </td>
        <td style="border: 1px solid #696565;text-align:center;"><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname;?></td>
        <td style="border:1px solid #696565;text-align:center;"><?php echo $row->thigh_name;?></td>
        <?php

$this->db->select('friends.*');

$this->db->from('friends');
$this->db->where('voter_id',$row->voter_id);
$this->db->where('friends.note !=',null);
$this->db->where('friends.note !=','');
$query=$this->db->get();
$notes_count=$query->num_rows() ;


?>   
        <?php

$this->db->select('friends.*,admin_voters.user_username');

$this->db->from('friends');
$this->db->join('admin_voters' , 'admin_voters.user_id = friends.user_id',"inner");

$this->db->where('voter_id',$row->voter_id);

$query=$this->db->get();

$data=$query->result();

?>

<?php if(isset($data) && !empty($data)){?>
<td style="border:1px solid #696565;text-align:center;">
  <?php foreach($data as $row1){ ?>  
    <?php 
        if($row1->status==0){ ?>
        مؤكد

      <?php  } else if($row1->status==1){ ?>
        غير مؤكد

      <?php }  else if($row1->status==2){ ?>
      ممكن

      <?php } ?>
      <br/>
      <hr>
    <?php }?>
</td>
<?php if(!isset($my)){ ?>
<td style="border:1px solid #696565;text-align:center;">
  <?php foreach($data as $row1){ ?>  
    
    <?php echo $row1->user_username;?>
      <br/>
      <hr>
    <?php }?>
</td>
  <?php } ?> 

<?php } ?>     
    <?php 
    $friend_notes_url=base_url()."admin/dashboard/friend_notes/".$row->voter_id;

    ?>    
<td  style="text-align:center;border:1px solid #696565;">
        <a  href="<?=$friend_notes_url?>" target="_blank">
        <span style="tex-align:right"><?php echo $notes_count;?></span>
        </a>
        </td>
       
        <td style="border:1px solid #696565;">
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                    
                    $up_url=base_url()."admin/dashboard/UpdateFriend/".$row->id.'/'.$row->kashf_num;
                       $delete_url=base_url()."admin/dashboard/DeleteFriend/".$row->id;
                 ?>
              
              <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a>
<br/>
                            <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
    </tbody>
  </table>
				
					</div>
	 <?php } else{
   
 echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد أصدقاء!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	