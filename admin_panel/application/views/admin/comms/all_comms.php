<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	

  <?php
$this->db->select('excludes.*');
$this->db->from('excludes');
$query=$this->db->get();
$count_excludes=$query->num_rows() ;
$excludes_url=base_url()."admin/dashboard/all_excludes";

?>

<script>
function edit_view()
{
	document.getElementById("btn_all_excludes").style.display = "block";
	document.getElementById("create-po").style.display = "block";
	document.getElementById("create-po2").style.display = "block";
	document.getElementById("th_inc").style.display = "table-cell";
	document.getElementById("th_exc").style.display = "table-cell";
		document.getElementById("view_data").style.display = "none";
		document.getElementById("th_visit").style.display = "none";

	var elms = document.querySelectorAll("[id='td_exc']");

	for(var i = 0; i < elms.length; i++) {
	  elms[i].style.display='table-cell'; // <-- whatever you need to do here.
	  
	}

	var elms2 = document.querySelectorAll("[id='td_inc']");

	for(var i = 0; i < elms2.length; i++) {
	  elms2[i].style.display='table-cell'; // <-- whatever you need to do here.
	  
	}

	var elms3 = document.querySelectorAll("[id='td_visit']");

	for(var i = 0; i < elms3.length; i++) {
	  elms3[i].style.display='none'; // <-- whatever you need to do here.
	  
	}	
		
	
}
</script>

	<h5>				
					<button class="btn btn-primary" style="background: #f1d4d4;display:none;" id="btn_all_excludes">
					 
					 <a  href="<?=$excludes_url?>" target="_blank" style="font-weight: bold;color:black;">
        <span style="tex-align:right"><?php echo 'المستبعدين ('.$count_excludes.')'; ?></span>
</a>

  </button>
  <br/>
   	<button class="btn btn-primary" value="إضافة"  onClick="edit_view()" name="view_data"  id="view_data"style="background: #f1d4d4;font-weight: bold;color:black; margin-bottom:5px;">دمج واستبعاد</button>
  
   </h5>
					
					<!-- /.dropdown js__dropdown -->
					
					<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	 		<script type="text/javascript">
			
$(document).ready(function() {
	   $('.voter_list').on('change', function() {

		    $('.voter_list').not(this).prop('checked', false);  

		});
  var detached = [];

    var table = $('#example1').DataTable({
    "bInfo": false, //Dont display info e.g. "Showing 1 to 4 of 4 entries"
    	       "paging": false,//Dont want paging                
    	       "bPaginate": false,//Dont want paging      
    	  
    	});
    var valArray = [];

    $('#example1').on('click', 'input[type="checkbox"]', function() {
         
    
         var value = $(this).val();
        // alert(value);
         if(this.checked) {
             valArray.push(value);   // record the value of the checkbox to valArray
         } else {
             valArray.pop(value);    // remove the recorded value of the checkbox
         }
 
     });

     $('#create-po2').on('click', function (e) {
console.log(valArray);           
		   $.ajax({

url:"<?php echo base_url(); ?>admin/dashboard/check_voters_exclude",

method:"POST",

data:{list_data:valArray},

success:function(data)

{
 // var result_id = $('#v_id').val();
  console.log(data);


if(data =='done' ){
         
              window.location.href = "<?php echo base_url(); ?>admin/dashboard/comms";

  } else  if(data =='notdone' ){
      

  window.location.href = "<?php echo base_url(); ?>admin/dashboard/comms";

  }

 

  
}

});

        })

    $('#create-po').on('click', function (e) {
console.log(valArray);           
		   $.ajax({

url:"<?php echo base_url(); ?>admin/dashboard/check_voters_merge",

method:"POST",

data:{list_data:valArray},

success:function(data)

{
 // var result_id = $('#v_id').val();
  console.log(data);


if(data =='exsist is null' ){
              //  redirect("admin/dashboard/choose_main_voter?merge_arr=".$string, 'refresh');
              var x = valArray.toString();

 window.location.href = "<?php echo base_url(); ?>admin/dashboard/choose_main_voter?merge_arr="+x;

  } else  if(data =='exsist not null' ){
        //  $this->message('error', 'تم دمج العناصر من قبل  ,من فضلك اختر عناصر لم يتم دمجها');
       // redirect("admin/dashboard/comms", 'refresh');

  window.location.href = "<?php echo base_url(); ?>admin/dashboard/comms";

  }

  else  if(data =='count array 0' ){
      //    $this->message('error', 'من فضلك اختر الناخبين الذين تريد دمجهم');
 //   redirect("admin/dashboard/comms", 'refresh');

 window.location.href = "<?php echo base_url(); ?>admin/dashboard/comms";
  }

  
}

});

        })
		
		

} );
</script>	
<script>
function editData(id)
{
	  $.ajax({
	url:"<?php echo base_url(); ?>admin/dashboard/increase_phone_counter",

	method:"POST",

	data:{voter_id:id},

	success:function(data)

	{
	 // var result_id = $('#v_id').val();
	  console.log(data);

	}

	  });
	
}
function edit_last_visit(id)
{
	  $.ajax({
	url:"<?php echo base_url(); ?>admin/dashboard/edit_last_visit",

	method:"POST",

	data:{voter_id:id},

	success:function(data)

	{
	 // var result_id = $('#v_id').val();
	  console.log(data);

	}

	  });
	
}

</script>

<?php 
     if(isset($all_voters) && !empty($all_voters)){
        
    ?>     

 <?php if(!isset($add_d)){?>

<button  id="create-po"  class="btn btn-primary" value="دمج"  name="includes"  style="background: #f1d4d4;display:none;    margin-bottom: 10px;">دمــــج</button>
<button  id="create-po2" class="btn btn-primary" value="استبعاد"  name="excludes" style="background: #f1d4d4;display:none; ">استبعاد</button>
<?php } ?>
 <?php if(isset($add_d) &&  $add_d==1){?>
  <form action="<?php echo base_url(); ?>admin/dashboard/add_diwan/voter_names" method="post">   
 	<button class="btn btn-primary" value="إضافة" name="add_to_diwan" style="background: #f1d4d4;font-weight: bold;color:black; margin-bottom:5px;">إضافة إلى الديوانية</button>
 
 	<?php } ?>
        <br/>
        <button class="btn btn-primary"  style='margin-right: 30% !important;'>
	<a  href="<?=base_url()."admin/dashboard/comms/"?>"  style="font-weight: bold;color:black;">الكل</a>
        </button>
        <button class="btn btn-primary" >
	<a  href="<?=base_url()."admin/dashboard/comms_merge/"?>"  style="font-weight: bold;color:black;">المدمجين</a>
        </button>
        <button class="btn btn-primary" >
	<a  href="<?=base_url()."admin/dashboard/all_excludes/"?>"  style="font-weight: bold;color:black;">المستبعدين</a>
        </button>
	
        <div class="table-responsive" data-pattern="priority-columns" >
	
 <?php if(!isset($all_comms_merge) &&  !isset($all_comms_exclude)){?>	
            <table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
		
			 <?php if(isset($add_d) &&  $add_d==1){?>
      <th style="display:none;">id </th>
 <th>اختيار المضاف للديوانية</th>
 <?php } ?>
       <th>م</th>
        <th>الأسم</th>
         <th>العدد</th>
        <th>العمر</th> 
        <th>الهواتف</th>
        <th>عدد الملاحظات</th>
                 <th  id="th_visit">زيارة</th>
        
        <?php if(!isset($add_d)){?>
       
        <th id="th_inc" style="display:none;">دمج</th>
        <th id="th_exc" style="display:none;">استبعاد</th>
<?php } ?>
			</thead>
						<tfoot>
						
			 <?php if(isset($add_d) &&  $add_d==1){?>
      <th style="display:none;">id </th>
 <th>اختيار المضاف للديوانية</th>
 <?php } ?>
       <th>م</th>
        <th>الأسم</th>
         <th>العدد</th>
        <th>العمر</th> 
        <th>الهواتف</th>
        <th>عدد الملاحظات</th>
         <th>زيارة</th>
        <?php if(!isset($add_d)){?>
       
        <th id="th_exc" style="display:none;">دمج</th>
        <th id="th_exc" style="display:none;">استبعاد</th>
        <?php } ?>
				</tfoot>
					 <tbody>
   <?php $i=1; foreach ($all_voters as $row ){ ?>
   
    <?php

$this->db->select('merges.*');
$this->db->from('merges');
$this->db->where('merges.main_voter_id !=',$row->id);
$this->db->where('merges.voter_id',$row->id);
$query=$this->db->get();
$count=$query->num_rows() ;
?>
<?php if($count ==0 ){ ?>
     <tr>
   <?php
$this->db->select('merges.*,notes.voter_id');
$this->db->from('merges');
$this->db->where('merges.main_voter_id',$row->id);
$this->db->join('notes' , 'notes.voter_id = merges.voter_id',"inner");
$query=$this->db->get();
$count_notes=$query->num_rows() ;


?>
         
     <?php

    
$this->db->select('merges.*');
$this->db->from('merges');
$this->db->where('merges.main_voter_id',$row->id);
$query=$this->db->get();
$count_merges=$query->num_rows() ;

$merges_url=base_url()."admin/dashboard/get_my_merges/".$row->id;

     $this->db->select('diwans.*');
     $this->db->from('diwans');
     $this->db->where('diwans.voter_id',$row->id);
     $query=$this->db->get(); 
     $query=$query->result_array();
      $diwan_loc=$query[0];
 //   print_r($diwan_loc['loc_counter']);exit();
   

?>
    
 <?php if(isset($add_d) &&  $add_d==1){?>
     <td style="display:none;"><?php echo $row->id;?></td>
		  <td style="width:4%;text-align:center">      <input type="checkbox" name="<?php echo $row->id; ?>"   class="voter_list" /> </td>        
<?php } ?>
<td><?php echo $i;?></td>
        <td><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname.' '.$row->fvname.' '.$row->thigh_name;?></td>
        <td> <a  href="<?=$merges_url?>" target="_blank">
        <span style="tex-align:right"><?php echo $count_merges;?></span>
        </a></td>
       
        <td> <?php 
        $dateOfBirth = "1-"."1-".$row->birth_date;
        $today = date("Y-m-d");
        $diff = date_diff(date_create($dateOfBirth), date_create($today));
        echo $diff->format('%y');

        ?>
        </td>

        <td> 
        <a class="btn btn-primary" href="<?php echo"tel:". $row->phone;?>" onClick="editData(<?php echo $row->id;?>)" >اتصال</a>
          <?php       if($row->phone2 !=null){ ?>
          <br/>
        <a   class="btn btn-primary"  href="<?php echo"tel:". $row->phone2;?>" onClick="editData(<?php echo $row->id;?>)" >اتصال</a>
       <?php } ?>
         <?php       if($row->last_call !=null){ ?>
           <br/>
           <span><?php echo '('.$row->last_call.')' ;?></span>
      <?php }?>
       <?php       if($row->phone_counter !=null){ ?>
           <br/>
           
            <a  href="<?=base_url()."admin/dashboard/voter_calls/".$row->id;?>"  ><?php echo '('.$row->phone_counter.')' ;?></a>
    
      <?php }?>
   
        
     
       
        </td>
          <td id="td_notes">
        <?php echo $count_notes;?>
        
        </td>
        
           <td  id="td_visit">
           <a   class="btn btn-primary" href="#" target="_blank" onClick="edit_last_visit(<?php echo $row->id;?>)">زيارة</a> 
            <?php  if( $row->last_visit  !=null){ ?> <br/>
            <span><?php echo $row->last_visit ;?></span>
            <br/>
         <a  href="<?=base_url()."admin/dashboard/voter_visits/".$row->id;?>"  ><?php echo '('.$row->visit_counter.')' ;?></a>

               <?php }?>
                
            </td>
           
     <?php if(!isset($add_d)){?>
      
      <?php if($count_merges !=0 &&$count_merges !=null  ){ ?> 
        <td id="td_inc" style="display:none;"  >     تم الدمج </td>

      <?php } else { ?>
        <td id="td_inc" style="display:none;">      <input type="checkbox" class='checkbox' value='<?= $row->id ?>'  /> </td>

      <?php } ?>
        <td id="td_exc" style="display:none;">      <input type="checkbox"  class='checkbox' value='<?= $row->id ?>' /> </td>
      <?php } ?>
      </tr>
<?php } ?>
<?php if($count ==0 ){ $i++; } ?>  
   
    <?php } ?>
    </tbody>
 </table>
 <?php } else if (isset($all_comms_merge) && $all_comms_merge==1){?>
            <table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
		
		
       <th>م</th>
        <th>الأسم</th>
         <th>العدد</th>
        <th>العمر</th> 
        <th>الهواتف</th>
        <th>زيارة</th>
    
			</thead>
						<tfoot>
						
			<th>م</th>
        <th>الأسم</th>
         <th>العدد</th>
        <th>العمر</th> 
        <th>الهواتف</th>
        <th>زيارة</th>
				</tfoot>
					 <tbody>
   <?php $i=1; foreach ($all_voters as $row ){ ?>
   
    <?php

$this->db->select('merges.*');
$this->db->from('merges');
$this->db->where('merges.main_voter_id !=',$row->id);
$this->db->where('merges.voter_id',$row->id);
$query=$this->db->get();
$count=$query->num_rows() ;
?>
<?php if($count ==0 ){ ?>
                                               <?php

    
$this->db->select('merges.*');
$this->db->from('merges');
$this->db->where('merges.main_voter_id',$row->id);
$query=$this->db->get();
$count_merges=$query->num_rows() ;

$merges_url=base_url()."admin/dashboard/get_my_merges/".$row->id;

     $this->db->select('diwans.*');
     $this->db->from('diwans');
     $this->db->where('diwans.voter_id',$row->id);
     $query=$this->db->get(); 
     $query=$query->result_array();
      $diwan_loc=$query[0];
 //   print_r($diwan_loc['loc_counter']);exit();
   

?>
   <?php if($count_merges!=0){ ?> 
     <tr>
     
   
 
<td><?php echo $i;?></td>
        <td><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname.' '.$row->fvname.' '.$row->thigh_name;?></td>
        <td> <a  href="<?=$merges_url?>" target="_blank">
        <span style="tex-align:right"><?php echo $count_merges;?></span>
        </a></td>
       
        <td> <?php 
        $dateOfBirth = "1-"."1-".$row->birth_date;
        $today = date("Y-m-d");
        $diff = date_diff(date_create($dateOfBirth), date_create($today));
        echo $diff->format('%y');

        ?>
        </td>

        <td> 
        <a class="btn btn-primary" href="<?php echo"tel:". $row->phone;?>" onClick="editData(<?php echo $row->id;?>)" >اتصال</a>
          <?php       if($row->phone2 !=null){ ?>
          <br/>
        <a   class="btn btn-primary"  href="<?php echo"tel:". $row->phone2;?>" onClick="editData(<?php echo $row->id;?>)" >اتصال</a>
       <?php } ?>
         <?php       if($row->last_call !=null){ ?>
           <br/>
           <span><?php echo '('.$row->last_call.')' ;?></span>
      <?php }?>
       <?php       if($row->phone_counter !=null){ ?>
           <br/>
           
            <a  href="<?=base_url()."admin/dashboard/voter_calls/".$row->id;?>"  ><?php echo '('.$row->phone_counter.')' ;?></a>
    
      <?php }?>
   
        
     
       
        </td>
          <td  id="td_visit">
           <a   class="btn btn-primary" href="#" target="_blank" onClick="edit_last_visit(<?php echo $row->id;?>)">زيارة</a> 
            <?php  if( $row->last_visit  !=null){ ?> <br/>
            <span><?php echo $row->last_visit ;?></span>
            <br/>
         <a  href="<?=base_url()."admin/dashboard/voter_visits/".$row->id;?>"  ><?php echo '('.$row->visit_counter.')' ;?></a>

               <?php }?>
                
            </td>
        
          
   
      </tr>
<?php } } ?>
<?php if($count_merges !=0){ $i++; } ?>  
   
    <?php } ?>
    </tbody>
 </table>
 <?php } else if (isset($all_comms_exclude) && $all_comms_exclude==1){?>
            	<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
      <th>الأسم</th>
        <th>العمر</th> 
        <th>الهواتف</th>
        <th>عمليات</th>

			</thead>
						<tfoot>
				 <th>م</th>
      <th>الأسم</th>
        <th>العمر</th> 
        <th>الهواتف</th>
        <th>عمليات</th>

				</tfoot>
				<tbody>
   <?php $i=1; foreach ($all_voters as $row ){ ?>
     <tr>
     

        <td> <?php echo $i;?> </td>
       
        <td><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname.' '.$row->fvname.' '.$row->thigh_name;?></td>
        <td> <?php 
        $dateOfBirth = "1-"."1-".$row->birth_date;
        $today = date("Y-m-d");
        $diff = date_diff(date_create($dateOfBirth), date_create($today));
        echo $diff->format('%y');
        $delete_url=base_url()."admin/dashboard/DeleteExclude/".$row->id;

        ?>
        </td>

        <td><?php echo $row->phone;?> <br/> <?php echo $row->phone2;?></td>
    
        <td>  <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a></td>
      
      </tr>
     <?php $i++;?>
   
    <?php } ?>
    </tbody>
					</table>
 <?php } ?>
  </div>
   <?php if(isset($add_d) &&  $add_d==1){?>
  </form> 
 	<?php } ?>
 	
	 <?php } else{
   
    echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد تواصل!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	