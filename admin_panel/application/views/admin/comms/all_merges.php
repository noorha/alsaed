<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	
					
					<!-- /.dropdown js__dropdown -->
	    						<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>	
    
<?php 
     if(isset($all_voters) && !empty($all_voters)){
        
    ?> 	
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
      <th>الأسم</th>
        <th>العمر</th> 
        <th>الهواتف</th>
          <th>عدد الملاحظات</th>
        <th>عمليات</th>

			</thead>
						<tfoot>
				 <th>م</th>
      <th>الأسم</th>
        <th>العمر</th> 
        <th>الهواتف</th>
        <th>عدد الملاحظات</th>

        <th>عمليات</th>

				</tfoot>
					 <tbody>
   <?php $i=1; foreach ($all_voters as $row ){ ?>
     <tr>
     <?php

$this->db->select('notes.*');
$this->db->from('notes');
$this->db->where('notes.voter_id',$row->id);
$query=$this->db->get();
$count_notes=$query->num_rows() ;

$notes_url=base_url()."admin/dashboard/notes/".$row->id;

?>
    

        <td> <?php echo $i;?> </td>
       
        <td><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname.' '.$row->fvname.' '.$row->thigh_name;?></td>
        <td> <?php 
        $dateOfBirth = "1-"."1-".$row->birth_date;
        $today = date("Y-m-d");
        $diff = date_diff(date_create($dateOfBirth), date_create($today));
        echo $diff->format('%y');
        $delete_url=base_url()."admin/dashboard/DeleteMerge/".$row->id.'/'.$row->main_voter_id;

        ?>
        </td>

        <td><?php echo $row->phone;?> <br/> <?php echo $row->phone2;?></td>
     <td id="td_notes">
        <a  href="<?=$notes_url?>" ><?php echo $count_notes;?></a>
        
        </td>
      <td>  <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a></td>
      
      </tr>
     <?php $i++;?>
   
    <?php } ?>
    </tbody>
					</table>
					</div>
	 <?php } else{
   
   echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد مدمجين!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	