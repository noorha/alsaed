			
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	 		<script type="text/javascript">
			
$(document).ready(function() {
	   $('.voter_list').on('change', function() {

		    $('.voter_list').not(this).prop('checked', false);  

		});
	   $('#example1').DataTable( {
	        "paging":   false,
	        "ordering": false,
	        "info":     false
	    } );
});
</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
   
<?php 
     if(isset($all_process) && !empty($all_process)){
        
    ?> 
 

   				<div class="table-responsive" data-pattern="priority-columns" id="printDiv">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			
   
 <th>م</th>
        <th>العملية</th>
        <th>اسم المستخدم</th>
        <th>التاريخ</th>
        <th>الساعة</th>
       
			</thead>
						<tfoot>
				 <th>م</th>
        <th>العملية</th>
           <th>اسم المستخدم</th>
        <th>التاريخ</th>
        <th>الساعة</th>
       
				</tfoot>
						<tbody>
				 <?php $i=1; foreach ($all_process as $row ){ ?>
     <tr>
   

    <td> <?php echo $i;?> </td> 
    
        <td><?php echo $row->note;?></td>
         <td ><?php echo $row->user_name;?></td>
        <td><?php echo $row->note_date;?> </td>
    
        <td ><?php echo $row->note_time;?></td>
      

      </tr>
     <?php $i++;?>
   
    <?php } ?>	
						</tbody>
					</table>
					</div>
					
					 
	 <?php } else{
   
   echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد عمليات!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	