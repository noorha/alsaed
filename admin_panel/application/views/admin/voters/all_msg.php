				<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title">الرسائل الخاصة</h4>
					<!-- /.box-title -->
					
					
		<?php 
     if(isset($all_voters) && !empty($all_voters)){
        
    ?> 
    
    <a class="btn btn-primary" href="<?php echo base_url()."admin/dashboard/send_voter_msgs/"."msg1"; ?>">إرسال الرسالة الأولى</a>
    <a class="btn btn-primary" href="<?php echo base_url()."admin/dashboard/send_voter_msgs/"."msg2"; ?>">إرسال الرسالة الثانية</a>
    <a class="btn btn-primary" href="<?php echo base_url()."admin/dashboard/send_voter_msgs/"."msg3"; ?>">إرسال الرسالة الثالثة</a>
<br/>
<br/>			
					<!-- /.dropdown js__dropdown -->
				<div class="table-responsive" data-pattern="priority-columns">	
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
     
        <th>الأسم</th>
                    <th>اللقب</th>

        <th>الفخذ</th>
       
        <th>الهواتف</th>
  
        <th>الرسائل </th>

        <th style="display:none;"> عمليات</th>
			</thead>
						<tfoot>
				 <th>م</th>
     
        <th>الأسم</th>
                    <th>اللقب</th>

        <th>الفخذ</th>
       
        <th>الهواتف</th>
  
        <th>الرسائل </th>

        <th style="display:none;"> عمليات</th>
				</tfoot>
						<tbody>
					<?php $i=1; foreach ($all_voters as $row ){ ?>
     <tr>
     
     <?php

$this->db->select('notes.*');
$this->db->from('notes');
$this->db->where('notes.voter_id',$row->id);
$query=$this->db->get();
$count_notes=$query->num_rows() ;

$notes_url=base_url()."admin/dashboard/notes/".$row->id;

?>
    

        <td> <?php echo $i;?> </td>
       
        <td><?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname;?></td>
                <td><?php echo $row->title;?></td>

        <td><?php echo $row->thigh_name;?></td>
        <td><?php echo $row->phone;?> <br/> <?php echo $row->phone2;?></td>
    
       
          <td>
            <?php if($row->phone_msg !=null ){  ?>
            <?php echo 'رسالة التحضير: '.$row->phone_msg; ?>

            <?php if($row->msg1 !=null ){  ?>
            <br/>
            <hr>
            <?php echo 'الرسالة الأولى: '.$row->msg1; ?> 
            <?php } ?>
            <?php if($row->msg2 !=null ){  ?>
            <br/>
            <hr>
            <?php echo 'الرسالة الثانية: '.$row->msg2; ?>
            <?php } ?>
            <?php if($row->msg3 !=null ){  ?>

            <br/>
            <hr>
            <?php echo 'الرسالة الثالثة: '.$row->msg3; ?>  
          
            <?php } } else {
          

?>
<a class="btn btn-primary" href="<?php echo base_url()."admin/dashboard/add_msgs/".$row->id; ?>">إضافة رسالة خاصة</a>


            <?php } ?>
          </td>
        <td style="display:none;">
        

        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       $Add_note=base_url()."admin/dashboard/Add_note/".$row->id;                       
                       $up_url=base_url()."admin/dashboard/UpdateVoters/".$row->id;
                       $delete_url=base_url()."admin/dashboard/DeleteVoters/".$row->id;
                 ?>
              <li><a  href="<?=$Add_note?>">إضافة ملاحظة</a></li>
                            <li><a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a></li>
                            <li><a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a></li>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
						</tbody>
					</table>
					</div>
					 <?php } else{
   
   echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد ناخبين!</strong> .
           </div>';
}  ?>

				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	
	
