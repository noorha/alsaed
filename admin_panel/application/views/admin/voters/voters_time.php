						<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>	
<script>
function editData(id,voter_id)
{
	if(voter_id==null){

		  $.ajax({
			  
				url:"<?php echo base_url(); ?>admin/dashboard/increase_diwan_counter",

				method:"POST",

				data:{diwan_id:id},

				success:function(data)

				{
				 // var result_id = $('#v_id').val();
				  console.log(data);

				}

				  });
				
		}
	else if(voter_id !=null && voter_id !=0){
		  $.ajax({
				url:"<?php echo base_url(); ?>admin/dashboard/edit_last_visit",

				method:"POST",

				data:{voter_id:voter_id},

				success:function(data)

				{
				 // var result_id = $('#v_id').val();
				  console.log(data);

				}

				  });
		}
	
}


function editphone(id)
{
	  $.ajax({
	url:"<?php echo base_url(); ?>admin/dashboard/increase_phone_counter_diwan",

	method:"POST",

	data:{diwan_id:id},

	success:function(data)

	{
	 // var result_id = $('#v_id').val();
	  console.log(data);

	}

	  });
	
}


</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	
    
<?php 
     if(isset($all_calls) && !empty($all_calls)){
        
    ?> 
    		
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
       <th>اسم الناخب</th>
         <th>اسم المستخدم</th>
   <th>الوقت</th>
      
        <th>التاريخ</th>
       
			</thead>
						<tfoot>
                             <th>م</th>                       
	<th>اسم الناخب</th> 
         <th>اسم المستخدم</th>					
   <th>الوقت</th>
      
        <th>التاريخ</th>
				</tfoot>
					  <tbody>
   <?php $i=1; foreach ($all_calls as $row ){ ?>
     <tr>
     
   

        <td> <?php echo $i;?> </td>
         <td> <?php echo $row->fname.' '. $row->sname .' '.$row->tname.' '.$row->foname.' '.$row->fvname;?>  </td>
          <td> <?php echo $row->user_name;?> </td>
       <?php if(isset($visits) && $visits !=null){ ?>
        <td><?php echo $row->visit_time;?></td>

        <td><?php echo $row->visit_date;?></td>
        
       <?php } else { ?>
  <td><?php echo $row->call_time;?></td>

        <td><?php echo $row->call_date;?></td>
             
 <?php } ?>
     </tr>
     <?php $i++;?>
   
    <?php } ?>
    </tbody>
  </table>
					</table>
					</div>
	 <?php } else{
   
 echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد أوقات للاتصال!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	