<div id="wrapper">
	<div class="main-content">
  <span id="message_alert">

<?php

if(isset($_SESSION['message']))

	echo $_SESSION['message'];

unset($_SESSION['message']);

?>

</span>
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
				<!-- /.dropdown js__dropdown -->
	    
<?php 
     if(isset($all_refuse) && !empty($all_refuse)){
        
    ?> 		
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
       <th>الأسم</th>
       <th>العنوان</th>
       <th>الهاتف</th>
        <th> عمليات</th>
			</thead>
						<tfoot>
            <th>م</th>
       <th>الأسم</th>
       <th>العنوان</th>
       <th>الهاتف</th>
        <th> عمليات</th>
				</tfoot>
						<tbody>
				 <?php $i=1; foreach ($all_refuse as $row ){ ?>
     <tr>
     <td> <?php echo $i;?> </td>
        <td><?php echo $row->name;?></td>
        <td><?php echo $row->address;?></td>
        <td><?php echo $row->phone;?></td>
        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       
                       $up_url=base_url()."admin/dashboard/UpdatePerson/".$row->id."/"."refuse";
                       $delete_url=base_url()."admin/dashboard/DeletePerson/".$row->id."/"."refuse";
                 ?>
              
                            <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a><br/>
                            <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
						</tbody>
					</table>
					</div>
	 <?php } else{
   
    echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد مرفوضين!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	