				<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	<h5>				
					<button class="btn btn-primary" style="background: #f1d4d4;">
					  <a  href="<?=base_url()."admin/dashboard/add_dept2/"?>"  style="font-weight: bold;color:black;">اضافة قسم جديد</a>


  </button> 
 
        </h5>
					
					<!-- /.dropdown js__dropdown -->
<?php 
     if(isset($all_depts) && !empty($all_depts)){
        
    ?> 
    	
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
  <th>اسم القسم</th>
       
        <th> عمليات</th>
			</thead>
						<tfoot>
				 <th>م</th>
 <th>اسم القسم</th>
       
        <th> عمليات</th>
				</tfoot>
						<tbody>
				<?php $i=1; foreach ($all_depts as $row ){ ?>
     <tr>

         <td> <?php echo $i;?> </td>
        <td><?php echo $row->name;?></td>
      
      </td>
       
        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       
                       $up_url=base_url()."admin/dashboard/UpdateDept2/".$row->id;
                       $delete_url=base_url()."admin/dashboard/DeleteDept2/".$row->id;
                 ?>
              
                           <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a><br/>
                           <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                        </ul>
                    </div>
        </td>
      </tr>
     <?php $i++;?>
   
    <?php } ?>
						</tbody>
					</table>
					</div>
	 <?php } else{
   
 echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد اقسام!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	