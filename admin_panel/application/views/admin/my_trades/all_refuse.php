				<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
                 
			<div class="col-xs-12">
                           
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->

					
					<!-- /.dropdown js__dropdown -->
<?php 
     if(isset($all_refuse) && !empty($all_refuse)){
        
    ?> 
  
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
        <th>الأسم</th>
          <th>الهاتف</th>
         <th>تاريخ الأضافة</th>
        <th> القسم</th>
          <th> الملاحظة</th>
              <th> صورة المستند</th>
			</thead>
						<tfoot>
				 <th>م</th>
        <th>الأسم</th>
          <th>الهاتف</th>
         <th>تاريخ الأضافة</th>
        <th> القسم</th>
        
          <th> الملاحظة</th>
              <th> صورة المستند</th>
				</tfoot>
						<tbody>
				<?php $i=1; foreach ($all_refuse as $row ){ ?>
     <tr>

        <?php if($row->voter_id!=null && $row->voter_id!=0){
     $this->db->select('thighs.name as thigh_name,voters.fname,voters.sname,voters.tname,voters.foname,voters.phone as voter_phone');
        $this->db->from("voters");
          $this->db->where("voters.id",$row->voter_id);
        $this->db->join('thighs' , 'thighs.id = voters.thigh_id',"inner");
        $query = $this->db->get();
       $test= $query->result() ;
       $voter_phone=$test[0]->voter_phone;
       $voter_name=$test[0]->fname.' '. $test[0]->sname .' '.$test[0]->tname.' '.$test[0]->foname.' '.$test[0]->thigh_name;
 } ?>
         <td> <?php echo $i;?> </td>
        <?php if($row->voter_id!=null && $row->voter_id!=0){?>
          <td><?php echo $voter_name;?></td>
        <td><?php echo $voter_phone;?></td>
        <?php } else { ?>
           <td><?php echo $row->name;?></td>
        <td><?php echo $row->phone;?></td>
        <?php } ?>
         <td><?php echo $row->add_date;?></td>
        <td><?php echo $row->dept_name;?></td>
	 <td><?php echo $row->notes;?></td>
        <td>
             <?php if($row->my_image !=null){ ?>
                 <a href="<?php echo base_url()."uploads/".$row->my_image?>"> <img src="<?php echo base_url()."uploads/".$row->my_image?>" class="img-circle" alt="User Image" width="50" height="50"/></a>

             <?php } else { ?>
             لا يوجد صورة
             <?php } ?>
         </td>
       
      </tr>
     <?php $i++;?>
   
    <?php } ?>
						</tbody>
					</table>
					</div>
	 <?php } else{
   
 echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد معاملات مرفوضة!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	