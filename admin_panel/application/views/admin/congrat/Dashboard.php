<?php
  class Dashboard extends MY_Controller{
    public   function __construct(){
        parent::__construct();
    }
  /**
 *  ================================================================================================================
 * 
 *  ----------------------------------------------------------------------------------------------------------------
 * 
 *  ================================================================================================================
 */

    public function  index(){
   
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/index_empty';
        $this->load->view('admin_index', $data);
    }
    
    public function  friend_msgs(){
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_friends();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "الرسائل الخاصة";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_msg';
        $this->load->view('admin_index', $data);
    } 
    public function  messages(){
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "الرسائل الخاصة";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/voters/all_msg';
        $this->load->view('admin_index', $data);
    } 
    
    public function  check_voters_merge(){
        $cart=$this->input->post('list_data');
     //   echo $cart;
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $all_voters=$this->Model_voters->get_all_voters();
   
      //  $check_id= $this->input->post('220');
             //     print_r($check_id);exit();
 


     if(count($cart) !=0 ){
       $exsist= $this->Model_voters->check_merge($cart);
       if($exsist==null){
       // $string = implode(',',$cart);
              //  redirect("admin/dashboard/choose_main_voter?merge_arr=".$string, 'refresh');
              echo'exsist is null';
       }
       else {
       // $this->message('error', 'تم دمج العناصر من قبل  ,من فضلك اختر عناصر لم يتم دمجها');
       // redirect("admin/dashboard/comms", 'refresh');
        echo'exsist not null';
       }
   }

else if(count($cart) ==0 ){
//    $this->message('error', 'من فضلك اختر الناخبين الذين تريد دمجهم');
 //   redirect("admin/dashboard/comms", 'refresh');
    echo'count array 0';
  
     }
     
   

    } 
    public function  check_voters_exclude(){
        $cart=$this->input->post('list_data');
     //   echo $cart;
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $all_voters=$this->Model_voters->get_all_voters();


        if(count($cart) !=0 ){
            foreach($cart as $one_row){

                $Idata['voter_id']=$one_row;
                $this->Model_voters->insert_excludes($Idata);
        }
       //   $this->message('success', 'تم الاستبعاد بنجاح');
       //   redirect("admin/dashboard/comms", 'refresh');
       echo 'done';
      }

      else if(count($cart) ==0 ){
       // $this->message('error', 'من فضلك اختر الناخبين الذى تريد استبعادهم');
       // redirect("admin/dashboard/comms", 'refresh');
        echo 'notdone';
         }
     
   

    } 
    public function  choose_main_voter($cart){
        $numbers=$this->input->get('merge_arr');
        $data['selected_rows']=$numbers;
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters_with_ids($numbers);
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "اختر الناخب الرئيسى لإتمام الدمج";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/comms/choose_main';
        $this->load->view('admin_index', $data);
    }
    
    public function  get_my_merges($main_id){
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters_merges($main_id);
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "المدمجين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/comms/all_merges';
        $this->load->view('admin_index', $data);
    }
    public function  all_excludes(){
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters_excludes();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "المستبعدين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/comms/all_excludes';
        $this->load->view('admin_index', $data);
    }
    public function  comms(){
        $parameter_x=$this->uri->segment(4);
       
        if($parameter_x=="add_diwan_x"){
            $data['add_d']=1;
        }
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters_comms();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "تواصل";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/comms/all_comms';
        $this->load->view('admin_index', $data);
    }

    public function  voters(){
        $parameter_x=$this->uri->segment(4);
        $parameter_y=$this->uri->segment(5);
         if($parameter_y!=null){
             $data['result_id']=$parameter_y;
        }
        if($parameter_x=="add_task_x"){
            $data['add_task']=1;
        }
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');
        $data["all_voters"]=$this->Model_voters->get_all_voters();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
    //  print_r($data);exit();
        $title = "الناخبين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/voters/all_voters';
        $this->load->view('admin_index', $data);
    }
    public function  attends(){
        $this->load->model('admin/Model_attendance');
        $data["all_attends"]=$this->Model_attendance->get_all_attends();
    //  print_r($data);exit();
        $title = "التحضير";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/all_attends';
        $this->load->view('admin_index', $data);
    }
    public function  thighs(){
   
        $this->load->model('admin/Model_thighs');
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
        $title = "الفخوذ";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/thighs/all_thighs';
        $this->load->view('admin_index', $data);
    }
    
    public function  total_friends_count($id){
   
        $this->load->model('admin/Model_voters');
      
        $data["all_count"]=$this->Model_voters->get_all_total_friends($id);
        
        $title = "الأصدقاء بهذا الفخذ";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/all_thigh_voters';
        $this->load->view('admin_index', $data);
    } 
    public function  total_thigh_count($id){
   
        $this->load->model('admin/Model_voters');
      
        $data["all_count"]=$this->Model_voters->get_all_total($id);
        
        $title = "أسماء المشاركين بهذا الفخذ";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/all_thigh_voters';
        $this->load->view('admin_index', $data);
    } 
    public function  not_attends($id){
   
        $this->load->model('admin/Model_voters');
      
        $data["all_not_attends2"]=$this->Model_voters->get_all_not_attends2($id);
        $data["all_not_attends"]=$this->Model_voters->get_all_not_attends($id);
        
        $title = "عدم الحضور";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/not_attend';
        $this->load->view('admin_index', $data);
    } 

    public function  cancel($id){
   
        $this->load->model('admin/Model_voters');

        $data["all_cancel"]=$this->Model_voters->get_all_cancel($id);
        
        $title = "ملغى";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/all_cancel';
        $this->load->view('admin_index', $data);
    } 
    
    public function  friends_statistics(){
   
        $this->load->model('admin/Model_voters');
        $data["all_statistics"]=$this->Model_voters->get_all_statistics();
     
        $title = "أحصائيات الأصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_statistics';
        $this->load->view('admin_index', $data);
    }
    public function  sure(){
   
        $this->load->model('admin/Model_voters');
   
        $data["friends"]=$this->Model_voters->get_status_friends('0');
        
        $title = "أصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
    public function  not_sure(){
   
        $this->load->model('admin/Model_voters');
    
        $data["friends"]=$this->Model_voters->get_status_friends('1');
        
        $title = "أصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
    public function  maybe(){
   
        $this->load->model('admin/Model_voters');
     
        $data["friends"]=$this->Model_voters->get_status_friends('2');
        
        $title = "أصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
    
    public function increase_diwan_counter(){
        $id=$this->input->post('diwan_id');
        $this->load->model('admin/Model_voters');
        $this->Model_voters->increase_diwan_counter($id);
        echo $id;
        //       redirect("admin/dashboard/add_occ/".$numbers, );
        
    }
     public function edit_last_visit_using_task($id){
       
        $this->load->model('admin/Model_voters');
        $this->Model_voters->edit_last_visit($id);
        echo $id;
        //       redirect("admin/dashboard/add_occ/".$numbers, );
        
    } 
    public function edit_last_visit(){
        $id=$this->input->post('voter_id');
        $this->load->model('admin/Model_voters');
        $this->Model_voters->edit_last_visit($id);
        echo $id;
        //       redirect("admin/dashboard/add_occ/".$numbers, );
        
    }
    public function increase_phone_counter_diwan(){
        $id=$this->input->post('diwan_id');
        $this->load->model('admin/Model_diwans');
        $this->Model_diwans->increase_counter($id);
        echo $id;
        //       redirect("admin/dashboard/add_occ/".$numbers, );
        
    }
     public function increase_phone_counter_using_task($id){
      
        $this->load->model('admin/Model_voters');
        $this->Model_voters->increase_counter($id);
        echo $id;
        //       redirect("admin/dashboard/add_occ/".$numbers, );
        
    }
    public function increase_phone_counter(){
        $id=$this->input->post('voter_id');
        $this->load->model('admin/Model_voters');
        $this->Model_voters->increase_counter($id);
        echo $id;
        //       redirect("admin/dashboard/add_occ/".$numbers, );
        
    }
    public function get_data(){
         $numbers=$this->input->post("form_data");
         $str=implode(',', $numbers);
         redirect("admin/dashboard/add_congrat?info=".$str, 'refresh');
         
     //   echo $numbers;
   /*      $this->load->model('admin/Model_voters');
         $val_data=array();  $val_all=array();
         $i=0 ;
         foreach($numbers as $one_number){
             $data=$this->Model_voters->get_phone_info($one_number);
             $val_data[$i]['id']=$data[0]->id;
             $val_data[$i]['name']=$data[0]->name;
             $val_data[$i]['phone1']=$data[0]->phone1;
             $val_data[$i]['phone2']=$data[0]->phone2;
           // array_push($val_data,$val_all);
           
             $i++;
         }
      //   print_r($val_data);exit();
         echo json_encode($val_data);
        */ 
        
        
    }
    public function fetch_numbers(){
        $numbers=$this->input->post('list_data');
       echo $numbers;
 //       redirect("admin/dashboard/add_occ/".$numbers, );
      
    }
    public function  phone_book(){
        
        $this->load->model('admin/Model_voters');
        $occ_p=$this->uri->segment('4');
      if($occ_p !=null ){
        $data['occ_phones']=$occ_p;
    
    }
        $data["all_phones"]=$this->Model_voters->get_all_phone_numbers();
        $title = "دليل الهواتف";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");

        $data['subview'] = 'admin/phone_books/all_phones';
        $this->load->view('admin_index', $data);
    }
    public function  friends(){
   
        $this->load->model('admin/Model_voters');
        $set_data = $this->session->all_userdata();
        $user_id=$set_data['user_id'];
        $data["friends"]=$this->Model_voters->get_my_friends($user_id);
        $data['my']=1;
        $title = "أصدقائى";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
    public function  all_friends(){
   
        $this->load->model('admin/Model_voters');

        $data["friends"]=$this->Model_voters->get_all_friends();
        
        $title = "كل الأصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_friends';
        $this->load->view('admin_index', $data);
    }
    
    public function  archief_task(){
        
        $this->load->model('admin/Model_voters');
        
        $data["all_tasks"]=$this->Model_voters->get_all_archief_tasks();
        $data['archief']=1;
        $title = "الأرشيف";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/tasks/all_tasks';
        $this->load->view('admin_index', $data);
    }
    public function  tasks(){
        
        $this->load->model('admin/Model_voters');
        
        $data["all_tasks"]=$this->Model_voters->get_all_tasks();
        
        $title = "مهام";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/tasks/all_tasks';
        $this->load->view('admin_index', $data);
    }
       
    public function  all_friend_attend(){
   
        $this->load->model('admin/Model_voters');

        $data["all_attends"]=$this->Model_voters->get_all_friend_attends();
        
        $title = "الحاضرين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/attend';
        $this->load->view('admin_index', $data);
    }
    public function  all_attends($id){
   
        $this->load->model('admin/Model_voters');

        $data["all_attends"]=$this->Model_voters->get_all_attends($id);
        
        $title = "الحضور";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/attends/attend';
        $this->load->view('admin_index', $data);
    }
    
    public function  count_not_attend(){
   
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');

        $data["all_statistics"]=$this->Model_voters->get_all_statistics();
        
        $title = "عدم حضور";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/statistics/count_not_attend';
        $this->load->view('admin_index', $data);
    }
    
    public function  attend_friends(){
   
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');

        $data["all_notAttend_friends"]=$this->Model_voters->get_all_notAttend_friends();
        $data["all_notAttend_friends2"]=$this->Model_voters->get_all_notAttend_friends2();
     //   $merged_arr = array_merge_recursive($all_notAttend_friends,$all_notAttend_friends2);
     //   $data["all_notAttend_friends"]=$merged_arr;
      //  print_r( $all_notAttend_friends2);
     //   print_r( $data["all_notAttend_friends2"]);
      // exit();
        $title = "تحضير الأصدقاء";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/attend_friends';
        $this->load->view('admin_index', $data);
    }
    public function  jobs(){
   
        $this->load->model('admin/Model_jobs');
        $data["all_jobs"]=$this->Model_jobs->get_all_jobs();

        $title = "المهن";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/jobs/all_jobs';
        $this->load->view('admin_index', $data);
    }
    public function  statistics(){
   
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_voters');

        $data["all_statistics"]=$this->Model_voters->get_all_statistics();
        
        $title = "الاحصائيات";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/statistics/all_statistics';
        $this->load->view('admin_index', $data);
    }
    
    public function add_merges(){  // AppClient/User
        $this->load->model('admin/Model_voters');
          //  $Idata = $this->input->post('data_post');
          
          $selected_rows = $this->input->post('selected_rows');
          $check_1='';
          $selected_rows = explode(',', $selected_rows);
          $all_voters=$this->Model_voters->get_all_voters_with_ids($selected_rows);
             foreach($all_voters as $one_voter){
                    $check_id=$one_voter->id;
                    $check= $this->input->post($check_id);

                 if($check!=null){
                     $check_1=$check_id;
                 }
                } 

$main_voter_id=$check_1;
          foreach($selected_rows as $one_row){
                  $Idata['voter_id']=$one_row;
                  $Idata['main_voter_id']=$main_voter_id;
                  $this->Model_voters->insert_merges($Idata);
          }
            $this->message('success', 'تم الدمج بنجاح');
            redirect("admin/dashboard/comms", 'refresh');
        
    
    }
    public function add_task(){  // AppClient/User
        $par_x=$this->uri->segment(4);
        $par_y=$this->uri->segment(5);
        //  print_r(  $par_x);exit();
        
        if($par_x=='voter_names') {
            $this->load->model('admin/Model_voters');
            $check_1='';
            // $selected_rows = explode(',', $selected_rows);
            $all_voters=$this->Model_voters->get_all_voters();
            foreach($all_voters as $one_voter){
                $check_id=$one_voter->id;
                $check= $this->input->post($check_id);
                
                if($check!=null){
                    $check_1=$check_id;
                    
                }
            }
            
            
        }
        
        $this->load->model('admin/Model_voters');
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
          $task_id= $this->Model_voters->insert_task($Idata);
            
              $task_name=$Idata['name'];
                $task_note=$Idata['note'];
                $task_phone=$Idata['phone'];
                $task_type_0=$Idata['task_type'];
                $voter_id=$Idata['voter_id'];
                if($task_type_0==0){$task_type='اتصال';}
                else if($task_type_0==1){$task_type='زيارة';}
                else if($task_type_0==2){$task_type='اخرى';}
                $occcu_msg= ' الأسم :'.$task_name.'<br/>'.' الهاتف :'.$task_phone;
                
                $occcu_msg=$occcu_msg.'<br/>'.'نوع المهمة :'.$task_type;
                
                if($task_note !=null){
                    $occcu_msg=$occcu_msg.'<br/>'.'ملاحظة :'.$task_note;
                }
               $cancel_url=base_url()."admin/dashboard/archiefTask/".$task_id;
               $accept_url=base_url()."admin/dashboard/acceptTask/".$task_id;
                $call_url=base_url()."admin/dashboard/increase_phone_counter_using_task/".$voter_id;
               $visit_url=base_url()."admin/dashboard/edit_last_visit_using_task/".$voter_id;
                  $all_phones=$this->Model_voters->fetch_voter_withId($voter_id);
                    // print_r( $all_phones[0]); exit();
                if($task_type_0 ==0){
                $occcu_msg=$occcu_msg.'<br/>'."<a  href=".$call_url.">"."للاتصال اضغط هنا"."</a>";
                }
                else if($task_type_0 ==1){
                $occcu_msg=$occcu_msg.'<br/>'."<a  href=".$visit_url.">"."للزيارة اضغط هنا"."</a>";
                }
                $occcu_msg=$occcu_msg.'<br/>'."<a  href=".$cancel_url.">"."لإلغاء المهمة إضغط هنا"."</a>";
                $occcu_msg=$occcu_msg.'<br/>'."<a  href=".$accept_url.">"."لإكتمال المهمة إضغط هنا"."</a>";

                
            //    print_r($occcu_msg);exit();
               $this->add_msg_to_voter($occcu_msg,'55544445');
                
               $phone1=$all_phones[0]->phone;
                    $phone2=$all_phones[0]->phone2;
                    
                    $this->add_msg_to_voter($occcu_msg,$phone1);
                    $this->add_msg_to_voter($occcu_msg,$phone2);
                    
                
            
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/tasks", 'refresh');
        }
        if( $check_1 !=null){
            
            $voter_details= $this->Model_voters->fetch_voter_withId( $check_1);
            
            $data['voter_details']=$voter_details[0];
            //  print_r( $data['voter_details']);exit();
        }
        
        if($par_y !=null && $par_y !=0){
            
            $data["result_id"] = $this->Model_voters->getByArray_task(array("id" => $par_y));
            
            
            $data['title']="تعديل مهمة";
            $data['metakeyword']=$data['title'];
            $data['metadiscription']=$data['title'];
            $data["my_footer"]=array("validator","live_select");
            $data['subview'] = 'admin/tasks/add_task';
            $this->load->view('admin_index', $data);
        }
        else{
        $data["out_fild"] = $this->Model_voters->get_field();
        
        $data['title']="اضافة مهمة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/tasks/add_task';
        $this->load->view('admin_index', $data);
        }
    }
    public function add_jobs(){  // AppClient/User
        $this->load->model('admin/Model_jobs');
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_jobs->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/jobs", 'refresh');
        }
        $data["out_fild"] = $this->Model_jobs->get_field();
       
         $data['title']="اضافة مهنة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/jobs/add_jobs';
        $this->load->view('admin_index', $data);
    }
    public function add_thighs(){  // AppClient/User
        $this->load->model('admin/Model_thighs');
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_thighs->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/thighs", 'refresh');
        }
        $data["out_fild"] = $this->Model_thighs->get_field();
       
         $data['title']="اضافة فخذ";
        $data['metakeyword']="اضافة فخذ";
        $data['metadiscription']="اضافة فخذ";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/thighs/add_thighs';
        $this->load->view('admin_index', $data);
    }
    public function add_friend_with_voter(){  // AppClient/User
        $this->load->model('admin/Model_voters');
$id=$this->uri->segment(4);
//print_r($id);exit();
        if ($this->input->post('INSERT') == "INSERT") {
           // print_r($Idata);exit();
            $Idata = $this->input->post('data_post');
            $set_data = $this->session->all_userdata();
            $Idata['user_id']=$set_data['user_id'];
            $this->Model_voters->insert_friend($Idata);

            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/friends", 'refresh');
        }
        $data["out_fild"] = $this->Model_voters->get_field();
        $voter_details= $this->Model_voters->fetch_voter_withId($id);
        $voter_check= $this->Model_voters->check_voter($id);
if($voter_check !=null){
    $this->message('error', 'تم اضافة هذا الناخب كصديق من قبل ,يرجى تحديد ناخب اخر');
            redirect("admin/dashboard/add_friend", 'refresh');
}
        $data['voter_details']=$voter_details;
                 $data['title']="اضافة صديق";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/friends/add_friend';
        $this->load->view('admin_index', $data);
    }    
    public function add_friend(){  // AppClient/User
        $this->load->model('admin/Model_voters');

     
        $data["out_fild"] = $this->Model_voters->get_field();
        $data["all_voters"]=$this->Model_voters->get_all_voters();
         $data['title']="اضافة صديق";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/all_voters';
        $this->load->view('admin_index', $data);
    }
    public function add_voters(){  // AppClient/User
        $this->load->model('admin/Model_voters');
        $this->load->model('admin/Model_thighs');
        $this->load->model('admin/Model_jobs');
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_voters->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/add_voters", 'refresh');
        }
        $data["out_fild"] = $this->Model_voters->get_field();
        $data["all_jobs"]=$this->Model_jobs->get_all_jobs();
        $data["all_thighs"]=$this->Model_thighs->get_all_thighs();
         $data['title']="اضافة ناخب";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_voter';
        $this->load->view('admin_index', $data);
    }
    
    public function   add_friend_msgs($voter_id){
        $this->load->model('admin/Model_voters');
       
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            
            $this->Model_voters->update($voter_id,$Idata);

            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/friend_msgs", 'refresh');
        }



        $voter_details= $this->Model_voters->fetch_voter_withId($voter_id);
        $data['voter_details']=$voter_details;
        $data["out_fild"] = $this->Model_voters->get_field();
        $data["all_voters"]=$this->Model_voters->get_all_voters();
         $data['title']="اضافة رسالة خاصة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/friends/add_msg';
     //   print_r($data);exit();
        $this->load->view('admin_index', $data);
    }

    public function   add_msgs($voter_id){
        $this->load->model('admin/Model_voters');
       
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            
            $this->Model_voters->update($voter_id,$Idata);

            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/messages", 'refresh');
        }



        $voter_details= $this->Model_voters->fetch_voter_withId($voter_id);
        $data['voter_details']=$voter_details;
        $data["out_fild"] = $this->Model_voters->get_field();
        $data["all_voters"]=$this->Model_voters->get_all_voters();
         $data['title']="اضافة رسالة خاصة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_msg';
     //   print_r($data);exit();
        $this->load->view('admin_index', $data);
    }
    public function fetch_with_num ()

    {  
        $this->load->model('admin/Model_attendance');

    $this->load->model('admin/Model_voters');
    $Idata = $this->input->post('data_post');

     $voter_details= $this->Model_voters->fetch_voter($Idata['kashf_num']);

     $data['voter_details']=$voter_details;
     $data["out_fild"] = $this->Model_attendance->get_field();
     $data["all_voters"]=$this->Model_voters->get_all_voters();
      $data['title']="اضافة تحضير";
     $data['metakeyword']= $data['title'];
     $data['metadiscription']= $data['title'];
     $data["my_footer"]=array("validator","live_select");
     $data['subview'] = 'admin/attends/add_attend_with_num';
  //   print_r($data);exit();
     $this->load->view('admin_index', $data);

    // print_r($voter_details);exit();
} 
  
public function UpdateAttend($attend_id,$kashf_num){  // AppClient/UpdateUser/
       
    $this->load->model('admin/Model_attendance');
    $this->load->model('admin/Model_voters'); 

   if ($this->input->post('UPDTATE') == "UPDTATE") {
        $Udata = $this->input->post('data_post');
        if($Udata['attend']==1){
            $Udata['reason1']=null;
        }
       else if ($Udata['attend']==0){ $Udata['reason1']= $this->input->post('reason1');
       }
        $Udata['add_time']=date('h:i:s');
        $voter_details= $this->Model_voters->fetch_voter($Udata['kashf_num']);

              
                $this->add_msg_to_voter($voter_details[0]->phone_msg,$voter_details[0]->phone);
               $this->add_msg_to_voter($voter_details[0]->phone_msg,$voter_details[0]->phone2);
             
         $this->Model_attendance->update($attend_id, $Udata);
        //-----------------------------------------------
        $this->message('info', 'تم التعديل بنجاح');
        redirect("admin/dashboard/attends");
    }
  
    $data["result_id"] = $this->Model_attendance->getByArray(array("id" => $attend_id));
//   print_r( $data["result_id"]); exit();
 $this->load->model('admin/Model_voters');

  $voter_details= $this->Model_voters->fetch_voter($kashf_num);

  $data['voter_details']=$voter_details;
    $data['title']="تعديل تحضير";
    $data['metakeyword']=$data['title'];
    $data['metadiscription']=$data['title'];
    $data["my_footer"]=array("validator","live_select");
    $data['subview'] = 'admin/attends/add_attend_with_num';
    $this->load->view('admin_index', $data);
}
public function UpdateTask($id){  // AppClient/UpdateUser/
    
    $this->load->model('admin/Model_voters');
    
    if ($this->input->post('UPDTATE') == "UPDTATE") {
        $Udata = $this->input->post('data_post');
        
        $this->Model_voters->update_task($id, $Udata);
        //-----------------------------------------------
        $this->message('info', 'تم التعديل بنجاح');
        redirect("admin/dashboard/tasks");
    }
    
    $data["result_id"] = $this->Model_voters->getByArray_task(array("id" => $id));
    
    
    $data['title']="تعديل مهمة";
    $data['metakeyword']=$data['title'];
    $data['metadiscription']=$data['title'];
    $data["my_footer"]=array("validator","live_select");
    $data['subview'] = 'admin/tasks/add_task';
    $this->load->view('admin_index', $data);
}
public function UpdatePhone($id){  // AppClient/UpdateUser/
       
    $this->load->model('admin/Model_voters'); 

   if ($this->input->post('UPDTATE') == "UPDTATE") {
        $Udata = $this->input->post('data_post');
    
        $this->Model_voters->update_phone($id, $Udata);
        //-----------------------------------------------
        $this->message('info', 'تم التعديل بنجاح');
        redirect("admin/dashboard/phone_book");
    }
  
    $data["result_id"] = $this->Model_voters->getByArray_phone(array("id" => $id));


    $data['title']="تعديل هاتف";
    $data['metakeyword']=$data['title'];
    $data['metadiscription']=$data['title'];
    $data["my_footer"]=array("validator","live_select");
    $data['subview'] = 'admin/phone_books/add_phone';
    $this->load->view('admin_index', $data);
}
public function UpdateFriend($id){  // AppClient/UpdateUser/
       
    $this->load->model('admin/Model_voters'); 

   if ($this->input->post('UPDTATE') == "UPDTATE") {
        $Udata = $this->input->post('data_post');
    
        $this->Model_voters->update_friend($id, $Udata);
        //-----------------------------------------------
        $this->message('info', 'تم التعديل بنجاح');
        redirect("admin/dashboard/friends");
    }
  
    $data["result_id"] = $this->Model_voters->getByArray_friend(array("id" => $id));

    $data["all_voters"]=$this->Model_voters->get_all_voters();

    $data['title']="تعديل صديق";
    $data['metakeyword']=$data['title'];
    $data['metadiscription']=$data['title'];
    $data["my_footer"]=array("validator","live_select");
    $data['subview'] = 'admin/friends/add_friend';
    $this->load->view('admin_index', $data);
}  

public function get_msg_with_gender ()

{
    $type_id= $this->input->post('occ_type');
    $gender_id= $this->input->post('gender');
    
    $this->load->model('admin/Model_congrat');
    
    $msg_text= $this->Model_congrat->get_msg_with_gender($type_id,$gender_id);
    echo $msg_text[0]->text;
}
public function get_msg ()

    {
       $type_id= $this->input->post('occ_type');
        $this->load->model('admin/Model_congrat');

        $msg_text= $this->Model_congrat->get_msg($type_id);
        echo $msg_text[0]->text;
    }
public function fetch_voter ()

    {

    $this->load->model('admin/Model_voters');

     $voter_details= $this->Model_voters->fetch_voter($this->input->post('kashf_num'));
if($voter_details !=null){
  $html_str='';

  

     $fname=$voter_details[0]->fname;

     $sname=$voter_details[0]->sname;

     $tname=$voter_details[0]->tname;

     $foname=$voter_details[0]->foname;
     $phone=$voter_details[0]->phone;
     $phone2=$voter_details[0]->phone2;
     $kashf_num=$voter_details[0]->kashf_num;
     $thigh_name=$voter_details[0]->thigh_name;


     $html_str.=" <label>الأسم: ".$fname." ".$sname." ".$tname." ".$fname." ".$thigh_name."</label>"."<br/>";
    $html_str.=" <label> الهواتف : ".$phone." , ".$phone2."</label>"."<br/>";
    $html_str.=" <label> رقم الكشف : ".$kashf_num."</label>"."<br/>";


   

    }
    else {
        $html_str.=" <label>"."لا يوجد ناخبين لهذا الرقم"."</label>"."<br/>";

    }
    echo $html_str;
    }
    public function add_phone(){  // AppClient/User
        $this->load->model('admin/Model_voters');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_voters->insert_phone($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/phone_book", 'refresh');
        }
        $data["out_fild"] = $this->Model_voters->get_field();
         $data['title']="اضافة هاتف";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/phone_books/add_phone';
        $this->load->view('admin_index', $data);
    }
    public function add_occ(){  // AppClient/User
        $this->load->model('admin/Model_occ');
        $this->load->model('admin/Model_voters');
        
        if($this->input->get('info') !=null ){
            $numbers=$this->input->get('info');
           // $numbers = explode( ',' , $numbers );
        }
        if ($this->input->post('INSERT') == "INSERT") {
           // print_r($numbers);exit();

            $Idata = $this->input->post('data_post');
           $Idata['occ_type']=$this->input->post('occ_type');
           $numbers = explode( ',' , $numbers );
         
           $insert_id= $this->Model_occ->insert($Idata);

            foreach($numbers as $one_value)  { 
              
                $Ip["occ_id"]=$insert_id;
              
                $Ip["phone_id"]=$one_value;
  

                $this->Model_occ->insert_phones($Ip);
            }
            $congrat_date=$Idata['occ_date'];
                  $occu_id=$insert_id;
                $occcu_msg=$Idata['occ_msg'];
              
                 $current = strtotime(date("yy/m/d"));
                 $date    = strtotime($congrat_date);
                 
                  
                  $datediff = $date - $current;
                  $difference = floor($datediff/(60*60*24));
                 // print_r($difference);exit();
 if($difference==0)
 {
       $all_phones=$this->Model_occ->get_this_occ_phones($occu_id);
                foreach($all_phones as $one_phone){
                    
                    $phone1=$one_phone->phone1;
                    $phone2=$one_phone->phone2;
                    
                    $this->add_msg_to_voter($occcu_msg,$phone1);
                    $this->add_msg_to_voter($occcu_msg,$phone2);
                    
                }
                
                $occu_name=$Idata['name'];
                $occu_note=$Idata['note'];
                $occ_loc=$Idata['location'];
                $occ_place=$Idata['place'];
 
                $occcu_msg= ' اسم المناسبة :'.$occu_name.', المكان :'.$occ_place;
                if($occu_loc !=null){
                    $occcu_msg=$occcu_msg.', اللوكيشن :'.$occ_place;
                }
                if($occu_note !=null){
                    $occcu_msg=$occcu_msg.', ملاحظات :'.$occu_note;
                }
                
                // print_r($occcu_msg);exit();
                $this->add_msg_to_voter($occcu_msg,'55544445');
                
                
     
 }
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/occasions", 'refresh');
      
   
            
            
            }
        $data['numbers']=$numbers;
        //    print_r(gettype($numbers));exit();
        $data['all_numbers']=$this->Model_voters->get_all_phone_numbers();
        $data["out_fild"] = $this->Model_occ->get_field();
         $data['title']="اضافة مناسبة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/occasions/add_occ';
        $this->load->view('admin_index', $data);
    } 
    public function add_congrat(){  // AppClient/User
       
      //  print_r($date);exit();
        $this->load->model('admin/Model_congrat');
        
        $this->load->model('admin/Model_voters');
        
        
        if($this->input->get('info') !=null ){
            $numbers=$this->input->get('info');
          // print_r($numbers);exit();

        }
       // print_r($data['numbers']);exit();
        if ($this->input->post('INSERT') == "INSERT") {
           // print_r($numbers);exit();

            $Idata = $this->input->post('data_post');
           $Idata['occ_type']=$this->input->post('occ_type');
           $Idata['gender']=$this->input->post('gender');

           $numbers = explode( ',' , $numbers );
         
           $insert_id= $this->Model_congrat->insert($Idata);

      
            foreach($numbers as $one_value)  { 
              
                $Ip["congrat_id"]=$insert_id;
              
                $Ip["phone_id"]=$one_value;
  

                $this->Model_congrat->insert_phones($Ip);
            }
            
                 $add_date=$Idata['occ_date'];
                  $add_time=$Idata['for_timepicker'];
                 $congrat_date=$add_date.' '.$add_time;
        
                 $occu_id=$insert_id;
                $occcu_msg=$Idata['occ_msg'];
    
                 $current = strtotime(date('Y/m/d H:i'));
                 $date    = strtotime($congrat_date);
                 
                  
                  $datediff = $date - $current;
                  $difference = floor($datediff/(60*60*24));
                 // print_r($difference);exit();
 if($difference==0)
 {
     //send to msg numbers
     $all_phones=$this->Model_congrat->get_this_congrat_phones($occu_id);
                foreach($all_phones as $one_phone){
                    
                    $phone1=$one_phone->phone1;
                    $phone2=$one_phone->phone2;
                    
                    $this->add_msg_to_voter($occcu_msg,$phone1);
                    $this->add_msg_to_voter($occcu_msg,$phone2);
                    
                }
       //send to admin only
           $occu_type=$Idata['occ_type'];
                $occu_name=$Idata['name'];
                if( $occu_type==0){
                    $type='عرس';
                } else if( $occu_type==1){
                    $type='حفل تخرج';
                } else if( $occu_type==2){
                    $type='عزاء';
                } else if($occu_type==3){
                    $type='عودة من السفر مرافق مريض';
                } else if( $occu_type==4){
                    $type='عشاء عودة من علاج';
                } else if( $occu_type==5){
                    $type='زيارة مريض';
                } else if( $occu_type==6){
                    $type='عشاء تمايم';
                } else if( $occu_type==7){
                    $type='عشاء منزل جديد';
                }else if( $occu_type==8){
                    $type='حج';
                }else if( $occu_type==9){
                    $type='وظيفة';
                }else if( $occu_type==10){
                    $type='افتتاح مشروع';
                }else if( $occu_type==11){
                    $type='حادث تصادم';
                }else if( $occu_type==12){
                    $type='نجاح بالانتخابات';
                }else if( $occu_type==13){
                    $type='عودة من سفر';
                }else if( $occu_type==14){
                    $type='ترقية';
                }else if( $occu_type==15){
                    $type='عقد قران';
                }
            $send_msg= 'تم ارسال رسالة '.$type.' إلى';
                $send_msg= $send_msg.'<br/>'.'اسم : ';
                
                $phones_num='';
                foreach($all_phones as $one_phone){
                    
                    $phone1=$one_phone->phone1;
                    $phone2=$one_phone->phone2;
                    $name=$one_phone->name;
                    if($phone2 !=null){
                    $phones_num=$phone1.','.$phone2;

                    }
                    else {
                         $phones_num=$phone1;
                    }
                 $send_msg= $send_msg.$name.' - '.$phones_num;
                       $send_msg= $send_msg.'<br/>';
                }
              //  $phones_num=substr($phones_num, 0, -1);

                $send_msg= $send_msg.'نص الرسالة:';
                 $send_msg= $send_msg.'<br/>'.$occcu_msg;
                 print_r($send_msg);exit();
                $this->add_msg_to_voter($send_msg,'55544445');
                
            }
           $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/congrat", 'refresh');
        }
       // $numbers = explode( ',' , $numbers );
        
        $data['numbers']=$numbers;
    //    print_r(gettype($numbers));exit();
        $data['all_numbers']=$this->Model_voters->get_all_phone_numbers();
        $data["out_fild"] = $this->Model_congrat->get_field();
         $data['title']="اضافة تهنئة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/congrat/add_congrat';
        $this->load->view('admin_index', $data);
    } 
    public function add_diwan(){  // AppClient/User
        $par_x=$this->uri->segment(4);
      //  print_r(  $par_x);exit();
        if($par_x=='voter_names') { 
        $this->load->model('admin/Model_voters');
         $check_1='';
       // $selected_rows = explode(',', $selected_rows);
         $all_voters=$this->Model_voters->get_all_voters();
        foreach($all_voters as $one_voter){
            $check_id=$one_voter->id;
            $check= $this->input->post($check_id);
            
            if($check!=null){
                $check_1=$check_id;
               
            }
        }
  
       
        }
        
        
        $this->load->model('admin/Model_diwans');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_diwans->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/diwans", 'refresh');
        }
        if( $check_1 !=null){
           
            $voter_details= $this->Model_voters->fetch_voter_withId( $check_1);
          
            $data['voter_details']=$voter_details[0];
          //  print_r( $data['voter_details']);exit();
        }
        $data["out_fild"] = $this->Model_diwans->get_field();
        $this->load->model('admin/Model_areas');
        $data["all_areas"]=$this->Model_areas->get_all_areas();
         $data['title']="اضافة دوانية";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/diwans/add_diwan';
        $this->load->view('admin_index', $data);
    }  
    
    public function  occ_msgs(){
   
        $this->load->model('admin/Model_occ');

        $data["all_occ"]=$this->Model_occ->get_all_occ();
        $title = "رسائل المناسبات";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/occasions/all_occ_msg';
        $this->load->view('admin_index', $data);
    }
    public function check_occ_msgs_to_admin(){
        $this->load->model('admin/Model_occ');

        $all_occ=$this->Model_occ->get_all_valid_occ();
   if($all_occ !=null){
       foreach($all_occ as $one_occ){
           $occu_id=$one_occ->id;
           $occu_name=$one_occ->name;
           $occu_note=$one_occ->note;
           $occ_loc=$one_occ->location;
           $occ_place=$one_occ->place;
           $occcu_msg= ' اسم المناسبة :'.$occu_name.'<br/>'.' المكان :'.$occ_place.'<br/>';
           if($occu_loc !=null){
               $occcu_msg=$occcu_msg.' اللوكيشن :'.$occ_place.'<br/>';
           }
           if($occu_note !=null){
            $occcu_msg=$occcu_msg.' ملاحظات :'.$occu_note;
        }

          // print_r($occcu_msg);exit();
            $this->add_msg_to_voter($occcu_msg,'55544445');

           


       }
    
    }
 echo 'done';
    }
    public function check_task_msgs_to_admin(){
        $this->load->model('admin/Model_voters');
        
        $all_tasks=$this->Model_voters->get_all_tasks();
        if($all_tasks !=null){
            foreach($all_tasks as $one_task){
                $task_name=$one_task->name;
                $task_note=$one_task->note;
                $task_phone=$$one_task->phone;
                $task_type=$one_task->task_type;
                $occcu_msg= ' الأسم :'.$task_name.'<br/>'.' الهاتف :'.$task_phone.'<br/>';

                $occcu_msg=$occcu_msg.' نوع المهمة :'.$task_type.'<br/>';
               
                if($occu_note !=null){
                    $occcu_msg=$occcu_msg.' ملاحظة :'.$task_note;
                }
                
                // print_r($occcu_msg);exit();
                $this->add_msg_to_voter($occcu_msg,'55544445');
                
                
                
                
            }
            
        }
        echo 'done';
    }
    public function check_congrat_msgs(){
        $this->load->model('admin/Model_congrat');

        $all_congrat=$this->Model_congrat->get_all_valid_congrat();
   if($all_congrat !=null){
       foreach($all_congrat as $one_congrat){
           $occu_id=$one_congrat->id;
           $occcu_msg=$one_congrat->occ_msg;
           
           $all_phones=$this->Model_congrat->get_this_congrat_phones($occu_id);
           foreach($all_phones as $one_phone){

            $phone1=$one_phone->phone1;
            $phone2=$one_phone->phone2;
          
            $this->add_msg_to_voter($occcu_msg,$phone1);
            $this->add_msg_to_voter($occcu_msg,$phone2);

           }


       }
    
    }
 echo 'done';
    }
    public function check_congrat_msgs_to_admin(){
        $this->load->model('admin/Model_congrat');

        $all_congrat=$this->Model_congrat->get_all_valid_congrat();
   if($all_congrat !=null){
       
    foreach($all_congrat as $one_congrat){
        $occu_id=$one_congrat->id;
        $occcu_msg=$one_congrat->occ_msg;
        $occu_type=$one_congrat->occ_type;
        $occu_name=$one_congrat->name;
        if($occu_type=='0'){
            $occ_type_name='عرس';
        } else if($occu_type=='1'){
            $occ_type_name='حفل تخرج';
        }else if($occu_type=='2'){
            $occ_type_name='عزاء';
        }
        else if($occu_type=='3'){
            $occ_type_name='عشاء';
        }
        else if($occu_type=='6'){
            $occ_type_name='عشاء تمايم';
        }
        else if($occu_type=='7'){
            $occ_type_name='عشاء منزل جديد';
        }else if($occu_type=='4'){
            $occ_type_name='عشاء عودة من علاج';
        }else if($occu_type=='5'){
            $occ_type_name='زيارة مريض';
        }
        else if($occu_type=='8'){
            $occ_type_name='حج';
        }
        $send_msg= ' تم تهنئة /'.$occu_name.'<br/>'.' بمناسبة )'.$occ_type_name.')<br/>';

        $all_phones=$this->Model_congrat->get_this_congrat_phones($occu_id);
       $phones_num='';
        foreach($all_phones as $one_phone){

         $phone1=$one_phone->phone1;
         $phone2=$one_phone->phone2;
       
            $phones_num=$phones_num.$phone1.',';
            if($phone2 !=null){
                $phones_num=$phones_num.$phone2;
            }
        }
        $phones_num=substr($phones_num, 0, -1);

           $send_msg= $send_msg.' الهواتف :'.$phones_num.'<br/>'.' الرسالة :'.$occcu_msg.'<br/>';

        $this->add_msg_to_voter($send_msg,'55544445');

    }
       }
    
    
 echo 'done';
    }
    public function check_occ_msgs(){
        $this->load->model('admin/Model_occ');

        $all_occ=$this->Model_occ->get_all_valid_occ();
   if($all_occ !=null){
       foreach($all_occ as $one_occ){
           $occu_id=$one_occ->id;
           $occcu_msg=$one_occ->occ_msg;
           
           $all_phones=$this->Model_occ->get_this_occ_phones($occu_id);
           foreach($all_phones as $one_phone){

            $phone1=$one_phone->phone1;
            $phone2=$one_phone->phone2;
          
            $this->add_msg_to_voter($occcu_msg,$phone1);
            $this->add_msg_to_voter($occcu_msg,$phone2);

           }


       }
    
    }
 echo 'done';
    }
    
      public function  archief_congrat(){
   
        $this->load->model('admin/Model_congrat');

        $data["all_congrat"]=$this->Model_congrat->get_all_congrat();
        $title = "أرشيف الرسائل";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/congrat/all_archief_congrat';
        $this->load->view('admin_index', $data);
    }
    public function  congrat(){
   
        $this->load->model('admin/Model_congrat');

        $data["all_congrat"]=$this->Model_congrat->get_all_congrat();
        $title = "الرسائل";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/congrat/all_congrat';
        $this->load->view('admin_index', $data);
    }
    
    public function  congrat_msgs(){
   
        $this->load->model('admin/Model_congrat');

        $data["all_congrat_msgs"]=$this->Model_congrat->get_all_congrat_msgs();
        $title = "رسائل التهانى";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/congrat/all_congrat_msgs';
        $this->load->view('admin_index', $data);
    }
    public function  occasions(){
   
        $this->load->model('admin/Model_occ');

        $data["all_occ"]=$this->Model_occ->get_all_occ();
        $title = "المناسبات";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/occasions/all_occ';
        $this->load->view('admin_index', $data);
    }
    public function  search_result($diwan_type1){
         $area=$this->input->post('area');
         $diwan_type=$this->input->post('diwan_types');
         $day_type=$this->input->post('day_type');
        $this->load->model('admin/Model_diwans');
        $this->load->model('admin/Model_areas');
        
        $page=$this->input->post('page');
        $data['d_type']=$diwan_type1;
         $data['page']=$page;
        $data["all_diwans"]=$this->Model_diwans->get_all_search_result($diwan_type,$area,$day_type);
        $data["all_areas"]=$this->Model_areas->get_all_areas();
          $data['area_result']=$area;
          $data['type_result']=$diwan_type;
          $data['day_result']=$day_type;
        $title = "نتائج البحث";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/diwans/all_diwans';
        $this->load->view('admin_index', $data);
    }
    
    public function  diwans($diwan_type){
         
     //   print_r($diwan_type);exit();
        $this->load->model('admin/Model_diwans');
        $this->load->model('admin/Model_areas');

            if ($diwan_type==null || $diwan_type=='all'){
                $diwan_type='all';
                $page='all_d';
            }
        $data["all_diwans"]=$this->Model_diwans->get_all_diwans($diwan_type);
        $data["all_areas"]=$this->Model_areas->get_all_areas();
            $data['d_type']=$diwan_type;
            $data['page']=$page;
        $title = "الدوانيات";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/diwans/all_diwans';
        $this->load->view('admin_index', $data);
    }
    
    public function  friend_notes($voter_id){
   
        $this->load->model('admin/Model_voters');

        $data["all_notes"]=$this->Model_voters->get_all_friend_notes($voter_id);
        $data['voter_id']=$voter_id;
        $title = "ملاحظات الصديق";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/friends/notes';
        $this->load->view('admin_index', $data);
    }
    public function  notes($voter_id){
   
        $this->load->model('admin/Model_voters');

        $data["all_notes"]=$this->Model_voters->get_all_notes($voter_id);
        $data['voter_id']=$voter_id;
        $title = "ملاحظات الناخب";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/voters/notes';
        $this->load->view('admin_index', $data);
    }
    public function Add_note($voter_id){  // AppClient/User
        $this->load->model('admin/Model_voters');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_voters->insert_notes($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/notes/".$Idata['voter_id'], 'refresh');
        }
        $data['voter_id']=$voter_id;
         $data['title']="اضافة ملاحظة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_note';
        $this->load->view('admin_index', $data);
    }
    public function add_msg_to_voter($phone_msg,$phone){

        $url="http://62.150.26.41/SmsWebService.asmx/send";
           
          
        $params = array(
                'username' => 'azizalsaid1',
                'password' => 'plkmnbcg',
                'token' => 'plomnzbvfrtaghxvdrajhgfx',
                'sender' => 'azizalsaid',
                'message' =>$phone_msg ,
                'dst' => $phone,
                'type' => 'text',
                'coding' => 'unicode', 
                'datetime' => 'now'
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
curl_setopt($ch, CURLOPT_TIMEOUT, 60);

// This should be the default Content-type for POST requests
//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));

$result = curl_exec($ch);
if(curl_errno($ch) !== 0) {
    //error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
}

curl_close($ch);  


    }
    
    public function  send_friend_msgs($msg_num){
        $this->load->model('admin/Model_voters'); 
        $all_voters= $this->Model_voters->get_all_voters();
  //     print_r($all_voters[0]);exit();
   //   print_r($all_voters[0]->friend_msg1);exit();

       foreach ($all_voters as $row ){ 
   //     print_r($row->friend_msg1);exit();
        if($msg_num =='msg1' && $row->friend_msg1 !=null  ){
           // print_r($row->friend_msg1);exit();
            $this->add_msg_to_voter($row->friend_msg1,$row->phone);
            $this->add_msg_to_voter($row->friend_msg1,$row->phone2);
            }
            else  if($msg_num=='msg2' && $row->friend_msg2 !=null && $row->friend_msg2 !='' ){
                $this->add_msg_to_voter($row->friend_msg2,$row->phone);
                $this->add_msg_to_voter($row->friend_msg2,$row->phone2);
                }
            else  if($msg_num=='msg3' && $row->friend_msg3 !=null && $row->friend_msg3 !=''){
                $this->add_msg_to_voter($row->friend_msg3,$row->phone);
                $this->add_msg_to_voter($row->friend_msg3,$row->phone2);
                    }
            else {
                continue;
            }
    
        }
        
    $this->message('success', 'تم ارسال الرسالة بنجاح');
    redirect("admin/dashboard/friend_msgs");
    }
  public function  send_voter_msgs($msg_num){
    $this->load->model('admin/Model_voters'); 
    $all_voters= $this->Model_voters->get_all_voters();
  //  print_r($all_voters);exit();
   foreach ($all_voters as $row ){ 
   
    if($msg_num=='msg1' && $row->msg1 !=null ){
        $this->add_msg_to_voter($row->msg1,$row->phone);
        $this->add_msg_to_voter($row->msg1,$row->phone2);
        }
        else  if($msg_num=='msg2' && $row->msg2 !=null ){
            $this->add_msg_to_voter($row->msg2,$row->phone);
            $this->add_msg_to_voter($row->msg2,$row->phone2);
            }
        else  if($msg_num=='msg3' && $row->msg3 !=null ){
            $this->add_msg_to_voter($row->msg3,$row->phone);
            $this->add_msg_to_voter($row->msg3,$row->phone2);
                }
        else {
            continue;
        }

    }

    
    $this->message('success', 'تم ارسال الرسالة بنجاح');
    redirect("admin/dashboard/messages");
    }
    public function add_attends(){  // AppClient/User
        $this->load->model('admin/Model_voters');
        $this->load->model('admin/Model_attendance');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $Idata['reason1']= $this->input->post('reason1');
            $Idata['add_time']=date('h:i:s');
            $this->Model_attendance->insert($Idata);
            
            $kashf_num=$Idata['kashf_num'];
            $this->load->model('admin/Model_voters'); 
            $voter_details= $this->Model_voters->fetch_voter($Idata['kashf_num']);

                  
                    $this->add_msg_to_voter($voter_details[0]->phone_msg,$voter_details[0]->phone);
                   $this->add_msg_to_voter($voter_details[0]->phone_msg,$voter_details[0]->phone2);
                  

            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/add_attends", 'refresh');
        }
        $data["out_fild"] = $this->Model_attendance->get_field();
        $data["all_voters"]=$this->Model_voters->get_all_voters();
         $data['title']="اضافة تحضير";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/attends/add_attend';
        $this->load->view('admin_index', $data);
    }
    
    public function UpdateNotes($note_id,$voter_id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_voters');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_voters->update_note($note_id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/notes/".$voter_id);
        }
      
        $data["result_id"] = $this->Model_voters->getByArray_note(array("id" => $note_id));
     //   print_r( $data["result_id"]); exit();
   
         $data['voter_id']=$voter_id;
        $data['title']="تعديل ملاحظة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_note';
        $this->load->view('admin_index', $data);
    }
    public function UpdateOcc($id){  // AppClient/UpdateUser/
        $this->load->model('admin/Model_voters');
        
        $this->load->model('admin/Model_occ');
        
        if($this->input->get('info') !=null ){
            $numbers=$this->input->get('info');
           // $numbers = explode( ',' , $numbers );
         // print_r($numbers);exit();

        }
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_occ->update($id, $Udata);
           
             $numbers = explode( ',' , $numbers );
         
             $this->Model_occ->delete_phones($id);
  
              foreach($numbers as $one_value)  { 
                
                  $Ip["occ_id"]=$id;
                
                  $Ip["phone_id"]=$one_value;
    
  
                  $this->Model_occ->insert_phones($Ip);
              }

            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/occasions/");
        }
      
        $data["result_id"] = $this->Model_occ->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
$values='';
       $result=$this->Model_occ->get_all_occ_phones($id);
      foreach($result as $one_res){
      $values=$values.$one_res->phone_id;     
      
      $values=$values.',';  
     
    }
    $values=substr($values, 0, -1);
     
    // print_r( $data["numbers"]); exit();
   
    if($this->input->get('get_p') !=null ){
$data['numbers']=$numbers;

    }else { $data["numbers"]=$values;}
    
    $data['all_numbers']=$this->Model_voters->get_all_phone_numbers();
    
        $data['title']="تعديل مناسبة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/occasions/add_occ';
        $this->load->view('admin_index', $data);
    }
    public function UpdateCongrat($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_congrat');
        $this->load->model('admin/Model_voters');
        if($this->input->get('info') !=null ){
            $numbers=$this->input->get('info');
           // $numbers = explode( ',' , $numbers );
         // print_r($numbers);exit();

        }
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');
           $Udata['occ_type']=$this->input->post('occ_type');
           $Udata['gender']=$this->input->post('gender');

             $this->Model_congrat->update($id, $Udata);
           
             $numbers = explode( ',' , $numbers );
         
             $this->Model_congrat->delete_phones($id);
  
              foreach($numbers as $one_value)  { 
                
                  $Ip["congrat_id"]=$id;
                
                  $Ip["phone_id"]=$one_value;
    
  
                  $this->Model_congrat->insert_phones($Ip);
              }

            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/congrat/");
        }
      
        $data["result_id"] = $this->Model_congrat->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
$values='';
       $result=$this->Model_congrat->get_all_congrat_phones($id);
      foreach($result as $one_res){
      $values=$values.$one_res->phone_id;     
      
      $values=$values.',';  
     
    }
    $values=substr($values, 0, -1);
     
    // print_r( $data["numbers"]); exit();
   
    if($this->input->get('get_p') !=null ){
$data['numbers']=$numbers;

    }else { $data["numbers"]=$values;}
    $data['all_numbers']=$this->Model_voters->get_all_phone_numbers();
    $data['title']="تعديل تهنئة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/congrat/add_congrat';
        $this->load->view('admin_index', $data);
    }
    
    public function UpdateCongratMsg($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_congrat');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_congrat->update_msg($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/congrat_msgs/");
        }
      
        $data["result_id"] = $this->Model_congrat->getByArray_msg(array("id" => $id));
   
        $data['title']="تعديل رسالة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/congrat/add_congrat_msg';
        $this->load->view('admin_index', $data);
    }
    public function UpdateDiwan($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_diwans');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_diwans->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/diwans/");
        }
        $this->load->model('admin/Model_areas');
        $data["all_areas"]=$this->Model_areas->get_all_areas();

        $data["result_id"] = $this->Model_diwans->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل ديوانية";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/diwans/add_diwan';
        $this->load->view('admin_index', $data);
    }
    public function UpdateVoters($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_voters');
       
        if ($this->input->post('update_msg')) {
            $this->Model_voters->update_msg($id);
            $this->message('info','تم إضافة الرسالة بنجاح');
            redirect('admin/dashboard/messages', 'refresh');
        }

       if ($this->input->post('UPDTATE') == "UPDTATE") {
        //   print_r();exit();
        
  
        $Udata = $this->input->post('data_post');

             $this->Model_voters->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/voters", 'refresh');
        }
      
        $data["result_id"] = $this->Model_voters->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
     $this->load->model('admin/Model_thighs');
     $this->load->model('admin/Model_jobs');

     $data["all_jobs"]=$this->Model_jobs->get_all_jobs();

     $data["all_thighs"]=$this->Model_thighs->get_all_thighs();

        $data['title']="تعديل ناخب";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/voters/add_voter';
        $this->load->view('admin_index', $data);
    }
    public function UpdateJobs($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_jobs');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_jobs->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/jobs", 'refresh');
        }
      
        $data["result_id"] = $this->Model_jobs->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل مهنة";
        $data['metakeyword']="تعديل مهنة";
        $data['metadiscription']="تعديل مهنة";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/jobs/add_jobs';
        $this->load->view('admin_index', $data);
    }
    public function UpdateThighs($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_thighs');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Idata = $this->input->post('data_post');

            $Udata["name"]=$Idata["name"];
            $Udata["count"]=$Idata["count"];
             $this->Model_thighs->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/thighs", 'refresh');
        }
      
        $data["result_id"] = $this->Model_thighs->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل فخذ";
        $data['metakeyword']="تعديل فخذ";
        $data['metadiscription']="تعديل فخذ";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/thighs/add_thighs';
        $this->load->view('admin_index', $data);
    }
    public function DeleteTask($id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete_task($id);
        
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/tasks", 'refresh');
    }
    
    public function archiefTask($id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->convert_to_archief_task($id);
        
        $this->message('error', 'تم التحويل إلى الأرشيف');
        redirect("admin/dashboard/tasks", 'refresh');
    }
    public function DeleteMerge($id,$main_id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete_merge($id);
      
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/get_my_merges/".$main_id, 'refresh');
    }
    public function DeleteExclude($id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete_exclude($id);
      
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/all_excludes", 'refresh');
    }
    public function DeletePhone($id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete_phone($id);
      
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/phone_book", 'refresh');
    }
    public function DeleteAttend($id){
        $this->load->model('admin/Model_attendance');
        $this->Model_attendance->delete($id);
      
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/attends", 'refresh');
    }
    public function DeleteNotes($id,$voter_id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete_note($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/notes/".$voter_id);
    }
    public function DeleteVoters($id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/voters", 'refresh');
    }  
    public function DeleteFriend($id){
        $this->load->model('admin/Model_voters');
        $this->Model_voters->delete_friend($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/friends", 'refresh');
    }   
    public function DeleteDiwan($id){
        $this->load->model('admin/Model_diwans');
        $this->Model_diwans->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/diwans", 'refresh');
    }
    
    public function DeleteOcc($id){
        $this->load->model('admin/Model_occ');
        $this->Model_occ->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/occasions", 'refresh');
    }
    public function DeleteCongrat($id){
        $this->load->model('admin/Model_congrat');
        $this->Model_congrat->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/congrat", 'refresh');
    }
    public function DeleteJobs($id){
        $this->load->model('admin/Model_jobs');
        //$Udata["user_show"] = 0;
        $this->Model_jobs->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/jobs", 'refresh');
    }
    public function DeleteThighs($id){
        $this->load->model('admin/Model_thighs');
        //$Udata["user_show"] = 0;
        $this->Model_thighs->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/thighs", 'refresh');
    }

    public function add_area(){  // AppClient/User
        $this->load->model('admin/Model_areas');

        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_areas->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/areas", 'refresh');
        }
        $data["out_fild"] = $this->Model_areas->get_field();
         $data['title']="اضافة منطقة";
        $data['metakeyword']= $data['title'];
        $data['metadiscription']= $data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/areas/add_area';
        $this->load->view('admin_index', $data);
    }
    public function UpdateArea($id){  // AppClient/UpdateUser/
       
        $this->load->model('admin/Model_areas');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_areas->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/areas/");
        }
      
        $data["result_id"] = $this->Model_areas->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
   
        $data['title']="تعديل منطقة";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/areas/add_area';
        $this->load->view('admin_index', $data);
    }
    public function DeleteArea($id){
        $this->load->model('admin/Model_areas');
        //$Udata["user_show"] = 0;
        $this->Model_areas->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/areas", 'refresh');
    }
    public function  areas(){
   
        $this->load->model('admin/Model_areas');
        $data["all_areas"]=$this->Model_areas->get_all_areas();
        $title = "المناطق";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/areas/all_areas';
        $this->load->view('admin_index', $data);
    }

	    public function  presented(){
   
        $this->load->model('admin/Model_basket_competitor');
        $data["all_present"]=$this->Model_basket_competitor->get_all_present();
        $title = "المتقدمين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/basket_competitor/all_present';
        $this->load->view('admin_index', $data);
    }
    public function  accepted(){
   
        $this->load->model('admin/Model_basket_competitor');
        $data["all_accept"]=$this->Model_basket_competitor->get_all_accept();
        $title = "المقبولين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/basket_competitor/all_accept';
        $this->load->view('admin_index', $data);
    }
    public function  refuse(){
   
        $this->load->model('admin/Model_basket_competitor');
        $data["all_refuse"]=$this->Model_basket_competitor->get_all_refuse();
        $title = "المرفوضين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/basket_competitor/all_refuse';
        $this->load->view('admin_index', $data);
    }

    public function UpdateStatus ($id,$status){
        $this->load->model('admin/Model_basket_competitor');
        
        if($status==1){
        $Udata["status"] = $status;
        $this->Model_basket_competitor->update_status($id,$Udata);
        $this->message('info', 'تم قبول المتقدم وتحوليه لصفحة المقبولين');
        redirect("admin/dashboard/presented", 'refresh');}
        else   if($status==2){
            $Udata["status"] = $status;
            $this->Model_basket_competitor->update_status($id,$Udata);
            $this->message('info', 'تم رفض المتقدم وتحويله لصفحة المرفوضين');
            redirect("admin/dashboard/presented", 'refresh');}
    }
 
    public function DeletePerson($id,$page_name){
        $this->load->model('admin/Model_basket_competitor');
        //$Udata["user_show"] = 0;
        $this->Model_basket_competitor->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/".$page_name, 'refresh');
    }
    
    public function UpdatePerson($id,$page_name){  // AppClient/UpdateUser/
       //print_r($page_name);exit();
        $this->load->model('admin/Model_basket_competitor');
       if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Udata = $this->input->post('data_post');

             $this->Model_basket_competitor->update_comp($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
          if($page_name=='presented')  {
            redirect("admin/Dashboard/presented", 'refresh');

            }
            else if($page_name=='accepted')  {
                redirect("admin/Dashboard/accepted", 'refresh');
    
                }
            else if($page_name=='refuse')  {
                redirect("admin/Dashboard/refuse", 'refresh');
    
                }
        }
      
        $data["result_id"] = $this->Model_basket_competitor->getByArray(array("id" => $id));
     //   print_r( $data["result_id"]); exit();
     $data['page_name']=$page_name;
        $data['title']="تعديل بيانات ";
        $data['metakeyword']=$data['title'];
        $data['metadiscription']=$data['title'];
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/basket_competitor/add_present';
        $this->load->view('admin_index', $data);
    }
    public function  form(){

        $data['subview'] = 'backend/form';
        $this->load->view('admin_index', $data);
    }

    public function  accepted_partners(){
        
        $this->load->model('admin/Model_competitor');
        $data["all_accepted"]=$this->Model_competitor->get_all_partners();
        $title = "المشاركين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/competitor/all_partners';
        $this->load->view('admin_index', $data);
    }
    public function  all(){
        
        $this->load->model('admin/Model_competitor');
        $data["all_accepted"]=$this->Model_competitor->get_all_data();
        $title = "الكل";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/competitor/all_partners';
        $this->load->view('admin_index', $data);
    }
    
   
    public function  awards(){
        
        $this->load->model('admin/Model_competitor');
        $data["all_awards"]=$this->Model_competitor->get_all_awards();
        $title = "الجوائز";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/award/all_awards';
        $this->load->view('admin_index', $data);
    }
    
 
    public function add_award(){  // AppClient/User
        $this->load->model('admin/Model_award');
        if ($this->input->post('INSERT') == "INSERT") {
            $Idata = $this->input->post('data_post');
            $this->Model_award->insert($Idata);
            $this->message('success', 'تمت الاضافة  بنجاح');
            redirect("admin/dashboard/awards", 'refresh');
        }
        $data["out_fild"] = $this->Model_award->get_field();
        
        $data['title']="اضافة جائزة";
        $data['metakeyword']="اضافة جائزة";
        $data['metadiscription']="اضافة جائزة";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/award/add_award';
        $this->load->view('admin_index', $data);
    }
    public function edit_image($id,$page_name){  // AppClient/UpdateUser/
        
        $this->load->model('admin/Model_competitor');
        $data["out_fild"] = $this->Model_competitor->get_field();
        $data["result_id"] = $this->Model_competitor->getByArray(array("id" => $id));
        $data['page_name']=$page_name;
        $data['title']="تعديل صورة الايصال";
        $data['metakeyword']="تعديل صورة الايصال";
        $data['metadiscription']="تعديل صورة الايصال";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/competitor/edit_image';
        $this->load->view('admin_index', $data);
    }
    function all_present()
    {
    $this->load->model('admin/Model_competitor');
    $data["all_presenters"]=$this->Model_competitor->get_all_presenters();
    //var_dump($data['all_country']);
    $title = "المتقدمين";
    $data['title']=$title;
    $data['metakeyword']=$title;
    $data['metadiscription']=$title;
    $data["my_footer"]=array("edit_data_table");
    $data['subview'] = 'admin/competitor/all_presenters';
    $this->load->view('admin_index', $data);
    }
    function send_sms($phone,$id,$name)
    {
        
        
        $url="http://62.150.26.41/SmsWebService.asmx/send";
        $link="http://al-saed.com/home/edit_my_photo/".$id;
        
        $params = array(
            'username' => 'azizalsaid1',
            'password' => 'plkmnbcg',
            'token' => 'plomnzbvfrtaghxvdrajhgfx',
            'sender' => 'azizalsaid',
            'message' => 'برجاء اعادة رفع صورة الايصال مرة أخرى للمشاركة'
            .'من خلال هذا الرابط'.
            $link,
            'dst' => $phone,
            'type' => 'text',
            'coding' => 'unicode',
            'datetime' => 'now'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        
        // This should be the default Content-type for POST requests
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
        
        $result = curl_exec($ch);
        
        if(curl_errno($ch) !== 0) {
            //error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
        }
        
        curl_close($ch);
        //print_r($result); exit();
        
        
    }
    public function  send_accept_message($phone,$page_name){
        
        $url="http://62.150.26.41/SmsWebService.asmx/send";
        
        
        $params = array(
            'username' => 'azizalsaid1',
            'password' => 'plkmnbcg',
            'token' => 'plomnzbvfrtaghxvdrajhgfx',
            'sender' => 'azizalsaid',
            'message' => 'تم قبولك في مسابقة القبيلة'
            .'ونتمنى لك التوفيق',
            'dst' => $phone,
            'type' => 'text',
            'coding' => 'unicode',
            'datetime' => 'now'
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        
        // This should be the default Content-type for POST requests
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
        
        $result = curl_exec($ch);
        
        if(curl_errno($ch) !== 0) {
            //error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
        }
        
        curl_close($ch);
        $this->message('info', 'تم ارسال الرسالة بنجاح');
        if($page_name=='presenters'){
            redirect("admin/Dashboard", 'refresh');
            
        }
        else {
            redirect("admin/Dashboard/".$page_name, 'refresh');
            
        }
    }
    
    public function send_message($phone,$id,$name,$page_name){  // AppClient/UpdateUser/
        
        $this->load->model('admin/Model_competitor');
        $this->send_sms($phone,$id,$name);
        // $Udata['send_request']=1;
        $this->Model_competitor->update_state($id);
        
        
        $this->message('info', 'تم ارسال الرسالة بنجاح');
        if($page_name=='presenters'){
            redirect("admin/Dashboard", 'refresh');
            
        }
        else {
            redirect("admin/Dashboard/".$page_name, 'refresh');
            
        }
    }
    public function edit_conditions(){  // AppClient/UpdateUser/
        
        $this->load->model('admin/Model_competitor');
        
        $Udata["descr"]=$this->input->post('descr');
        $this->Model_competitor->update_conditions($Udata);
        //-----------------------------------------------
        $this->message('info', 'تم التعديل بنجاح');
        redirect("admin/dashboard/conditions", 'refresh');
        
        
    }
    public function edit_targets(){  // AppClient/UpdateUser/
        
        $this->load->model('admin/Model_competitor');
        
        $Udata["descr"]=$this->input->post('descr');
        $this->Model_competitor->update_targets($Udata);
        //-----------------------------------------------
        $this->message('info', 'تم التعديل بنجاح');
        redirect("admin/dashboard/targets", 'refresh');
        
        
    }
    
    public function UpdateImage($id,$page_name){  // AppClient/UpdateUser/
        // print_r($page_name);exit();
        $this->load->model('admin/Model_competitor');
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|doc|jpeg';
        $config['max_size']             = "20971520";
        
        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('wasl_photo'))
        {
            $data['imageError'] =  $this->upload->display_errors();
            
        }
        else
        {
            $imageDetailArray = $this->upload->data();
            $image =  $imageDetailArray['file_name'];
        }
        
        //    $main_iamge = $this->upload_image("wasl_photo");
        //    print_r($image);exit();
        if($image==null){
            $this->message('error', 'يوجد خطأ بالصورة
                الرجاء رفع الصورة مرة اخرى');
            redirect("admin/dashboard/edit_image/".$id, 'refresh');
            
        }
        else{
            $Udata["wasl_photo"] = $image;
        }
        
        $this->Model_competitor->update_image($id, $Udata);
        //-----------------------------------------------
        $this->message('info', 'تم التعديل بنجاح');
        if($page_name=='presenters'){
            redirect("admin/Dashboard", 'refresh');
            
        }
        else {
            redirect("admin/Dashboard/".$page_name, 'refresh');
            
        }
        
        
    }
    
    public function UpdatePresenter($id){  // AppClient/UpdateUser/
        
        $this->load->model('admin/Model_competitor');
        if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Idata = $this->input->post('data_post');
            
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png|pdf|doc|jpeg';
            $config['max_size']             = "20971520";
            
            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('wasl_photo'))
            {
                $data['imageError'] =  $this->upload->display_errors();
                
            }
            else
            {
                $imageDetailArray = $this->upload->data();
                $image =  $imageDetailArray['file_name'];
            }
            
            //    $main_iamge = $this->upload_image("wasl_photo");
            //    print_r($image);exit();
            if($image==null){
                $this->message('error', 'يوجد خطأ بالصورة
                الرجاء رفع الصورة مرة اخرى');
                redirect("admin/dashboard/edit_image/".$id, 'refresh');
                
            }
            else{
                $Idata["wasl_photo"] = $image;
            }
            
            $this->Model_competitor->update($id, $Idata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard", 'refresh');
        }
        
        $data["result_id"] = $this->Model_competitor->getByArray(array("id" => $id));
        //   print_r( $data["result_id"]); exit();
        
        $data['title']="تعديل متقدم";
        $data['metakeyword']="تعديل متقدم";
        $data['metadiscription']="تعديل متقدم";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/competitor/update_competitor';
        $this->load->view('admin_index', $data);
    }
   
    public function UpdateAward($id){  // AppClient/UpdateUser/
        
        $this->load->model('admin/Model_award');
        if ($this->input->post('UPDTATE') == "UPDTATE") {
            $Idata = $this->input->post('data_post');
            
            $Udata["name"]=$Idata["name"];
            $Udata["descr"]=$Idata["descr"];
            $this->Model_award->update($id, $Udata);
            //-----------------------------------------------
            $this->message('info', 'تم التعديل بنجاح');
            redirect("admin/dashboard/awards", 'refresh');
        }
        
        $data["result_id"] = $this->Model_award->getByArray(array("id" => $id));
        //   print_r( $data["result_id"]); exit();
        
        $data['title']="تعديل جائزة";
        $data['metakeyword']="تعديل جائزة";
        $data['metadiscription']="تعديل جائزة";
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'admin/award/add_award';
        $this->load->view('admin_index', $data);
    }
    
    
    //------------------------------------------------------------------
    public function DeleteAward($id){
        $this->load->model('admin/Model_award');
        //$Udata["user_show"] = 0;
        $this->Model_award->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard/awards", 'refresh');
    }
    
    public function  conditions(){
        
        $this->load->model('admin/Model_competitor');
        $data["all_conditions"]=$this->Model_competitor->get_all_conditions();
        $title = "الشروط";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/competitor/all_conditions';
        $this->load->view('admin_index', $data);
    }
    public function  targets(){
        
        $this->load->model('admin/Model_competitor');
        $data["all_targets"]=$this->Model_competitor->get_all_targets();
        $title = "الأهداف";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/competitor/all_targets';
        $this->load->view('admin_index', $data);
    }
    public function  refused_partners(){
        
        $this->load->model('admin/Model_competitor');
        $data["all_refused"]=$this->Model_competitor->get_all_refused();
        $title = "المرفوضين";
        $data['title']=$title;
        $data['metakeyword']=$title;
        $data['metadiscription']=$title;
        $data["my_footer"]=array("edit_data_table");
        $data['subview'] = 'admin/competitor/all_refused';
        $this->load->view('admin_index', $data);
    }
    function send_sms_accept($phone,$name)
    {
        
        
        $url="http://62.150.26.41/SmsWebService.asmx/send";
        
        
        $params = array(
            'username' => 'azizalsaid1',
            'password' => 'plkmnbcg',
            'token' => 'plomnzbvfrtaghxvdrajhgfx',
            'sender' => 'azizalsaid',
            'message' => 'تم قبولك في مسابقة القبيلة'
            .'ونتمنى لك التوفيق',
            'dst' => $phone,
            'type' => 'text',
            'coding' => 'unicode',
            'datetime' => 'now'
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        
        // This should be the default Content-type for POST requests
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
        
        $result = curl_exec($ch);
        
        if(curl_errno($ch) !== 0) {
            //error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
        }
        
        curl_close($ch);
        //print_r($result);
        
        
    }
    public function AcceptPresenter ($phone,$id,$name){
        $this->load->model('admin/Model_competitor');
        //$Udata["user_show"] = 0;
        //   $this->send_sms_accept($phone,$name);
        
        $this->Model_competitor->update_accept($id);
        $this->message('info', 'تم قبول المتقدم وتحوليه لصفحة المشاركين');
        redirect("admin/dashboard", 'refresh');
    }
    public function RefusedPresenter ($id){
        $this->load->model('admin/Model_competitor');
        //$Udata["user_show"] = 0;
        $this->Model_competitor->update_refuse($id);
        $this->message('info', 'تم رفض المتقدم وتحويله لصفحة المرفوضين');
        redirect("admin/dashboard", 'refresh');
    }
    public function DeletePresenter($id){
        $this->load->model('admin/Model_competitor');
        //$Udata["user_show"] = 0;
        $this->Model_competitor->delete($id);
        $this->message('error', 'تم الحذف');
        redirect("admin/dashboard", 'refresh');
    }

    /**
 * ===============================================================================================================
 * 
 * ========================             ALL FUNCTIONS            =================================================
 * 
 * ===============================================================================================================
 */
    //===================================================================================================================
    public  function GroupsPages(){  // Dashboard/GroupsPages
        $this->load->model('system_management/Groups');
        $this->load->model('Difined_model');
        if ($this->input->post('add_group')) {
            $file= $this->upload_image('page_image',"images");
            $this->Groups->addGroup($file);
            $this->message('success','تمت إضافة المجموعة بنجاح');
            redirect('admin/Dashboard/GroupsPages', 'refresh');
        }
        $data["font_icon"]=$this->Difined_model->select_limit("font_icons","","id","ASC");
        $data["last_order"]=$this->Groups->select_order(0);
        $data["groups_table"] = $this->Groups->fetch_groups('', '');
        $data['title'] = 'إضافة إدارات البرنامج';
        $data['metakeyword'] = 'إضافة إدارات البرنامج';
        $data['metadiscription'] = 'إضافة إدارات البرنامج';
        $data['subview'] = 'backend/groups_pages/group';
        $data["my_footer"]=array("edit_data_table","validator","live_select");
        $this->load->view('admin_index', $data);
    }
    public function UpdateGroupsPages($id){
        $this->load->model('system_management/Groups');
        $this->load->model('Difined_model');
        $data['result_id'] = $this->Groups->getgroupbyid($id);
        if ($this->input->post('edit_group')) {
            $file= $this->upload_image('page_image',"images");
            $this->Groups->updateGroup($id,$file);
            $this->message('info','تمت تعديل المجموعة بنجاح');
            redirect('admin/Dashboard/GroupsPages', 'refresh');
        }
        $data["font_icon"]=$this->Difined_model->select_limit("font_icons","","id","ASC");
        $data['title'] = 'تعديل بيانات الإدارة';
        $data['metakeyword'] = 'تعديل بيانات الإدارة';
        $data['metadiscription'] = '';
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'backend/groups_pages/group';
        $this->load->view('admin_index', $data);
    }
    /*  public function DeleteGroupsPages($id){
     $this->load->model('Difined_model');
     $this->Difined_model->delete("pages",array("page_id"=>$id));
    // $this->message('success','تم الحذف');
     redirect('Dashboard/GroupsPages', 'refresh');
     }
    */
    //===================================================================================================================
    public  function AddPages(){  // Dashboard/AddPages
        $this->load->model('system_management/Pages');
        $this->load->model('system_management/Groups');
        $this->load->model('Difined_model');
        if ($this->input->post('add_group')) {
            // $file= $this->upload_image('page_image');
            $file="0";
            $this->Pages->insert($file);
            $this->message('success','تمت إضافة الصفحة بنجاح');
            redirect('admin/Dashboard/AddPages', 'refresh');
        } if ($this->input->post('get_page_order')) {
            echo (1+$this->Groups->select_order($this->input->post('get_page_order')));
        }else{
            $data["font_icon"]=$this->Difined_model->select_limit("font_icons","","id","ASC");
            $data["groups_table"] = $this->Pages->all_pages('', '');
            $data['pages_name']=$this->Pages->main_groups_name();
            $data["main_groups"]=$this->Groups->level_groups();
            $data['title'] = ' بيانات صفحة تحكم';
            $data['metakeyword'] = 'اعدادات الموقع ';
            $data['metadiscription'] = '';
            $data["my_footer"]=array("edit_data_table","validator","live_select");
            $data['subview'] = 'backend/groups_pages/pages';
            $this->load->view('admin_index', $data);
        }
    }
    public function UpdatePages($id){
        $this->load->model('system_management/Pages');
        $this->load->model('system_management/Groups');
        $this->load->model('Difined_model');
        $data['result_id'] = $this->Pages->get_by_id($id);
        if ($this->input->post('edit_group')) {
            $file= $this->upload_image('page_image',"images");
            $this->Pages->update($id,$file);
            $this->message('info','تمت تعديل الصفحة بنجاح');
            redirect('admin/Dashboard/AddPages', 'refresh');
        }
        $data['pages_name']=$this->Pages->main_groups_name();
        $data["main_groups"]=$this->Groups->level_groups();
        $data["font_icon"]=$this->Difined_model->select_limit("font_icons","","id","ASC");
        $data['title'] = 'تعديل بيانات صفحة تحكم';
        $data['metakeyword'] = 'اعدادات الموقع ';
        $data['metadiscription'] = '';
        $data["my_footer"]=array("validator","live_select");
        $data['subview'] = 'backend/groups_pages/pages';
        $this->load->view('admin_index', $data);
    }
    public function DeletePages($id){
        $this->load->model('Difined_model');
        $this->Difined_model->delete("pages",array("page_id"=>$id));
        $this->message('error','تم الحذف');
        redirect('admin/Dashboard/AddPages', 'refresh');
    }
    //===================================================================================================================
    public function AddUsers(){  // Dashboard/AddUsers
        $this->load->model('system_management/User');
        if ($this->input->post('add_user')) {
            $file= $this->upload_image('user_photo');
            $this->User->insert($file);
            $this->message('success','تمت إضافة المستخدم بنجاح');
            redirect('admin/Dashboard/AddUsers', 'refresh');
        }
        $data["user_per"]=$this->User->select_user_per();
        $data["users_table"]=$this->User->all_users($_SESSION["role_id_fk"]);
        $data['title']='بيانات المستخدم ';
        $data['metakeyword']='بيانات المستخدم ';
        $data['metadiscription']='بيانات المستخدم ';
        $data["my_footer"]=array("edit_data_table","validator","live_select");
        $data['subview'] = 'backend/users/user';
        $this->load->view('admin_index', $data);
    }

    public function UpdateUsers($id){
        $this->load->model('system_management/User');
        $data['result_id'] = $this->User->get_by_id($id);
        if ($this->input->post('edit_user')) {
            $file= $this->upload_image('user_photo');
            $this->User->update($id,$file);
            $this->message('info','تمت تعديل المستخدم بنجاح');
            redirect('admin/Dashboard/AddUsers', 'refresh');
        }
        if ($this->input->post('update_pw')) {
            $this->User->update_pw($id);
            $this->message('info','تمت تعديل كلمة المرور بنجاح');
            redirect('admin/Dashboard/UpdateUsers/'.$id, 'refresh');
        }
        $data["user_per"]=$this->User->select_user_per();
        $data['title']='بيانات المستخدم ';
        $data['metakeyword']='بيانات المستخدم ';
        $data['metadiscription']='بيانات المستخدم ';
        $data["my_footer"]=array("edit_data_table","validator");
        $data['subview'] = 'backend/users/user';
        $this->load->view('admin_index', $data);
    }
    public function DeleteUsers($id){
        $this->Difined_model->delete("admin_voters",array("user_id"=>$id));
        $this->message('error','تم الحذف');
        redirect('admin/Dashboard/AddUsers', 'refresh');
    }
    public function  UpdateUserPassWord(){
        $this->load->model('system_management/User');
        if ($this->input->post('update_pw')) {
            $this->User->update_pw($this->input->post('id'));
            $this->message('info','تمت تعديل كلمة المرور بنجاح');
            redirect('admin/Dashboard', 'refresh');
        }
    }
   //===================================================================================================================
    public function CreateRole(){ // Dashboard/CreateRole
        //-------------------------------------------
        $this->load->model('system_management/Groups');
        $this->load->model('system_management/Permission');
        $this->load->model('Difined_model');
        $data["results_main"]=$this->Groups->get_categories();
        $data["user_in"]=$this->Permission->user_in();
        if ($this->input->post('add_role')) {
            $this->Permission->insert_user_role();
            $this->message('success','تم إضافة الدور بنجاح');
            redirect('admin/Dashboard/CreateRole', 'refresh');
        }
        $data['users']=$this->Difined_model->select_all("admin_voters","user_id","ASC");
        $data['per_table']=$this->Difined_model->select_where_groupy("permissions",array("user_id !="=>0),"user_id");//
        $data["user_name"]=$this->Permission->users_name();
        $data['title'] = 'إضافة أدوار التحكم';
        $data['metakeyword'] = 'إعدادات الموقع ';
        $data['metadiscription'] = '';
        $data["my_footer"]=array("edit_data_table","validator","checkbox");
        $data['subview'] = 'backend/users/roles';
        $this->load->view('admin_index', $data);
    }
    public function UpdateCreateRole($id){
        $this->load->model('system_management/Groups');
        $this->load->model('system_management/Permission');
        $this->load->model('Difined_model');
        $data['users']=$this->Difined_model->select_all("admin_voters","user_id","ASC");
       // print_r( $data['users']);exit();
        $data["results_main"]=$this->Groups->get_categories();
        $data['user_data'] = $this->Difined_model->getByArray("admin_voters",array("user_id"=>$id));
        $data["user_permations"]=$this->Permission->select_per($id);
        if ($this->input->post('edit_role')) {
            $this->Difined_model->delete('permissions',array("user_id"=>$id));
            $this->Permission->insert_user_role();
            $this->message('info','تم التعديل بنجاح');
            redirect('admin/Dashboard/CreateRole', 'refresh');

        }
         $data['title'] = 'إضافة أدوار التحكم';
        $data['metakeyword'] = 'إعدادات الموقع ';
        $data['metadiscription'] = '';
        $data["my_footer"]=array("validator","checkbox");
        $data['subview'] = 'backend/users/roles';
        $this->load->view('admin_index', $data);
    }
    public function DeleteCreateRole($user_id){
        $this->load->model('Difined_model');
        $this->Difined_model->delete("permissions",array("user_id"=>$user_id));
        $this->message('error','تم الحذف');
        redirect('admin/dashboard/CreateRole', 'refresh');
    }
   //===================================================================================================================

}// END CLASS 