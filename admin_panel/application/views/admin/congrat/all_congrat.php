<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-xs-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title;?></h4>
					<!-- /.box-title -->
	<h5>				
					<button class="btn btn-primary" style="background: #f1d4d4;">
  <a  href="<?=base_url()."admin/dashboard/add_congrat/"?>" style="font-weight: bold;color:black;">اضافة رسالة</a>

  </button> 

  <button class="btn btn-primary" style="background: #f1d4d4;">
  <a  href="<?=base_url()."admin/dashboard/congrat_msgs/"?>" style="font-weight: bold;color:black;">أنواع الرسائل</a>
  </button>
    <button class="btn btn-primary" style="background: #f1d4d4;">
  <a  href="<?=base_url()."admin/dashboard/archief_congrat/"?>" style="font-weight: bold;color:black;">أرشيف الرسائل</a>
  </button>
  </h5>
							<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>					
					<!-- /.dropdown js__dropdown -->
	    
<?php 
     if(isset($all_congrat) && !empty($all_congrat)){
        
    ?> 
    	
	<div class="table-responsive" data-pattern="priority-columns">
					<table id="example1" class="table table-striped table-bordered display" style="width:100%">
						<thead>
			 <th>م</th>
     <th>نوع الرسالة</th>
        <th>الاسم</th>
        <th>التاريخ</th>
        <th>ملاحظة</th>

        <th> عمليات</th>
			</thead>
						<tfoot>
				 <th>م</th>
     <th>نوع الرسالة</th>
        <th>الاسم</th>
        <th>التاريخ</th>
        <th>ملاحظة</th>

        <th> عمليات</th>
				</tfoot>
    <tbody>
   <?php $i=1; foreach ($all_congrat as $row ){ ?>
        
  <?php 
   $add_date=$row->occ_date;
  
   $add_time=$row->for_timepicker;
   $congrat_date=$add_date.' '.$add_time;
     date_default_timezone_set('Asia/Kuwait');
    $current = strtotime(date('Y/m/d H:i'));
     //  print_r(  $congrat_date);

   // print_r(date('Y/m/d H:i'));
   // exit();
  
   $date    = strtotime($congrat_date);
   $datediff = $date - $current;

   $difference = floor($datediff/(60*60*24));
 if($difference >=0)
 {               //   print_r($datediff);

     ?>
        <tr>

     


        <td> <?php echo $i;?> </td>
        
        <?php if( $row->occ_type==0){ 
               $type='عرس';
        } else if( $row->occ_type==1){ 
 $type='حفل تخرج';
 } else if( $row->occ_type==2){ 
                    $type='عزاء';
 } else if( $row->occ_type==3){ 
   $type='عودة من السفر مرافق مريض';
 } else if( $row->occ_type==4){ 
     $type='عشاء عودة من علاج';
 } else if( $row->occ_type==5){ 
  $type='زيارة مريض';
 } else if( $row->occ_type==6){ 
  $type='عشاء تمايم';
 } else if( $row->occ_type==7){ 
  $type='عشاء منزل جديد';
 }else if( $row->occ_type==8){ 
  $type='حج';
 }else if( $row->occ_type==9){
                    $type='وظيفة';
                }else if( $row->occ_type==10){
                    $type='افتتاح مشروع';
                }else if( $row->occ_type==11){
                    $type='حادث تصادم';
                }else if( $row->occ_type==12){
                    $type='نجاح بالانتخابات';
                }else if( $row->occ_type==13){
                    $type='عودة من سفر';
                }else if( $row->occ_type==14){
                    $type='ترقية';
                }else if( $row->occ_type==15){
                    $type='عقد قران';
                }else if( $row->occ_type==16){
                    $type='عودة من عمرة';
                }?>   
        <td><?php echo $type;?></td>
        <td><?php echo $row->name;?></td>
        <td><?php echo $row->occ_date;?> <br/> <?php echo $row->for_timepicker;?></td>
        <td><?php echo $row->note;?></td>


        <td>
        <div class="el-overlay">
                        <ul class="el-info"> 
                        <?php  $up_url="#"; $delete_url="#";   //  DeleteUser
                       $up_url=base_url()."admin/dashboard/UpdateCongrat/".$row->id;
                       $delete_url=base_url()."admin/dashboard/DeleteCongrat/".$row->id;
                 ?>
                            <a  href="<?=$up_url?>">
                                       <i class="fa fa-pencil"></i></a><br/>
                           <a  href="<?=$delete_url?>" onclick="return confirm('هل انت متأكد من عملية الحذف ؟');">
                                       <i class="fa fa-trash-o"></i></a>
                        </ul>
                    </div>
        </td>
       
      </tr>
        <?php } ?>
     <?php  if($difference >=0) { $i++;} ?>
   
    <?php } ?>
    </tbody>
					</table>
					</div>
	 <?php } else{
    
   echo ' <div class="alert alert-danger alert-dismissable">
           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>لا يوجد تهانى!</strong> .
           </div>';
}  ?>
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	