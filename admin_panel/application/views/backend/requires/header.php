<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>لوحة تحكم الناخبين -  الرئيسية</title>

	<!-- Main Styles -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/styles/style.min.css">
	
	<!-- Material Design Icon -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/material-design/css/materialdesignicons.css">

	<!-- mCustomScrollbar -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css">

	<!-- Waves Effect -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/waves/waves.min.css">

	<!-- Sweet Alert -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/sweet-alert/sweetalert.css">
	
	<!-- Percent Circle -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/percircle/css/percircle.css">

	<!-- Chartist Chart -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/chart/chartist/chartist.min.css">

	<!-- FullCalendar -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/fullcalendar/fullcalendar.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/fullcalendar/fullcalendar.print.css" media='print'>

	<link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/fontello/fontello.css">

<!-- Data Tables -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/datatables/media/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css">

	<!-- Dark Themes -->

	<!-- RTL -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/styles/style-rtl.min.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/color-switcher/color-switcher.min.css">
		<style>

.btn-primary {
  background-color: #19da43 !important;
  border-color: #19da43 !important;
}
</style>
</head>

<body>
<div class="main-menu" style="    position: fixed !important;">
	<header class="header">
		<a href="<?php echo base_url()?>" class="logo">الناخبين</a>
		<button type="button" class="button-close fa fa-times js__menu_close"></button>
	</header>
	<!-- /.header -->
	<div class="content">

		<div class="navigation">
		 <ul class="menu js__accordion">
            
      <?php // print_r($this->sidbarpages);exit();?>
        <?php if(isset($this->sidbarpages) && !empty($this->sidbarpages) && $this->sidbarpages!=null){?>

<?php foreach( $this->sidbarpages as $main_row){?>

<li>
        <?php if(  $main_row->page_id=="157"){?>
            <a class="waves-effect parent-item js__control"
                    href="<?php echo base_url().$main_row->page_link?>" >
                <i class="<?php echo $main_row->page_icon_code?>"></i> 
                <span ><?php echo $main_row->page_title?></span>
                <span class="menu-arrow fa fa-angle-down"></a>

                <?php if(sizeof($main_row->sub) >0 && !empty($main_row->sub)){
  ?>
<ul class="sub-menu js__content">


    <?php foreach( $main_row->sub as $sub_row){?>
       
   
      <li>  
 <?php
    
 
    if($sub_row->page_id==159 || $sub_row->page_id==161)    {
       
       if( $sub_row->page_id==159 ){
           $page_title='كل الاحتياجات';
        $page_link='admin/Dashboard/needs';
        
       }
       else if( $sub_row->page_id==161 ){
           $page_title='احتياجاتى';
         $page_link='admin/Dashboard/my_needs_only';
        
       }
        
    }
    
    else  if($sub_row->page_id==158 || $sub_row->page_id==160)    {
         
       if( $sub_row->page_id==158 ){ 
           $page_title='كل المعاملات';
         $page_link='admin/Dashboard/trades';
        
       }
       
       else if( $sub_row->page_id==160 ){
              $page_title='معاملاتى';
         $page_link='admin/Dashboard/my_trades_only';
        
       }
        
    }
  
   
     ?>
 <a href="<?php echo base_url().$page_link?>" >

            <i class="<?php echo $sub_row->page_icon_code?>"></i>

            <?php echo $page_title?></a>
          </li>
       
       
        


  

    <?php }// end sub foereach ?>

</ul>

<?php }// end sub  if ?>
    
 
 


    <?php } else if(  $main_row->page_id=="155"){
    $page_link='';
    $set_data = $this->session->all_userdata();
    $user_id=$set_data['user_id'];
          
  $this->db->select('permissions.*');
    $this->db->from('permissions');
    $this->db->where('permissions.user_id',$user_id);
     $this->db->where('permissions.page_id_fk','164');
   //  print_r($this->db->get());
    $query = $this->db->get();
        if ($query->num_rows() > 0) {
         $page_link='admin/Dashboard/tasks';
        }
    else {
         $this->db->select('permissions.*');
    $this->db->from('permissions');
    $this->db->where('permissions.user_id',$user_id);
     $this->db->where('permissions.page_id_fk','165');
    $query = $this->db->get();
        if ($query->num_rows() > 0) {
         $page_link='admin/Dashboard/my_tasks_only';
        }
    //print_r($page_link);
    }
    
    ?>
    
   <a class="waves-effect" 
      href="<?php echo base_url().$page_link?>" 
      aria-expanded="false"><i class="<?php echo $main_row->page_icon_code?>"></i>
       <span><?php echo $main_row->page_title?></span></a>



<?php } else if(  $main_row->page_id=="134"){
    $page_link='';
    $set_data = $this->session->all_userdata();
    $user_id=$set_data['user_id'];
          
  $this->db->select('permissions.*');
    $this->db->from('permissions');
    $this->db->where('permissions.user_id',$user_id);
     $this->db->where('permissions.page_id_fk','162');
   //  print_r($this->db->get());
    $query = $this->db->get();
        if ($query->num_rows() > 0) {
         $page_link='admin/Dashboard/diwans/all';
        }
    else {
         $this->db->select('permissions.*');
    $this->db->from('permissions');
    $this->db->where('permissions.user_id',$user_id);
     $this->db->where('permissions.page_id_fk','163');
    $query = $this->db->get();
        if ($query->num_rows() > 0) {
         $page_link='admin/Dashboard/my_diwans_only/all';
        }
    //print_r($page_link);
    }
    
    ?>
    
   <a class="waves-effect" 
      href="<?php echo base_url().$page_link?>" 
      aria-expanded="false"><i class="<?php echo $main_row->page_icon_code?>"></i>
       <span><?php echo $main_row->page_title?></span></a>


<?php } else if(  $main_row->page_link=="#" &&   $main_row->page_id!="166"){ ?>



<a class="waves-effect parent-item js__control" href="<?php echo base_url().$main_row->page_link?>" ><i class="<?php echo $main_row->page_icon_code?>"></i> <span ><?php echo $main_row->page_title?></span><span class="menu-arrow fa fa-angle-down"></a>

<?php } else if(  $main_row->page_id!="166"){?>



<a class="waves-effect" href="<?php echo base_url().$main_row->page_link?>" aria-expanded="false"><i class="<?php echo $main_row->page_icon_code?>"></i> <span><?php echo $main_row->page_title?></span></a>

<?php } ?>

   

<?php if(sizeof($main_row->sub) >0 && !empty($main_row->sub)){?>

<ul class="sub-menu js__content">


    <?php foreach( $main_row->sub as $sub_row){?>
       
    
    <li><a href="<?php echo base_url().$sub_row->page_link?>">

            <i class="<?php echo $sub_row->page_icon_code?>"></i>

            <?php echo $sub_row->page_title?></a>

    </li>

    <?php }// end sub foereach ?>

</ul>

<?php }// end sub  if ?>

</li>

<?php }// end main foereach ?>

<?php }// end if ?>	


            </ul>
			<!-- /.menu js__accordion -->
		</div>
		<!-- /.navigation -->
	</div>
	<!-- /.content -->
</div>
<!-- /.main-menu -->
<style>
tfoot{
    display: none;
}
</style>

   <?php  $this->load->view('backend/requires/sidebar'); ?>
