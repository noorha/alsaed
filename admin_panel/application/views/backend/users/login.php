<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>لوحة تحكم الناخبين</title>
	<link rel="stylesheet" href="<?php echo base_url()?>assets/styles/style.min.css">

	<!-- Waves Effect -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/plugin/waves/waves.min.css">

	<!-- RTL -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/styles/style-rtl.min.css">
</head>

<body>


<div id="single-wrapper">

 <?php if(isset($response)):?>

                        <h5 class="alert alert-danger text-center rtl">

                            <i class="fa fa-lock fa-fw fa-spin icn-xs"></i><?php echo $response;?></h5>

                    <?php endif?>

	  <?php echo form_open('login/check_login',array("class"=>"frm-single","id"=>"loginform"))?>



		<div class="inside">

			<div class="title"><strong>لوحة تحكم الناخبين</strong></div>

			<!-- /.title -->

			<div class="frm-title">تسجيل الدخول</div>

			<!-- /.frm-title -->

			<div class="frm-input"><input name="user_name" type="text" placeholder="إدخل إسم المستخدم الخاص بك" class="frm-inp"><i class="fa fa-user frm-ico"></i></div>

			<!-- /.frm-input -->

			<div class="frm-input"><input type="password"  name="user_pass" placeholder="******" class="frm-inp"><i class="fa fa-lock frm-ico"></i></div>

			<!-- /.frm-input -->

			

			<!-- /.clearfix -->

			<button type="submit" class="frm-submit">دخول<i class="fa fa-arrow-circle-right"></i></button>



			<!-- /.row -->

			<div class="frm-footer">لوحة تحكم الناخبينً@ 2020.</div>

			<!-- /.footer -->

		</div>

		<!-- .inside -->

	 <?php echo form_close()?>

	<!-- /.frm-single -->

</div><!--/#single-wrapper -->


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="assets/script/html5shiv.min.js"></script>
		<script src="assets/script/respond.min.js"></script>
	<![endif]-->
	<!-- 
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url()?>assets/scripts/jquery.min.js"></script>
	<script src="<?php echo base_url()?>assets/scripts/modernizr.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugin/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugin/nprogress/nprogress.js"></script>
	<script src="<?php echo base_url()?>assets/plugin/waves/waves.min.js"></script>

	<script src="<?php echo base_url()?>assets/scripts/main.min.js"></script>
</body>
</html>