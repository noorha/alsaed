<?php if(isset($result_id) && !empty($result_id) && $result_id!=null ):
    $out['form']='admin/Dashboard/UpdateUsers/'.$result_id['user_id'];
    $out['user_name']=$result_id["user_name"];
    $out['user_username']=$result_id["user_username"];
    $out['user_pass']=$result_id["user_pass"];
    $out['user_email']=$result_id["user_email"];
    $out['user_phone']=$result_id["user_phone"];
    $out['role_id_fk']=$result_id["role_id_fk"];
    $out['user_photo']=$result_id["user_photo"];

    $out['per_id_fk']=$result_id['per_id_fk'];
    $out['input']='edit_user';
    $out['input_title']='تعديل ';
else:
    $out['form']='admin/Dashboard/AddUsers';
    $out['user_name']="";
    $out['user_username']="";
    $out['user_email']="";
    $out['user_phone']="";
    $out['role_id_fk']="";
    $out['per_id_fk']="user";
    $out['user_photo']="";
   
    $out['user_pass']="";
    $out['input']='add_user';
    $out['input_title']='حفظ ';
endif?>
<script>


    function valid()
    {
        if($("#user_pass").val().length > 5){
            document.getElementById('validate1').style.color = '#00FF00';
            document.getElementById('validate1').innerHTML = 'كلمة المرور قوية';
        }else{
            document.getElementById('validate1').style.color = '#F00';
            document.getElementById('validate1').innerHTML = 'كلمة المرور ضعيفة';
        }
    }
    function valid2()
    {
        if($("#user_pass").val() == $("#user_pass_validate").val()){
            document.getElementById('validate').style.color = '#00FF00';
            document.getElementById('validate').innerHTML = 'كلمة المرور متطابقة';
        }else{
            document.getElementById('validate').style.color = '#F00';
            document.getElementById('validate').innerHTML = 'كلمة المرور غير متطابقة';
        }
    }

</script>
<div id="wrapper">
	<div class="main-content">
		<div class="row small-spacing">
			<div class="col-lg-6 col-md-12">
				<div class="box-content">
					<h4 class="box-title"><?php echo $title?></h4>
					<!-- /.box-title -->
					
					
					
					<!-- /.dropdown js__dropdown -->
					
					
					 <?php echo form_open_multipart($out['form']);
              ?>
              
  
 <div class="col-sm-12 row">
                <div class="form-group col-sm-6">
                    <label >الاسم </label>
                    <input type="text" name="user_name" value="<?php echo $out['user_name']; ?>"  class="form-control half "   placeholder="الاسم " data-validation="required">
                    <span style="color: red" id="span_user_username"></span>
                </div>
                <div class="form-group col-sm-6">
                    <label >إسم المستخدم</label>
                    <input type="text" name="user_username" value="<?php echo $out['user_username']; ?>" class="form-control half unique-field" field-name="user_username" data-db="users" placeholder="إسم المستخدم" data-validation="required">
                </div>
            </div>
            <div class="col-sm-12 row">
            <?php if((isset($result_id)) && !empty($result_id) && $result_id != null): ?>
                <div class="form-group col-sm-6">
                    <label > كلمة المرور</label>
                    <button type="button" class="btn dropdown-toggle btn-info" data-toggle="modal" data-target="#modal-update-<?php echo $result_id['user_id'];?>">تغير كلمة المرور <i class="fa fa-pencil"></i></button>
                </div>
                <div class="modal" id="modal-update-<?php echo $result_id['user_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-wow-duration="0.5s">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title">تعديل بيانات كلمة المرور : <?php echo $result_id['user_name'] ;?></h1>
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                             
                            </div>
                            <div class="modal-body col-sm-12">
                                <div class="col-sm-12">
                                    <div class="form-group col-sm-6">
                                        <label class="control-label   ">كلمة المرور</label>
                                        <input type="password" name="user_pass" id="user_pass" onkeyup="return valid();"    class="form-control " placeholder="أدخل كلمة المرور" >
                                        <span  id="validate1" class="help-block"></span>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label class="control-label   ">تأكيد كلمة المرور</label>
                                        <input type="password" name="user_pass_validate" onkeyup="return valid2();"  id="user_pass_validate"  class="form-control " placeholder="أدخل كلمة المرور" >
                                        <span  id="validate" class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">إغلاق</button>
                                <button type="submit" name="update_pw" value="update_pw" class="btn btn-success">تعديل</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            <?php else: ?>
                    <div class="form-group col-sm-6">
                        <label >كلمة المرور</label>
                        <input type="password" name="user_pass" id="user_pass" onkeyup="return valid();"  data-validation="required" value="<?php echo $out['user_pass']; ?>" class="form-control half  " placeholder="********" >
                        <span  id="validate1" class="help-block"></span>
                    </div>
                    <div class="form-group col-sm-6">
                        <label >تأكيد كلمة المرور</label>
                        <input type="password" name="user_pass_validate" onkeyup="return valid2();"  id="user_pass_validate" data-validation="required" value="" class="form-control half  " placeholder="***********" >
                        <span  id="validate" class="help-block"></span>
                    </div>
            <?php endif; ?>
            </div>
            <div class="col-sm-12 row">
               <!-- <div class="form-group col-sm-6">
                    <label class="label label-green  half">الصلاحيات  </label>
                    <select  name="per_id_fk" id="per_id_fk" class="selectpicker form-control half" data-validation="required" aria-required="true" data-show-subtext="true" data-live-search="true"  >
                        <option value=""> إختر     </option>
                        <option value="0" <?php /*if($out['per_id_fk'] == 0){echo 'selected';}*/?>> صلاحيات جديدة    </option>
                     <?php /*foreach ($user_per as $one_per):
                            $select=""; if(isset($result_id) ){

                            if($one_per->user_id == $out['per_id_fk']){$select='selected="selected"';}
                        } */?>
                            <option  value="<?php /*echo $one_per->user_id*/?>" <?php /*echo  $select*/?>> مثل  <?php /*echo $one_per->name*/?></option>
                        <?php /*endforeach;*/?>
                    </select>
                </div>-->
               <div class="form-group col-sm-6" style="display:none;">
                    <label >الدور الوظيفى  </label>
                    <?php $arr_role = array(2=>"مدير العمليات",3=>"مسؤول الجودة",4=>"مسؤول الحركة ");?>
                    <select  name="role_id_fk" id="" class=" form-control half"  aria-required="true" data-show-subtext="true" data-live-search="true" >
                        <option value=""> إختر     </option>
                        <?php foreach ($arr_role as $key=>$value){?>
                        <option value="<?=$key?>"  <?php if($out['role_id_fk'] == $key)?> > <?=$value?>     </option>
                        <?php }?>
                    </select>
                </div>

            </div>
            <div class="col-sm-12 row">
            <div class="form-group col-sm-6">
                <label >البريد الألكتروني</label>
                <input type="email" name="user_email"  value="<?php echo $out['user_email']; ?>" class="form-control half " placeholder="أدخل البيانات" >
            </div>
            <div class="form-group col-sm-6">
                <label >رقم الهاتف </label>
                <input type="text" name="user_phone" onkeypress="validate_number(event);"   value="<?php echo $out['user_phone']; ?>" class="form-control half " placeholder="أدخل البيانات" >
            </div>
    </div>
    <div class="col-sm-12 row">
        <div class="form-group col-sm-6 col-xs-12">
        <label >  صورة المستخدم  </label>
        <input type="file" name="user_photo" class="form-control half " placeholder="أدخل البيانات" />
        <?php if(isset($out['user_photo']) && !empty($out['user_photo']) && $out['user_photo']!=null ): ?>
            <span  id="" class="help-block text-danger">لعدم تغير الصورة لاتختار شيء</span>
        <?php endif; ?>
        </div>
        <?php if(isset($out['user_photo']) && !empty($out['user_photo']) && $out['user_photo']!=null ): ?>
        <div class="form-group col-sm-6 col-xs-12">
            <img src="<?php echo base_url()."uploads/".$out['user_photo']?>" class="img-circle" alt="User Image" width="50" height="50"/>
        </div>
        <?php endif; ?>
    </div>
  <div class="col-xs-12 ">
            <button  type="submit" name="<?php echo $out['input']?>" value="<?php echo $out['input']?>"  class="btn btn-primary" style="background: #f1d4d4;font-weight: bold;color:black;">
                <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span> <?php echo $out['input_title']?></button>
        </div>

        
        <?php echo form_close()?>
        
   </div> </div>
   <div class="col-lg-6 col-md-12">
   <div class="box-content">
					<h4 class="box-title">المستخدمين</h4>
		<?php if(isset($users_table ) && $users_table!=null && !empty($users_table)):?>
		
	<div class="table-responsive" data-pattern="priority-columns">
         			<table class="table">
						<thead>
			 <th>م</th>
        <th>الاسم</th>
        <th>اسم المستخدم</th>

        <th>الصورة</th>
        <th>التحكم</th>
			</thead>
						<tfoot>
			 <th>م</th>
        <th>الاسم</th>
        <th>اسم المستخدم</th>

        <th>الصورة</th>
        <th>التحكم</th>
				</tfoot>
						<tbody>
						
			 <?php $i=1; foreach($users_table as $group):?>
        <tr>
            <td ><?php echo $i; ?></td>
            <td > <?php echo $group->user_name?> </td>
            <td ><?php echo $group->user_username?></td>
            <?php if(!empty($group->user_photo)  && $group->user_photo !="0"){
                $image= '<img src="'.base_url().'uploads/'.$group->user_photo.'" class="img-circle" alt="User Image" width="50" height="50"/>';
            }else{ $image="لم يتم الاضافة "; }
            ?>
            <td><?php echo $image ?> </td>
            <td data-title="التحكم" class="text-center">
                <a href="<?php echo base_url().'admin/Dashboard/UpdateUsers/'.$group->user_id?>">
                    <button type="button" class="btn btn-add btn-xs" title="تعديل ">
                        <i class="fa fa-pencil"></i></button></a>
                           <br/>
                <?php  if($_SESSION['user_id'] != $group->user_id){?>
                    <a href="<?php echo base_url().'admin/Dashboard/DeleteUsers/'.$group->user_id?>">
                        <button type="button" class="btn btn-danger btn-xs" ><i class="fa fa-trash-o"> </i> </button>
                    </a>
                <br/>
                <?php }?>
                  <a href="<?php echo base_url().'admin/Dashboard/UpdateCreateRole/'.$group->user_id?>">
                    <button type="button" class="btn btn-add btn-xs" title="صلاحيات المستخدم ">
                         <i class="fa fa-check-square-o"></i></button></a>
                    <br/>
                 <a href="<?php echo base_url().'admin/Dashboard/user_operations/'.$group->user_id?>">
                    <button type="button" class="btn btn-add btn-xs" title="عمليات المستخدم">
                         <i class="fa fa-tasks"></i></button></a>   
            </td>
			
        </tr>
		     <?php $i++;?>
    <?php endforeach ;?>
						</tbody>
					</table>

</div>
		<?php endif;?>			
					
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->		
		
	
