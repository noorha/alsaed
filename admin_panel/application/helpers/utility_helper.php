<?php
function calculate_from_time($time_from){
    $year=date('Y');
    $prev_month=date('m')-1;
    if($prev_month == 0){
        $prev_month=12;
        $year =$year -1;
    }
    $prev_month_days = cal_days_in_month(CAL_GREGORIAN, $prev_month, $year);
    $result=0;
    $dif = abs( time() - $time_from);
    //--------------------------------------
    $result = date("d/m/Y" ,$time_from);
    if($dif <   (60 * 60 * 24 * $prev_month_days )  ){
        $result =  " منذ ".ceil($dif/(60 * 60 * 24 ))." يوم " ;
    }
    if($dif <  ( 60 * 60 * 24 )  ){
        $result =  " منذ ".ceil($dif/(60 * 60  ))." ساعة " ;
    }
    if($dif <   (60 * 60) ){
        $result =  " منذ ".ceil($dif / 60 )." دقيقة " ;
    }
    if($dif < 60 ){
        $result =  " منذ ".$dif." ثانية " ;
    }
    //-------------------------------------- 
    return $result;
}
//==========================================================================
function multi_dimention_array_sort($my_array,$key,$sort_type){
    $sortArray = array();
    foreach($my_array as $person){
        foreach($person as $key=>$value){
            if(!isset($sortArray[$key])){
                $sortArray[$key] = array();
            }
            $sortArray[$key][] = $value;
        }
    }
    $orderby = $key; //change this to whatever key you want from the array   SORT_ASC   SORT_DESC
    if($sort_type =="DESC"){
        array_multisort($sortArray[$orderby],SORT_DESC,$my_array);
    }else{
        array_multisort($sortArray[$orderby],SORT_ASC,$my_array);
    }
    return $my_array ;
}
//==========================================================================
function array_from_to($start,$end,$my_array){
    $from_to=array();
    if($end > sizeof($my_array) ){
        $end= sizeof($my_array);
    }
    for($x=$start ;$x<$end;$x++){
        $from_to[] = $my_array[$x];
    }
    return $from_to;
}
//==========================================================================
function google_distance($lat1, $lon1, $lat2, $lon2, $unit) {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);
    if ($unit == "K") {       //  Kilometers
        $out= $miles * 1.609344;
    } else if ($unit == "N") {     //  Nautical Miles
        $out= $miles * 0.8684 ;
    } else {    // Miles
        $out= $miles;
    }
    return round($out,2);
}
//===========================================================================
 function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row->{$col};
        }

        array_multisort($sort_col, $dir, $arr);
    }
//===========================================================================
 function limit_array($arr,$limit){
      $sort_col = array();
       $x=0; foreach ($arr as $row) {
            if($x== $limit){
                break;
            }else{
            $sort_col[]= $row; 
            $x++;
            }
        }
     return $sort_col;   
 }

/*
function GetDrivingDistance($lat1, $lat2, $long1, $long2)
{
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
    //return array('distance' => $dist, 'time' => $time);
     return  $dist;
}*/
//==========================================================================
function thumb($data,$folder){
    $CI =& get_instance();
    $config['image_library'] = 'gd2';
    $config['source_image'] =$data['full_path'];
    $config['new_image'] = 'uploads/'.$folder.'/thumbs/'.$data['file_name'];
    $config['create_thumb'] = TRUE;
    $config['maintain_ratio'] = TRUE;
    $config['thumb_marker']='';
    $config['width'] = 275;
    $config['height'] = 250;
    $CI->load->library('image_lib', $config);
    $CI->image_lib->resize();
}
 function upload_file($file_name){
        $CI =& get_instance();
        $config['upload_path'] = 'uploads/files';
        $config['allowed_types'] = 'gif|Gif|ico|ICO|jpg|JPG|jpeg|JPEG|BNG|png|PNG|bmp|BMP|WMV|wmv|
                                    MP3|mp3|FLV|flv|SWF|swf|pdf|PDF|xls|xlsx|video/mp4|video/MP4|MP4|mp4|doc|docx|txt|rar|tar.gz|zip';
        
        //$config['allowed_types'] = 'video/mp4|video/MP4';
        $config['max_size']    = '1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
        $config['overwrite'] = true;
        $config['remove_spaces'] = TRUE;
        //   $video_name = rand(1,1450);
        //$config['file_name'] = $video_name;
        $config['encrypt_name']=true;
        $CI->load->library('upload',$config);
        if(! $CI->upload->do_upload($file_name)){
           return   $CI->upload->display_errors();
        // return  0;
        }else {
            $datafile = $CI->upload->data();
            return $datafile['file_name'];
        }
    }
//=========================================================================    
     function uploadFile($file_name,$folder_name){
        $CI =& get_instance();
        $config['upload_path'] = 'uploads/'.$folder_name;
       // $config['allowed_types'] = 'gif|Gif|ico|ICO|jpg|JPG|jpeg|JPEG|BNG|png|PNG|bmp|BMP|WMV|wmv|m4a|M4A|AMR|amr|
       //                             MP3|mp3|FLV|flv|SWF|swf|pdf|PDF|xls|xlsx|video/mp4|video/MP4|MP4|mp4|doc|docx|txt|rar|tar.gz|zip';
        $config['allowed_types'] = '*';
        $config['max_size']    = '1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
        $config['overwrite'] = true;
        $config['remove_spaces'] = TRUE;
        //   $video_name = rand(1,1450);
        //$config['file_name'] = $video_name;
        $config['encrypt_name']=true;
        $CI->load->library('upload',$config);
        if(! $CI->upload->do_upload($file_name)){
         //  return   $CI->upload->display_errors();
            return  0;
        }else {
            $datafile = $CI->upload->data();
            return $datafile['file_name'];
        }
    }
//==========================================================================    
function upload_image($file_name,$folder = "images"){
    $CI =& get_instance();
    $config['upload_path'] = 'uploads/'.$folder;
    $config['allowed_types'] = 'gif|Gif|ico|ICO|jpg|JPG|jpeg|JPEG|BNG|png|PNG|bmp|BMP|WMV|wmv|MP3|mp3|FLV|flv|SWF|swf';
    $config['max_size']    = '1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
    $config['encrypt_name']=true;
    $CI->load->library('upload',$config);
    if(! $CI->upload->do_upload($file_name)){
        // return   $CI->upload->display_errors();
       //return "error_image.png";
      return 0;
    }else{
        $datafile = $CI->upload->data();
        thumb($datafile,$folder);
        return  $datafile['file_name'];
    }
}
//===========================================================================
function base_uploder($FileName){
    $target_dir = "uploads/images/";
    //-------------------------------
     
      $file_name = $_FILES[$FileName]['name'];
      $file_size =$_FILES[$FileName]['size'];
      $file_tmp =$_FILES[$FileName]['tmp_name'];
      $file_type=$_FILES[$FileName]['type'];
      //$file_ext=strtolower(end(explode('.',$_FILES[$FileName]['name'])));
      move_uploaded_file($file_tmp,$target_dir.$file_name);
    //--------------------------------
}



//============================================================================



function upload_muli_image($input_name ,$folder = "images"){
    $filesCount = count($_FILES[$input_name]['name']);
    for($i = 0; $i < $filesCount; $i++){
        $_FILES['userFile']['name'] = $_FILES[$input_name]['name'][$i];
        $_FILES['userFile']['type'] = $_FILES[$input_name]['type'][$i];
        $_FILES['userFile']['tmp_name'] = $_FILES[$input_name]['tmp_name'][$i];
        $_FILES['userFile']['error'] = $_FILES[$input_name]['error'][$i];
        $_FILES['userFile']['size'] = $_FILES[$input_name]['size'][$i];
        $all_img[]=upload_image("userFile",$folder);
    }
    return $all_img;
}
//================================================================================
function my_pagination($controller,$total_records, $limit_per_page){
    $CI =& get_instance();
    $CI->load->library('pagination');
    $config['base_url'] = base_url() . $controller;
    $config['total_rows'] = $total_records;
    $config['per_page'] = $limit_per_page;
    $config["uri_segment"] = 3;
    $config['num_links'] = 2;
    $config['use_page_numbers'] = TRUE;
    $config['reuse_query_string'] = TRUE;
    $config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['num_links'] = 5;
    $config['page_query_string'] = FALSE;
    $config['prev_link'] = '&lt; السابق';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = 'التالى &gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    $CI->pagination->initialize($config);
    return  $CI->pagination->create_links();
}
//================================================================================






function clear_txt($t){
    $clearTxt= preg_replace("/(<\/?)(\w+)([^>]*>)/e","",$t);
    $clearTxt=strip_tags($clearTxt);
    return $clearTxt;
}
function short_text($t,$num){
    $clearTxt= preg_replace("/(<\/?)(\w+)([^>]*>)/e","",$t);
    $clearTxtII=strip_tags($clearTxt);
    $vireb_befor[] ="";
    $clearTxtIII = eregi_replace($vireb_befor," ",$clearTxtII);
    $clearTxtIIII = eregi_replace("&nbsp;"," ",$clearTxtIII);
    $clearTxtIIII = eregi_replace("&raquo;"," ",$clearTxtIIII);
    $clearTxtIIII = eregi_replace("&laquo;"," ",$clearTxtIIII);
    $clearTxtIIII = eregi_replace("&rlm;"," ",$clearTxtIIII);
    $exploddescriptions = explode(" ",$clearTxtIIII);
    $short="";
    for($i=0;$i<$num;$i++){
        $short .=$exploddescriptions[$i]." ";
    }
    return $short;
}
function Cleanertext($t){
    $clearTxt= preg_replace("/(<\/?)(\w+)([^>]*>)/e","",$t);
    $clearTxtII=strip_tags($clearTxt);
    $vireb_befor[] ="";
    $clearTxtIII = eregi_replace($vireb_befor," ",$clearTxtII);
    $clearTxtIIII = eregi_replace("&nbsp;"," ",$clearTxtIII);
    $clearTxtIIII = eregi_replace("&raquo;"," ",$clearTxtIIII);
    $clearTxtIIII = eregi_replace("&laquo;"," ",$clearTxtIIII);
    $clearTxtIIII = eregi_replace("&rlm;"," ",$clearTxtIIII);
    return $clearTxtIIII;
}
function get_arr($var){
    $arr = explode(",", $var);
    return $arr;
}
function get_string($arr){
    $strings="";
    foreach ($arr as $key=>$value) {
        $strings .= $value.",";
    }
    return $strings;
}
 function check_null($text){
     if(isset($text) &&  !empty($text) && $text != null && $text !="" && $text !=false){
         return strip_tags($text);
     }else{
        return "0";
     }
 } 


//================================================================================
function arabicDate($format, $timestamp) {
    /* 
      written by Salah Faya (visualmind@php.net) http://www.php4arab.info/scripts/arabicDate
    $format: 
      [*]hj|ar|english:[jdl][Fmn][Yy][Aa]  (php.date function handles the rest chars)
      * will add <span dir=rtl lang=ar-sa>..</span>
      examples: 
      echo arabicDate('hj:l d-F-Y هـ', time());  
      echo arabicDate('ar:l d/F - h:iA', time());
      */
    $format=trim($format);
    if (substr($format,0,1)=='*') {
        $use_span=true;
        $format=substr($format,1);
    } else $use_span=false;
    $type=substr($format,0,3);
    $arDay = array("Sat"=>"السبت", "Sun"=>"الأحد", "Mon"=>"الإثنين", "Tue"=>"الثلاثاء",
        "Wed"=>"الأربعاء", "Thu"=>"الخميس", "Fri"=>"الجمعة");
    $ampm=array('am'=>'صباحا','pm'=>'مساء');
    list($d,$m,$y,$dayname,$monthname,$am)=explode(' ',date('d m Y D M a', $timestamp));
    if ($type=='hj:') {
        if (($y>1582)||(($y==1582)&&($m>10))||(($y==1582)&&($m==10)&&($d>14))) {
            $jd=$this->ard_int((1461*($y+4800+$this->ard_int(($m-14)/12)))/4);
            $jd+=$this->ard_int((367*($m-2-12*($this->ard_int(($m-14)/12))))/12);
            $jd-=$this->ard_int((3*($this->ard_int(($y+4900+$this->ard_int(($m-14)/12))/100)))/4);
            $jd+=$d-32075;
        } else {
            $jd = 367*$y-$this->ard_int((7*($y+5001 + $this->ard_int(($m-9)/7)))/4) + $this->ard_int((275*$m)/9)+$d+1729777;
        }
        $l=$jd-1948440+10632;
        $n=$this->ard_int(($l-1)/10631);
        $l=$l-10631*$n+355;  // Correction: 355 instead of 354
        $j=($this->ard_int((10985-$l)/5316)) * ($this->ard_int((50*$l)/17719)) + ($this->ard_int($l/5670)) * ($this->ard_int((43*$l)/15238));
        $l=$l-($this->ard_int((30-$j)/15)) * ($this->ard_int((17719*$j)/50)) - ($this->ard_int($j/16)) * ($this->ard_int((15238*$j)/43))+29;
        $m=$this->ard_int((24*$l)/709);
        $d=$l-$this->ard_int((709*$m)/24);
        $y=30*$n+$j-30;
        $format=substr($format,3);
        $hjMonth = array("محرم", "صفر", "ربيع أول", "ربيع ثاني",
            "جماد أول", "جماد ثاني", "رجب", "شعبان", "رمضان", "شوال", "ذو القعدة", "ذو الحجة");
        $format=str_replace('j', $d, $format);
        $format=str_replace('d', str_pad($d,2,0,STR_PAD_LEFT), $format);
        $format=str_replace('l', $arDay[$dayname], $format);
        $format=str_replace('F', $hjMonth[$m-1], $format);
        $format=str_replace('m', str_pad($m,2,0,STR_PAD_LEFT), $format);
        $format=str_replace('n', $m, $format);
        $format=str_replace('Y', $y, $format);
        $format=str_replace('y', substr($y,2), $format);
        $format=str_replace('a', substr($ampm[$am],0,1), $format);
        $format=str_replace('A', $ampm[$am], $format);
    } elseif ($type=='ar:') {
        $format=substr($format,3);
        $arMonth=array("Jan"=>"يناير", "Feb"=>"فبراير","Mar"=>"مارس", "Apr"=>"ابريل", "May"=>"مايو",
            "Jun"=>"يونيو", "Jul"=>"يوليو", "Aug"=>"اغسطس", "Sep"=>"سبتمبر", "Oct"=>"اكتوبر",
            "Nov"=>"نوفمبر", "Dec"=>"ديسمبر");
        $format=str_replace('l', $arDay[$dayname], $format);
        $format=str_replace('F', $arMonth[$monthname], $format);
        $format=str_replace('a', substr($ampm[$am],0,1), $format);
        $format=str_replace('A', $ampm[$am], $format);
    }
    $date = date($format, $timestamp);
    if ($use_span) return '<span dir="rtl" lang="ar-sa">'.$date.'</span>';
    else return $date;
}
function ar_date($d)
{
    if($d!="")
    {
        if($_SEESION['lang']=='english')
        {
            $da=date('l d F Y - h:i A', $d);
        }
        else
        {
            $da=$this->arabicDate('ar:l d F Y - h:i A', $d);
        }
    }
    else
    {
        $da='0000-00-00';
    }
    return $da;
}
function short_ar_date($d)
{
    if($d!="")
    {
        $da=$this->arabicDate('ar:l d F Y ', $d);
    }
    else
    {
        $da='0000-00-00';
    }
    return $da;
}
function en_date($d)
{
    if($d!="")
    {
        ///$da=date('l d F Y - h:i A', $d);
        $da =  gmdate('D, d M Y H:i:s \G\M\T', $d);
    }
    else
    {
        $da='0000-00-00';
    }
    return $da;
}
//================================================================================
function delete_file($dir,$file)
{
    if(is_file($dir.$file))
    {
        unlink($dir.$file);
    }
}
//================================================================================
function check_post($arr_names,$post){
    $data=array();
    foreach($arr_names as $key=>$value){
        if(isset($post[$value]) && !empty($post[$value])){
            $data[$value]= $post[$value];
        }
    }
    return $data;
}
//================================================================================
function decodeimage($post){
    $dir='uploads/images/';
    $image_base64=base64_decode($post);
    $file =  uniqid() . '.jpg';
    file_put_contents($dir .$file, $image_base64);
    return $file;
}
function restful($data){
    header('Content-type: application/json');
    echo json_encode($data);
}

 define( 'API_ACCESS_KEY', 'AAAA2mM7RhE:APA91bEM-nW7KCJKJYkJYNuDu90mLNGUDWJ0gN-OmohemgiRsojOvIOpR3Pbb7lSUsq0fekCZAzZctr6qfz137N0erh0aGr8eMcZ3lNF7gb62sGz4nLsGC7Doi9BCx5pHRODoUyUx76_');
       
function sendGCM($messssage, $sssid) {
    // API access key from Google API's Console
    $registrationIds =$sssid;
    // prep the bundle
    $msg =$messssage;
    /*  $msg = array
      (
          'message' 	=> 'here is a message. message',
          'title'		=> 'This is a title. title',
          'subtitle'	=> 'This is a subtitle. subtitle',
          'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
          'vibrate'	=> 1,
          'sound'		=> 1,
          'largeIcon'	=> 'large_icon',
          'smallIcon'	=> 'small_icon'
      );*/
    $fields = array
    (
        'registration_ids' 	=> $registrationIds,
        'data'			=> $msg
    );
    $headers = array
    (
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
    return $result;
}
